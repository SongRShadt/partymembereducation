package com.shadt.signmeeting.model;

import java.io.Serializable;
/**
 * 回看视频
 * @author SongR
 *
 */
@SuppressWarnings("serial")
public class Camera_Video_List implements Serializable{
	private String cameraNo;//摄像头编号
	private String videoId;//视频编号
	private String video_desc;//视频描述
	private String startTime;//开始时间
	private String endTime;//结束时间
	private String video_url;//播放路径
	private String cameraName;//摄像头名称
	public String getCameraNo() {
		return cameraNo;
	}
	public void setCameraNo(String cameraNo) {
		this.cameraNo = cameraNo;
	}
	public String getVideoId() {
		return videoId;
	}
	public void setVideoId(String videoId) {
		this.videoId = videoId;
	}
	public String getVideo_desc() {
		return video_desc;
	}
	public void setVideo_desc(String video_desc) {
		this.video_desc = video_desc;
	}
	public String getStartTime() {
		return startTime;
	}
	public void setStartTime(String startTime) {
		this.startTime = startTime.substring(0,startTime.length()-2);
	}
	public String getEndTime() {
		return endTime;
	}
	public void setEndTime(String endTime) {
		this.endTime = endTime;
	}
	public String getVideo_url() {
		return video_url;
	}
	public void setVideo_url(String video_url) {
		this.video_url = video_url;
	}
	public String getCameraName() {
		return cameraName;
	}
	public void setCameraName(String cameraName) {
		this.cameraName = cameraName;
	}
	
}
