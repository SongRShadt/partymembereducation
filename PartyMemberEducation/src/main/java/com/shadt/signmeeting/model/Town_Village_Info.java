package com.shadt.signmeeting.model;

import java.io.Serializable;

/**
 * 村镇信息
 * 
 * @author SongR
 * 
 */
@SuppressWarnings("serial")
public class Town_Village_Info implements Serializable {
	private String townName;// 乡镇名称
	private String townKeyValue;// 镇key值
	private String villageId;// 村编号
	private String villageKeyValue;// 村key值
	private String orgCode; // 村组织编号
	public String getTownName() {
		return townName;
	}
	public void setTownName(String townName) {
		this.townName = townName;
	}
	public String getTownKeyValue() {
		return townKeyValue;
	}
	public void setTownKeyValue(String townKeyValue) {
		this.townKeyValue = townKeyValue;
	}
	public String getVillageId() {
		return villageId;
	}
	public void setVillageId(String villageId) {
		this.villageId = villageId;
	}
	public String getVillageKeyValue() {
		return villageKeyValue;
	}
	public void setVillageKeyValue(String villageKeyValue) {
		this.villageKeyValue = villageKeyValue;
	}
	public String getOrgCode() {
		return orgCode;
	}
	public void setOrgCode(String orgCode) {
		this.orgCode = orgCode;
	}
	
}
