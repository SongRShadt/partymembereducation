package com.shadt.signmeeting.model;

import java.io.Serializable;

/**
 * 机顶盒对应的摄像头信息
 * 
 * @author SongR
 * 
 */
@SuppressWarnings("serial")
public class Camera_Info implements Serializable {
	private String cameraNo;//摄像头编号
	private String cameraName;//摄像头名称
	private String cameraUrl;//摄像头路径
	private String imgUrl;//图片路径
	private String streamId;//流编号
	
	public String getCameraNo() {
		return cameraNo;
	}
	public void setCameraNo(String cameraNo) {
		this.cameraNo = cameraNo;
	}
	public String getCameraName() {
		return cameraName;
	}
	public void setCameraName(String cameraName) {
		this.cameraName = cameraName;
	}
	public String getCameraUrl() {
		return cameraUrl;
	}
	public void setCameraUrl(String cameraUrl) {
		this.cameraUrl = cameraUrl;
	}
	public String getImgUrl() {
		return imgUrl;
	}
	public void setImgUrl(String imgUrl) {
		this.imgUrl = imgUrl;
	}
	public String getStreamId() {
		return streamId;
	}
	public void setStreamId(String streamId) {
		this.streamId = streamId;
	}
	
}
