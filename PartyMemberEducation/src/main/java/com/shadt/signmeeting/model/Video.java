package com.shadt.signmeeting.model;

import java.io.Serializable;

/**
 * 录制的视频信息
 * @author liusongren
 *
 */
import java.util.Date;
/**
 * 视频录制信息
 * @author liusongren
 *
 */
@SuppressWarnings("serial")
public class Video implements Serializable{
	private String id;
	private String taskId;//任务编号
	private Date startTime;//开始录制时间
	private Date endTime;//结束录制时间
	private String cameraNo;//摄像头编号
	private String downloadUrl;//下载文件（mp4）地址
	private String playUrl;//播放地址
	private Integer duration;//时长
	
	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
	public String getTaskId() {
		return taskId;
	}
	public void setTaskId(String taskId) {
		this.taskId = taskId;
	}
	public Date getStartTime() {
		return startTime;
	}
	public void setStartTime(Date startTime) {
		this.startTime = startTime;
	}
	public Date getEndTime() {
		return endTime;
	}
	public void setEndTime(Date endTime) {
		this.endTime = endTime;
	}
	public String getCameraNo() {
		return cameraNo;
	}
	public void setCameraNo(String cameraNo) {
		this.cameraNo = cameraNo;
	}
	public String getDownloadUrl() {
		return downloadUrl;
	}
	public void setDownloadUrl(String downloadUrl) {
		this.downloadUrl = downloadUrl;
	}
	public String getPlayUrl() {
		return playUrl;
	}
	public void setPlayUrl(String playUrl) {
		this.playUrl = playUrl;
	}
	public Integer getDuration() {
		return duration;
	}
	public void setDuration(Integer duration) {
		this.duration = duration;
	}
	
}
