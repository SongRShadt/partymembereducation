package com.shadt.signmeeting.model;

import java.io.Serializable;

/**
 * 镇
 * @author SongR
 *
 */
@SuppressWarnings("serial")
public class Town_Info implements Serializable{
	private String  townId;//镇编号
	private String townName; //镇名称
	
	public String getTownId() {
		return townId;
	}
	public void setTownId(String townId) {
		this.townId = townId;
	}
	public String getTownName() {
		return townName;
	}
	public void setTownName(String townName) {
		this.townName = townName;
	}
}
