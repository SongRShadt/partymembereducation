package com.shadt.signmeeting.model;

import java.io.Serializable;
/**
 * 摄像头
 * @author SongR
 *
 */
@SuppressWarnings("serial")
public class MeetingRoom implements Serializable{

	private String cameraNo;//摄像头编号
	private String cameraName;//摄像头名称
	private String cameraUrl;//播放地址
	private String cameraType;//摄像头分类
	public String getCameraNo() {
		return cameraNo;
	}
	public void setCameraNo(String cameraNo) {
		this.cameraNo = cameraNo;
	}
	public String getCameraName() {
		return cameraName;
	}
	public void setCameraName(String cameraName) {
		this.cameraName = cameraName;
	}
	public String getCameraUrl() {
		return cameraUrl;
	}
	public void setCameraUrl(String cameraUrl) {
		this.cameraUrl = cameraUrl;
	}
	public String getCameraType() {
		return cameraType;
	}
	public void setCameraType(String cameraType) {
		this.cameraType = cameraType;
	}
		
}
