package com.shadt.signmeeting.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.shadt.signmeeting.dao.StbInfoDao;
import com.shadt.signmeeting.dao.VillageDao;
import com.shadt.signmeeting.entity.Stb_Info;
import com.shadt.signmeeting.entity.Village_Info;
import com.shadt.signmeeting.service.StbInfoService;

/**
 * 机顶盒业务逻辑层实现类
 * 
 * @author liusongren
 *
 */
@Service
public class StbInfoServiceI implements StbInfoService {

	@Autowired
	StbInfoDao dao;
	@Autowired
	VillageDao vdao;

	/**
	 * 根据村组织编号获取机顶盒编号
	 */
	@ResponseBody
	@RequestMapping("/getStbNoByOrgId")
	public String getStbNoByOrgId(String orgId) {
		String sql = "select * from village_info a,stb_info b where a.villageid = b.villageid and orgcode='" + orgId
				+ "'";
		return dao.getStbNoByOrgId(sql);
	}

	/**
	 * 根据机顶盒mac获取机顶盒级别
	 */
	public Village_Info getStbByStbNo(String stbNo) {
		String sql = "select * from stb_info where stbno = '"+stbNo+"'";
		List<Stb_Info> stbList = dao.getBySql(sql);
		Stb_Info stb = new Stb_Info();
		Village_Info vill = new Village_Info();
		if(stbList.size()>0){
			stb = stbList.get(0);
			String vsql = "select * from village_info where villageid = '"+stb.getVillageId()+"'";
			List<Village_Info> vList =  vdao.getBySql(vsql);
			if(vList.size()>0){
				vill = vList.get(0);
			}else{
			}
		}else {
		}
		return vill;
	}

}
