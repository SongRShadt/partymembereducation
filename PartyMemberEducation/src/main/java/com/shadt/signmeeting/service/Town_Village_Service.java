package com.shadt.signmeeting.service;

import com.shadt.signmeeting.model.Town_Village_Info;

/**
 * 机顶盒对应信息Service
 * @author SongR
 *
 */
public interface Town_Village_Service {
	/**
	 * 根据机顶盒查询乡镇名称,村key值,村名称,村key值,组织编号
	 * @param stbNo
	 * @return
	 */
	Town_Village_Info getByStbNo(String stbNo);
}
