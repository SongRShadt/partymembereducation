package com.shadt.signmeeting.service;

import com.shadt.signmeeting.entity.Village_Info;

/**
 * 机顶盒业务逻辑层接口
 * @author liusongren
 *
 */
public interface StbInfoService {

	/**
	 * 根据村组织编号获取机顶盒编号
	 * @param orgId
	 * @return
	 */
	String getStbNoByOrgId(String orgId);

	Village_Info getStbByStbNo(String stbNo);

}
