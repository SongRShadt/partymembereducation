package com.shadt.signmeeting.service;

import java.util.List;

import com.shadt.signmeeting.entity.Village_Info;

/**
 * 村信息业务层接口
 * @author liusongren
 *
 */
public interface VillageService {

	/**
	 * 根据orgCode获取villagekey
	 * @param orgCode
	 * @return
	 */
	String getKeyByOrgCode(String orgCode);

	/**
	 * 根据镇编号获取村列表
	 * @param townId
	 * @return
	 */
	List<Village_Info> getVillageByTownId(String townId);

}
