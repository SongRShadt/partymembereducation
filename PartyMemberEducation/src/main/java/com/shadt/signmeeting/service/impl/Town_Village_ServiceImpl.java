package com.shadt.signmeeting.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.shadt.signmeeting.dao.Town_Village_Dao;
import com.shadt.signmeeting.model.Town_Village_Info;
import com.shadt.signmeeting.service.Town_Village_Service;

/**
 * 机顶盒对应信息ServiceImpl
 * 
 * @author SongR
 * 
 */
@Service
public class Town_Village_ServiceImpl implements Town_Village_Service {
	@Autowired
	Town_Village_Dao dao;

	/**
	 * 根据机顶盒查询乡镇名称,村key值,村名称,村key值,组织编号
	 */
	public Town_Village_Info getByStbNo(String stbNo) {
		return dao.getByStbNo(stbNo);
	}

}
