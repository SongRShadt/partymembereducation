package com.shadt.signmeeting.service;

/**
 * 回看Service
 * @author SongR
 *
 */
public interface Camera_Video_List_Service {

	/**
	 * 根据摄像头编号获取摄像头对应的回看视频列表
	 * @param cameraNo
	 * @return
	 */
	String getByCameraNo(String cameraNo);

}
