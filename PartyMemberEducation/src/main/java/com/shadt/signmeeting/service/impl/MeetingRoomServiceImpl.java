
package com.shadt.signmeeting.service.impl;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.alibaba.fastjson.JSONArray;
import com.shadt.core.model.Json;
import com.shadt.signmeeting.dao.MeetingRoomDao;
import com.shadt.signmeeting.model.MeetingRoom;
import com.shadt.signmeeting.service.MeetingRoomService;

/**
 * 会议室业务层实现类
 * @author SongR
 *
 */
@Service
public class MeetingRoomServiceImpl implements MeetingRoomService{

	@Autowired
	MeetingRoomDao dao;
	
	/**
	 * 实现获取镇对应的会议室
	 */
	public String getMeetingRoomByTown(String stbNo, String townId,String cType) {
		Json j = new Json();
		List<MeetingRoom> cList = new ArrayList<MeetingRoom>();
		if (null != stbNo && !"".equals(stbNo)) {
			cList = dao.getMeetingRoomByTown(stbNo,townId,cType);
			j.setSuccess(true);
			if (cList.size()==0) {
				j.setMsg("未查询到相关数据！请检查机顶盒编号是否正确！");
				j.setObj("");
			} else {
				j.setMsg("获取成功！");
				j.setObj(cList);
			}
		} else {
			j.setSuccess(false);
			j.setMsg("获取失败！机顶盒编号为空！");
			j.setObj("");
		}
		return JSONArray.toJSONString(j);
	}

}
