package com.shadt.signmeeting.service.impl;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.alibaba.fastjson.JSONArray;
import com.shadt.core.model.Json;
import com.shadt.signmeeting.dao.Camera_Info_Dao;
import com.shadt.signmeeting.model.Camera_Info;
import com.shadt.signmeeting.service.Camera_Info_Service;

/**
 * 机顶盒对应的摄像头信息
 * @author SongR
 *
 */
@Service
public class Camera_Info_ServiceImpl implements Camera_Info_Service{
	
	@Autowired
	Camera_Info_Dao dao;
	
	
	
	/**
	 * 根据机顶盒编号获取机顶盒对应的摄像头信息
	 */
	public String getCameraInfoByStbNo(String stbNo) {
		Json j = new Json();
		List<Camera_Info> cList = new ArrayList<Camera_Info>();
		if (null != stbNo && !"".equals(stbNo)) {
			cList = dao.getCameraInfoByStbNo(stbNo);
			j.setSuccess(true);
			if (cList.size()==0) {
				j.setMsg("未查询到相关数据！请检查机顶盒编号是否正确！");
				j.setObj("");
			} else {
				j.setMsg("获取成功！");
				j.setObj(cList);
			}
		} else {
			j.setSuccess(false);
			j.setMsg("获取失败！机顶盒编号为空！");
			j.setObj("");
		}
		return JSONArray.toJSONString(j);
	}
	
	/**
	 * 实现根据摄像头编号获取摄像头信息
	 */
	public String getCameraInfoByCameraNo(String cameraNo) {
		Json j = new Json();
		Camera_Info c = new Camera_Info();
		if (null != cameraNo && !"".equals(cameraNo)) {
			c = dao.getCameraInfoByCameraNo(cameraNo);
			j.setSuccess(true);
			if (c==null) {
				j.setMsg("未查询到相关数据！请检查摄像头编号是否正确！");
				j.setObj("");
			} else {
				j.setMsg("获取成功！");
				j.setObj(c);
			}
		} else {
			j.setSuccess(false);
			j.setMsg("获取失败！摄像头编号为空！");
			j.setObj("");
		}
		return JSONArray.toJSONString(j);
	}

	/**
	 * 实现根据村组织编号获取村组对应的会议室摄像头路径
	 */
	public Camera_Info getLiveUrl(String orgId) {
		return dao.getLiveUrl(orgId);
	}

	/**
	 * 实现根据机顶盒和摄像头类型获取摄像头信息
	 */
	public Camera_Info getByType(String stbNo, String type) {
		
		
		return null;
	}

}
