package com.shadt.signmeeting.service;

/**
 * 会议室service
 * @author SongR
 *
 */
public interface MeetingRoomService {

	/**
	 * 根据镇获取会议室列表
	 * @param stbNo
	 * @param townId
	 * @return
	 */
	String getMeetingRoomByTown(String stbNo, String townId,String cType);

}
