package com.shadt.signmeeting.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.shadt.signmeeting.dao.PartyChannelDao;
import com.shadt.signmeeting.entity.PartyChannel;
import com.shadt.signmeeting.service.PartyChannelService;

/**
 * @author SongR
 * 党建频道业务逻辑层实现类
 */
@Service
public class PartyChannelServiceImpl implements PartyChannelService{

	@Autowired
	PartyChannelDao dao;
	
	/**
	 * 实现获取党建频道列表
	 */
	public List<PartyChannel> getChannelUrl(String size) {
		return dao.find("from PartyChannel order by channelSort", 0,Integer.parseInt(size));
	}

}
