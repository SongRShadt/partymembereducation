package com.shadt.signmeeting.service;

import java.util.List;

import com.shadt.signmeeting.entity.PartyChannel;

/**
 * @author SongR
 * 党建频道业务逻辑层接口
 */
public interface PartyChannelService {

	/**
	 * 获取党建频道列表
	 * @param size 
	 * @return
	 */
	List<PartyChannel> getChannelUrl(String size);

}
