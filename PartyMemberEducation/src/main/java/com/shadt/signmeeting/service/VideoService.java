package com.shadt.signmeeting.service;

import java.util.List;

import com.shadt.signmeeting.model.Video;

/**
 * 录制视频业务层
 * @author liusongren
 *
 */
public interface VideoService {
	
	/**
	 *  保存录制视频信息
	 * @param v
	 */
	void saveVideo(Video v);

	/**
	 * 根据摄像头编号获取录制视频列表
	 * @param cameraNo
	 * @return
	 */
	List<Video> getAllVideoByCameraNo(String cameraNo);

}
