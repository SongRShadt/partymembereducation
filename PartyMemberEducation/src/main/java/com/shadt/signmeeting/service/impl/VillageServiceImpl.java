package com.shadt.signmeeting.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.shadt.signmeeting.dao.VillageDao;
import com.shadt.signmeeting.entity.Village_Info;
import com.shadt.signmeeting.service.VillageService;

/**
 * 村信息业务层实现类
 * 
 * @author liusongren
 *
 */
@Service
public class VillageServiceImpl implements VillageService {
	@Autowired
	VillageDao dao;

	/**
	 * 实现根据orgcode获取key
	 */
	public String getKeyByOrgCode(String orgCode) {

		return dao.getKeyByOrgCode(orgCode);
	}

	/**
	 * 实现根据镇编号获取村列表
	 */
	public List<Village_Info> getVillageByTownId(String townId) {
//		Integer count = dao.getCountBySql("select count(*) from village_info where townid='" + townId
//				+ "' and orgCode not in ('0','-1')  order by orgcode");
//		
		
		String sql = "select * from village_info where townid='" + townId
				+ "' and orgCode >0  order by orgcode";
		return dao.getBySql(sql);
	}

}
