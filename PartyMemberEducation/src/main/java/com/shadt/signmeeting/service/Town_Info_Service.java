package com.shadt.signmeeting.service;

/**
 * 镇信息业务层接口
 * @author SongR
 *
 */
public interface Town_Info_Service {

	/**
	 * 获取所有镇信息
	 * @return
	 */
	String getAllTown();

	String getTownByStbNo(String stbNo);

}
