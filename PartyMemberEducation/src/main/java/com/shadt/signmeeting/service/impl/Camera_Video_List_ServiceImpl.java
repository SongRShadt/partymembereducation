package com.shadt.signmeeting.service.impl;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.alibaba.fastjson.JSONArray;
import com.shadt.core.model.Json;
import com.shadt.signmeeting.dao.Camera_Video_List_Dao;
import com.shadt.signmeeting.model.Camera_Video_List;
import com.shadt.signmeeting.service.Camera_Video_List_Service;

/**
 * 回看ServiceImpl
 * @author SongR
 *
 */
@Service
public class Camera_Video_List_ServiceImpl implements Camera_Video_List_Service {

	@Autowired
	Camera_Video_List_Dao dao;
	
	/**
	 * 实现根据摄像头编号获取摄像头对应的回看列表
	 */
	public String getByCameraNo(String cameraNo) {
		Json j = new Json();
		List<Camera_Video_List> cList = new ArrayList<Camera_Video_List>();
		if (null != cameraNo && !"".equals(cameraNo)) {
			cList = dao.getByCameraNo(cameraNo);
			j.setSuccess(true);
			if (cList.size()==0) {
				j.setMsg("未查询到相关数据！请检查机摄像头编号是否正确！");
				j.setObj("");
			} else {
				j.setMsg("获取成功！");
				j.setObj(cList);
			}
		} else {
			j.setSuccess(false);
			j.setMsg("获取失败！机顶盒编号为空！");
			j.setObj("");
		}
		return JSONArray.toJSONString(j);
	}

}
