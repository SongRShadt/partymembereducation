package com.shadt.signmeeting.service;

import com.shadt.signmeeting.model.Camera_Info;

/**
 * 机顶盒对应的摄像头信息Service
 * @author SongR
 *
 */
public interface Camera_Info_Service {
	/**
	 * 根据机顶盒编号获取机顶盒对应的摄像头信息
	 * @param stbNo 机顶盒编号
	 * @return
	 */
	String getCameraInfoByStbNo(String stbNo);

	/**
	 * 根据摄像头编号获取摄像头信息
	 * @param cameraNo
	 * @return
	 */
	String getCameraInfoByCameraNo(String cameraNo);
	/**
	 * 获取视频会议摄像头路径
	 * @param orgId
	 * @return
	 */
	Camera_Info getLiveUrl(String orgId);

	/**
	 * 根据机顶盒和摄像头类型获取摄像头信息
	 * @param stbNo
	 * @param type
	 * @return
	 */
	Camera_Info getByType(String stbNo, String type);
}
