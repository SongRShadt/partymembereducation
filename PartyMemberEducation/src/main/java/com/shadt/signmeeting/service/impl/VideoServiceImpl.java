package com.shadt.signmeeting.service.impl;

import java.util.Date;
import java.util.List;
import java.util.UUID;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.shadt.core.util.DateUtil;
import com.shadt.signmeeting.dao.VideoDao;
import com.shadt.signmeeting.model.Video;
import com.shadt.signmeeting.service.VideoService;

/**
 * 录制视频业务层实现类
 * @author liusongren
 *
 */
@Service
public class VideoServiceImpl implements VideoService{
	@Autowired
	VideoDao dao;
	
	
	/**
	 * 实现保存录制的视频信息
	 */
	public void saveVideo(Video v) {
		v.setId(UUID.randomUUID().toString());
		Long end = DateUtil.DateToMillis(new Date());
		Long star = end - v.getDuration()*1000;
		v.setEndTime(DateUtil.MillisToDate(end));
		v.setStartTime(DateUtil.MillisToDate(star));
		dao.saveVideo(v);
	}

	/**
	 * 实现根据摄像头编号获取录制视频列表
	 */
	public List<Video> getAllVideoByCameraNo(String cameraNo) {
		return dao.getAllByCameraNo(cameraNo);
	}

}
