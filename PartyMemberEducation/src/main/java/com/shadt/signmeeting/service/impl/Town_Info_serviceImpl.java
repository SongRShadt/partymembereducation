package com.shadt.signmeeting.service.impl;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.alibaba.fastjson.JSONArray;
import com.shadt.core.model.Json;
import com.shadt.signmeeting.dao.Town_Info_Dao;
import com.shadt.signmeeting.model.Town_Info;
import com.shadt.signmeeting.service.Town_Info_Service;

/**
 * 镇信息业务层实现类
 * 
 * @author SongR
 * 
 */
@Service
public class Town_Info_serviceImpl implements Town_Info_Service {

	@Autowired
	Town_Info_Dao dao;
	
	/**
	 * 实现获取所有镇信息
	 */
	public String getAllTown() {
		Json j = new Json();
		List<Town_Info> cList = new ArrayList<Town_Info>(); 
		try {
			cList=dao.getAllTown();
			j.setSuccess(true);
			if (cList.size()==0) {
				j.setMsg("未查询到相关数据！请检查机摄像头编号是否正确！");
				j.setObj("");
			} else {
				j.setMsg("获取成功！");
				j.setObj(cList);
			}
		} catch (Exception e) {
			j.setSuccess(false);
			j.setMsg("查询出错！");
			j.setObj("");
		}
		return JSONArray.toJSONString(j);
	}
	@Override
	public String getTownByStbNo(String stbNo) {
		Json j = new Json();
		List<Town_Info> cList = new ArrayList<Town_Info>();
		try {
			cList = dao.getByStbNo(stbNo);
			j.setSuccess(true);
			if (cList.size()==0) {
				j.setMsg("未查询到相关数据！请检查机摄像头编号是否正确！");
				j.setObj("");
			} else {
				j.setMsg("获取成功！");
				j.setObj(cList);
			}
		} catch (Exception e) {
			j.setSuccess(false);
			j.setMsg("查询出错！");
			j.setObj("");
		}
		return JSONArray.toJSONString(j);
	}

}
