package com.shadt.signmeeting.dao.impl;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import org.springframework.stereotype.Repository;

import com.shadt.core.dao.impl.BaseDaoImpl;
import com.shadt.signmeeting.dao.Camera_Info_Dao;
import com.shadt.signmeeting.model.Camera_Info;
import com.shadt.signmeeting.util.PropertiesGetSql;

/**
 * 机顶盒对应的摄像头信息数据访问层实现类
 * @author SongR
 * 
 */
@Repository
public class Camera_Info_DaoImpl extends BaseDaoImpl<Camera_Info> implements
		Camera_Info_Dao {

	/**
	 * 根据机顶盒编号获取机顶盒对应的摄像头信息
	 */
	@SuppressWarnings("rawtypes")
	public List<Camera_Info> getCameraInfoByStbNo(String stbNo) {
		String sql =String.format("select b.camerano,b.cameraname,b.cameraurl,b.imgurl from stb_camera_rlt a,camera_info b where stbno = '%s' and a.camerano = b.camerano and a.rlt_type = 0 order by camerano",stbNo);
//		System.out.println(sql);
		List list = this.getJdbcTemplate().queryForList(sql);
		List<Camera_Info> cList = new ArrayList<Camera_Info>();
		Iterator it = list.iterator();
		if(list.size()>0){
			while (it.hasNext()) {
				Map result = (Map) it.next();
				Camera_Info c = new Camera_Info();
				c.setCameraName(result.get("CameraName") != null ? result.get("CameraName").toString() : "");
				c.setCameraNo(result.get("CameraNo") != null ? result.get("CameraNo").toString() : "");
				c.setCameraUrl(result.get("CameraUrl") != null ? result.get("CameraUrl").toString() : "");
				c.setImgUrl(result.get("ImgUrl") != null ? result.get("ImgUrl").toString() : "");
				cList.add(c);
			}
		}
		return cList;
	}

	/**
	 * 实现根据摄像头编号获取摄像头信息
	 */
	@SuppressWarnings("rawtypes")
	public Camera_Info getCameraInfoByCameraNo(String cameraNo) {
		String sql =String.format("select * from camera_info where camerano = '%s'",cameraNo);
		List list = this.getJdbcTemplate().queryForList(sql);
		Camera_Info c = new Camera_Info();
		Iterator it = list.iterator();
		if(list.size()==1){
			while (it.hasNext()) {
				Map result = (Map) it.next();
				c.setCameraName(result.get("CameraName") != null ? result.get("CameraName").toString() : "");
				c.setCameraNo(result.get("CameraNo") != null ? result.get("CameraNo").toString() : "");
				c.setCameraUrl(result.get("CameraUrl") != null ? result.get("CameraUrl").toString() : "");
				c.setImgUrl(result.get("ImgUrl") != null ? result.get("ImgUrl").toString() : "");
			}
		}
		return c;
	}

	/**
	 * 实现根据村组织编号获取对应的会议室摄像头路径
	 */
	@SuppressWarnings("rawtypes")
	public Camera_Info getLiveUrl(String orgId) {
		String sql =String.format(PropertiesGetSql.get("Camera_Info_DaoImpl_getLiveUrl"),orgId);
		List list = this.getJdbcTemplate().queryForList(sql);
		Iterator it = list.iterator();
		Camera_Info c = new Camera_Info();
		while (it.hasNext()) {
			Map result = (Map) it.next();
			c.setCameraUrl(result.get("CameraUrl") != null ? result.get("CameraUrl").toString() : "");
			c.setCameraName(result.get("CameraName") != null ? result.get("CameraName").toString() : "");
			c.setCameraNo(result.get("CameraNo") != null ? result.get("CameraNo").toString() : "");
			c.setImgUrl(result.get("ImgUrl") != null ? result.get("ImgUrl").toString() : "");
			c.setStreamId(result.get("StreamId") != null ? result.get("StreamId").toString() : "");
		}
		return c;
	}

}
