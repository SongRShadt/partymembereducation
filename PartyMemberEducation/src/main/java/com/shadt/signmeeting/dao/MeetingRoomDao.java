package com.shadt.signmeeting.dao;

import java.util.List;

import com.shadt.core.dao.BaseDao;
import com.shadt.signmeeting.model.MeetingRoom;


/**
 * 会议室数据访问层接口
 * @author SongR
 *
 */
public interface MeetingRoomDao extends BaseDao<MeetingRoom>{

	/**
	 * 获取镇对应的会议室
	 * @param stbNo
	 * @param townId
	 * @return
	 */
	List<MeetingRoom> getMeetingRoomByTown(String stbNo, String townId,String cType);

}
