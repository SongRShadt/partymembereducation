package com.shadt.signmeeting.dao;

import java.util.List;

import com.shadt.core.dao.BaseDao;
import com.shadt.signmeeting.model.Camera_Video_List;
/**
 * 回看dao
 * @author SongR
 *
 */
public interface Camera_Video_List_Dao extends BaseDao<Camera_Video_List>{

	/**
	 * 根据摄像头编号获取摄像头对应的回看列表
	 * @param cameraNo
	 * @return
	 */
	List<Camera_Video_List> getByCameraNo(String cameraNo);
	
	

}
