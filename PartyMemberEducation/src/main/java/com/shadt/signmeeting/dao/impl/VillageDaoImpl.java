package com.shadt.signmeeting.dao.impl;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import org.springframework.stereotype.Repository;

import com.shadt.core.dao.impl.BaseDaoImpl;
import com.shadt.signmeeting.dao.VillageDao;
import com.shadt.signmeeting.entity.Village_Info;

/**
 * 村信息数据访问层实现类
 * 
 * @author liusongren
 *
 */
@Repository
public class VillageDaoImpl extends BaseDaoImpl<Village_Info> implements VillageDao {

	/**
	 * 实现根据orgcode获取villagekey
	 */
	@SuppressWarnings("rawtypes")
	public String getKeyByOrgCode(String orgCode) {
		List l = this.getJdbcTemplate()
				.queryForList("select villagekeyvalue from village_info where orgcode = '" + orgCode + "'");
		Iterator it = l.iterator();
		String keyValue = "";
		if (l.size() > 0) {
			while (it.hasNext()) {
				Map r = (Map) it.next();
				keyValue = r.get("villagekeyvalue") != null ? r.get("villagekeyvalue").toString() : "暂无此key";
			}
		}
		return keyValue;
	}

	/**
	 * 根据sql获取村信息
	 */
	@SuppressWarnings("rawtypes")
	public List<Village_Info> getBySql(String sql) {
		List l = this.getJdbcTemplate().queryForList(sql);
		List<Village_Info> vList = new ArrayList<Village_Info>();
		Iterator it = l.iterator();
		if (l.size() > 0) {
			while (it.hasNext()) {
				Map m = (Map) it.next();
				Village_Info vill = new Village_Info();
				vill.setVillageId(m.get("villageid").toString() == null ? "" : m.get("villageid").toString());
				vill.setVillageName(m.get("villagename").toString() == null ? "" : m.get("villagename").toString());
				vill.setVillageKeyValue(m.get("villagekeyvalue") == null ? "" : m.get("villagekeyvalue").toString());
				vill.setTownId(m.get("townid").toString() == null ? "" : m.get("townid").toString());
				vill.setOrgCode(m.get("orgcode").toString() == null ? "" : m.get("orgcode").toString());
				vList.add(vill);
			}
		}
		return vList;
	}

	/**
	 * 实现获取总记录数
	 */
	@SuppressWarnings("deprecation")
	public Integer getCountBySql(String sql) {
		return this.getJdbcTemplate().queryForInt(sql);
	}

}
