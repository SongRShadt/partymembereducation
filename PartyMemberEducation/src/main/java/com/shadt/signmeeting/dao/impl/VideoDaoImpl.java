package com.shadt.signmeeting.dao.impl;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import org.springframework.stereotype.Repository;

import com.shadt.core.dao.impl.BaseDaoImpl;
import com.shadt.core.util.DateUtil;
import com.shadt.signmeeting.dao.VideoDao;
import com.shadt.signmeeting.model.Video;

/**
 * 视频录制dao
 * 
 * @author liusongren
 *
 */
@Repository
public class VideoDaoImpl extends BaseDaoImpl<Video> implements VideoDao {

	/**
	 * 实现保存视频录制
	 */
	public void saveVideo(Video v) {
		int i = this.getJdbcTemplate().update(
				"insert into video(id,starttime,endtime,camerano,taskid,download_url,play_url,duration,status) values(?,?,?,?,?,?,?,?)",
				new Object[] { v.getId(), v.getStartTime(), v.getEndTime(), v.getCameraNo(), v.getTaskId(),
						v.getDownloadUrl(), v.getPlayUrl(), v.getDuration(),0 });
	}

	/**
	 * 实现根据摄像头编号获取视频录制列表
	 * 
	 * @param cameraNo
	 * @return
	 */
	@SuppressWarnings("rawtypes")
	public List<Video> getAllByCameraNo(String cameraNo) {
		String sqlc = "select * from camera_info where camerano = '"+cameraNo+"'";
		List listc = this.getJdbcTemplate().queryForList(sqlc);
		Iterator itc = listc.iterator();
		String url = "";
		if(listc.size()>0){
			while(itc.hasNext()){
				Map r = (Map) itc.next();
				url =r.get("cameraurl").toString().split("/")[2];
			}
		}
		
		String sql = "select * from video where camerano ='" + cameraNo + "' and video_status=0 order by starttime desc";
		List list = this.getJdbcTemplate().queryForList(sql);
		List<Video> vList = new ArrayList<Video>();
		Iterator it = list.iterator();
		if (list.size() > 0) {
			while (it.hasNext()) {
				Map r = (Map) it.next();
				Video v = new Video();
				v.setCameraNo(r.get("CameraNo") != null ? r.get("CameraNo").toString() : "");
				v.setStartTime(DateUtil.strToDate(r.get("StartTime").toString(), "yyyy-MM-dd HH:mm:ss"));
				v.setEndTime(DateUtil.strToDate(r.get("EndTime").toString(), "yyyy-MM-dd HH:mm:ss"));
				v.setPlayUrl(r.get("play_url") != null ? ("http://"+url+r.get("play_url").toString()) : "");
				v.setDuration(Integer.parseInt(r.get("Duration").toString()));
				vList.add(v);
			}
		}
		return vList;
	}

}
