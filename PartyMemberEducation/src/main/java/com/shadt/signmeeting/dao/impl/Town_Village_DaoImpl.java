package com.shadt.signmeeting.dao.impl;

import java.util.Iterator;
import java.util.List;
import java.util.Map;

import org.springframework.stereotype.Repository;

import com.shadt.core.dao.impl.BaseDaoImpl;
import com.shadt.signmeeting.dao.Town_Village_Dao;
import com.shadt.signmeeting.model.Town_Village_Info;
import com.shadt.signmeeting.util.PropertiesGetSql;
/**
 * 机顶盒对应信息数据访问层实现
 * @author SongR
 *
 */
@Repository
public class Town_Village_DaoImpl extends BaseDaoImpl<Town_Village_Info> implements Town_Village_Dao{
	/**
	 * 根据机顶盒查询乡镇名称,村key值,村名称,村key值,组织编号
	 */
	@SuppressWarnings("rawtypes")
	public Town_Village_Info getByStbNo(String stbNo) {
		String sql =String.format(PropertiesGetSql.get("Stb_Info_DaoImpl_getByStbNo"),stbNo);
//		System.out.println(sql);
		List list = this.getJdbcTemplate().queryForList(sql);
		Town_Village_Info tvi = null;
		Iterator it = list.iterator();
//		System.out.println(list.size());
		if(list.size()==1){
			while (it.hasNext()) {
				Map result = (Map) it.next();
				tvi = new Town_Village_Info();
				tvi.setOrgCode(result.get("OrgCode") != null ? result.get("OrgCode").toString() : "");
				tvi.setTownKeyValue(result.get("TownKeyValue") != null ? result.get("TownKeyValue").toString() : "");
				tvi.setTownName(result.get("TownName") != null ? result.get("TownName").toString() : "");
				tvi.setVillageId(result.get("VillageId") != null ? result.get("VillageId").toString() : "");
				tvi.setVillageKeyValue(result.get("VillageKeyValue") != null ? result.get("VillageKeyValue").toString() : "");
			
			}
		}
		return tvi;
	}

}
