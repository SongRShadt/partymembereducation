package com.shadt.signmeeting.dao.impl;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import org.springframework.stereotype.Repository;

import com.shadt.core.dao.impl.BaseDaoImpl;
import com.shadt.signmeeting.dao.StbInfoDao;
import com.shadt.signmeeting.entity.Stb_Info;

@Repository
@SuppressWarnings("rawtypes")
public class StbInfoDaoI extends BaseDaoImpl implements StbInfoDao{

	@Override
	public String getStbNoByOrgId(String sql) {
		List l = this.getJdbcTemplate().queryForList(sql);
		Iterator it = l.iterator();
		String keyValue ="";
		if (l.size() > 0) {
			while (it.hasNext()) {
				Map r = (Map) it.next();
				keyValue = r.get("stbno").toString();
			}
		}
		return keyValue;
	}

	/**
	 * 实现根据sql获取机顶盒信息
	 */
	public List<Stb_Info> getBySql(String sql) {
		List l = this.getJdbcTemplate().queryForList(sql);
		Iterator it = l.iterator();
		List<Stb_Info> stbList = new ArrayList<Stb_Info>();
		if(l.size()>0){
			while(it.hasNext()){
				Map m = (Map)it.next();
				Stb_Info stb = new Stb_Info();
				stb.setStbNo(m.get("stbNo").toString());
				stb.setVillageId(m.get("villageid").toString());
				stbList.add(stb);
			}
		}
		return stbList;
	}
}
