package com.shadt.signmeeting.dao;

import java.util.List;

import com.shadt.core.dao.BaseDao;
import com.shadt.signmeeting.model.Town_Info;

/**
 * 镇信息数据访问层接口
 * @author SongR
 *
 */
public interface Town_Info_Dao extends BaseDao<Town_Info>{

	/**
	 * 获取所有镇信息
	 * @return
	 */
	List<Town_Info> getAllTown();
	
	List<Town_Info>  getByStbNo(String stbNo);
	

}
