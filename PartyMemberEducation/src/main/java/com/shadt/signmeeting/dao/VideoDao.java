package com.shadt.signmeeting.dao;

import java.util.List;

import com.shadt.signmeeting.model.Video;

/**
 * 视频录制信息
 * @author liusongren
 *
 */
public interface VideoDao {
	
	/**
	 * 保存
	 * @param v
	 */
	void saveVideo(Video v);
	/**
	 * 根据摄像头编号获取录制视频列表
	 * @param cameraNo
	 * @return
	 */
	List<Video> getAllByCameraNo(String cameraNo);
}
