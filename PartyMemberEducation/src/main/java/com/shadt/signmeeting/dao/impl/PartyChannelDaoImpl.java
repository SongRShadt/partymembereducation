package com.shadt.signmeeting.dao.impl;

import org.springframework.stereotype.Repository;

import com.shadt.core.dao.impl.BaseDaoImpl;
import com.shadt.signmeeting.dao.PartyChannelDao;
import com.shadt.signmeeting.entity.PartyChannel;

/**
 * @author SongR
 * 党建频道数据访问层实现类继承BaseDaoImpl
 */
@Repository
public class PartyChannelDaoImpl extends BaseDaoImpl<PartyChannel> implements PartyChannelDao{

}
