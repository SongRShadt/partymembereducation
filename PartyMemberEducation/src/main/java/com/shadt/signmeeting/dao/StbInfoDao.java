package com.shadt.signmeeting.dao;

import java.util.List;

import com.shadt.core.dao.BaseDao;
import com.shadt.signmeeting.entity.Stb_Info;

/**
 * 机顶盒信息数据访问层接口
 * @author liusongren
 *
 */
@SuppressWarnings("rawtypes")
public interface StbInfoDao extends BaseDao{

	/**
	 * 根据orgId获取机顶盒编号
	 * @param orgId
	 * @return
	 */
	String getStbNoByOrgId(String sql);

	List<Stb_Info> getBySql(String sql);

}
