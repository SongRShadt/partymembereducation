package com.shadt.signmeeting.dao.impl;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import org.springframework.stereotype.Repository;

import com.shadt.core.dao.impl.BaseDaoImpl;
import com.shadt.signmeeting.dao.MeetingRoomDao;
import com.shadt.signmeeting.model.MeetingRoom;
import com.shadt.signmeeting.util.PropertiesGetSql;

/**
 * 会议室数据访问层实现类
 * @author SongR
 *
 */
@Repository
public class MeetingRoomDaoImpl extends BaseDaoImpl<MeetingRoom> implements MeetingRoomDao{

	/**
	 * 实现根据镇获取会议室
	 */
	@SuppressWarnings("rawtypes")
	public List<MeetingRoom> getMeetingRoomByTown(String stbNo, String townId,String cType) {
//		System.out.println("townid===="+townId+"---------------------------");
		String sql =String.format(PropertiesGetSql.get("MeetingRoom_getMeetingRoomByTown"),stbNo,townId,cType);
//		System.out.println(sql);
		List list = this.getJdbcTemplate().queryForList(sql);
		List<MeetingRoom> cList = new ArrayList<MeetingRoom>();
		Iterator it = list.iterator();
		if(list.size()>0){
			while (it.hasNext()) {
				Map result = (Map) it.next();
				MeetingRoom c = new MeetingRoom();
				c.setCameraName(result.get("CameraName") != null ? result.get("CameraName").toString() : "");
				c.setCameraNo(result.get("CameraNo") != null ? result.get("CameraNo").toString() : "");
				c.setCameraUrl(result.get("CameraUrl") != null ? result.get("CameraUrl").toString() : "");
				c.setCameraType(result.get("CameraType") != null ? result.get("CameraType").toString() : "");
				cList.add(c);
			}
		}
		return cList;
	}

}
