package com.shadt.signmeeting.dao;

import java.util.List;

import com.shadt.signmeeting.entity.Village_Info;

/**
 * 村信息数据访问层接口
 * 
 * @author liusongren
 *
 */
public interface VillageDao {

	/**
	 * 根据orgcode获取villagekey
	 * 
	 * @param orgCode
	 * @return
	 */
	String getKeyByOrgCode(String orgCode);

	/**
	 * 根据sql获取村信息
	 * @param sql
	 * @return
	 */
	List<Village_Info> getBySql(String sql);

	/**
	 * 获取总记录数
	 * @param string
	 * @return
	 */
	Integer getCountBySql(String sql);

}
