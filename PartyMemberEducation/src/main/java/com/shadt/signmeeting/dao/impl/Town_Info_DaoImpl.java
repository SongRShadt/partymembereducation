package com.shadt.signmeeting.dao.impl;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import org.springframework.stereotype.Repository;

import com.shadt.core.dao.impl.BaseDaoImpl;
import com.shadt.signmeeting.dao.Town_Info_Dao;
import com.shadt.signmeeting.model.Town_Info;
import com.shadt.signmeeting.util.PropertiesGetSql;

/**
 * 镇信息数据访问层实现类
 * 
 * @author SongR
 * 
 */
@Repository
public class Town_Info_DaoImpl extends BaseDaoImpl<Town_Info> implements
		Town_Info_Dao {

	/**
	 * 实现获取所有镇信息
	 */
	@SuppressWarnings("rawtypes")
	public List<Town_Info> getAllTown() {
		String sql = String
				.format(PropertiesGetSql.get("Town_Info_getAllTown"));
		List list = this.getJdbcTemplate().queryForList(sql);
		List<Town_Info> cList = new ArrayList<Town_Info>();
		Iterator it = list.iterator();
		if (list.size() > 0) {
			while (it.hasNext()) {
				Map result = (Map) it.next();
				Town_Info c = new Town_Info();
				c.setTownId(result.get("TownId") != null ? result.get("TownId")
						.toString() : "");
				c.setTownName(result.get("TownName") != null ? result.get(
						"TownName").toString() : "");
				cList.add(c);
			}
		}
		return cList;
	}

	@SuppressWarnings("rawtypes")
	public  List<Town_Info>  getByStbNo(String stbNo) {
		String cSql = "select * from stb_camera_rlt where stbno = '"+stbNo+"'";
		List listc = this.getJdbcTemplate().queryForList(cSql);
		String sql = "select * from town_info a where townid in(select distinct (b.townid) from village_info b,camera_info c,stb_camera_rlt d where d.stbno = '"+stbNo+"'and d.camerano = c.camerano and c.villageid = b.villageid)";
//		System.out.println("该机顶盒已绑定了"+listc.size()+"个摄像头");
		if(listc.size()<=0){
			sql = "select distinct c.* from stb_info a,village_info b,town_info c where a.villageid = b.villageid and b.townid=c.townid and a.stbno ='"+stbNo+"'";
		}
		List list = this.getJdbcTemplate().queryForList(sql);
		List<Town_Info> cList = new ArrayList<Town_Info>();
		Iterator it = list.iterator();
		if (list.size() > 0) {
			while (it.hasNext()) {
				Map result = (Map) it.next();
				Town_Info c = new Town_Info();
				c.setTownId(result.get("TownId") != null ? result.get("TownId")
						.toString() : "");
				c.setTownName(result.get("TownName") != null ? result.get(
						"TownName").toString() : "");
				cList.add(c);
			}
		}
		return cList;
	}

}
