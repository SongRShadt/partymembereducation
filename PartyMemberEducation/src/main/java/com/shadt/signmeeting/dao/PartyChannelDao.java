package com.shadt.signmeeting.dao;

import com.shadt.core.dao.BaseDao;
import com.shadt.signmeeting.entity.PartyChannel;

/**
 * 党建频道数据访问层接口
 * @author SongR
 *
 */
public interface PartyChannelDao extends BaseDao<PartyChannel>{

}
