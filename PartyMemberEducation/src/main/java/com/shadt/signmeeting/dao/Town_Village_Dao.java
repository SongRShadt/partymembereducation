package com.shadt.signmeeting.dao;

import com.shadt.core.dao.BaseDao;
import com.shadt.signmeeting.model.Town_Village_Info;
/**
 * 机顶盒对应信息数据访问层接口
 * @author SongR
 *
 */
public interface Town_Village_Dao extends BaseDao<Town_Village_Info>{
	/**
	 * 根据机顶盒查询乡镇名称,村key值,村名称,村key值,组织编号
	 * @param stbNo
	 * @return
	 */
	Town_Village_Info getByStbNo(String stbNo);
}
