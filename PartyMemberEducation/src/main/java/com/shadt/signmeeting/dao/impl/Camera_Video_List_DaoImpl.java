package com.shadt.signmeeting.dao.impl;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import org.springframework.stereotype.Repository;

import com.shadt.core.dao.impl.BaseDaoImpl;
import com.shadt.signmeeting.dao.Camera_Video_List_Dao;
import com.shadt.signmeeting.model.Camera_Video_List;

/**
 * 回看数据访问层实现类
 * @author SongR
 *
 */
@Repository
public class Camera_Video_List_DaoImpl extends BaseDaoImpl<Camera_Video_List> implements Camera_Video_List_Dao{

	/**
	 * 实现根据摄像头编号获取摄像头对应的回看列表
	 */
	@SuppressWarnings({ "rawtypes"})
	public List<Camera_Video_List> getByCameraNo(String cameraNo) {
		String sql ="select a.STARTTIME StartTime,DATE_FORMAT(a.STARTTIME+a.DURATION,'%Y-%m-%d %H:%i:%s') EndTime,a.URL Video_url,b.'NAME' CameraNo from nvod_program a,nvod_channel b where a.CHANNEL_ID = b.ID and b.name = '"+cameraNo+"' ORDER BY a.CREATETIME DESC";
		List list = new ArrayList();//this.getJdbcTemplate_108().queryForList(sql);
//		System.out.println(sql);
		List<Camera_Video_List> cList = new ArrayList<Camera_Video_List>();
		Iterator it = list.iterator();
		if(list.size()>0){
			while (it.hasNext()) {
				Map result = (Map) it.next();
				Camera_Video_List c = new Camera_Video_List();
				c.setCameraNo(result.get("CameraNo") != null ? result.get("CameraNo").toString() : "");
				c.setStartTime(result.get("StartTime") != null ? result.get("StartTime").toString() : "");
				c.setEndTime(result.get("EndTime") != null ? result.get("EndTime").toString() : "");
				c.setVideo_url(result.get("Video_url") != null ? result.get("Video_url").toString() : "");
//				c.setVideo_desc(result.get("Video_desc") != null ? result.get("Video_desc").toString() : "");
//				c.setVideoId(result.get("VideoId") != null ? result.get("VideoId").toString() : "");
//				c.setCameraName(result.get("CameraName") != null ? result.get("CameraName").toString() : "");
				cList.add(c);
			}
		}
		return cList;
	}
}
