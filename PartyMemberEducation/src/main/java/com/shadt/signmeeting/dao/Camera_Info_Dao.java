package com.shadt.signmeeting.dao;

import java.util.List;

import com.shadt.core.dao.BaseDao;
import com.shadt.signmeeting.model.Camera_Info;

/**
 * 机顶盒对应的摄像头信息数据访问层接口
 * @author SongR
 *
 */
public interface Camera_Info_Dao extends BaseDao<Camera_Info>{

	/**
	 * 根据机顶盒编号获取机顶盒对应的摄像头信息
	 * @param stbNo
	 * @return
	 */
	List<Camera_Info> getCameraInfoByStbNo(String stbNo);

	/**
	 * 根据摄像头编号获取摄像头信息
	 * @param cameraNo
	 * @return
	 */
	Camera_Info getCameraInfoByCameraNo(String cameraNo);
	
	/**
	 * 根据村组织编号获取对应的会议室摄像头路径
	 * @param orgId
	 * @return
	 */
	Camera_Info getLiveUrl(String orgId);

}
