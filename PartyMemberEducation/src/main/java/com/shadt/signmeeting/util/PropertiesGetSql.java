package com.shadt.signmeeting.util;

import java.util.ResourceBundle;

/**
 * 获取sql
 * @author SongR
 *
 */
public class PropertiesGetSql {
	private static final ResourceBundle bundle = ResourceBundle
			.getBundle("signmeetingSql");

	/**
	 * 通过键获取值
	 * 
	 * @param key
	 * @return
	 */
	public static final String get(String key) {
		return bundle.getString(key);
	}
}
