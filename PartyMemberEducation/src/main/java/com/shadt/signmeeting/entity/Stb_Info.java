package com.shadt.signmeeting.entity;

import java.io.Serializable;
/**
 * 机顶盒信息
 * @author SongR
 *
 */
@SuppressWarnings("serial")
public class Stb_Info implements Serializable {
	private String stbNo;//机顶盒编号
	private String villageId;//村组织编号
	private String villageKeyValue;
	
	public String getVillageKeyValue() {
		return villageKeyValue;
	}
	public void setVillageKeyValue(String villageKeyValue) {
		this.villageKeyValue = villageKeyValue;
	}
	public String getStbNo() {
		return stbNo;
	}
	public void setStbNo(String stbNo) {
		this.stbNo = stbNo;
	}
	public String getVillageId() {
		return villageId;
	}
	public void setVillageId(String villageId) {
		this.villageId = villageId;
	}
	
}
