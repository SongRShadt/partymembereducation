package com.shadt.signmeeting.entity;

import java.io.Serializable;

/**
 * 机顶盒摄像头关系表
 * @author SongR
 *
 */
@SuppressWarnings("serial")
public class Stb_Camera_rlt implements Serializable{
	private String stbNo;//机顶盒编号
	private String cameraNo;//摄像头编号
	public String getStbNo() {
		return stbNo;
	}
	public void setStbNo(String stbNo) {
		this.stbNo = stbNo;
	}
	public String getCameraNo() {
		return cameraNo;
	}
	public void setCameraNo(String cameraNo) {
		this.cameraNo = cameraNo;
	}
	
}
