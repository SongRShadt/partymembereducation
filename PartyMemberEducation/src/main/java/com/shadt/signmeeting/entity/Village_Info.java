package com.shadt.signmeeting.entity;

import java.io.Serializable;

/**
 * 村组织信息
 * @author SongR
 *
 */
@SuppressWarnings("serial")
public class Village_Info implements Serializable{
	private String villageId;//编号
	private String villageName;//村组织名称
	private String villageKeyValue;//村对应到智慧城市栏目的key值
	private String townId;//村所属镇编号
	private String orgCode;//村组织编号
	public String getVillageId() {
		return villageId;
	}
	public void setVillageId(String villageId) {
		this.villageId = villageId;
	}
	public String getVillageName() {
		return villageName;
	}
	public void setVillageName(String villageName) {
		this.villageName = villageName;
	}
	public String getVillageKeyValue() {
		return villageKeyValue;
	}
	public void setVillageKeyValue(String villageKeyValue) {
		this.villageKeyValue = villageKeyValue;
	}
	public String getTownId() {
		return townId;
	}
	public void setTownId(String townId) {
		this.townId = townId;
	}
	public String getOrgCode() {
		return orgCode;
	}
	public void setOrgCode(String orgCode) {
		this.orgCode = orgCode;
	}
	
	
}
