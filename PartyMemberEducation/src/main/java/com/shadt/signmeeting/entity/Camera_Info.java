package com.shadt.signmeeting.entity;

import java.io.Serializable;
/**
 * 
 * @author SongR
 *
 */
@SuppressWarnings("serial")
public class Camera_Info implements Serializable{
	private String cameraNo;//摄像头编号
	private String cameraName;//摄像头名称
	private String cameraUrl;//摄像头播放地址
	private String cameraType;//摄像头类型:0-会议室摄像头;1-监控摄像头;
	public String getCameraNo() {
		return cameraNo;
	}
	public void setCameraNo(String cameraNo) {
		this.cameraNo = cameraNo;
	}
	public String getCameraName() {
		return cameraName;
	}
	public void setCameraName(String cameraName) {
		this.cameraName = cameraName;
	}
	public String getCameraUrl() {
		return cameraUrl;
	}
	public void setCameraUrl(String cameraUrl) {
		this.cameraUrl = cameraUrl;
	}
	public String getCameraType() {
		return cameraType;
	}
	public void setCameraType(String cameraType) {
		this.cameraType = cameraType;
	}
}
