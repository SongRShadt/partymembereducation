package com.shadt.signmeeting.entity;

import java.io.Serializable;

/**
 * 镇信息
 * @author SongR
 *
 */
@SuppressWarnings("serial")
public class Town_Info implements Serializable{
	private String townId;//镇编号
	private String townName;//镇名称
	private String townKeyValue;//镇对应到智慧城市栏目的key值
	public String getTownId() {
		return townId;
	}
	public void setTownId(String townId) {
		this.townId = townId;
	}
	public String getTownName() {
		return townName;
	}
	public void setTownName(String townName) {
		this.townName = townName;
	}
	public String getTownKeyValue() {
		return townKeyValue;
	}
	public void setTownKeyValue(String townKeyValue) {
		this.townKeyValue = townKeyValue;
	}
	
	
}
