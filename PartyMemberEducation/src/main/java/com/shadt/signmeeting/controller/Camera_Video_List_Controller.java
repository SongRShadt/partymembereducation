package com.shadt.signmeeting.controller;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.shadt.core.controller.BaseController;
import com.shadt.signmeeting.service.Camera_Video_List_Service;

/**
 * 回看控制器
 * 
 * @author SongR
 * 
 */
@Controller
@RequestMapping("/cameraVideoListController")
public class Camera_Video_List_Controller extends BaseController {

	@Autowired
	Camera_Video_List_Service service;

	/**
	 * 根据摄像头编号获取摄像头对应的录制视频列表
	 * 
	 * @param req
	 * @param session
	 * @param cameraNo
	 *            摄像头编号
	 * @return
	 */
	@ResponseBody
	@RequestMapping("/getByCameraNo")
	public String getByCameraNo(HttpServletRequest req, HttpSession session,
			String cameraNo) {
		return service.getByCameraNo(cameraNo);
	}
}
