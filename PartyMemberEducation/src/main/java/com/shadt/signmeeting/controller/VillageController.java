package com.shadt.signmeeting.controller;

import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.shadt.core.controller.BaseController;
import com.shadt.core.model.Json;
import com.shadt.signmeeting.entity.Village_Info;
import com.shadt.signmeeting.service.VillageService;

/**
 * 村信息控制器
 * 
 * @author liusongren
 *
 */
@Controller
@RequestMapping("/villageController")
public class VillageController extends BaseController {
	@Autowired
	VillageService service;

	/**
	 * 根据orgcode获取villagekeyvalue
	 * 
	 * @return
	 */
	@ResponseBody
	@RequestMapping("/getKeyByOrgCode")
	public String getKeyByOrgCode(HttpServletRequest req, String orgCode) {
		String keyValue = service.getKeyByOrgCode(orgCode);
		return keyValue;
	}

	/**
	 * 跟据镇编号获取村列表
	 * 
	 * @param townId
	 * @return
	 */
	@ResponseBody
	@RequestMapping("/getVillageByTownId")
	public Json getVillageByTownId(String townId) {
		Json j = new Json();
		List<Village_Info> vList = service.getVillageByTownId(townId);
		if(vList.size()>0){
			j.setObj(vList);
			j.setMsg("获取成功！");
			j.setSuccess(true);
		}
		return j;
	}

}
