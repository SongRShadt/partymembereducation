package com.shadt.signmeeting.controller;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.shadt.core.controller.BaseController;
import com.shadt.signmeeting.service.MeetingRoomService;

/**
 * 会议室摄像头控制器
 * @author SongR
 *
 */
@Controller
@RequestMapping("/meetingRoomController")
public class MeetingRoomController extends BaseController {
	@Autowired
	MeetingRoomService service;
	
	/**
	 * 获取镇对应的摄像头
	 * @return
	 */
	@ResponseBody
	@RequestMapping("/getMeetingRoomByTown")
	public String getMeetingRoomByTown(HttpServletRequest request,HttpSession session,String stbNo,String townId,String cType){
		return service.getMeetingRoomByTown(stbNo,townId,cType);
	}

}
