package com.shadt.signmeeting.controller;

import java.util.List;
import java.util.ResourceBundle;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.shadt.core.controller.BaseController;
import com.shadt.core.model.Json;
import com.shadt.signmeeting.entity.PartyChannel;
import com.shadt.signmeeting.service.PartyChannelService;

@Controller
@RequestMapping(value="/partychannel")
public class PartyChannelController extends BaseController{
	Logger log = Logger.getLogger(this.getClass());
	private static final ResourceBundle bundle = java.util.ResourceBundle.getBundle("config");
	@Autowired
	PartyChannelService service;
	
	/**
	 * 获取频道路径
	 * @return
	 */
	@ResponseBody
	@RequestMapping(value="/channelurl")
	public Json getChannelUrl(){
		Json j = new Json();
		try {
			List<PartyChannel> channels = service.getChannelUrl(bundle.getString("partychannelsize"));
			j.setMsg("获取党建频道成功！");
			j.setSuccess(true);
			j.setObj(channels);
		} catch (Exception e) {
			j.setMsg("获取党建频道出错！");
			log.error("获取党建频道url出错："+e);
		}finally{
			
		}
		return j;
	}
	
}
