package com.shadt.signmeeting.controller;

import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.shadt.core.controller.BaseController;
import com.shadt.core.model.Json;
import com.shadt.signmeeting.model.Camera_Info;
import com.shadt.signmeeting.service.Camera_Info_Service;
import com.shadt.signmeeting.util.PropertiesGetSql;

/**
 * 机顶盒对应的摄像头
 * @author SongR
 */
@Controller
@RequestMapping("/cameraInfoController")
public class Camera_Info_Controller extends BaseController{
	Logger l = Logger.getLogger(this.getClass());
	@Autowired
	Camera_Info_Service service;
	
	/**
	 * 根据机顶盒编号获取机顶盒对应的摄像头信息
	 * @param req
	 * @param session
	 * @param stbNo 机顶盒编号
	 * @return
	 */
	@ResponseBody
	@RequestMapping("/getCameraInfoByStbNo")
	public String getCameraInfoByStbNo(HttpServletRequest req,HttpSession session,String stbNo){
		return service.getCameraInfoByStbNo(stbNo);
	}
	
	/**
	 * 根据摄像头编号获取摄像头信息
	 * @param req
	 * @param session
	 * @param CameraNo
	 * @return
	 */
	@ResponseBody
	@RequestMapping("/getCameraInfoByCameraNo")
	public String getCameraInfoByCameraNo(HttpServletRequest req,HttpSession session,String cameraNo){
		return  service.getCameraInfoByCameraNo(cameraNo);
	}

	
	/**
	 * 获取视频会议直播摄像头路径
	 * @return
	 */
	@ResponseBody
	@RequestMapping("/getLiveUrl")
	public Json getLiveUrl(HttpServletRequest req,HttpSession session,String orgId){
		Json j = new Json();
		Camera_Info c = service.getLiveUrl(orgId);
		j.setObj(c);
		String ss[] = c.getCameraUrl().split("/");
		String httpUrl= "http://"+ss[2]+"/record?do=start&streamid="+c.getStreamId()+"&callback="+PropertiesGetSql.get("callbackurl")+"?cameraNo="+c.getCameraNo();
		BufferedReader reader = null;
		String result = null;
		StringBuffer sbf = new StringBuffer();
		try {
			URL url = new URL(httpUrl);
			HttpURLConnection connection = (HttpURLConnection) url.openConnection();
			connection.setRequestMethod("GET");
			connection.connect();
			InputStream is = connection.getInputStream();
			reader = new BufferedReader(new InputStreamReader(is, "UTF-8"));
			String strRead = null;
			while ((strRead = reader.readLine()) != null) {
				sbf.append(strRead);
				sbf.append("\r\n");
			}
			reader.close();
			result = sbf.toString();
			j.setMsg(result);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return j;
	} 
	
	
	/**
	 * 根据机顶盒编号类型获取摄像头
	 * @return
	 */
	@ResponseBody
	@RequestMapping(value="/getByType/{stbNo}/{type}",method={RequestMethod.GET})
	public Json getByType(@PathVariable String stbNo, @PathVariable String type){
		Json j = new Json();
		
		Camera_Info c = service.getByType(stbNo,type);
		
		
		return j;
	}
	
}
