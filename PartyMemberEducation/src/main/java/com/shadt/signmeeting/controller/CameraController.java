package com.shadt.signmeeting.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.shadt.core.controller.BaseController;
import com.shadt.signmeeting.dao.Camera_Info_Dao;
import com.shadt.signmeeting.model.Camera_Info;

@Controller
@RequestMapping(value="/cameracontroller")
public class CameraController extends BaseController{

	@Autowired
	Camera_Info_Dao dao;
	
	/**
	 * 获取摄像头ip
	 * @return
	 */
	@ResponseBody
	@RequestMapping(value="/getcameraip")
	public String getCameraIp(String cameraNo){
		String ip = "";
		try {
			Camera_Info camera = dao.getCameraInfoByCameraNo(cameraNo);
			if(null!=camera){
			 	String ss[] = camera.getCameraUrl().split("/");
			 	ip = ss[2];
			}
		} catch (Exception e) {
		}
		return ip;
	}
	
	
}


