package com.shadt.signmeeting.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.shadt.core.controller.BaseController;
import com.shadt.core.model.Json;
import com.shadt.signmeeting.entity.Village_Info;
import com.shadt.signmeeting.service.StbInfoService;

/**
 * 机顶盒控制器
 * @author liusongren
 *
 */
@Controller
@RequestMapping("/stbController")
public class StbController extends BaseController{

	@Autowired
	StbInfoService service;
	
	/**
	 * 根据orgid获取stbno
	 * @return
	 */
	@ResponseBody
	@RequestMapping("/getStbNoByOrgId")
	public String getStbNoByOrgId(String orgId){
		String s = service.getStbNoByOrgId(orgId);
		return s;
	}
	
	/**
	 * 根据机顶盒编号获取机顶盒信息
	 * @param stbNo
	 * @return
	 */
	@ResponseBody
	@RequestMapping("/getStbByStbNo")
	public Json getStbByStbNo(String stbNo){
		Json j = new Json();
		Village_Info vill = service.getStbByStbNo(stbNo);
		if(null!=vill){
			j.setMsg("获取成功！");
			j.setObj(vill);
			j.setSuccess(true);
		}
		return j;
	}
	
}
