package com.shadt.signmeeting.controller;

import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.alibaba.fastjson.JSONArray;
import com.shadt.core.controller.BaseController;
import com.shadt.core.model.Json;
import com.shadt.signmeeting.model.Video;
import com.shadt.signmeeting.service.VideoService;

/**
 * 视频录制控制器
 * 
 * @author liusongren
 *
 */
@Controller
@RequestMapping("/videoController")
public class VideoController extends BaseController {
	Logger log = Logger.getLogger(this.getClass());
	@Autowired
	VideoService service;

	/**
	 * 获取视频录制回调参数
	 * 
	 * @param request
	 * @param taskid
	 *            任务编号
	 * @param download_url
	 *            下载地址（mp4）
	 * @param play_url
	 *            播放地址（m3u8）
	 * @param duration
	 *            录制时长
	 * @return
	 */
	@ResponseBody
	@RequestMapping("/videoCallBack")
	public Json videoCallBack(HttpServletRequest request, HttpSession session, String taskid, String download_url,
			String play_url, Integer duration,String cameraNo) {
		Json j = new Json();
		if(duration>20){
			Video v = new Video();
			v.setCameraNo(cameraNo);
			v.setTaskId(taskid);
			v.setDownloadUrl(download_url);
			v.setPlayUrl(play_url);
			v.setDuration(duration);
			service.saveVideo(v);	
		}
		return j;
	}

	/**
	 * 根据摄像头编号获取录制视频列表
	 * 
	 * @return
	 */
	@ResponseBody
	@RequestMapping("/getAllVideoByCameraNo")
	public Json getAllVideoByCameraNo(HttpServletRequest request, HttpServletResponse response, HttpSession session,
			String cameraNo) {
		System.out.println(111);
		Json j = new Json();
		try {
			List<Video> vList = service.getAllVideoByCameraNo(cameraNo);
			j.setObj(vList);
			j.setSuccess(true);
		} catch (Exception e) {
			e.printStackTrace();
			log.error("接口异常",e);
		}
		return j;
	}
	
	/**
	 * 停止录制
	 * @return
	 */
	@ResponseBody
	@RequestMapping("/stop")
	public Json stop(HttpServletRequest request,HttpServletResponse response,HttpSession session,String taskid){
		Json j = new Json();
		String httpUrl = "http://172.29.0.113:5080/record?do=stop&taskid="+taskid;
		BufferedReader reader = null;
		String result = null;
		StringBuffer sbf = new StringBuffer();
		try {
			URL url = new URL(httpUrl);
			HttpURLConnection connection = (HttpURLConnection) url.openConnection();
			connection.setRequestMethod("POST");
			connection.connect();
			InputStream is = connection.getInputStream();
			reader = new BufferedReader(new InputStreamReader(is, "UTF-8"));
			String strRead = null;
			while ((strRead = reader.readLine()) != null) {
				sbf.append(strRead);
				sbf.append("\r\n");
			}
			reader.close();
			result = sbf.toString();
			j.setMsg(result);
		} catch (Exception e) {
			e.printStackTrace();
			log.error("接口异常",e);
		}
		return j;
	}
	
}
