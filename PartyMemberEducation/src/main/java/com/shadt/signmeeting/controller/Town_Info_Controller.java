package com.shadt.signmeeting.controller;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.shadt.core.controller.BaseController;
import com.shadt.signmeeting.service.Town_Info_Service;

/**
 * 镇信息控制器
 * @author SongR
 *
 */
@Controller
@RequestMapping("/townInfoController")
public class Town_Info_Controller extends BaseController{

	@Autowired
	Town_Info_Service service;
	
	@ResponseBody
	@RequestMapping("/gettownbystbno")
	public String getTownByStbNo(String stbNo){
		return service.getTownByStbNo(stbNo);
	}
	
	
	/**
	 * 获取所有镇信息
	 * @return
	 */
	@ResponseBody
	@RequestMapping("/getAllTown")
	public String getAllTown(HttpServletRequest req,HttpSession session){
		return service.getAllTown();
	}
	
}
