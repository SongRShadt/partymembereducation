package com.shadt.signmeeting.controller;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.shadt.core.controller.BaseController;
import com.shadt.core.model.Json;
import com.shadt.signmeeting.model.Town_Village_Info;
import com.shadt.signmeeting.service.Town_Village_Service;
/**
 * 机顶盒对应信息控制器
 * @author SongR
 *
 */
@Controller
@RequestMapping("/townVillageController")
public class Town_Village_Controller extends BaseController {
	@Autowired
	Town_Village_Service service;

	/**
	 * 根据机顶盒查询乡镇名称,村key值,村名称,村key值,组织编号
	 * @param req
	 * @param session
	 * @param stbNo
	 * @return
	 */
	@ResponseBody
	@RequestMapping("/getByStbNo")
	public Json getByStbNo(HttpServletRequest req, HttpSession session,
			String stbNo) {
		Json j = new Json();
		Town_Village_Info tvi = null;
		if (null != stbNo && !"".equals(stbNo)) {
			tvi = service.getByStbNo(stbNo);
			j.setSuccess(true);
			if (null == tvi) {
				j.setMsg("未查询到相关数据！请检查机顶盒编号是否正确！");
				j.setObj("");
			} else {
				j.setMsg("获取成功！");
				j.setObj(tvi);
			}
		} else {
			j.setSuccess(false);
			j.setMsg("获取失败！机顶盒编号为空！");
			j.setObj("");
		}
		return j;
	}
}
