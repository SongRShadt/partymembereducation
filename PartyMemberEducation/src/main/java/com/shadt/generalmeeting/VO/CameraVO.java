package com.shadt.generalmeeting.VO;

public class CameraVO {
	private String id;//编号
	private String cameraName;//摄像头名称
	private String cameraUrl;//摄像头播放地址
	private Integer cameraType;//摄像头类型
	private String imgUrl;//摄像头封面图路径
	private String streamId;//视频流编号
	private String villageId;//所属村
	private Long sort;
	
	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
	public String getCameraName() {
		return cameraName;
	}
	public void setCameraName(String cameraName) {
		this.cameraName = cameraName;
	}
	public String getCameraUrl() {
		return cameraUrl;
	}
	public void setCameraUrl(String cameraUrl) {
		this.cameraUrl = cameraUrl;
	}
	public Integer getCameraType() {
		return cameraType;
	}
	public void setCameraType(Integer cameraType) {
		this.cameraType = cameraType;
	}
	public String getImgUrl() {
		return imgUrl;
	}
	public void setImgUrl(String imgUrl) {
		this.imgUrl = imgUrl;
	}
	public String getStreamId() {
		return streamId;
	}
	public void setStreamId(String streamId) {
		this.streamId = streamId;
	}
	public String getVillageId() {
		return villageId;
	}
	public void setVillageId(String villageId) {
		this.villageId = villageId;
	}
	public Long getSort() {
		return sort;
	}
	public void setSort(Long sort) {
		this.sort = sort;
	}
	
	@Override
	public String toString() {
		return "CameraVO [id=" + id + ", cameraName=" + cameraName + ", cameraUrl=" + cameraUrl + ", cameraType="
				+ cameraType + ", imgUrl=" + imgUrl + ", streamId=" + streamId + ", villageId=" + villageId + ", sort="
				+ sort + "]";
	}
	
	
}
