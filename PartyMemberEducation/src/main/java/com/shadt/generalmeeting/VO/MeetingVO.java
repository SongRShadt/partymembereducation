package com.shadt.generalmeeting.VO;

public class MeetingVO {
	
	private String meetingName;		//会议名称
	private String orgcode;			//村编号
	private String meetingDate;		//会议开始时间
	private String meetingEndDate;		//会议结束时间
	private String meetingCameraType;		//会议的录制摄像头
	private Integer meetingTime;		//会议时长
	private String meetingPlayUrl;		//播放地址
	private String meetingDownloadUrl;		//下载地址
	private String typeId;				//会议类型
	
	public String getMeetingName() {
		return meetingName;
	}
	public void setMeetingName(String meetingName) {
		this.meetingName = meetingName;
	}
	public String getMeetingDate() {
		return meetingDate;
	}
	public void setMeetingDate(String meetingDate) {
		this.meetingDate = meetingDate;
	}
	public String getMeetingCameraType() {
		return meetingCameraType;
	}
	public void setMeetingCameraType(String meetingCameraType) {
		this.meetingCameraType = meetingCameraType;
	}
	public Integer getMeetingTime() {
		return meetingTime;
	}
	public void setMeetingTime(Integer meetingTime) {
		this.meetingTime = meetingTime;
	}
	public String getMeetingPlayUrl() {
		return meetingPlayUrl;
	}
	public void setMeetingPlayUrl(String meetingPlayUrl) {
		this.meetingPlayUrl = meetingPlayUrl;
	}
	public String getMeetingDownloadUrl() {
		return meetingDownloadUrl;
	}
	public void setMeetingDownloadUrl(String meetingDownloadUrl) {
		this.meetingDownloadUrl = meetingDownloadUrl;
	}
	public String getOrgcode() {
		return orgcode;
	}
	public void setOrgcode(String orgcode) {
		this.orgcode = orgcode;
	}
	public String getMeetingEndDate() {
		return meetingEndDate;
	}
	public void setMeetingEndDate(String meetingEndDate) {
		this.meetingEndDate = meetingEndDate;
	}
	public String getTypeId() {
		return typeId;
	}
	public void setTypeId(String typeId) {
		this.typeId = typeId;
	}
	
	
	@Override
	public String toString() {
		return "MeetingVO [meetingName=" + meetingName + ", orgcode=" + orgcode + ", meetingDate=" + meetingDate
				+ ", meetingEndDate=" + meetingEndDate + ", meetingCameraType=" + meetingCameraType + ", meetingTime="
				+ meetingTime + ", meetingPlayUrl=" + meetingPlayUrl + ", meetingDownloadUrl=" + meetingDownloadUrl
				+ ", typeId=" + typeId + "]";
	}
	
}
