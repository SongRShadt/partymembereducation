package com.shadt.generalmeeting.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

/**
 * 换届视频会议类型
 * @author SongR
 *
 */
@Entity
@Table(name = "general_meeting_type")
public class GeneralMeetingType {
	
	@Id
	@Column(name = "meeting_type_id")
	private Integer id;				//会议类型编号
	@Column(name = "meeting_type_name")
	private String name;			//会议类型名称
	@Column(name = "meeting_type_sort")
	private Integer sort;	//会议类型序号
	
	public Integer getId() {
		return id;
	}
	public void setId(Integer id) {
		this.id = id;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public Integer getSort() {
		return sort;
	}
	public void setSort(Integer sort) {
		this.sort = sort;
	}
	
	@Override
	public String toString() {
		return "GeneralMettingType [id=" + id + ", name=" + name + ", sort=" + sort + "]";
	}
	
}
