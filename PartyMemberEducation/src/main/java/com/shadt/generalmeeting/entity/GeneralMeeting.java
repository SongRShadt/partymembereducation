package com.shadt.generalmeeting.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

/**
 * 换届视频会议
 * @author SongR
 *
 */
@Entity
@Table(name = "general_meeting")
public class GeneralMeeting {
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	@Column(name = "meeting_id")
	private Long id;		//会议编号
	@Column(name = "meeting_name")
	private String name;	//会议名称
	@Column(name = "meeting_type")
	private String meetingType;//会议类型
	@Column(name = "taskid")
	private String taskid;	//会议录制taskid
	@Column(name = "camera_type")
	private String cameraType;//会议录制摄像头类型
	@Column(name = "meeting_status")
	private Integer status;//会议状态（状态 -1、已删除  0、录制 1、会议结束）
	@Column(name = "orgcode")
	private String orgcode;//村编号
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getMeetingType() {
		return meetingType;
	}
	public void setMeetingType(String meetingType) {
		this.meetingType = meetingType;
	}
	public String getTaskid() {
		return taskid;
	}
	public void setTaskid(String taskid) {
		this.taskid = taskid;
	}
	public String getCameraType() {
		return cameraType;
	}
	public void setCameraType(String cameraType) {
		this.cameraType = cameraType;
	}
	public Integer getStatus() {
		return status;
	}
	public void setStatus(Integer status) {
		this.status = status;
	}
	public String getOrgcode() {
		return orgcode;
	}
	public void setOrgcode(String orgcode) {
		this.orgcode = orgcode;
	}
	
	@Override
	public String toString() {
		return "GeneralMeeting [id=" + id + ", name=" + name + ", meetingType=" + meetingType + ", taskid=" + taskid
				+ ", cameraType=" + cameraType + ", status=" + status + ", orgcode=" + orgcode + "]";
	}
	
}
