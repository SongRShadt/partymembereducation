package com.shadt.generalmeeting.dao;

import com.shadt.generalmeeting.VO.TownVillageVO;

public interface GeneralTownVillageDao{
	
	TownVillageVO getByStbNo(String stbNo);
}
