package com.shadt.generalmeeting.dao.impl;

import java.util.List;
import java.util.Map;

import org.springframework.stereotype.Repository;

import com.shadt.core.dao.impl.BaseDaoImpl;
import com.shadt.generalmeeting.VO.CameraVO;
import com.shadt.generalmeeting.dao.GeneralCameraDao;

/**
 * 视频录制dao
 * 
 * @author liusongren
 *
 */
@Repository
public class GeneralCameraDaoImpl extends BaseDaoImpl<CameraVO> implements  GeneralCameraDao {

	@Override
	public CameraVO getByType(String villageId, Integer typeId) {
		List<Map<String, Object>> list = this.getJdbcTemplate().queryForList("select * from camera_info where villageid = ? and cameratype = ?",new Object[]{ villageId, typeId });
		CameraVO vo = new CameraVO();
		for (Map<String, Object> result : list) {
			vo.setCameraName(result.get("cameraname") != null ? result.get("cameraname").toString() : "");
			vo.setCameraType(typeId);
			vo.setCameraUrl(result.get("cameraurl") != null ? result.get("cameraurl").toString() : "");
			vo.setId(result.get("camerano") != null ? result.get("camerano").toString() : "");
			vo.setStreamId(result.get("streamid") != null ? result.get("streamid").toString() : "");
			vo.setVillageId(villageId);
		}
		return vo;
	}

}
