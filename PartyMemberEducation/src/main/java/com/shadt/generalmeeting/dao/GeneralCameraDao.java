package com.shadt.generalmeeting.dao;

import com.shadt.generalmeeting.VO.CameraVO;

/**
 * 视频录制信息
 * @author liusongren
 *
 */
public interface GeneralCameraDao {
	
	/**
	 * 获取村摄像头
	 * @param villageId 村编号
	 * @param type 摄像头类型
	 * @return
	 */
	CameraVO getByType(String villageId,Integer typeId);
}
