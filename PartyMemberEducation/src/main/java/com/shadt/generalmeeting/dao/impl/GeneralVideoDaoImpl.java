package com.shadt.generalmeeting.dao.impl;

import org.springframework.stereotype.Repository;

import com.shadt.core.dao.impl.BaseDaoImpl;
import com.shadt.generalmeeting.dao.GeneralVideoDao;
import com.shadt.generalmeeting.entity.GeneralMeetingVideo;

/**
 * 视频录制dao
 * 
 * @author liusongren
 *
 */
@Repository
public class GeneralVideoDaoImpl extends BaseDaoImpl<GeneralMeetingVideo> implements GeneralVideoDao {

	/**
	 * 实现保存视频录制
	 */
	public void saveVideo(GeneralMeetingVideo v) {
		this.getJdbcTemplate().update(
				"insert into general_meeting_video(id,starttime,endtime,camerano,taskid,download_url,play_url,duration,video_status) values(?,?,?,?,?,?,?,?,?)",
				new Object[] { v.getId(), v.getStartTime(), v.getEndTime(), v.getCameraNo(), v.getTaskId(),
						v.getDownloadUrl(), v.getPlayUrl(), v.getDuration(),0 });
	}

}
