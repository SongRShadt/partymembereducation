package com.shadt.generalmeeting.dao.impl;

import org.springframework.stereotype.Repository;

import com.shadt.core.dao.impl.BaseDaoImpl;
import com.shadt.generalmeeting.dao.GeneralMeetingTypeDao;
import com.shadt.generalmeeting.entity.GeneralMeetingType;

@Repository
public class GeneralMeetingTypeDaoImpl extends BaseDaoImpl<GeneralMeetingType> implements GeneralMeetingTypeDao {

}
