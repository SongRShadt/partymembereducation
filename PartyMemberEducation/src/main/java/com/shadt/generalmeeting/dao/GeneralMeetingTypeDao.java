package com.shadt.generalmeeting.dao;

import com.shadt.core.dao.BaseDao;
import com.shadt.generalmeeting.entity.GeneralMeetingType;

public interface GeneralMeetingTypeDao extends BaseDao<GeneralMeetingType>{
	
}
