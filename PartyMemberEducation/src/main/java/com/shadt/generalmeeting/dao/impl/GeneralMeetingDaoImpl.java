package com.shadt.generalmeeting.dao.impl;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import org.springframework.stereotype.Repository;

import com.shadt.core.dao.impl.BaseDaoImpl;
import com.shadt.generalmeeting.VO.MeetingVO;
import com.shadt.generalmeeting.dao.GeneralMeetingDao;
import com.shadt.generalmeeting.entity.GeneralMeeting;

@Repository
public class GeneralMeetingDaoImpl extends BaseDaoImpl<GeneralMeeting> implements GeneralMeetingDao {

	@Override
	public List<MeetingVO> getAll(String orgcode,String typeId,String meetingDate) {
		String sql = "";
		Object [] parms = null;
		if( orgcode.equals("")&&typeId.equals("") ){
			sql = "select m.meeting_name,m.camera_type,m.orgcode,m.meeting_type,v.starttime,v.endtime,v.duration,v.play_url,v.download_url from general_meeting m,general_meeting_video v where m.taskid = v.taskid and m.meeting_status = 1 ";
			
		}
		if( !orgcode.equals("") ){
			sql = "select m.meeting_name,m.camera_type,m.orgcode,m.meeting_type,v.starttime,v.endtime,v.duration,v.play_url,v.download_url from general_meeting m,general_meeting_video v where m.taskid = v.taskid and m.meeting_status = 1 and m.orgcode = ? ";
			parms = new Object[]{ orgcode };
		}
		if( !typeId.equals("") ){
			sql = "select m.meeting_name,m.camera_type,m.orgcode,m.meeting_type,v.starttime,v.endtime,v.duration,v.play_url,v.download_url from general_meeting m,general_meeting_video v where m.taskid = v.taskid and m.meeting_status = 1 and m.meeting_type like ? ";
			parms = new Object[]{ "%"+typeId+"%" };
		}
		if( !orgcode.equals("") && !typeId.equals("") ){
			sql = "select m.meeting_name,m.camera_type,m.orgcode,m.meeting_type,v.starttime,v.endtime,v.duration,v.play_url,v.download_url from general_meeting m,general_meeting_video v where m.taskid = v.taskid and m.meeting_status = 1 and m.orgcode = ? and m.meeting_type like ? ";
			parms = new Object[]{ orgcode, "%"+typeId+"%" };
		}
		if( !meetingDate.equals("") )
		{
			sql +="and DATE(v.starttime) = ? ";
			if(parms == null){
				parms = new Object[]{ meetingDate };
			}else{
				if(!orgcode.equals("")){
					parms = new Object[]{ orgcode, meetingDate };
				}
				if(!typeId.equals("")){
					parms = new Object[]{ "%"+typeId+"%", meetingDate };
				}
				if(!orgcode.equals("")&&!typeId.equals("")){
					parms = new Object[]{ orgcode, "%"+typeId+"%", meetingDate };
				}
			}
		}
		sql +="order by v.starttime desc";
		
		List<Map<String, Object>> list = this.getJdbcTemplate().queryForList(sql,parms);
		
		List<MeetingVO> voList = new ArrayList<MeetingVO>();
		if(list.size()>0){
			for (Map<String, Object> result : list) {
				MeetingVO vo = new MeetingVO();
				//System.out.println(vo.toString());
				vo.setMeetingName(result.get("meeting_name") != null ? result.get("meeting_name").toString() : "");
				vo.setMeetingCameraType(result.get("camera_type") != null ? result.get("camera_type").toString() : "");
				vo.setTypeId(result.get("meeting_type") != null ? result.get("meeting_type").toString() : "");
				vo.setOrgcode(result.get("orgcode") != null ? result.get("orgcode").toString() : "");
				vo.setMeetingDate(result.get("starttime").toString().replace(".0", ""));
				vo.setMeetingEndDate(result.get("endtime").toString().replace(".0", ""));
				vo.setMeetingTime(Integer.parseInt(result.get("duration").toString()));
				vo.setMeetingPlayUrl(result.get("play_url") != null ? result.get("play_url").toString() : "");
				vo.setMeetingDownloadUrl(result.get("download_url") != null ? result.get("download_url").toString() : "");
				voList.add(vo);
			}
		}
		return voList;
	}

}
