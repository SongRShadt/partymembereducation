package com.shadt.generalmeeting.dao;

import com.shadt.generalmeeting.entity.GeneralMeetingVideo;

/**
 * 视频录制信息
 * @author liusongren
 *
 */
public interface GeneralVideoDao {
	
	/**
	 * 保存录制视频
	 * @param v
	 */
	void saveVideo(GeneralMeetingVideo v);
	
}
