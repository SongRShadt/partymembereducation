package com.shadt.generalmeeting.dao;

import java.util.List;

import com.shadt.core.dao.BaseDao;
import com.shadt.generalmeeting.VO.MeetingVO;
import com.shadt.generalmeeting.entity.GeneralMeeting;

public interface GeneralMeetingDao extends BaseDao<GeneralMeeting>{
	
	/**
	 * 获取所有换届录制会议
	 * @return
	 */
	List<MeetingVO> getAll(String orgcode,String typeId,String meetingDate);
}
