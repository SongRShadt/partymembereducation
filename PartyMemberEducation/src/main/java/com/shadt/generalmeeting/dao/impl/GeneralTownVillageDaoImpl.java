package com.shadt.generalmeeting.dao.impl;

import java.util.List;
import java.util.Map;

import org.springframework.stereotype.Repository;

import com.shadt.core.dao.impl.BaseDaoImpl;
import com.shadt.generalmeeting.VO.TownVillageVO;
import com.shadt.generalmeeting.dao.GeneralTownVillageDao;

@Repository
public class GeneralTownVillageDaoImpl extends BaseDaoImpl<TownVillageVO> implements GeneralTownVillageDao  {

	@Override
	public TownVillageVO getByStbNo(String stbNo) {
		List<Map<String, Object>> list = this.getJdbcTemplate().queryForList("select b.townname,b.townkeyvalue,c.villageid,c.villagename,c.villagekeyvalue,c.orgcode from stb_info a,town_info b,village_info c  where a.villageid = c.villageid and c.townid = b.townid and a.stbno = ?",new Object[]{stbNo});
		TownVillageVO vo = new TownVillageVO();
		for (Map<String, Object> result : list) {
			vo.setOrgCode(result.get("orgcode") != null ? result.get("orgcode").toString() : "");
			vo.setTownKeyValue(result.get("townkeyvalue") != null ? result.get("townkeyvalue").toString() : "");
			vo.setTownName(result.get("townname") != null ? result.get("townname").toString() : "");
			vo.setVillageId(result.get("villageid") != null ? result.get("villageid").toString() : "");
			vo.setVillageKeyValue(result.get("villagekeyvalue") != null ? result.get("villagekeyvalue").toString() : "");
			vo.setVillageName(result.get("villagename") != null ? result.get("villagename").toString() : "");
		}
		return vo;
	}


}
