package com.shadt.generalmeeting.service.impl;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.UUID;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.shadt.generalmeeting.dao.GeneralVideoDao;
import com.shadt.generalmeeting.entity.GeneralMeetingVideo;
import com.shadt.generalmeeting.service.GeneralCameraVideoService;
import com.shadt.signmeeting.dao.Camera_Info_Dao;
import com.shadt.signmeeting.model.Camera_Info;

/**
 * 录制视频业务层实现类
 * @author liusongren
 *
 */
@Service
public class GeneralCameraVideoServiceImpl implements GeneralCameraVideoService{
	@Autowired
	GeneralVideoDao dao;
	
	@Autowired
	Camera_Info_Dao cameradao;
	
	
	/**
	 * 实现保存录制的视频信息
	 */
	public void saveVideo(GeneralMeetingVideo v) {
		v.setId(UUID.randomUUID().toString());
		Date date = new Date();
		Long end = date.getTime(); //获取当前时间毫秒
		v.setEndTime(date);
		Long star = end - v.getDuration()*1000;		//计算开始时间 -毫秒
		SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd HH-mm-ss");
		Calendar cal = Calendar.getInstance();
		cal.setTimeInMillis(star);
		DateFormat format = new SimpleDateFormat("yyyy-MM-dd HH-mm-ss"); 
		String time = dateFormat.format(cal.getTime()); //毫秒转化为字符串时间
		try {
			Date d = format.parse(time);		//字符串转化为date
			v.setStartTime(d);
		} catch (ParseException e) {
			e.printStackTrace();
		}
		
		dao.saveVideo(v);
	}

	@Override
	public Camera_Info getCamera(String cameraNo) {
		return cameradao.getCameraInfoByCameraNo(cameraNo);
	}

}
