package com.shadt.generalmeeting.service.impl;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.shadt.generalmeeting.VO.CameraVO;
import com.shadt.generalmeeting.VO.MeetingVO;
import com.shadt.generalmeeting.VO.TownVillageVO;
import com.shadt.generalmeeting.dao.GeneralCameraDao;
import com.shadt.generalmeeting.dao.GeneralMeetingDao;
import com.shadt.generalmeeting.dao.GeneralMeetingTypeDao;
import com.shadt.generalmeeting.dao.GeneralTownVillageDao;
import com.shadt.generalmeeting.entity.GeneralMeeting;
import com.shadt.generalmeeting.entity.GeneralMeetingType;
import com.shadt.generalmeeting.service.GeneralMeetingService;
import com.shadt.pme.dao.StbDao;
import com.shadt.pme.entity.StbInfo;

@Service
public class GeneralMeetingServiceImpl implements GeneralMeetingService {
	
	Logger log = Logger.getLogger(this.getClass());

	@Autowired
	GeneralMeetingDao meetingDao;
	@Autowired
	GeneralMeetingTypeDao meetingTypeDao;
	@Autowired
	GeneralTownVillageDao tvDao;
	@Autowired
	GeneralCameraDao dao;
	@Autowired
	StbDao stbDao;
	
	@Override
	public List<GeneralMeetingType> getAllType() {
		return meetingTypeDao.find("from GeneralMeetingType order by sort");
	}

	@Override
	public GeneralMeetingType getType(Integer id) {
		Map<String, Object> params = new HashMap<String, Object>();
		params.put("id", id);
		return meetingTypeDao.get("from GeneralMeetingType where id = :id", params);
	}

	@Override
	public CameraVO getCamera(String stbNo, Integer typeId) {
		CameraVO camera = null;
		try {
			StbInfo stb = stbDao.get("from StbInfo where stbno ='" + stbNo + "'");
			if (null != stb) {
				camera =  dao.getByType(stb.getVillageId(), typeId);
			}
		} catch (Exception e) {
			e.printStackTrace();
			log.error("获取录制摄像头异常！"+e.getMessage());
		}
		return camera;
	}

	@Override
	public Long add(String stbNo,String tid, Integer cid, String taskid) {
		GeneralMeeting meeting = new GeneralMeeting();
		meeting.setMeetingType(tid);
		meeting.setStatus(0);
		meeting.setTaskid(taskid);
		if(cid==0){
			meeting.setCameraType("会议室");
		}
		if(cid==2){
			meeting.setCameraType("值班室");
		}
		TownVillageVO vo =  tvDao.getByStbNo(stbNo);
		String n = vo.getTownName()+vo.getVillageName();
		String t ="";
		if(tid.indexOf(",")>0){
			String id[] = tid.split(",");
			for(int i=0;i<id.length;i++){
				t += this.getType(Integer.parseInt(id[i])).getName()+",";
			}
		}else{
			t = this.getType(Integer.parseInt(tid)).getName()+",";
		}
		t = t.substring(0,t.length()-1);
		meeting.setName(n+t);
		meeting.setOrgcode(vo.getOrgCode());
		return  (Long) meetingDao.save(meeting);
	}

	@Override
	public void stop(String taskid) {
		GeneralMeeting m = meetingDao.get("from GeneralMeeting where taskid='"+taskid+"'");
		m.setStatus(1);
		meetingDao.update(m);
	}

	@Override
	public List<MeetingVO> getAll(String orgcode,String typeId,String meetingDate) {
		return meetingDao.getAll(orgcode,typeId,meetingDate);
	}

}
