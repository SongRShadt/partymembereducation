package com.shadt.generalmeeting.service;

import com.shadt.generalmeeting.entity.GeneralMeetingVideo;
import com.shadt.signmeeting.model.Camera_Info;

/**
 * 录制视频业务层
 * @author liusongren
 *
 */
public interface GeneralCameraVideoService {
	
	/**
	 *  保存录制视频信息
	 * @param v
	 */
	void saveVideo(GeneralMeetingVideo v);

	
	/**
	 * 根据摄像头编号获取摄像头信息
	 * @param cameraNo 摄像头编号
	 * @return
	 */
	Camera_Info getCamera(String cameraNo);
}
