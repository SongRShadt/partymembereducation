package com.shadt.generalmeeting.service;

import java.util.List;

import com.shadt.generalmeeting.VO.CameraVO;
import com.shadt.generalmeeting.VO.MeetingVO;
import com.shadt.generalmeeting.entity.GeneralMeetingType;

public interface GeneralMeetingService {

	/**
	 * 获取会议类型集合
	 * @return
	 */
	List<GeneralMeetingType> getAllType();
	
	/**
	 * 通过编号获取类型
	 * @param id
	 * @return
	 */
	GeneralMeetingType getType(Integer id);

	/**
	 * 获取摄像头
	 * @param stbNo 机顶盒编号
	 * @param typeId 摄像头类型
	 * @return
	 */
	CameraVO getCamera(String stbNo, Integer typeId);

	/**
	 * 创建换届会议
	 * @param stbNo 机顶盒编号
	 * @param tid 会议类型编号
	 * @param cid 摄像头类型
	 * @param taskid 会议录制taskid
	 * @return
	 */
	Long add(String stbNo,String tid, Integer cid, String taskid);
	
	/**
	 * 结束换届会议
	 * @param taskid 会议录制taskid
	 * @return
	 */
	void stop(String taskid);

	/**
	 * 获取所有换届录制会议
	 * @return
	 */
	List<MeetingVO> getAll(String orgcode,String typeId,String meetingDate);
}
