package com.shadt.generalmeeting.controller;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.shadt.core.controller.BaseController;
import com.shadt.core.model.Json;
import com.shadt.generalmeeting.entity.GeneralMeetingVideo;
import com.shadt.generalmeeting.service.GeneralCameraVideoService;
import com.shadt.generalmeeting.service.GeneralMeetingService;
import com.shadt.signmeeting.model.Camera_Info;

/**
 * 视频录制控制器
 * 
 * @author liusongren
 *
 */
@Controller
@RequestMapping("/generalvideo")
public class GeneralVideoController extends BaseController {
	Logger log = Logger.getLogger(this.getClass());
	@Autowired
	GeneralCameraVideoService service;
	@Autowired
	GeneralMeetingService meetingService;
	/**
	 * 获取视频录制回调参数
	 * 
	 * @param request
	 * @param taskid
	 *            任务编号
	 * @param download_url
	 *            下载地址（mp4）
	 * @param play_url
	 *            播放地址（m3u8）
	 * @param duration
	 *            录制时长
	 * @return
	 */
	@ResponseBody
	@RequestMapping("/callback")
	public Json videoCallBack(HttpServletRequest request, HttpSession session, String taskid, String download_url,
			String play_url, Integer duration,String cameraNo) {
		log.info("进入录制回调，录制时长:"+duration);
		Json j = new Json();
		if(duration>20){
			GeneralMeetingVideo v = new GeneralMeetingVideo();
			v.setCameraNo(cameraNo);
			Camera_Info camera = service.getCamera(cameraNo);
			String []ip = camera.getCameraUrl().split("/");
			v.setTaskId(taskid);
			v.setDownloadUrl("http://"+ip[2]+download_url);
			v.setPlayUrl("http://"+ip[2]+play_url);
			v.setDuration(duration);
			service.saveVideo(v);	
			meetingService.stop(taskid);
		}
		return j;
	}
	
	/**
	 * 停止录制
	 * @return
	 
	@ResponseBody
	@RequestMapping(value = "/stop", method = RequestMethod.GET)
	public Json stop(String id,String taskid){
		Json j = new Json();
		try {
			Camera_Info camera = service.getCamera(id);
			if(null!=camera){
				if(null!=camera.getCameraUrl()){
					String []ip = camera.getCameraUrl().split("/");
					if(ip.length>3&&!ip[2].trim().equals("")&&null!=camera.getStreamId()&&!"".equals(camera.getStreamId())){
						String httpUrl= "http://"+ip[2]+"/record";
						Map<String,String> m = new HashMap<String,String>();
						m.put("do", "stop");
						m.put("taskid", taskid);
						String result = HttpClientUtil.doPost(httpUrl, m);
						log.info("调用sewise录制结束。。。返回结果为："+result);
						meetingService.stop(taskid);
						j.setMsg("换届会议录制完成");
						j.setSuccess(true);
					}
				} 
			}
		} catch (Exception e) {
			e.printStackTrace();
			j.setSuccess(false);
			log.error("换届会议录制结束异常"+e.getMessage());
		}
		return j;
	}
	*/
}
