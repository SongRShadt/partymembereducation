package com.shadt.generalmeeting.controller;

import java.io.PrintWriter;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletResponse;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.alibaba.fastjson.JSON;
import com.shadt.core.controller.BaseController;
import com.shadt.core.model.Json;
import com.shadt.core.util.ConfigUtil;
import com.shadt.core.util.HttpClientUtil;
import com.shadt.generalmeeting.VO.CameraVO;
import com.shadt.generalmeeting.VO.MeetingVO;
import com.shadt.generalmeeting.entity.GeneralMeetingType;
import com.shadt.generalmeeting.service.GeneralMeetingService;

/**
 * 摄像头信息控制器
 * @author SongR
 *
 */
@Controller
@RequestMapping(value="/generalmeeting")
public class GeneralMeetingController extends BaseController{
	
	Logger log = Logger.getLogger(this.getClass());
	
	@Autowired
	private GeneralMeetingService service;
	
	/**
	 * 换届会议类型
	 * @return
	 */
	@ResponseBody
	@RequestMapping(value="/gettype",method=RequestMethod.GET)
	public Json getType(){
		Json j = new Json();
		try {
			List<GeneralMeetingType> typeList = service.getAllType();
			j.setSuccess(true);
			j.setMsg("获取会议类型成功！");
			j.setObj(typeList);
		} catch (Exception e) {
			j.setMsg("获取会议类型异常！"+e.getMessage());
			j.setSuccess(false);
			log.error("获取会议类型异常！"+ e.getMessage());
		}
		return j;
	}
	
	/**
	 * 创建会议
	 * @return
	 */
	@ResponseBody
	@RequestMapping(value="/addmeeting",method=RequestMethod.POST)
	public Json addmeeting(String stbNo,String tid,Integer cid,String taskid){
		Json j = new Json();
		try {
			Long id = service.add(stbNo,tid,cid,taskid);
			j.setMsg("创建会议成功！");
			j.setSuccess(true);
			j.setObj(id);
			log.info("创建会议成功！");
		} catch (Exception e) {
			j.setMsg("创建会议失败！请退出重试！");
			log.error("创建会议失败！"+e.getMessage());
		}
		return j;
	}
	
	/**
	 * 按机顶盒和摄像头类型进行视频录制
	 * @return
	 */
	@ResponseBody
	@RequestMapping(value="/recording",method=RequestMethod.GET)
	public Json recording(String stbNo, Integer typeId){
		Json j = new Json();
		try {
			CameraVO camera = service.getCamera(stbNo,typeId);
			if(null!=camera){
				j.setObj(camera);
				if(null!=camera.getCameraUrl()){
					String ip[] = camera.getCameraUrl().split("/");
					if(ip.length>3&&!ip[2].trim().equals("")&&null!=camera.getStreamId()&&!"".equals(camera.getStreamId())){
						String httpUrl= "http://"+ip[2]+"/record";
						Map<String,String> map = new HashMap<String,String>();
						map.put("do", "start");
						map.put("streamid", camera.getStreamId());
						map.put("duration", ConfigUtil.get("generalmeeting.duration"));
						map.put("callback", ConfigUtil.get("generalmeeting.callback")+"?cameraNo="+camera.getId());
						log.info("开始调用sewise录制。。。sewise录制请求地址为："+httpUrl);
						log.info("回调地址："+ConfigUtil.get("generalmeeting.callback")+"?cameraNo="+camera.getId());
						String result = HttpClientUtil.doPost(httpUrl, map);
						log.info("sewise录制返回结果为："+result);
						j.setMsg(result);			
					}
				} 
			}
			j.setSuccess(true);
		} catch (Exception e) {
			j.setMsg("接口异常！");
			j.setSuccess(false);
			log.error("调用录制接口异常"+e.getMessage());
		}
		return j;
	}
	
	/**
	 * 换届会议 
	 * @return json / jsonp
	 */
	@RequestMapping(value="/getall",method=RequestMethod.GET)
	public void  getAll(HttpServletResponse response,String orgcode,String typeId,String meetingDate,String callback){
		response.setContentType("text/plain");  
	    response.setHeader("Pragma", "No-cache");  
	    response.setHeader("Cache-Control", "no-cache");  
	    response.setDateHeader("Expires", 0);  
	    Json j = new Json();
		try {
			if(orgcode==null){
				orgcode = "";
			}
			if(typeId==null){
				typeId = "";
			}
			if(meetingDate==null){
				meetingDate = "";
			}
			if(callback==null){
				callback = "";
			}
			PrintWriter out = response.getWriter();
			List<MeetingVO> list = service.getAll(orgcode,typeId,meetingDate);	
			j.setSuccess(true);
			j.setMsg("获取会议集合成功！");
			j.setObj(list);
			// 用户组对象转JSON串  
	        String jsonString = JSON.toJSONString(j);  
			//callback为空，返回结果集
			if(StringUtils.isEmpty(callback.trim())){
				out.println(jsonString);//返回json格式数据  
		        out.flush();  
		        out.close(); 
			}else{
				out.println(callback+"("+jsonString+")");//返回jsonp格式数据  
		        out.flush();  
		        out.close();  
			} 
		} catch (Exception e) {  
			log.error("获取会议集合异常！"+ e.getMessage());
		}
	}
}
