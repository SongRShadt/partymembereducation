package com.shadt.yanzhou.entity;

import java.io.Serializable;

import javax.persistence.Entity;
import javax.persistence.Table;

import org.hibernate.annotations.DynamicInsert;
import org.hibernate.annotations.DynamicUpdate;

@SuppressWarnings("serial")
@Entity
@Table(name = "yz_mettingtype")
@DynamicInsert(true)
@DynamicUpdate(true)
public class MettingType implements Serializable{
	
	private Long id;
	
	private String name;
	
	
	
}
