package com.shadt.yanzhou.service;

import com.shadt.yanzhou.vo.YZCameraVo;

public interface YZCameraService {
	/**
	 * 根据机顶盒编号获取摄像头信息并录制视频
	 */
	YZCameraVo recording(String stbNo) throws Exception;
}
