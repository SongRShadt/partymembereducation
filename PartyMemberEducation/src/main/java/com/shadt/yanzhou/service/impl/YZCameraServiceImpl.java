package com.shadt.yanzhou.service.impl;

import java.util.Date;
import java.util.HashMap;
import java.util.Map;
import java.util.UUID;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.alibaba.fastjson.JSONObject;
import com.shadt.core.util.ConfigUtil;
import com.shadt.core.util.HttpClientUtil;
import com.shadt.pme.dao.CameraDao;
import com.shadt.pme.dao.NewVideoDao;
import com.shadt.pme.dao.StbDao;
import com.shadt.pme.entity.CameraInfo;
import com.shadt.pme.entity.StbInfo;
import com.shadt.pme.entity.Video;
import com.shadt.yanzhou.service.YZCameraService;
import com.shadt.yanzhou.vo.YZCameraVo;

/**
 * 兖州摄像头信息业务逻辑层实现类
 * @author SongR
 *
 */
@Service
public class YZCameraServiceImpl implements YZCameraService{

	@Autowired
	CameraDao dao;
	@Autowired
	StbDao stbDao;
	@Autowired
	NewVideoDao vDao;
	
	/**
	 * 实现录制视频
	 */
	public YZCameraVo recording(String stbNo) throws Exception {
		StbInfo stb = stbDao.get("from StbInfo where stbno ='" + stbNo + "'");
		CameraInfo camera = null;
		if (null != stb) {
			camera = dao.get("from CameraInfo where villageId='" + stb.getVillageId() + "' and cameraType='0'  order by sort");
		}
		YZCameraVo cvo = new YZCameraVo();
		if(null!=camera){
			if(null!=camera.getCameraUrl()){
				String []ip = camera.getCameraUrl().split("/");
				if(ip.length>3&&!ip[2].trim().equals("")&&null!=camera.getStreamId()&&!"".equals(camera.getStreamId())){
					String httpUrl= "http://"+ip[2]+"/record";
					Map<String,String> map = new HashMap<String,String>();
					map.put("do", "start");
					map.put("streamid", camera.getStreamId());
					map.put("callback", ConfigUtil.get("sewise.callbackyz"));
					System.out.println(ConfigUtil.get("sewise.callbackyz"));
					String result = HttpClientUtil.doPost(httpUrl, map);
					JSONObject obj = JSONObject.parseObject(result);
					if(obj!=null){
						Boolean success = obj.getBoolean("success");
						String taskId = "";
						if(success){
							taskId = obj.getString("taskid");
							cvo.setCameraUrl(camera.getCameraUrl());
							cvo.setCameraName(camera.getCameraName());
							cvo.setId(camera.getId());
							cvo.setTaskId(taskId);
							Video video = new Video();
							video.setId(UUID.randomUUID().toString());
							video.setStartTime(new Date());
							video.setCameraNo(camera.getId());
							video.setType(0);
							video.setTaskId(taskId);
							video.setStatus(1);
							vDao.save(video);
						}else{
							System.out.println("接口调用失败!");
						}
					}else{
						System.out.println(result);
					}
					
				}
			} 
		}else{
			cvo = null;
		}
		return cvo;
	}
}
