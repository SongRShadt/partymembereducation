package com.shadt.yanzhou.controller;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.shadt.core.controller.BaseController;
import com.shadt.core.model.Json;
import com.shadt.yanzhou.service.YZCameraService;
import com.shadt.yanzhou.vo.YZCameraVo;

/**
 * 兖州摄像头控制器
 * @author SongR
 *
 */
@Controller
@RequestMapping(value="/yzcamera")
public class YZCameraController extends BaseController{
	
	Logger log = Logger.getLogger(this.getClass());
	@Autowired
	YZCameraService service;
	
	/**
	 * 视频录制
	 * @return
	 */
	@ResponseBody
	@RequestMapping(value="/recording",method={RequestMethod.GET,RequestMethod.POST})
	public Json getCameraRecording(String stbNo){
		Json j = new Json();
		try {
			YZCameraVo camera = service.recording(stbNo);
			j.setSuccess(true);
			if(null!=camera){
				j.setMsg("开始录制视频！");
				j.setObj(camera);
			}else{
				j.setMsg("摄像头不存在！");
			}
		} catch (Exception e) {
			j.setMsg("接口异常！");
			j.setSuccess(false);
			log.error(e.toString(),e);
		}
		return j;
	}
	
}
