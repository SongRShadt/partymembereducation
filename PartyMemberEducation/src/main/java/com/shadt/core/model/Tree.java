package com.shadt.core.model;

import java.io.Serializable;
import java.util.List;

/**
 * 菜单数据模型
 * @author SongR
 *
 */
@SuppressWarnings("serial")
public class Tree implements Serializable{
	private String id;
	private String text;
	private Object attributes;//
	private List<Tree> children;//子节点
	private String iconCls;//图标
	private String pid;//是否是父节点
	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
	public String getText() {
		return text;
	}
	public void setText(String text) {
		this.text = text;
	}
	public Object getAttributes() {
		return attributes;
	}
	public void setAttributes(Object attributes) {
		this.attributes = attributes;
	}
	public List<Tree> getChildren() {
		return children;
	}
	public void setChildren(List<Tree> children) {
		this.children = children;
	}
	public String getIconCls() {
		return iconCls;
	}
	public void setIconCls(String iconCls) {
		this.iconCls = iconCls;
	}
	public String getPid() {
		return pid;
	}
	public void setPid(String pid) {
		this.pid = pid;
	}
	
	
	
}
