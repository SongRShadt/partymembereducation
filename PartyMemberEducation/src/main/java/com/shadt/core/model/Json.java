package com.shadt.core.model;

import java.io.Serializable;

/**
 * 
 * JSON模型
 * 
 * 用户后台向前台返回的JSON对象
 * 
 * @author SongR
 * 
 */
@SuppressWarnings("serial")
public class Json implements Serializable {
	private boolean success = false;//是否成功
	private String msg;//返回信息
	private Object obj="";//返回数据

	public boolean isSuccess() {
		return success;
	}

	public void setSuccess(boolean success) {
		this.success = success;
	}

	public String getMsg() {
		return msg;
	}

	public void setMsg(String msg) {
		this.msg = msg;
	}

	public Object getObj() {
		return obj;
	}

	public void setObj(Object obj) {
		this.obj = obj;
	}

}
