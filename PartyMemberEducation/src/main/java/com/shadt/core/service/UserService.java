package com.shadt.core.service;

import java.util.List;

import com.shadt.core.model.Tree;
import com.shadt.core.util.SessionInfo;
import com.shadt.core.vo.UserVo;

/**
 * 用户业务层接口
 * 
 * @author SongR
 * 
 */
public interface UserService {
	/**
	 * 用户登录
	 * 
	 * @param userVo
	 * @return
	 */
	UserVo login(UserVo userVo);

	/**
	 * 用户注册
	 * 
	 * @param user
	 */
	void reg(UserVo user) throws Exception;

	/**
	 * 根据当前登陆用户获取可访问权限组
	 * 
	 * @param id
	 * @return
	 */
	List<String> resourceList(String id);

	/**
	 * 根据当前登陆的用户获取可访问栏目
	 * @param id
	 * @return
	 */
	List<Tree> getMenuList(SessionInfo sessionInfo);

	/**
	 * 获取所有用户
	 * @return
	 */
	List<UserVo> getAllUsers(String str,Integer pageNo,Integer rows);
	/**
	 * 根据条件获取记录总数
	 * @param str
	 * @return
	 */
	Long getUsersCount(String str);
	/**
	 * 判断用户名是否存在
	 * @param name
	 * @return
	 */
	 boolean checkUserName(String name);
	 /**
	  * 验证密码是否正确
	  * @param oldpwd
	  * @return
	  */
	 boolean checkPwd(String oldpwd,String id);
	 /**
	  * 根据用户id删除用户
	  * @param id
	  * @return
	  */
	boolean del(String id);

	/**
	 * 修改密码
	 * @param id
	 * @param vo
	 * @return
	 */
	boolean editPwd(String id, UserVo vo);

	/**
	 * 根据用户id获取用户信息
	 * @param id
	 * @return
	 */
	UserVo getUserById(String id);

}
