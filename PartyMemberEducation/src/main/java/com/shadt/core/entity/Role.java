package com.shadt.core.entity;

import java.io.Serializable;
import java.util.HashSet;
import java.util.Set;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import org.hibernate.annotations.DynamicInsert;
import org.hibernate.annotations.DynamicUpdate;
/**
 * 角色信息
 * @author SongR
 *
 */
@SuppressWarnings("serial")
@Entity
@Table(name = "AD_SYS_role",schema="Song")
@DynamicInsert(true)
@DynamicUpdate(true)
public class Role implements Serializable{
	@Id
	@Column(name = "ID", nullable = false, length = 36)
	private String id;//编号
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "PID")
	private Role role;//上级角色
	@Column(name = "NAME", nullable = false, length = 100)
	private String name;//角色名称
	@Column(name = "REMARK", length = 200)
	private String remark;//备注
	@Column(name = "SEQ")
	private Integer seq;
	@OneToMany(fetch = FetchType.LAZY, mappedBy = "role")
	private Set<Role> roles = new HashSet<Role>(0);//下级角色
	@ManyToMany(fetch = FetchType.LAZY)
	@JoinTable(name = "AD_SYS_role_Resource_rec",schema="Song", joinColumns = { @JoinColumn(name = "ROLE_ID", nullable = false, updatable = false) }, inverseJoinColumns = { @JoinColumn(name = "FUNCTION_ID", nullable = false, updatable = false) })
	private Set<Resource> resources = new HashSet<Resource>(0);//拥有资源
	@ManyToMany(fetch = FetchType.LAZY)
	@JoinTable(name = "AD_SYS_user_role_rec", schema="Song",joinColumns = { @JoinColumn(name = "ROLE_ID", nullable = false, updatable = false) }, inverseJoinColumns = { @JoinColumn(name = "USER_ID", nullable = false, updatable = false) })
	private Set<User> users = new HashSet<User>(0);//拥有用户
	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
	public Role getRole() {
		return role;
	}
	public void setRole(Role role) {
		this.role = role;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getRemark() {
		return remark;
	}
	public void setRemark(String remark) {
		this.remark = remark;
	}
	public Integer getSeq() {
		return seq;
	}
	public void setSeq(Integer seq) {
		this.seq = seq;
	}
	public Set<Role> getRoles() {
		return roles;
	}
	public void setRoles(Set<Role> roles) {
		this.roles = roles;
	}

	public Set<Resource> getResources() {
		return resources;
	}
	public void setResources(Set<Resource> resources) {
		this.resources = resources;
	}
	public Set<User> getUsers() {
		return users;
	}
	public void setUsers(Set<User> users) {
		this.users = users;
	}

	
	
}
