package com.shadt.core.entity;

import java.io.Serializable;
import java.util.Date;
import java.util.HashSet;
import java.util.Set;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import org.hibernate.annotations.DynamicInsert;
import org.hibernate.annotations.DynamicUpdate;

import com.shadt.core.entity.Role;

/**
 * 用户信息
 * 
 * @author SongR
 * 
 */
@Entity
@Table(name = "ad_SYS_user",schema="Song")
@DynamicInsert(true)
@DynamicUpdate(true)
@SuppressWarnings("serial")
public class User implements Serializable {
	@Id
	@Column(name = "ID", nullable = false, length = 36)
	private String id;// 用户编号
	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "CREATEDATETIME", length = 19)
	private Date createdatetime;// 创建时间
	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "MODIFYDATETIME", length = 19)
	private Date modifydatetime;// 最后修改时间
	@Column(name = "NAME", unique = true, nullable = false, length = 100)
	private String name;// 登录名
	@Column(name = "PWD", nullable = false, length = 100)
	private String pwd;// 密码
	@Column(name = "NIKENAME")
	private String nikeName;// 昵称
	@Column(name = "PHONE")
	private String phone;// 电话号码
	@Column(name = "EMAIL")
	private String email;// 邮箱
	@Column(name = "PHOTO")
	private String photo;//用户头像
	@Column(name = "REGCODE")
	private String regCode;// 注册许可号
	@Column(name = "ISDELETE")
	private String isDelete;//是否删除（0、未删除 1、已删除）
	@Column(name = "ISAVAILABLE")
	private String isAvailable;//是否可用(0、可用1、不可用)
	@ManyToMany(fetch = FetchType.LAZY)
	@JoinTable(name = "ad_sys_user_role_rec",schema="song", joinColumns = { @JoinColumn(name = "USER_ID", nullable = false, updatable = false) }, inverseJoinColumns = { @JoinColumn(name = "ROLE_ID", nullable = false, updatable = false) })
	private Set<Role> roles = new HashSet<Role>(0);// 对应角色

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public Date getCreatedatetime() {
		return createdatetime;
	}

	public void setCreatedatetime(Date createdatetime) {
		this.createdatetime = createdatetime;
	}

	public Date getModifydatetime() {
		return modifydatetime;
	}

	public void setModifydatetime(Date modifydatetime) {
		this.modifydatetime = modifydatetime;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getPwd() {
		return pwd;
	}

	public void setPwd(String pwd) {
		this.pwd = pwd;
	}

	public String getNikeName() {
		return nikeName;
	}

	public void setNikeName(String nikeName) {
		this.nikeName = nikeName;
	}

	public String getPhone() {
		return phone;
	}

	public void setPhone(String phone) {
		this.phone = phone;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getRegCode() {
		return regCode;
	}

	public void setRegCode(String regCode) {
		this.regCode = regCode;
	}

	public Set<Role> getRoles() {
		return roles;
	}

	public void setRoles(Set<Role> roles) {
		this.roles = roles;
	}

	public String getPhoto() {
		return photo;
	}

	public void setPhoto(String photo) {
		this.photo = photo;
	}

	public String getIsDelete() {
		return isDelete;
	}

	public void setIsDelete(String isDelete) {
		this.isDelete = isDelete;
	}

	public String getIsAvailable() {
		return isAvailable;
	}

	public void setIsAvailable(String isAvailable) {
		this.isAvailable = isAvailable;
	}

	
	
}
