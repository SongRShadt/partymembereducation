package com.shadt.core.entity;

import java.io.Serializable;
import java.util.HashSet;
import java.util.Set;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import org.hibernate.annotations.DynamicInsert;
import org.hibernate.annotations.DynamicUpdate;

/**
 * 功能信息
 * @author SongR
 *
 */
@SuppressWarnings("serial")
@Entity
@Table(name = "AD_SYS_RESOURCE")
@DynamicInsert(true)
@DynamicUpdate(true)
public class Resource implements Serializable{
	@Id
	@Column(name = "ID", nullable = false, length = 36)
	private String id;
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "RESOURCETYPE_ID", nullable = false)
	private ResourceType resourceType;
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "PID")
	private Resource resource;
	@Column(name = "NAME", nullable = false, length = 100)
	private String name;
	@Column(name = "REMARK", length = 200)
	private String remark;
	@Column(name = "SEQ")
	private Integer seq;
	@Column(name = "URL", length = 200)
	private String url;
	@Column(name = "ICON", length = 100)
	private String icon;
	@ManyToMany(fetch = FetchType.LAZY)
	@JoinTable(name = "AD_SYS_role_resource_rec" , schema="Song", joinColumns = { @JoinColumn(name = "FUNCTION_ID", nullable = false, updatable = false) }, inverseJoinColumns = { @JoinColumn(name = "ROLE_ID", nullable = false, updatable = false) })
	private Set<Role> roles = new HashSet<Role>(0);
	@OneToMany(fetch = FetchType.LAZY, mappedBy = "resource")
	private Set<Resource> resources = new HashSet<Resource>(0);
	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
	public ResourceType getResourceType() {
		return resourceType;
	}
	public void setResourceType(ResourceType resourceType) {
		this.resourceType = resourceType;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getRemark() {
		return remark;
	}
	public void setRemark(String remark) {
		this.remark = remark;
	}
	public Integer getSeq() {
		return seq;
	}
	public void setSeq(Integer seq) {
		this.seq = seq;
	}
	public String getUrl() {
		return url;
	}
	public void setUrl(String url) {
		this.url = url;
	}
	public String getIcon() {
		return icon;
	}
	public void setIcon(String icon) {
		this.icon = icon;
	}
	public Set<Role> getRoles() {
		return roles;
	}
	public void setRoles(Set<Role> roles) {
		this.roles = roles;
	}
	public Resource getResource() {
		return resource;
	}
	public void setResource(Resource resource) {
		this.resource = resource;
	}
	public Set<Resource> getResources() {
		return resources;
	}
	public void setResources(Set<Resource> resources) {
		this.resources = resources;
	}
	
	
}
