package com.shadt.core.entity;

import java.io.Serializable;
import java.util.HashSet;
import java.util.Set;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import org.hibernate.annotations.DynamicInsert;
import org.hibernate.annotations.DynamicUpdate;
/**
 * 功能类型
 * @author SongR
 *
 */
@SuppressWarnings("serial")
@Entity
@Table(name = "AD_SYS_RESOURCEYPE",schema="Song")
@DynamicInsert(true)
@DynamicUpdate(true)
public class ResourceType implements Serializable {
	@Id
	@Column(name = "ID", nullable = false, length = 36)
	private String id;
	@Column(name = "NAME", nullable = false, length = 100)
	private String name;
	@OneToMany(fetch = FetchType.LAZY, mappedBy = "resourceType")
	private Set<Resource> resources = new HashSet<Resource>(0);
	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public Set<Resource> getResources() {
		return resources;
	}
	public void setResources(Set<Resource> resources) {
		this.resources = resources;
	}
}
