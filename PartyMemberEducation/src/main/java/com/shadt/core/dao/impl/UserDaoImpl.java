package com.shadt.core.dao.impl;

import org.springframework.stereotype.Repository;

import com.shadt.core.dao.UserDao;
import com.shadt.core.dao.impl.BaseDaoImpl;
import com.shadt.core.entity.User;
/**
 * 用户信息数据访问层接口
 * @author SongR
 *
 */
@Repository
public class UserDaoImpl extends BaseDaoImpl<User> implements UserDao{

}
