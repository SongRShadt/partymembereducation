package com.shadt.core.dao;

import com.shadt.core.entity.ResourceType;

/**
 * 资源栏目类型数据访问层接口
 * @author SongR
 *
 */
public interface ResourceTypeDao extends BaseDao<ResourceType> {

}
