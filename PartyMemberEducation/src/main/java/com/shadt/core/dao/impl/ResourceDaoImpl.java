package com.shadt.core.dao.impl;

import org.springframework.stereotype.Repository;

import com.shadt.core.dao.ResourceDao;
import com.shadt.core.entity.Resource;

/**
 *  资源栏目数据访问层实现类
 *  本类继承基础数据查询类
 * @author SongR
 *
 */
@Repository
public class ResourceDaoImpl extends BaseDaoImpl<Resource> implements ResourceDao {

}
