package com.shadt.core.dao;

import com.shadt.core.dao.BaseDao;
import com.shadt.core.entity.User;

public interface UserDao extends BaseDao<User> {

}
