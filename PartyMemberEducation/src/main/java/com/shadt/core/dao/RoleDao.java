package com.shadt.core.dao;

import com.shadt.core.entity.Role;
/**
 * 角色信息数据访问接口
 * @author SongR
 *
 */
public interface RoleDao extends BaseDao<Role>{

}
