package com.shadt.core.dao.impl;

import org.springframework.stereotype.Repository;

import com.shadt.core.dao.ResourceTypeDao;
import com.shadt.core.entity.ResourceType;

/**
 *  资源栏目类型数据访问层实现类
 *  本类继承基础数据查询类
 * @author SongR
 *
 */
@Repository
public class ResourceTypeDaoImpl extends BaseDaoImpl<ResourceType> implements ResourceTypeDao {

}
