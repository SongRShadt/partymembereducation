package com.shadt.core.dao.impl;

import org.springframework.stereotype.Repository;

import com.shadt.core.dao.RoleDao;
import com.shadt.core.entity.Role;
/**
 * 角色数据访问实现类
 * @author SongR
 *
 */
@Repository
public class RoleDaoImpl extends BaseDaoImpl<Role> implements RoleDao {

}
