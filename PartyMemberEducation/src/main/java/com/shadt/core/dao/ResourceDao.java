package com.shadt.core.dao;

import com.shadt.core.entity.Resource;

/**
 * 资源栏目数据访问层接口
 * @author SongR
 *
 */
public interface ResourceDao extends BaseDao<Resource> {

}
