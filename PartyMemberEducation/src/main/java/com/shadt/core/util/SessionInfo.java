package com.shadt.core.util;

import java.util.List;

import com.shadt.core.model.Tree;

/**
 * session信息模型
 * 
 * @author alex
 * 
 */
@SuppressWarnings("serial")
public class SessionInfo implements java.io.Serializable {

	private String id;// 用户ID
	private String name;// 用户登录名
	private String phone;
	private String nikeName;
	private String email;
	private String ip;// 用户IP
	private List<String> resourceList;// 用户可以访问的资源地址列表
	private List<Tree> menuList;// 用户可以访问的菜单

	public String getPhone() {
		return phone;
	}

	public void setPhone(String phone) {
		this.phone = phone;
	}

	public String getNikeName() {
		return nikeName;
	}

	public void setNikeName(String nikeName) {
		this.nikeName = nikeName;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getIp() {
		return ip;
	}

	public void setIp(String ip) {
		this.ip = ip;
	}

	public String getId() {
		return id;
	}

	public List<String> getResourceList() {
		return resourceList;
	}

	public void setResourceList(List<String> resourceList) {
		this.resourceList = resourceList;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String toString() {
		return this.name;
	}

	public List<Tree> getMenuList() {
		return menuList;
	}

	public void setMenuList(List<Tree> menuList) {
		this.menuList = menuList;
	}


}
