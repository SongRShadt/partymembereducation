package com.shadt.core.util;

import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.util.Properties;


public class PropertiesUtil {

	public static void main(String[] args)throws Exception { 
		Properties prop = new Properties();// 属性集合对象 
		FileInputStream fis = new FileInputStream("src/config.properties");// 属性文件输入流 
		prop.load(fis);// 将属性文件流装载到Properties对象中 
		fis.close();// 关闭流 
		System.out.println("获取属性值：password=" + prop.getProperty("startnum")); 
		prop.setProperty("password", "heihei"); 
		// 文件输出流 
		FileOutputStream fos = new FileOutputStream("src/test.properties"); 
		// 将Properties集合保存到流中 
		prop.store(fos, "Copyright (c) Boxcode Studio"); 
		fos.close();// 关闭流 
		System.out.println("获取修改后的属性值：password=" + prop.getProperty("password")); 

		} 

}
