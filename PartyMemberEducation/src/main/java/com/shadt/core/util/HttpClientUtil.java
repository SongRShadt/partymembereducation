package com.shadt.core.util;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.client.HttpClient;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.HttpClients;
import org.apache.http.message.BasicNameValuePair;
import org.apache.http.util.EntityUtils;
import org.apache.log4j.Logger;

import com.alibaba.fastjson.JSONObject;

public class HttpClientUtil {
	static Logger log = Logger.getLogger(HttpClientUtil.class);

	@SuppressWarnings({ "unchecked", "rawtypes" })
	public static String doPost(String url, Map<String, String> map) {
		String result = null;
		HttpClient httpClient = HttpClients.createDefault();
		try {
			HttpPost httpPost = new HttpPost(url);
			// 设置参数
			List<NameValuePair> list = new ArrayList<NameValuePair>();
			Iterator iterator = map.entrySet().iterator();
			while (iterator.hasNext()) {
				Entry<String, String> elem = (Entry<String, String>) iterator.next();
				list.add(new BasicNameValuePair(elem.getKey(), elem.getValue()));
			}
			if (list.size() > 0) {
				UrlEncodedFormEntity entity = new UrlEncodedFormEntity(list, "utf-8");
				httpPost.setEntity(entity);
			}
			HttpResponse response = httpClient.execute(httpPost);
			if (response != null) {
				HttpEntity resEntity = response.getEntity();
				if (resEntity != null) {
					result = EntityUtils.toString(resEntity, "utf-8");
				}
			}
		} catch (Exception e) {
			log.error("向" + url + "发送post请求异常:" + e);
		} finally {

		}
		return result;
	}

	public static String doGet(String url, Map<String, String> map) {
		String result = null;
		HttpClient httpClient = HttpClients.createDefault();
		try {
			HttpGet httpPost = new HttpGet(url);
			HttpResponse response = httpClient.execute(httpPost);
			if (response != null) {
				HttpEntity resEntity = response.getEntity();
				if (resEntity != null) {
					result = EntityUtils.toString(resEntity, "utf-8");
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
		} finally {

		}
		return result;
	}

	public static JSONObject getJsonp(String jsonp) {
//		jsonp = "1({'success':true,'message':'调用成功','data':[{'Townname':'南张街道','Villagename':'东陈村','orgId':125},{'Townname':'南张街道','Villagename':'乔家村','orgId':121},{'Townname':'南张街道','Villagename':'于家村','orgId':108},{'Townname':'南张街道','Villagename':'后店村','orgId':106},{'Townname':'南张街道','Villagename':'孔家村','orgId':135},{'Townname':'南张街道','Villagename':'孙家村','orgId':114},{'Townname':'南张街道','Villagename':'房家村','orgId':141},{'Townname':'南张街道','Villagename':'文郑村','orgId':144},{'Townname':'南张街道','Villagename':'苏家村','orgId':119},{'Townname':'南张街道','Villagename':'赵庙村','orgId':136},{'Townname':'唐口街道','Villagename':'后闫村','orgId':532},{'Townname':'唐口街道','Villagename':'姜庙村','orgId':543},{'Townname':'唐口街道','Villagename':'常户村','orgId':477},{'Townname':'唐口街道','Villagename':'景村','orgId':505},{'Townname':'唐口街道','Villagename':'河东村','orgId':544},{'Townname':'唐口街道','Villagename':'谷庄村','orgId':486},{'Townname':'唐口街道','Villagename':'路口村','orgId':494},{'Townname':'唐口街道','Villagename':'魏楼村','orgId':510},{'Townname':'喻屯镇','Villagename':'九子集村','orgId':611},{'Townname':'喻屯镇','Villagename':'兴福集村','orgId':590},{'Townname':'喻屯镇','Villagename':'南田村','orgId':606},{'Townname':'喻屯镇','Villagename':'城南张村','orgId':582},{'Townname':'喻屯镇','Villagename':'张桥村','orgId':583},{'Townname':'喻屯镇','Villagename':'李户村','orgId':596},{'Townname':'喻屯镇','Villagename':'河湾村','orgId':551},{'Townname':'喻屯镇','Villagename':'谭口集村','orgId':554},{'Townname':'安居街道','Villagename':'东李村','orgId':456},{'Townname':'安居街道','Villagename':'前埝口村','orgId':459},{'Townname':'安居街道','Villagename':'北薛屯村','orgId':467},{'Townname':'安居街道','Villagename':'后埝口村','orgId':460},{'Townname':'安居街道','Villagename':'居北村','orgId':427},{'Townname':'安居街道','Villagename':'张天楼村','orgId':457},{'Townname':'安居街道','Villagename':'满营村','orgId':420},{'Townname':'安居街道','Villagename':'白咀村','orgId':463},{'Townname':'安居街道','Villagename':'西朱庄村','orgId':434},{'Townname':'廿里铺街道','Villagename':'南村','orgId':11},{'Townname':'廿里铺街道','Villagename':'后屯','orgId':39},{'Townname':'廿里铺街道','Villagename':'吕庄','orgId':18},{'Townname':'廿里铺街道','Villagename':'姜庄','orgId':9},{'Townname':'廿里铺街道','Villagename':'朱营','orgId':45},{'Townname':'廿里铺街道','Villagename':'李林','orgId':7},{'Townname':'廿里铺街道','Villagename':'杨场','orgId':23},{'Townname':'廿里铺街道','Villagename':'梁马','orgId':42},{'Townname':'廿里铺街道','Villagename':'王府集','orgId':40},{'Townname':'廿里铺街道','Villagename':'聂庄','orgId':31},{'Townname':'李营街道','Villagename':'前双村','orgId':299},{'Townname':'李营街道','Villagename':'北孙庄村','orgId':303},{'Townname':'李营街道','Villagename':'北杨庄村','orgId':304},{'Townname':'李营街道','Villagename':'北郑庄村','orgId':301},{'Townname':'李营街道','Villagename':'后双村','orgId':298},{'Townname':'李营街道','Villagename':'林屯村','orgId':259},{'Townname':'李营街道','Villagename':'汪庄村','orgId':264},{'Townname':'李营街道','Villagename':'黄楼村','orgId':256},{'Townname':'长沟镇','Villagename':'党庄村','orgId':361},{'Townname':'长沟镇','Villagename':'刘庄村','orgId':359},{'Townname':'长沟镇','Villagename':'城崖村','orgId':401},{'Townname':'长沟镇','Villagename':'天宝寺村','orgId':387},{'Townname':'长沟镇','Villagename':'新陈村','orgId':621},{'Townname':'长沟镇','Villagename':'曹林村','orgId':364},{'Townname':'长沟镇','Villagename':'梁庄村','orgId':358},{'Townname':'长沟镇','Villagename':'王庙村','orgId':389},{'Townname':'长沟镇','Villagename':'白果树村','orgId':393},{'Townname':'长沟镇','Villagename':'秦咀村','orgId':367},{'Townname':'长沟镇','Villagename':'范庄村','orgId':385},{'Townname':'长沟镇','Villagename':'蔡北村','orgId':383},{'Townname':'长沟镇','Villagename':'钱海村','orgId':363}]})";
		int start = jsonp.indexOf("(");
		int end = jsonp.indexOf(")");
		jsonp = jsonp.substring(start + 1, end);
		JSONObject jodata = JSONObject.parseObject(jsonp);
		return jodata;
	}

}
