package com.shadt.grabdata.dao;

import com.shadt.core.dao.BaseDao;
import com.shadt.grabdata.entity.Person;

/**
 * 人员信息数据访问层接口
 * @author SongR
 *
 */
public interface PersonDao extends BaseDao<Person>{

}
