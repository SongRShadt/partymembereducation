package com.shadt.grabdata.dao.impl;

import com.shadt.core.dao.impl.BaseDaoImpl;
import com.shadt.grabdata.dao.PersonDao;
import com.shadt.grabdata.entity.Person;

/**
 * 人员信息数据访问层实现类
 * @author SongR
 *
 */
public class PersonDaoImpl extends BaseDaoImpl<Person> implements PersonDao{

}
