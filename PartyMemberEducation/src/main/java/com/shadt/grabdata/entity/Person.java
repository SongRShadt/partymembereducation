package com.shadt.grabdata.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

/**
 * 人员信息
 * @author SongR
 *
 */
@Entity
@Table(name = "pme_person")
public class Person {
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	@Column(name = "id")
	private Long id;
	@Column(name = "name")
	private String name;// 姓名
	@Column(name = "sid")
	private String sid;// 身份证号
	@Column(name = "sbrith")
	private String sbrith;// 出生日期
	@Column(name = "sex")
	private String sex;// 性别
	@Column(name = "phone")
	private String phone;// 电话号码
	@Column(name = "zzmm")
	private String zzmm;// 组织面貌
	@Column(name = "zw")
	private String zw;// 职务
	@Column(name = "butieflag")
	private Integer butieflag;
	@Column(name = "villageid")
	private String villageid;//村组织编号
	@Column(name = "group")
	private Integer group;//用户类型
	@Column(name = "groupname")
	private String groupname;//用户类型名称
	
	
	
	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getSid() {
		return sid;
	}

	public void setSid(String sid) {
		this.sid = sid;
	}

	public String getSbrith() {
		return sbrith;
	}

	public void setSbrith(String sbrith) {
		this.sbrith = sbrith;
	}

	public String getSex() {
		return sex;
	}

	public void setSex(String sex) {
		this.sex = sex;
	}

	public String getPhone() {
		return phone;
	}

	public void setPhone(String phone) {
		this.phone = phone;
	}

	public String getZzmm() {
		return zzmm;
	}

	public void setZzmm(String zzmm) {
		this.zzmm = zzmm;
	}

	public String getZw() {
		return zw;
	}

	public void setZw(String zw) {
		this.zw = zw;
	}

	public Integer getButieflag() {
		return butieflag;
	}

	public void setButieflag(Integer butieflag) {
		this.butieflag = butieflag;
	}

	public String getVillageid() {
		return villageid;
	}

	public void setVillageid(String villageid) {
		this.villageid = villageid;
	}

	public Integer getGroup() {
		return group;
	}

	public void setGroup(Integer group) {
		this.group = group;
	}

	public String getGroupname() {
		return groupname;
	}

	public void setGroupname(String groupname) {
		this.groupname = groupname;
	}
	
	
}
