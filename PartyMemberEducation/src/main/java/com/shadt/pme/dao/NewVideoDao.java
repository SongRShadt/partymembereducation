package com.shadt.pme.dao;

import com.shadt.core.dao.BaseDao;
import com.shadt.pme.entity.Video;

/**
 * 视频数据访问层接口
 * @author SongR
 *
 */
public interface NewVideoDao extends BaseDao<Video>{

}
