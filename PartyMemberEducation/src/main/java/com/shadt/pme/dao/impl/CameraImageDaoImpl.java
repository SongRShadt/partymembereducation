package com.shadt.pme.dao.impl;

import org.springframework.stereotype.Repository;

import com.shadt.core.dao.impl.BaseDaoImpl;
import com.shadt.pme.dao.CameraImageDao;
import com.shadt.pme.entity.CameraImage;

/**
 * 摄像头截图数据访问层接口
 * @author SongR
 *
 */
@Repository
public class CameraImageDaoImpl extends BaseDaoImpl<CameraImage> implements CameraImageDao{

}
