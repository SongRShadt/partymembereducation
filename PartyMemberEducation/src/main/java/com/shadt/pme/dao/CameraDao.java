package com.shadt.pme.dao;

import com.shadt.core.dao.BaseDao;
import com.shadt.pme.entity.CameraInfo;

/**
 * 摄像头信息数据访问层接口
 * @author SongR
 *
 */
public interface CameraDao extends BaseDao<CameraInfo>{

}
