package com.shadt.pme.dao;

import com.shadt.core.dao.BaseDao;
import com.shadt.pme.entity.CameraImage;

/**
 * 摄像头截图
 * @author SongR
 *
 */
public interface CameraImageDao extends BaseDao<CameraImage>{

}
