package com.shadt.pme.dao;

import com.shadt.core.dao.BaseDao;
import com.shadt.pme.entity.VillageInfo;

/**
 * 村信息数据访问层接口
 * @author SongR
 *
 */
public interface VillageDao extends BaseDao<VillageInfo>{

}
