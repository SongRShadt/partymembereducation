package com.shadt.pme.dao.impl;

import org.springframework.stereotype.Repository;

import com.shadt.core.dao.impl.BaseDaoImpl;
import com.shadt.pme.dao.TownDao;
import com.shadt.pme.entity.TownInfo;

/**
 * 镇信息数据访问层实现类
 * @author SongR
 *
 */
@Repository
public class PmeTownDaoImpl extends BaseDaoImpl<TownInfo> implements TownDao{

}
