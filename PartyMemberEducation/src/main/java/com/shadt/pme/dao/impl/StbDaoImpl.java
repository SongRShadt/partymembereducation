package com.shadt.pme.dao.impl;

import org.springframework.stereotype.Repository;

import com.shadt.core.dao.impl.BaseDaoImpl;
import com.shadt.pme.dao.StbDao;
import com.shadt.pme.entity.StbInfo;

/**
 * 机顶盒信息数据访问层实现类
 * @author SongR
 *
 */
@Repository
public class StbDaoImpl extends BaseDaoImpl<StbInfo> implements StbDao{

}
