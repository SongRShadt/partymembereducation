package com.shadt.pme.dao;

import com.shadt.core.dao.BaseDao;
import com.shadt.pme.entity.MettingVideo;

/**
 * 视频会议数据访问层接口
 * @author SongR
 *
 */
public interface MettingVideoDao extends BaseDao<MettingVideo>{

}
