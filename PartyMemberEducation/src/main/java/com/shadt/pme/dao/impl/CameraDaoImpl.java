package com.shadt.pme.dao.impl;

import org.springframework.stereotype.Repository;

import com.shadt.core.dao.impl.BaseDaoImpl;
import com.shadt.pme.dao.CameraDao;
import com.shadt.pme.entity.CameraInfo;

/**
 * 摄像头信息数据访问层实现类
 * @author SongR
 *
 */
@Repository
public class CameraDaoImpl extends BaseDaoImpl<CameraInfo> implements CameraDao{

}
