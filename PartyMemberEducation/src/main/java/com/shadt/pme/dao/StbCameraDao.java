package com.shadt.pme.dao;

import com.shadt.core.dao.BaseDao;
import com.shadt.pme.entity.StbCamera;
/**
 * 机顶盒摄像头绑定数据访问层接口
 * @author SongR
 *
 */
public interface StbCameraDao extends BaseDao<StbCamera>{

}
