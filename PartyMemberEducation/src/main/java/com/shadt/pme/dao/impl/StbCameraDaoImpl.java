package com.shadt.pme.dao.impl;

import org.springframework.stereotype.Repository;

import com.shadt.core.dao.impl.BaseDaoImpl;
import com.shadt.pme.dao.StbCameraDao;
import com.shadt.pme.entity.StbCamera;

/**
 * 摄像头机顶盒绑定数据访问层实现类
 * @author SongR
 *
 */
@Repository
public class StbCameraDaoImpl extends BaseDaoImpl<StbCamera> implements StbCameraDao{

}
