package com.shadt.pme.dao.impl;

import org.springframework.stereotype.Repository;

import com.shadt.core.dao.impl.BaseDaoImpl;
import com.shadt.pme.dao.MettingVideoDao;
import com.shadt.pme.entity.MettingVideo;

/**
 * 视频会议数据访问层实现类
 * @author SongR
 *
 */
@Repository
public class MettingVideoDaoImpl extends BaseDaoImpl<MettingVideo> implements MettingVideoDao{

}
