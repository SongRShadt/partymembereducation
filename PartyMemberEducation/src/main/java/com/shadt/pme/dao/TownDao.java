package com.shadt.pme.dao;

import com.shadt.core.dao.BaseDao;
import com.shadt.pme.entity.TownInfo;

/**
 * 镇信息数据访问层接口
 * @author SongR
 *
 */
public interface TownDao extends BaseDao<TownInfo>{

}
