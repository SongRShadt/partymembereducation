package com.shadt.pme.dao;

import com.shadt.core.dao.BaseDao;
import com.shadt.pme.entity.StbInfo;

/**
 * 机顶盒信息数据访问层接口
 * @author SongR
 *
 */
public interface StbDao extends BaseDao<StbInfo>{

}
