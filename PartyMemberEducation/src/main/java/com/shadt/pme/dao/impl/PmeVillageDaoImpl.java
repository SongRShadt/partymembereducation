package com.shadt.pme.dao.impl;

import org.springframework.stereotype.Repository;

import com.shadt.core.dao.impl.BaseDaoImpl;
import com.shadt.pme.dao.VillageDao;
import com.shadt.pme.entity.VillageInfo;

/**
 * 村信息数据访问层实现类
 * @author SongR
 *
 */
@Repository
public class PmeVillageDaoImpl extends BaseDaoImpl<VillageInfo> implements VillageDao{

}
