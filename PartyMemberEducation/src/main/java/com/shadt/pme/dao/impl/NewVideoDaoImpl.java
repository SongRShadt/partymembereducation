package com.shadt.pme.dao.impl;

import org.springframework.stereotype.Repository;

import com.shadt.core.dao.impl.BaseDaoImpl;
import com.shadt.pme.dao.NewVideoDao;
import com.shadt.pme.entity.Video;

/**
 * 视频数据访问层实现类
 * @author SongR
 *
 */
@Repository
public class NewVideoDaoImpl extends BaseDaoImpl<Video> implements NewVideoDao{

}
