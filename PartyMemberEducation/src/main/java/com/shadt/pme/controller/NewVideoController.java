package com.shadt.pme.controller;

import java.util.Date;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.shadt.core.controller.BaseController;
import com.shadt.pme.entity.Video;
import com.shadt.pme.service.NewVideoService;
/**
 * 视频控制器
 * @author SongR
 *
 */
@Controller
@RequestMapping(value="/video")
public class NewVideoController extends BaseController{
	Logger log = Logger.getLogger(this.getClass());
	@Autowired
	NewVideoService service;
	/**
	 * 视频录制回调
	 */
	@ResponseBody
	@RequestMapping(value="/callback")
	public void recordingCallBack(HttpServletRequest request, HttpSession session, String taskid, String download_url,
			String play_url, Integer duration){
		Video video = new Video();
		video.setDownloadUrl(download_url);
		video.setDuration(duration.toString());
		video.setTaskId(taskid);
		video.setPlayUrl(play_url);
		video.setEndTime(new Date());
		try {
			service.update(video);
		} catch (Exception e) {
			log.error("接口异常！",e);
			e.printStackTrace();
		}
	}
	
}
