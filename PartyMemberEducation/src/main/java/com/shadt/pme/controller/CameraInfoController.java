package com.shadt.pme.controller;

import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.shadt.core.controller.BaseController;
import com.shadt.core.model.Json;
import com.shadt.pme.entity.CameraInfo;
import com.shadt.pme.service.CameraInfoService;

/**
 * 摄像头信息控制器
 * @author SongR
 *
 */
@Controller
@RequestMapping(value="/camera")
public class CameraInfoController extends BaseController{
	Logger log = Logger.getLogger(this.getClass());
	
	@Autowired
	CameraInfoService service;
	
	
	/**
	 * 根据村编号和摄像头类型获取摄像头信息
	 * @return
	 */
	@ResponseBody
	@RequestMapping(value="/getByVillage/{villageId}/{type}",method=RequestMethod.GET)
	public Json getByVillage(@PathVariable String villageId,@PathVariable String type){
		Json j = new Json();
		try {
			CameraInfo camera = service.getByVillage(villageId,type);
			j.setSuccess(true);
			if(null!=camera){
				j.setObj(camera);
				j.setMsg("成功获取摄像头信息！");
			}else{
				j.setMsg("暂无摄像头信息！");
			}		
		} catch (Exception e) {
			j.setMsg("接口异常！");
			log.error(e.toString(),e);
		}
		return j;
	}
	
	/**
	 * 根据摄像头类型获取摄像头信息
	 * @return
	 */
	@ResponseBody
	@RequestMapping(value="/getByType/{stbNo}/{type}",method=RequestMethod.GET)
	public Json getByType(@PathVariable String stbNo,@PathVariable String type){
		Json j = new Json();
		try {
			CameraInfo camera = service.getByType(stbNo,type);
			if(null!=camera){
				j.setMsg("成功获取摄像头信息！");
				j.setObj(camera);
				j.setSuccess(true);
			}else{
				j.setMsg("暂无摄像头信息！");
				j.setObj("");
				j.setSuccess(true);
			}
		} catch (Exception e) {
			j.setMsg("接口异常！");
			j.setObj("");
			j.setSuccess(true);
			e.printStackTrace();
			log.error(e.toString(), e);
		}
		return j;
	}
	
	/**
	 * 按机顶盒和摄像头类型截图并保存
	 * @return
	 */
	@ResponseBody
	@RequestMapping(value="/saveImgByVillage/{villageId}/{type}",method=RequestMethod.GET)
	public Json saveImageByVillaget(HttpServletRequest request,@PathVariable String villageId,@PathVariable String type){
		Json j = new Json();
		try {
			String path = System.getProperty("catalina.home");
			service.saveImageByVillage(villageId,type,path);
			j.setMsg("截图成功！");
			j.setSuccess(true);
		} catch (Exception e) {
			j.setMsg("接口异常！");
			j.setSuccess(false);
			log.error(e.toString(),e);
		}
		return j;
	}
	
	
	/**
	 * 按机顶盒和摄像头类型截图并保存
	 * @return
	 */
	@ResponseBody
	@RequestMapping(value="/saveImage/{stbNo}/{type}",method=RequestMethod.GET)
	public Json saveImage(HttpServletRequest request,@PathVariable String stbNo,@PathVariable String type){
		Json j = new Json();
		try {
			String path = System.getProperty("catalina.home");
			service.saveImage(stbNo,type,path);
			j.setMsg("截图成功！");
			j.setSuccess(true);
		} catch (Exception e) {
			j.setMsg("接口异常！");
			j.setSuccess(false);
			log.error(e.toString(),e);
		}
		return j;
	}
	
	/**
	 * 按机顶盒和摄像头类型进行视频录制
	 * @return
	 */
	@ResponseBody
	@RequestMapping(value="/recording/{stbNo}/{type}",method=RequestMethod.GET)
	public Json recording(HttpServletRequest request,@PathVariable String stbNo,@PathVariable String type){
		Json j = new Json();
		try {
			CameraInfo camera = service.recording(stbNo,type);
			j.setSuccess(true);
			if(null!=camera){
				j.setMsg("开始录制视频！");
				j.setObj(camera);
			}else{
				j.setMsg("摄像头不存在！");
			}
		} catch (Exception e) {
			j.setMsg("接口异常！");
			j.setSuccess(false);
			log.error(e.toString(),e);
		}
		return j;
	}
	
	/**
	 * 根据机摄像头类型获取镇对应的摄像头列表
	 * @return
	 */
	@ResponseBody
	@RequestMapping(value="/town/{stbNo}/{townId}/{cType}",method=RequestMethod.GET)
	public Json getByTown(@PathVariable String stbNo,@PathVariable String townId,@PathVariable String cType,Integer start,Integer size){
		Json j = new Json();
		try {
			List<CameraInfo> cs = service.getByTown(stbNo,townId,cType,start,size);
			j.setSuccess(true);
			if(cs.size()>0){
				j.setMsg("{'msg':'成功获取摄像头信息！','count':'"+service.countCamera(stbNo, townId, cType)+"'}");
				j.setObj(cs);
			}else{
				j.setMsg("{'msg':'暂无摄像头信息！'}");
			}
		} catch (Exception e) {
			j.setMsg("接口异常！");
			log.error(e.toString(),e);
		}
		return j;
	}
	
}
