package com.shadt.pme.controller;

import java.util.List;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.shadt.core.controller.BaseController;
import com.shadt.core.model.Json;
import com.shadt.pme.entity.TownInfo;
import com.shadt.pme.service.TownService;

/**
 * 镇控制器
 * @author SongR
 *
 */
@Controller
@RequestMapping(value="/town")
public class TownController extends BaseController{
	Logger log = Logger.getLogger(this.getClass());
	
	@Autowired
	TownService service;
	
	
	/**
	 * 获取镇列表
	 * @return
	 */
	@ResponseBody
	@RequestMapping(value="/get",method=RequestMethod.GET)
	public Json getTown(String townId,Integer start,Integer size){
		Json j = new Json();
		try {
			List<TownInfo> ts = service.getTown(townId,start,size);
			if(ts.size()>0){
				j.setMsg("{'msg':'成功获取镇列表！','count':'"+service.countTown(townId)+"'}");
				j.setObj(ts);
				j.setSuccess(true);
			}else{
				j.setMsg("{'msg':'暂无镇信息！'");
				j.setSuccess(true);
			}
		} catch (Exception e) {
			j.setMsg("接口异常！");
			log.error(e.toString(),e);
			e.printStackTrace();
		}
		return j;
	}
	
}
