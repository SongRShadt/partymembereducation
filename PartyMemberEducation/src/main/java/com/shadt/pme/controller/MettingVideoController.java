package com.shadt.pme.controller;

import java.util.List;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.shadt.core.controller.BaseController;
import com.shadt.core.model.Json;
import com.shadt.pme.entity.MettingVideo;
import com.shadt.pme.service.MettingVideoService;

/**
 * 视频会议控制器
 * @author SongR
 *
 */
@Controller
@RequestMapping(value="/mettingvideo")
public class MettingVideoController extends BaseController{

	Logger log = Logger.getLogger(this.getClass());
	
	@Autowired
	MettingVideoService service;
	/**
	 * 获取所有视频会议
	 * @return
	 */
	@ResponseBody
	@RequestMapping(value="/all/{stbNo}",method=RequestMethod.GET)
	public Json getAll(@PathVariable String stbNo,Integer start,Integer size){
		Json j = new Json();
		try {
			List<MettingVideo> mvArray = service.getAll(stbNo,start,size);
			j.setSuccess(true);
			if(mvArray.size()>0){
				j.setObj(mvArray);
				j.setMsg("成功获取视频会议列表！");
			}else{
				j.setMsg("暂无视频会议！");
			}
		} catch (Exception e) {
			j.setMsg("接口异常！");
			e.printStackTrace( );
			log.error("获取所有视频会议异常！",e);
		}
		return j;
	}
	
	
}
