package com.shadt.pme.controller;

import java.util.ArrayList;
import java.util.List;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.shadt.core.controller.BaseController;
import com.shadt.core.model.Json;
import com.shadt.pme.entity.VillageInfo;
import com.shadt.pme.service.VillageService;

/**
 * 村控制器
 * @author SongR
 *
 */
@Controller
@RequestMapping(value="/village")
public class PmeVillageController extends BaseController{
	Logger log = Logger.getLogger(this.getClass());
	
	@Autowired
	VillageService service;
	
	
	/**
	 * 根据机顶盒获取对应村信息
	 * @return
	 */
	@ResponseBody
	@RequestMapping(value="/stb/{stbNo}")
	public Json getByStb(@PathVariable String stbNo){
		Json j = new Json();
		try {
			VillageInfo v = service.getByStb(stbNo);
			if(v!=null){
				j.setMsg("成功获取村信息！");
				j.setObj(v);
				j.setSuccess(true);
			}else{
				j.setMsg("该机顶盒暂无村绑定！");
				j.setSuccess(true);
			}
		} catch (Exception e) {
			j.setMsg("接口异常！");
			j.setSuccess(false);
			log.error(e.toString(),e);
			e.printStackTrace();
		}
		return j;
	}
	
	/**
	 * 获取镇拥有的村
	 * @return
	 */
	@ResponseBody
	@RequestMapping(value="/town/{townId}",method=RequestMethod.GET)
	public Json getByTown(@PathVariable String townId,String villageId,Integer start,Integer size){
		Json j = new Json();
		try {
			List<VillageInfo> vs = new ArrayList<VillageInfo>();
			String count ="1";
			if("".equals(villageId)||null==villageId||"null".equals(villageId)){
				vs=service.getByTown(townId,start,size);
				count=service.countVillage(townId);
			}else{
				vs=service.get(villageId);
			}
			if(vs.size()>0){
				j.setMsg("{'msg':'成功获取村列表！','count':'"+count+"'}");
				j.setObj(vs);
				j.setSuccess(true);
			}else{
				j.setMsg("{'msg':'该镇暂无村列表！'}");
				j.setSuccess(true);
			}
		} catch (Exception e) {
			j.setMsg("接口异常！");
			j.setSuccess(false);
			log.error(e.toString(),e);
			e.printStackTrace();
		}
		return j;
	}
	
	
	
	/**
	 * 获取所有镇级以上节点
	 * @return
	 */
	@ResponseBody
	@RequestMapping(value="/gettown",method={RequestMethod.GET})
	public Json getTown(Integer start,Integer size){
		Json j = new Json();
		try {
			List<VillageInfo> villageArray = service.getTown(start,size);
			if(villageArray.size()>0){
				j.setMsg("成功获取县镇！");
				j.setObj(villageArray);
			}else{
				j.setMsg("暂无县镇信息！");
			}
		} catch (Exception e) {
			j.setMsg("接口异常！");
			j.setSuccess(false);
			log.error("接口gettown异常:",e);
		}
		return j;
	}
	
	/**
	 * 根据镇编号获取村列表
	 * 驻村联户模块
	 * @return
	 */
	@ResponseBody
	@RequestMapping(value="/get/{townid}",method=RequestMethod.GET)
	public Json getVillage(@PathVariable String townid,String villageId,Integer start,Integer size){
		Json j = new Json();
		try {
			List<VillageInfo> vs =service.getZclh(townid, villageId, start, size);
			if(vs.size()>0){
				j.setMsg("{'msg':'成功获取驻村联户信息！','count':'"+service.countZclh(townid)+"'}");
				j.setObj(vs);
			}else{
				j.setMsg("{'msg':'该镇暂无村列表！'}");
			}
			j.setSuccess(true);
		} catch (Exception e) {
			j.setMsg("获取驻村联户信息异常！");
			log.error("接口异常！",e);
		}
		return j;
	}
	

	/**
	 * 验证本村有没有驻村干部
	 * @return
	 */
	@ResponseBody
	@RequestMapping(value="/checkzclh/{stbNo}",method=RequestMethod.GET)
	public Json checkZclh(@PathVariable String stbNo){
		Json j = new Json();
		try {
			VillageInfo v = service.getByStb(stbNo);
			if(null!=v){
				j.setObj(v);
				if(v.getLevel()==2){
					List<VillageInfo> vs = service.getZclh(null, v.getVillageId(), null, null);
					if(vs.size()>0){
						j.setMsg("{'msg':'成功获取驻村联户信息！','ishave':true}");
					}else{
						j.setMsg("{'msg':'暂无驻村连户信息！','ishave':false}");
					}
				}else if(v.getLevel()==1){
					List<VillageInfo> vs = service.getZclh(v.getTownId(), null, null, null);
					if(vs.size()>0){
						j.setMsg("{'msg':'成功获取驻村联户信息！','ishave':true}");
					}else{
						j.setMsg("{'msg':'暂无驻村连户信息！','ishave':false}");
					}
				}else{
					j.setMsg("{'msg':'不需要权限！','ishave':true}");
				}
			}else{
				j.setMsg("机顶盒信息异常！请联系管理员！");
			}
			j.setSuccess(true);
		} catch (Exception e) {
			j.setMsg("获取驻村联户信息异常！");
			log.error("接口异常！",e);
			e.printStackTrace();
		}
		return j;
	}
	
	
}
