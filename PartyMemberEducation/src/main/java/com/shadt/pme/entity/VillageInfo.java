package com.shadt.pme.entity;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "village_info")
public class VillageInfo implements Serializable{
	private static final long serialVersionUID = 1L;
	@Id
	@Column(name = "villageid")
	private String villageId;//编号
	@Column(name = "villagename")
	private String villageName;//村名称
	@Column(name = "villagekey")
	private String villageKey;//对应cms村key
	@Column(name = "townid")
	private String townId;//镇编号
	@Column(name = "orgcode")
	private String orgCode;//orgCode
	@Column(name = "villagesort")
	private Long sort;//排序
	@Column(name="villagelevel")
	private Integer level;//级别(0、县 1、镇 2、村)
	@Column(name="villagestatus",nullable=false,columnDefinition="INT default 0")
	private Integer status;//状态(-1、删除 0、可用 1、不可用)
	
	public String getVillageId() {
		return villageId;
	}
	public void setVillageId(String villageId) {
		this.villageId = villageId;
	}
	public String getVillageName() {
		return villageName;
	}
	public void setVillageName(String villageName) {
		this.villageName = villageName;
	}
	public String getVillageKey() {
		return villageKey;
	}
	public void setVillageKey(String villageKey) {
		this.villageKey = villageKey;
	}
	public String getTownId() {
		return townId;
	}
	public void setTownId(String townId) {
		this.townId = townId;
	}
	public String getOrgCode() {
		return orgCode;
	}
	public void setOrgCode(String orgCode) {
		this.orgCode = orgCode;
	}
	public Long getSort() {
		return sort;
	}
	public void setSort(Long sort) {
		this.sort = sort;
	}
	public Integer getLevel() {
		return level;
	}
	public void setLevel(Integer level) {
		this.level = level;
	}
	public Integer getStatus() {
		return status;
	}
	public void setStatus(Integer status) {
		this.status = status;
	}
}
