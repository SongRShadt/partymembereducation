package com.shadt.pme.entity;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
/**
 * 录制视频
 * @author SongR
 *
 */
@Entity
@Table(name = "video")
public class Video {
	@Id
	@Column(name="id")
	private String id;//编号
	@Column(name="starttime")
	private Date startTime;//开始时间
	@Column(name="endtime")
	private Date endTime;//结束时间
	@Column(name="camerano")
	private String cameraNo;//摄像头编号
	@Column(name="taskid")
	private String taskId;//任务编号
	@Column(name="download_url")
	private String downloadUrl;//下载地址
	@Column(name="play_url")
	private String playUrl;//播放地址
	@Column(name="duration")
	private String duration;//时长
	@Column(name="video_type")
	private Integer type;//录制类型
	@Column(name="video_status")
	private Integer status;//状态(-1、删除 0、可用 1、不可用)

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public Date getStartTime() {
		return startTime;
	}

	public void setStartTime(Date startTime) {
		this.startTime = startTime;
	}

	public Date getEndTime() {
		return endTime;
	}

	public void setEndTime(Date endTime) {
		this.endTime = endTime;
	}

	public String getCameraNo() {
		return cameraNo;
	}

	public void setCameraNo(String cameraNo) {
		this.cameraNo = cameraNo;
	}

	public String getTaskId() {
		return taskId;
	}

	public void setTaskId(String taskId) {
		this.taskId = taskId;
	}

	public String getDownloadUrl() {
		return downloadUrl;
	}

	public void setDownloadUrl(String downloadUrl) {
		this.downloadUrl = downloadUrl;
	}

	public String getPlayUrl() {
		return playUrl;
	}

	public void setPlayUrl(String playUrl) {
		this.playUrl = playUrl;
	}

	public String getDuration() {
		return duration;
	}

	public void setDuration(String duration) {
		this.duration = duration;
	}

	public Integer getType() {
		return type;
	}

	public void setType(Integer type) {
		this.type = type;
	}

	public Integer getStatus() {
		return status;
	}

	public void setStatus(Integer status) {
		this.status = status;
	}
}
