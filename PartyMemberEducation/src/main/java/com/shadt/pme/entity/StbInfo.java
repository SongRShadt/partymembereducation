package com.shadt.pme.entity;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

/**
 * 机顶盒信息
 * @author SongR
 *
 */
@Entity
@Table(name = "stb_info")
public class StbInfo implements Serializable{
	private static final long serialVersionUID = 1L;
	@Id
	@Column(name = "stbno")
	private String stbNo;
	@Column(name = "villageid")
	private String villageId;
	@Column(name = "remark")
	private String remark;
	@Column(name = "sort")
	private Long sort;
	public String getStbNo() {
		return stbNo;
	}
	public void setStbNo(String stbNo) {
		this.stbNo = stbNo;
	}
	public String getVillageId() {
		return villageId;
	}
	public void setVillageId(String villageId) {
		this.villageId = villageId;
	}
	public String getRemark() {
		return remark;
	}
	public void setRemark(String remark) {
		this.remark = remark;
	}
	public Long getSort() {
		return sort;
	}
	public void setSort(Long sort) {
		this.sort = sort;
	}
	
}
