package com.shadt.pme.entity;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

/**
 * 镇信息
 * 
 * @author SongR
 *
 */
@Entity
@Table(name = "town_info")
public class TownInfo implements Serializable {
	private static final long serialVersionUID = 1L;
	@Id
	@Column(name = "townid")
	private String townId;//镇编号
	@Column(name = "townname")
	private String townName;//镇名称
	@Column(name = "townkeyvalue")
	private String townKeyValue;//镇key
	@Column(name = "townsort")
	private Long sort;//排序

	public String getTownId() {
		return townId;
	}

	public void setTownId(String townId) {
		this.townId = townId;
	}

	public String getTownName() {
		return townName;
	}

	public void setTownName(String townName) {
		this.townName = townName;
	}

	public String getTownKeyValue() {
		return townKeyValue;
	}

	public void setTownKeyValue(String townKeyValue) {
		this.townKeyValue = townKeyValue;
	}

	public Long getSort() {
		return sort;
	}

	public void setSort(Long sort) {
		this.sort = sort;
	}
}
