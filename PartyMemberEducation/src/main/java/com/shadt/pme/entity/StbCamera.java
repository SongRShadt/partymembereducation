package com.shadt.pme.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

/**
 * 机顶盒摄像头绑定表
 * @author SongR
 *
 */
@Entity
@Table(name = "stb_camera_rlt")
public class StbCamera {
	@Id
	@Column(name="stbno")
	private String stbNo;
	@Column(name="camerano")
	private String cameraNo;
	@Column(name="rlt_type")
	private String rltType;
	@Column(name="rlt_sort")
	private Long sort;
	@Column(name="remark")
	private String remark;

	public String getStbNo() {
		return stbNo;
	}

	public void setStbNo(String stbNo) {
		this.stbNo = stbNo;
	}

	public String getCameraNo() {
		return cameraNo;
	}

	public void setCameraNo(String cameraNo) {
		this.cameraNo = cameraNo;
	}

	public String getRltType() {
		return rltType;
	}

	public void setRltType(String rltType) {
		this.rltType = rltType;
	}

	public Long getSort() {
		return sort;
	}

	public void setSort(Long sort) {
		this.sort = sort;
	}

	public String getRemark() {
		return remark;
	}

	public void setRemark(String remark) {
		this.remark = remark;
	}

}
