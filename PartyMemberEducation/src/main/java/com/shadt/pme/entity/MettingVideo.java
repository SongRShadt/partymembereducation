package com.shadt.pme.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

/**
 * 视频会议视频
 * @author SongR
 *
 */
@Entity
@Table(name = "metting_video")
public class MettingVideo {
	@Id
	@Column(name = "id")
	private Long id;//编号
	@Column(name = "video_name")
	private String name;//名称
	@Column(name = "video_url")
	private String url;//路径
	@Column(name = "video_type")
	private Integer type;//类型
	@Column(name = "rtsp")
	private String rtsp;//视频源路径
	@Column(name = "sourceid")
	private String sourceId;//sewise视频源编号
	@Column(name = "village_id")
	private String villageId;//对应区域
	@Column(name = "video_sort")
	private Long sort;//排序
	@Column(name = "video_status")
	private Integer status;//状态（-1、已删除 0、可用 1、不可用）
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getUrl() {
		return url;
	}
	public void setUrl(String url) {
		this.url = url;
	}
	public Integer getType() {
		return type;
	}
	public void setType(Integer type) {
		this.type = type;
	}
	public String getRtsp() {
		return rtsp;
	}
	public void setRtsp(String rtsp) {
		this.rtsp = rtsp;
	}
	public String getSourceId() {
		return sourceId;
	}
	public void setSourceId(String sourceId) {
		this.sourceId = sourceId;
	}
	public String getVillageId() {
		return villageId;
	}
	public void setVillageId(String villageId) {
		this.villageId = villageId;
	}
	public Long getSort() {
		return sort;
	}
	public void setSort(Long sort) {
		this.sort = sort;
	}
	public Integer getStatus() {
		return status;
	}
	public void setStatus(Integer status) {
		this.status = status;
	}
}
