package com.shadt.pme.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

/**
 * 视频会议视频与村关系
 * @author SongR
 *
 */
@Entity
@Table(name = "metting_village_video")
public class MettingVillageVideo {
	@Id
	@Column(name = "id")
	private Long id;
	@Column(name = "villageid")
	private String villageId;//村编号
	@Column(name = "videoid")
	private Long videoId;//视频编号
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	public String getVillageId() {
		return villageId;
	}
	public void setVillageId(String villageId) {
		this.villageId = villageId;
	}
	public Long getVideoId() {
		return videoId;
	}
	public void setVideoId(Long videoId) {
		this.videoId = videoId;
	}
}
