package com.shadt.pme.entity;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

/**
 * 摄像头信息
 * 
 * @author SongR
 *
 */
@Entity
@Table(name = "camera_info")
public class CameraInfo implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@Column(name = "camerano")
	private String id;//编号
	@Column(name = "cameraname")
	private String cameraName;//摄像头名称
	@Column(name = "cameraurl")
	private String cameraUrl;//摄像头播放地址
	@Column(name = "cameratype")
	private String cameraType;//摄像头类型
	@Column(name = "imgurl")
	private String imgUrl;//摄像头封面图路径
	@Column(name = "streamid")
	private String streamId;//视频流编号
	@Column(name = "villageid")
	private String villageId;//所属村
	@Column(name = "camerasort")
	private Long sort;

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getCameraName() {
		return cameraName;
	}

	public void setCameraName(String cameraName) {
		this.cameraName = cameraName;
	}

	public String getCameraUrl() {
		return cameraUrl;
	}

	public void setCameraUrl(String cameraUrl) {
		this.cameraUrl = cameraUrl;
	}

	public String getCameraType() {
		return cameraType;
	}

	public void setCameraType(String cameraType) {
		this.cameraType = cameraType;
	}

	public String getImgUrl() {
		return imgUrl;
	}

	public void setImgUrl(String imgUrl) {
		this.imgUrl = imgUrl;
	}

	public String getStreamId() {
		return streamId;
	}

	public void setStreamId(String streamId) {
		this.streamId = streamId;
	}

	public String getVillageId() {
		return villageId;
	}

	public void setVillageId(String villageId) {
		this.villageId = villageId;
	}

	public Long getSort() {
		return sort;
	}

	public void setSort(Long sort) {
		this.sort = sort;
	}

}
