package com.shadt.pme.service;

import java.util.List;

import com.shadt.pme.entity.TownInfo;

/**
 * 镇信息业务逻辑层接口
 * @author SongR
 *
 */
public interface TownService {

	/**
	 * 获取镇列表
	 */
	List<TownInfo> getTown(String townId, Integer start, Integer size)throws Exception;

	/**
	 * 获取镇数量
	 */
	String countTown(String townId) throws Exception;
	
	
}
