package com.shadt.pme.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.shadt.pme.dao.MettingVideoDao;
import com.shadt.pme.dao.VillageDao;
import com.shadt.pme.entity.MettingVideo;
import com.shadt.pme.entity.VillageInfo;
import com.shadt.pme.service.MettingVideoService;

/**
 * 视频会议业务逻辑层实现类
 * 
 * @author SongR
 *
 */
@Service
public class MettingVideoServiceImpl implements MettingVideoService {

	
	@Autowired
	MettingVideoDao dao;
	@Autowired
	VillageDao vdao;

	/**
	 * 实现获取视频会议信息
	 */
	public List<MettingVideo> getAll(String stbNo, Integer start, Integer size) throws Exception {
		// TODO Auto-generated method stub
		String vHql = "select a from VillageInfo a,StbInfo b where a.status=0 and a.villageId=b.villageId and b.stbNo='"+stbNo+"'";
		VillageInfo village = vdao.get(vHql);
		String hql = "from MettingVideo a where a.status=0 and a.type=0";
		switch (village.getLevel()) {
			case 0:
				break;
			case 1:
				hql += " and a.id in (select videoId from MettingVillageVideo where villageId='"+village.getVillageId()+"')";
				break;
			case 2:
				hql += " and a.id in (select videoId from MettingVillageVideo where villageId='"+village.getVillageId()+"')";
				break;
			default:
				break;
		}
		hql+=" order by a.sort";
		List<MettingVideo> mvArray = dao.find(hql);
		return mvArray;
	}

}
