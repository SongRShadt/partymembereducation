package com.shadt.pme.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.shadt.pme.dao.NewVideoDao;
import com.shadt.pme.entity.Video;
import com.shadt.pme.service.NewVideoService;

/**
 * 视频业务逻辑层实现类
 * @author SongR
 *
 */
@Service
public class NewVideoServiceImpl implements NewVideoService{

	@Autowired
	NewVideoDao dao ;
	/**
	 * 实现修改视频信息
	 */
	public void update(Video video) throws Exception {
		Video v = dao.get("from Video where taskId='"+video.getTaskId()+"'");
		if(null!=v){
			v.setDownloadUrl(video.getDownloadUrl());
			v.setDuration(video.getDuration());
			v.setTaskId(video.getTaskId());
			v.setPlayUrl(video.getPlayUrl());
			v.setEndTime(video.getEndTime());
			v.setStatus(0);
			if(Integer.parseInt(v.getDuration())>20){
				dao.update(v);
			}
		}
		
	}

}
