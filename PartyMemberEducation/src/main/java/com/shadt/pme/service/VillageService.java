package com.shadt.pme.service;

import java.util.List;

import com.shadt.pme.entity.VillageInfo;

/**
 * 村信息业务逻辑层控制器
 * @author SongR
 *
 */
public interface VillageService {
	/**
	 * 根据mac地址获取村信息
	 * @param stbNo
	 * @return
	 */
	VillageInfo getByStb(String stbNo) throws Exception;

	/**
	 * 根据镇编号获取村列表
	 */
	List<VillageInfo> getByTown(String townId, Integer start, Integer size) throws Exception;
	
	/**
	 * 获取镇的村数量 
	 */
	String countVillage(String townId) throws Exception;

	/**
	 * 获取村信息
	 */
	List<VillageInfo> get(String villageId) throws Exception;

	/**
	 * 获取镇级以上信息
	 */
	List<VillageInfo> getTown(Integer start, Integer size) throws Exception;

	/**
	 * 驻村连户 
	 */
	List<VillageInfo> getZclh(String townId,String villageId, Integer start, Integer size)  throws Exception;

	
	/**
	 * 获取镇的村数量 
	 */
	String countZclh(String townId) throws Exception;

	/**
	 * 验证本村有没有驻村连户
	 */
	List<VillageInfo> checkZclh(String stbNo) throws Exception;

}
