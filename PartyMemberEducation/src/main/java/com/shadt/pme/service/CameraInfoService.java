package com.shadt.pme.service;

import java.util.List;

import com.shadt.pme.entity.CameraInfo;

/**
 * 摄像头信息 业务逻辑层接口
 * 
 * @author SongR
 *
 */
public interface CameraInfoService {

	/**
	 * 根据摄像头类型获取摄像头信息
	 */
	CameraInfo getByType(String stbNo, String type) throws Exception;

	/**
	 * 摄像头截图
	 */
	void saveImage(String stbNo, String type, String path) throws Exception;

	/**
	 * 获取摄像头列表
	 */
	List<CameraInfo> getByTown(String stbNo, String townId, String cType, Integer start, Integer size) throws Exception;

	/**
	 * 获取摄像头数量
	 */
	String countCamera(String stbNo, String townId, String cType) throws Exception;

	/**
	 * 根据村获取摄像头
	 */
	CameraInfo getByVillage(String villageId, String type) throws Exception;

	/**
	 * 保存村获取摄像头图像
	 */
	void saveImageByVillage(String villageId, String type, String path) throws Exception;

	/**
	 * 摄像头视频录制
	 */
	CameraInfo recording(String stbNo, String type) throws Exception;

}
