package com.shadt.pme.service;

import com.shadt.pme.entity.Video;

/**
 * 视频业务逻辑层接口
 * @author SongR
 *
 */
public interface NewVideoService {

	/**
	 * 修改视频信息
	 */
	void update(Video video) throws Exception;

}
