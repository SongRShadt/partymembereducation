package com.shadt.pme.service.impl;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.UUID;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.alibaba.fastjson.JSONObject;
import com.shadt.core.util.ConfigUtil;
import com.shadt.core.util.DownLoadUtil;
import com.shadt.core.util.HttpClientUtil;
import com.shadt.pme.dao.CameraDao;
import com.shadt.pme.dao.CameraImageDao;
import com.shadt.pme.dao.NewVideoDao;
import com.shadt.pme.dao.StbDao;
import com.shadt.pme.entity.CameraImage;
import com.shadt.pme.entity.CameraInfo;
import com.shadt.pme.entity.StbInfo;
import com.shadt.pme.entity.Video;
import com.shadt.pme.service.CameraInfoService;

/**
 * 摄像头信息业务逻辑层实现类
 * 
 * @author SongR
 *
 */
@Service
public class CameraInfoServiceImpl implements CameraInfoService {
	
	Logger log = Logger.getLogger(this.getClass());

	@Autowired
	CameraDao dao;
	@Autowired
	StbDao stbDao;
	@Autowired
	CameraImageDao ciDao;
	
	@Autowired
	NewVideoDao vDao;
	
	/**
	 * 实现根据摄像头类型获取摄像头信息
	 */
	public CameraInfo getByType(String stbNo, String type) throws Exception {
		StbInfo stb = stbDao.get("from StbInfo where stbno ='" + stbNo + "'");
		CameraInfo c = null;
		if (null != stb) {
			c = dao.get("from CameraInfo where villageId='" + stb.getVillageId() + "' and cameraType='" + type + "'  order by sort");
		}
		return c;
	}
	/**
	 * 实现个根据村获取摄像头信息
	 */
	public CameraInfo getByVillage(String villageId, String type) throws Exception {
		CameraInfo c = dao.get("from CameraInfo where villageId='" + villageId + "' and cameraType='" + type + "'  order by sort");
		return c;
	}

	/**
	 * 保存截图
	 */
	public void saveImage(String stbNo, String type,String path)throws Exception {
		CameraInfo camera = this.getByType(stbNo, type);
		if(null!=camera){
			if(null!=camera.getCameraUrl()&&!"".equals(camera.getCameraUrl())){
				String url[] = camera.getCameraUrl().split("/");
				String ipd[] = url[2].split(":");
				String ip = ipd[0];
				String imageName=UUID.randomUUID()+".jpeg";
				String paths = path+"/webapps/resource/sewise/image";
				DownLoadUtil.download("http://"+ip+"/snaptshot/"+camera.getStreamId()+".jpeg",imageName, paths);
				CameraImage ci = new CameraImage();
				ci.setCreatTime(new Date());
				ci.setImageUrl("/resource/sewise/image/"+imageName);
				ci.setCameraId(camera.getId());
				ciDao.save(ci);
			}
		}
	}
	
	
	/**
	 * 截图保存村摄像头图像
	 */
	public void saveImageByVillage(String villageId, String type, String path) throws Exception {
		CameraInfo camera = this.getByVillage(villageId, type);
		if(null!=camera){
			if(null!=camera.getCameraUrl()&&!"".equals(camera.getCameraUrl())){
				String url[] = camera.getCameraUrl().split("/");
				String ipd[] = url[2].split(":");
				String ip = ipd[0];
				String imageName=UUID.randomUUID()+".jpeg";
				String paths = path+"/webapps/resource/sewise/image";
				DownLoadUtil.download("http://"+ip+"/snaptshot/"+camera.getStreamId()+".jpeg",imageName, paths);
				CameraImage ci = new CameraImage();
				ci.setCreatTime(new Date());
				ci.setImageUrl("/resource/sewise/image/"+imageName);
				ci.setCameraId(camera.getId());
				ciDao.save(ci);
			}
		}
	}
	
	/**
	 * 实现录制视频
	 */
	public CameraInfo recording(String stbNo, String type) throws Exception {
		CameraInfo camera = this.getByType(stbNo, type);
		if(null!=camera){
			if(null!=camera.getCameraUrl()){
				String []ip = camera.getCameraUrl().split("/");
				if(ip.length>3&&!ip[2].trim().equals("")&&null!=camera.getStreamId()&&!"".equals(camera.getStreamId())){
					String httpUrl= "http://"+ip[2]+"/record";
					Map<String,String> map = new HashMap<String,String>();
					map.put("do", "start");
					map.put("streamid", camera.getStreamId());
					map.put("callback", ConfigUtil.get("sewise.callback1"));
					log.error("开始调用sewise录制。。。");
					System.out.println("sewise录制请求地址为："+httpUrl);
					log.error("sewise录制请求地址为："+httpUrl);
//					log.error("sewise录制请求参数为："+JSONObject.toJSONString(map));
					String result = HttpClientUtil.doPost(httpUrl, map);
					log.error("调用sewise录制结束。。。");
					log.error("sewise录制返回结果为："+result);
					System.out.println(result);
					JSONObject obj = JSONObject.parseObject(result);
					if(obj!=null){
						Boolean success = obj.getBoolean("success");
						String taskId = "";
						if(success){
							taskId = obj.getString("taskid");
							camera.setCameraUrl(camera.getCameraUrl()+"?taskid="+taskId);
							Video video = new Video();
							video.setId(UUID.randomUUID().toString());
							video.setStartTime(new Date());
							video.setCameraNo(camera.getId());
							video.setType(2);
							video.setTaskId(taskId);
							video.setStatus(1);
							vDao.save(video);
						}else{
							log.error("sewise录制接口调用异常！");
						}
					}else{
						System.out.println(result);
					}
					
				}
			} 
		}else{
			camera = null;
		}
		return camera;
	}
	
	
	
	/**
	 * 实现获取机顶盒的摄像头列表
	 */
	public List<CameraInfo> getByTown(String stbNo, String townId, String cType,Integer start, Integer size) throws Exception {
		String hql = "select c from StbCamera a,VillageInfo b,CameraInfo c where a.stbNo='"+stbNo+"' and b.townId='"+townId+"' and c.cameraType='"+cType+"' and a.cameraNo=c.id and a.rltType=1 and c.villageId=b.villageId  order by b.sort";
		List<CameraInfo> cs = new ArrayList<CameraInfo>();
		if(null!=start&&null!=size&&size>0){
			cs=dao.findPage(hql, start, size);
		}else{
			cs=dao.find(hql);
		}
		return cs;
	}

	/**
	 * 实现获取摄像头数量
	 */
	public String countCamera(String stbNo, String townId, String cType) throws Exception {
		String hql = "select count(c) from StbCamera a,VillageInfo b,CameraInfo c where a.stbNo='"+stbNo+"' and b.townId='"+townId+"' and c.cameraType='"+cType+"' and a.cameraNo=c.id and a.rltType=1 and c.villageId=b.villageId";
		String count = dao.count(hql).toString();
		return count;
	}
	
}
