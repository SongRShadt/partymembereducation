package com.shadt.pme.service.impl;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import com.shadt.core.util.ConfigUtil;
import com.shadt.core.util.HttpClientUtil;
import com.shadt.pme.dao.VillageDao;
import com.shadt.pme.entity.VillageInfo;
import com.shadt.pme.service.VillageService;

/**
 * 村信息业务逻辑层实现类
 * 
 * @author SongR
 *
 */
@Service
public class PmeVillageServiceImpl implements VillageService {

	Logger log = Logger.getLogger(this.getClass());
	@Autowired
	VillageDao dao;

	/**
	 * 实现根据mac地址获取村信息
	 */
	public VillageInfo getByStb(String stbNo) {
		String hql = "select a from VillageInfo a,StbInfo b where a.villageId =b.villageId and a.status=0 and b.stbNo='"
				+ stbNo + "' order by sort";
		VillageInfo v = dao.get(hql);
		return v;
	}

	/**
	 * 实现根据镇编号获取村列表
	 */
	public List<VillageInfo> getByTown(String townId, Integer start, Integer size) throws Exception {
		String hql = "from VillageInfo where townId='" + townId + "' and status=0 and level=2 order by sort";
		List<VillageInfo> vs = new ArrayList<VillageInfo>();
		if (null != start && null != size && size > 0) {
			vs = dao.findPage(hql, start, size);
		} else {
			vs = dao.find(hql);
		}
		return vs;
	}

	/**
	 * 实现获取镇的村数量
	 */
	public String countVillage(String townId) throws Exception {
		String hql = "select count(*) from VillageInfo where townId=:townId and status = :status and level = :level";
		Map<String, Object> m = new HashMap<String, Object>();
		m.put("townId", townId);
		m.put("status", 0);
		m.put("level", 2);
		String count = dao.count(hql, m).toString();
		return count;
	}

	/**
	 * 实现获取村信息
	 */
	public List<VillageInfo> get(String villageId) {
		List<VillageInfo> vs = new ArrayList<VillageInfo>();
		VillageInfo v = dao.get(VillageInfo.class, villageId);
		vs.add(v);
		return vs;
	}

	/**
	 * 实现获取镇级以上节点
	 */
	public List<VillageInfo> getTown(Integer start, Integer size) throws Exception {
		String hql = "from VillageInfo where status=0 and (level=0 or level=1)";
		List<VillageInfo> villageArray = dao.findPage(hql, start, size);
		return villageArray;
	}

	/**
	 * 驻村联户
	 */
	@SuppressWarnings("rawtypes")
	public List<VillageInfo> getZclh(String townId, String villageId, Integer start, Integer size) {
		// TODO : 驻村联户
		List<VillageInfo> villageArray = new ArrayList<VillageInfo>();
		if ("".equals(villageId) || null == villageId || "null".equals(villageId)) {
			villageArray = dao
					.find("from VillageInfo where townId='" + townId + "' and status=0 and level=2 order by sort");
		} else {
			villageArray = dao.find("from VillageInfo where villageId='" + villageId + "'");
		}
		String jsonp = HttpClientUtil.doGet(ConfigUtil.get("zclh.url"),null);
//		"1({'success':true,'message':'调用成功','data':[{'Townname':'南张街道','Villagename':'东陈村','orgId':125},{'Townname':'南张街道','Villagename':'乔家村','orgId':121},{'Townname':'南张街道','Villagename':'于家村','orgId':108},{'Townname':'南张街道','Villagename':'后店村','orgId':106},{'Townname':'南张街道','Villagename':'孔家村','orgId':135},{'Townname':'南张街道','Villagename':'孙家村','orgId':114},{'Townname':'南张街道','Villagename':'房家村','orgId':141},{'Townname':'南张街道','Villagename':'文郑村','orgId':144},{'Townname':'南张街道','Villagename':'苏家村','orgId':119},{'Townname':'南张街道','Villagename':'赵庙村','orgId':136},{'Townname':'唐口街道','Villagename':'后闫村','orgId':532},{'Townname':'唐口街道','Villagename':'姜庙村','orgId':543},{'Townname':'唐口街道','Villagename':'常户村','orgId':477},{'Townname':'唐口街道','Villagename':'景村','orgId':505},{'Townname':'唐口街道','Villagename':'河东村','orgId':544},{'Townname':'唐口街道','Villagename':'谷庄村','orgId':486},{'Townname':'唐口街道','Villagename':'路口村','orgId':494},{'Townname':'唐口街道','Villagename':'魏楼村','orgId':510},{'Townname':'喻屯镇','Villagename':'九子集村','orgId':611},{'Townname':'喻屯镇','Villagename':'兴福集村','orgId':590},{'Townname':'喻屯镇','Villagename':'南田村','orgId':606},{'Townname':'喻屯镇','Villagename':'城南张村','orgId':582},{'Townname':'喻屯镇','Villagename':'张桥村','orgId':583},{'Townname':'喻屯镇','Villagename':'李户村','orgId':596},{'Townname':'喻屯镇','Villagename':'河湾村','orgId':551},{'Townname':'喻屯镇','Villagename':'谭口集村','orgId':554},{'Townname':'安居街道','Villagename':'东李村','orgId':456},{'Townname':'安居街道','Villagename':'前埝口村','orgId':459},{'Townname':'安居街道','Villagename':'北薛屯村','orgId':467},{'Townname':'安居街道','Villagename':'后埝口村','orgId':460},{'Townname':'安居街道','Villagename':'居北村','orgId':427},{'Townname':'安居街道','Villagename':'张天楼村','orgId':457},{'Townname':'安居街道','Villagename':'满营村','orgId':420},{'Townname':'安居街道','Villagename':'白咀村','orgId':463},{'Townname':'安居街道','Villagename':'西朱庄村','orgId':434},{'Townname':'廿里铺街道','Villagename':'南村','orgId':11},{'Townname':'廿里铺街道','Villagename':'后屯','orgId':39},{'Townname':'廿里铺街道','Villagename':'吕庄','orgId':18},{'Townname':'廿里铺街道','Villagename':'姜庄','orgId':9},{'Townname':'廿里铺街道','Villagename':'朱营','orgId':45},{'Townname':'廿里铺街道','Villagename':'李林','orgId':7},{'Townname':'廿里铺街道','Villagename':'杨场','orgId':23},{'Townname':'廿里铺街道','Villagename':'梁马','orgId':42},{'Townname':'廿里铺街道','Villagename':'王府集','orgId':40},{'Townname':'廿里铺街道','Villagename':'聂庄','orgId':31},{'Townname':'李营街道','Villagename':'前双村','orgId':299},{'Townname':'李营街道','Villagename':'北孙庄村','orgId':303},{'Townname':'李营街道','Villagename':'北杨庄村','orgId':304},{'Townname':'李营街道','Villagename':'北郑庄村','orgId':301},{'Townname':'李营街道','Villagename':'后双村','orgId':298},{'Townname':'李营街道','Villagename':'林屯村','orgId':259},{'Townname':'李营街道','Villagename':'汪庄村','orgId':264},{'Townname':'李营街道','Villagename':'黄楼村','orgId':256},{'Townname':'长沟镇','Villagename':'党庄村','orgId':361},{'Townname':'长沟镇','Villagename':'刘庄村','orgId':359},{'Townname':'长沟镇','Villagename':'城崖村','orgId':401},{'Townname':'长沟镇','Villagename':'天宝寺村','orgId':387},{'Townname':'长沟镇','Villagename':'新陈村','orgId':621},{'Townname':'长沟镇','Villagename':'曹林村','orgId':364},{'Townname':'长沟镇','Villagename':'梁庄村','orgId':358},{'Townname':'长沟镇','Villagename':'王庙村','orgId':389},{'Townname':'长沟镇','Villagename':'白果树村','orgId':393},{'Townname':'长沟镇','Villagename':'秦咀村','orgId':367},{'Townname':'长沟镇','Villagename':'范庄村','orgId':385},{'Townname':'长沟镇','Villagename':'蔡北村','orgId':383},{'Townname':'长沟镇','Villagename':'钱海村','orgId':363}]})";
		JSONObject json = HttpClientUtil.getJsonp(jsonp);
		List<VillageInfo> vs = new ArrayList<VillageInfo>();
		if (json.getBoolean("success")) {
			List<HashMap> list = JSON.parseArray(json.getString("data"), HashMap.class);
			for (HashMap m : list) {
				for (VillageInfo v : villageArray) {
					log.info("----------------");
					log.info(JSONObject.toJSON(m));
					log.info(JSONObject.toJSON(v));
					log.info(m);
					log.info(m.get("Villagename"));
					log.info(m.get("orgId"));
					log.info(v);
					log.info(v.getVillageName());
					log.info(v.getOrgCode());
					log.info("----------------");
					if(m.get("Villagename")!=null&&m.get("orgId")!=null)
					if (m.get("Villagename").toString().equals(v.getVillageName())
							|| m.get("orgId").toString().equals(v.getOrgCode())) {
						vs.add(v);
						break;
					}
				}
			}
		}
		List<VillageInfo> vs1 = new ArrayList<VillageInfo>();
		if(null!=size&&size!=0){
			for (int i = 0; i < vs.size(); i++) {
				if (i < start || i >= start + size) {
					vs1.add(vs.get(i));
				}
			}
		}
		vs.removeAll(vs1);
		return vs;
	}
	/**
	 * 实现获取镇下驻村连户的村数量
	 */
	@SuppressWarnings("rawtypes")
	public String countZclh(String townId) throws Exception {
		List<VillageInfo> villageArray = dao
				.find("from VillageInfo where townId='" + townId + "' and status=0 and level=2 order by sort");
		String jsonp = HttpClientUtil.doGet(ConfigUtil.get("zclh.url"),null);
//		"1({'success':true,'message':'调用成功','data':[{'Townname':'南张街道','Villagename':'东陈村','orgId':125},{'Townname':'南张街道','Villagename':'乔家村','orgId':121},{'Townname':'南张街道','Villagename':'于家村','orgId':108},{'Townname':'南张街道','Villagename':'后店村','orgId':106},{'Townname':'南张街道','Villagename':'孔家村','orgId':135},{'Townname':'南张街道','Villagename':'孙家村','orgId':114},{'Townname':'南张街道','Villagename':'房家村','orgId':141},{'Townname':'南张街道','Villagename':'文郑村','orgId':144},{'Townname':'南张街道','Villagename':'苏家村','orgId':119},{'Townname':'南张街道','Villagename':'赵庙村','orgId':136},{'Townname':'唐口街道','Villagename':'后闫村','orgId':532},{'Townname':'唐口街道','Villagename':'姜庙村','orgId':543},{'Townname':'唐口街道','Villagename':'常户村','orgId':477},{'Townname':'唐口街道','Villagename':'景村','orgId':505},{'Townname':'唐口街道','Villagename':'河东村','orgId':544},{'Townname':'唐口街道','Villagename':'谷庄村','orgId':486},{'Townname':'唐口街道','Villagename':'路口村','orgId':494},{'Townname':'唐口街道','Villagename':'魏楼村','orgId':510},{'Townname':'喻屯镇','Villagename':'九子集村','orgId':611},{'Townname':'喻屯镇','Villagename':'兴福集村','orgId':590},{'Townname':'喻屯镇','Villagename':'南田村','orgId':606},{'Townname':'喻屯镇','Villagename':'城南张村','orgId':582},{'Townname':'喻屯镇','Villagename':'张桥村','orgId':583},{'Townname':'喻屯镇','Villagename':'李户村','orgId':596},{'Townname':'喻屯镇','Villagename':'河湾村','orgId':551},{'Townname':'喻屯镇','Villagename':'谭口集村','orgId':554},{'Townname':'安居街道','Villagename':'东李村','orgId':456},{'Townname':'安居街道','Villagename':'前埝口村','orgId':459},{'Townname':'安居街道','Villagename':'北薛屯村','orgId':467},{'Townname':'安居街道','Villagename':'后埝口村','orgId':460},{'Townname':'安居街道','Villagename':'居北村','orgId':427},{'Townname':'安居街道','Villagename':'张天楼村','orgId':457},{'Townname':'安居街道','Villagename':'满营村','orgId':420},{'Townname':'安居街道','Villagename':'白咀村','orgId':463},{'Townname':'安居街道','Villagename':'西朱庄村','orgId':434},{'Townname':'廿里铺街道','Villagename':'南村','orgId':11},{'Townname':'廿里铺街道','Villagename':'后屯','orgId':39},{'Townname':'廿里铺街道','Villagename':'吕庄','orgId':18},{'Townname':'廿里铺街道','Villagename':'姜庄','orgId':9},{'Townname':'廿里铺街道','Villagename':'朱营','orgId':45},{'Townname':'廿里铺街道','Villagename':'李林','orgId':7},{'Townname':'廿里铺街道','Villagename':'杨场','orgId':23},{'Townname':'廿里铺街道','Villagename':'梁马','orgId':42},{'Townname':'廿里铺街道','Villagename':'王府集','orgId':40},{'Townname':'廿里铺街道','Villagename':'聂庄','orgId':31},{'Townname':'李营街道','Villagename':'前双村','orgId':299},{'Townname':'李营街道','Villagename':'北孙庄村','orgId':303},{'Townname':'李营街道','Villagename':'北杨庄村','orgId':304},{'Townname':'李营街道','Villagename':'北郑庄村','orgId':301},{'Townname':'李营街道','Villagename':'后双村','orgId':298},{'Townname':'李营街道','Villagename':'林屯村','orgId':259},{'Townname':'李营街道','Villagename':'汪庄村','orgId':264},{'Townname':'李营街道','Villagename':'黄楼村','orgId':256},{'Townname':'长沟镇','Villagename':'党庄村','orgId':361},{'Townname':'长沟镇','Villagename':'刘庄村','orgId':359},{'Townname':'长沟镇','Villagename':'城崖村','orgId':401},{'Townname':'长沟镇','Villagename':'天宝寺村','orgId':387},{'Townname':'长沟镇','Villagename':'新陈村','orgId':621},{'Townname':'长沟镇','Villagename':'曹林村','orgId':364},{'Townname':'长沟镇','Villagename':'梁庄村','orgId':358},{'Townname':'长沟镇','Villagename':'王庙村','orgId':389},{'Townname':'长沟镇','Villagename':'白果树村','orgId':393},{'Townname':'长沟镇','Villagename':'秦咀村','orgId':367},{'Townname':'长沟镇','Villagename':'范庄村','orgId':385},{'Townname':'长沟镇','Villagename':'蔡北村','orgId':383},{'Townname':'长沟镇','Villagename':'钱海村','orgId':363}]})";
		JSONObject json = HttpClientUtil.getJsonp(jsonp);
		List<VillageInfo> vs = new ArrayList<VillageInfo>();
		if (json.getBoolean("success")) {
			List<HashMap> list = JSON.parseArray(json.getString("data"), HashMap.class);
			for (HashMap m : list) {
				for (VillageInfo v : villageArray) {
					if (m.get("Villagename").toString().equals(v.getVillageName())
							|| m.get("orgId").toString().equals(v.getOrgCode())) {
						vs.add(v);
					}
				}
			}
		}
		return vs.size() + "";
	}

	public List<VillageInfo> checkZclh(String stbNo) throws Exception {
		// TODO Auto-generated method stub
		return null;
	}

}
