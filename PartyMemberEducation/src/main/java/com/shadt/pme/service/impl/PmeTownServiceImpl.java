package com.shadt.pme.service.impl;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.shadt.pme.dao.TownDao;
import com.shadt.pme.entity.TownInfo;
import com.shadt.pme.service.TownService;

/**
 * 镇信息业务逻辑层实现类
 * @author SongR
 *
 */
@Service
public class PmeTownServiceImpl implements TownService{

	
	@Autowired
	TownDao dao;
	/**
	 * 实现获取镇列表
	 */
	public List<TownInfo> getTown(String townId, Integer start, Integer size) throws Exception {
		String hql = "from TownInfo where townId='"+townId+"' order by sort";
		if(null==townId||"".equals(townId)||townId.equals("all")){
			hql="from TownInfo order by sort";
		}
		List<TownInfo> ts = new ArrayList<TownInfo>();
		if(null!=start&&null!=size&&size>0){
			ts=dao.findPage(hql, start, size);
		}else{
			ts=dao.find(hql);
		}
		return ts;
	}
	/**
	 * 实现获取镇数量
	 */
	public String countTown(String townId) throws Exception {
		String hql = "select count(*) from TownInfo where townId='"+townId+"'";
		if(null==townId||"".equals(townId)||townId.equals("all")){
			hql="select count(*) from TownInfo";
		}
		String count = dao.count(hql).toString();
		return count;
	}

}
