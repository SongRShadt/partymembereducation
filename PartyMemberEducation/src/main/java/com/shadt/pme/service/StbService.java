package com.shadt.pme.service;

import com.shadt.pme.entity.StbInfo;

public interface StbService {

	/**
	 * 根据mac地址获取机顶盒信息
	 * @param stbNo
	 * @return
	 * @throws Exception
	 */
	StbInfo get(String stbNo) throws Exception;

}
