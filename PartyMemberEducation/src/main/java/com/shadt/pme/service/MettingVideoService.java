package com.shadt.pme.service;

import java.util.List;

import com.shadt.pme.entity.MettingVideo;

/**
 * 视频会议业务逻辑层接口
 * @author SongR
 *
 */
public interface MettingVideoService {

	/**
	 * 获取所有视频会议
	 */
	List<MettingVideo> getAll(String stbNo, Integer start, Integer size) throws Exception;

}
