<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<meta name="page-view-size" content="1280*720" />
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<title>会议摄像头</title>
<link rel="stylesheet" href="../generalmeeting/css/hylx.css" type="text/css"></link>
</head>
<body>
	<div class="main">
		<div class="header"></div>
		<div class="content">
			<div class="content_main"> 
				<div class="content_main_top">
				</div>
				<div class="content_main_bottom">
				<div class="next">开始会议</div>
				<div class="msg">请选择录制摄像头!</div>
				</div>
			</div>
		</div>
		<div class="footer"></div>
	</div>
	<script type="text/javascript" src="../generalmeeting/js/jquery-1.8.3.js"></script>
	<script type="text/javascript" src="../generalmeeting/js/config.js"></script>
	<script type="text/javascript" src="../generalmeeting/js/hylx.js"></script>
	
	
</body>
</html>