/**
 * 参数配置
 */
/*美丽家园*/
var mljy_cmsparam={"key":"mljyuan","model":"zclmycjllb"};//美丽家园cms对应参数，key：cms的key值  model：config.properties 中配置的链接对应的键
var mljy_link = ['LRORMULXLUM'];//美丽家园模块直接跳转的链接的栏目key
/*美丽家园详情*/
var mljy_info_cmsparam={"navmodel":"zclmycjllb","contentmodel":"zzjs"};//美丽家园详情页参数，navmodel左侧导航模板，contentmodel右侧内容模板（config.properties中的键值）
var mljy_navBanKeys=['TVPMNIJUFSZ','PYFIRJVZSDD','SXWUUJDBNWW','RHWBGQOPNOF','ETSVKCNDNXE','EFYLEFCBAAQ','QFHAPOVVZRB'];//左侧栏目禁止跳转key（cms的key值）
var mljy_contentbankeys = ['FGSRYZTKOGG'];//内容区域不显示的栏目key
var mljy_er_url="http://172.23.255.67:8010/syncottsdjnwsdj/Entrance/CityModel/ModelEffect/ModelContext/ContextPage.jsp";
/*美丽家园村列表*/
var mljy_cun_cmsparam = {"contentmodel":"zclmycjllb"};
var mljy_cun_bankey = [];

/*驻村联户详情页*/
var zclh_info_cmsparam={"model":"jlxq"};
var zclh_info_banKeys=[""];
var zclh_bf_param={"url":"http://172.23.255.67:8010/syncottsdjnwsdj/Entrance/CityModel/ModelEffect/ModelListItems/ListItemsTransversePage.jsp",
"url1":"http://172.23.255.67:8010/syncottsdjnwsdj/Entrance/CityModel/ModelEffect/ModelItems/ItemsTextPage.jsp"};
/*精准扶贫 */
var jzfp_cmsparam={"key":"RULEXKZIHGH","navmodel":"zzjs","contentmodel":"zzjs"};
var jzfp_navbankeys=["HLWJLKBXVUL"];
var jzfp_contentbankeys=["UKXCMMNZVTS","FWEPALMSVGU"];
var jzfp_er_url="http://172.23.255.67:8010/syncottsdjnwsdj/Entrance/CityModel/ModelEffect/ModelContext/ContextPage.jsp";
/*精准扶贫村列表*/
var jzfp_cun_cmsparam={"contentmodel":"zclmycjllb"};
/*组织建设*/
var zzjs_ygzw_child_url="http://172.23.255.67:8010/syncottsdjnwsdj/Entrance/CityModel/ModelEffect/ModelContext/ContextPage.jsp";
/*组织建设职务key*/
var zzjs_dzbwyh_key="dzbwyh";
var zzjs_zyzfwdw_key="zyzfwdw";
/*
var get_all_key=zzjs_dzbwyh_key+","+zzjs_zyzfwdw_key;*/
var get_all_key="0,";
/**
 * 公用js
 */
var CONFIG = function(){
	return {
		getUrlParam : function(){
			var url = decodeURI(location.search); //获取url中"?"符后的字串
			var theRequest = new Object();
			if (url.indexOf("?") != -1) {
				var str = url.substr(1);
			    strs = str.split("&");
			    for(var i = 0; i < strs.length; i ++) {
			    	theRequest[strs[i].split("=")[0]]=(strs[i].split("=")[1]);
			    }
			}
			return theRequest;
		}		
	};
}();
