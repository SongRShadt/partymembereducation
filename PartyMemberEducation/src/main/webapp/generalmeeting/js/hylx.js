/**
 *会议类型
 */
var HYLX = function() {
	
	var navData = [ 
	{
		'id' : 0,
		'name' : '会议室摄像头',
	},  {
		'id' : 2,
		'name' : '值班室摄像头',
	},];
	
	/**
	 * 初始化内容区域
	 */
	var handleContent = function(param){
		var contentHtml = "";
		$.each(navData,function(i){
			contentHtml+="<div class='btn check"+(i+1)+"' cid='"+this.id+"'>"+this.name+"</div>";
		});
		$(".content_main_top").html(contentHtml);
		$(".check"+click.num).addClass("btn_on");
	}
	var click = {
		num : 1,
		contentsize:2
	};

	/**
	 * 绑定键盘事件
	 */
	var handleKeyDown = function() {
		$(document).keydown(function(e) {
			e = e || window.event; // 取得事件对象
			var keyCode = e.which || e.keyCode; // 按键值
			switch (keyCode) {
			case 37://left
				if(click.num>click.contentsize){
					$(".next").removeClass("next_on");
				}
				if($(".btn_on").hasClass("btn_check_on")){
					$(".btn_on").removeClass("btn_check_on").addClass("btn_check");
				}
				$(".btn_on").removeClass("btn_on");
				if(click.num>1){
					--click.num;
				}
				if($(".check"+click.num).hasClass("btn_check")){
					$(".check"+click.num).removeClass("btn_check").addClass("btn_check_on");
				}
				$(".check"+click.num).addClass("btn_on");
				break;
			case 39:
				if($(".btn_on").hasClass("btn_check_on")){
					$(".btn_on").removeClass("btn_check_on").addClass("btn_check");
				}
				$(".btn_on").removeClass("btn_on");
				if(click.num<=click.contentsize){
					++click.num;
					$(".check"+click.num).addClass("btn_on");
					if($(".check"+click.num).hasClass("btn_check")){
						$(".check"+click.num).removeClass("btn_check").addClass("btn_check_on");
					}
				}
				if(click.num>click.contentsize){
					$(".next").addClass("next_on");
				}
				break;//right
			case 38:// up
				if($(".btn_on").hasClass("btn_check_on")){
					$(".btn_on").removeClass("btn_check_on").addClass("btn_check");
				}
				if(click.num>$(".btn").length){
					$(".next").removeClass("next_on");
				}
				$(".btn_on").removeClass("btn_on");
				if(click.num>2){
					click.num-=2;
				}
				if($(".check"+click.num).hasClass("btn_check")){
					$(".check"+click.num).removeClass("btn_check").addClass("btn_check_on");
				}
				$(".check"+click.num).addClass("btn_on");
				break;
			case 40:// down
				if($(".btn_on").hasClass("btn_check_on")){
					$(".btn_on").removeClass("btn_check_on").addClass("btn_check");
				}
				$(".btn_on").removeClass("btn_on");
				if(click.num<=click.contentsize){
					click.num+=2;
					$(".check"+click.num).addClass("btn_on");
					if($(".check"+click.num).hasClass("btn_check")){
						$(".check"+click.num).removeClass("btn_check").addClass("btn_check_on");
					}
				}
				if(click.num>$(".btn").length){
					$(".next").addClass("next_on");
				}
				break;
			case 13:// ok
				if($(".next").hasClass("next_on")){
					if($(".btn").hasClass("btn_check")){
						$.get("../generalmeeting/recording",{"stbNo":CONFIG.getUrlParam().stbNo,"typeId":$(".btn_check").attr("cid")},function(data){
							data=eval("("+data+")");
							var json = eval("("+data.msg+")");  //返回的数据没有msg
							if(data.success){
//								console.log(mettingid+"--"+task.taskid);
								$.post("../generalmeeting/addmeeting",{"stbNo":CONFIG.getUrlParam().stbNo,"tid":CONFIG.getUrlParam().typeid,"cid":$(".btn_check").attr("cid"),"taskid":json.taskid},function(result){
									result=eval("("+result+")");
									if(result.success){
										//window.location.href=data.obj.cameraUrl+"?host="+location.host+"&id="+data.obj.id+"&taskid="+json.taskid;
										window.location.href=data.obj.cameraUrl+"?taskid="+json.taskid;
									}
								});
							}
						});
					}else{
						$(".msg").show();
					}
				}else{
					$(".msg").hide();
					$(".btn_check").removeClass("btn_check");
					$(".btn_on").addClass("btn_check_on");
				}
				break;
			case 8:
				break;
			}
		});
	}
	
	return {
		init : function() {
			handleContent();
			handleKeyDown();
		}
	};
	
}();
HYLX.init();