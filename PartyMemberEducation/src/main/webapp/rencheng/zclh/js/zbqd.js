/**
 * 坐班签到
 */
var ZBQD = function(){
	var param = {
		conference_id:0,
	};
	/**
	 * 获取url参数
	 */
	var getUrlParam = function(){
		var url = decodeURI(location.search); //获取url中"?"符后的字串
		var theRequest = new Object();
		if (url.indexOf("?") != -1) {
			var str = url.substr(1);
		    strs = str.split("&");
		    for(var i = 0; i < strs.length; i ++) {
		    	theRequest[strs[i].split("=")[0]]=(strs[i].split("=")[1]);
		    }
		}
		return theRequest;
	};
	/**
	 * 初始化内容区域
	 */
	var handleContent = function() {
		$.ajax({
			type : "GET",
			dataType : 'jsonp',
			async : false,
			data : {
				"callback" : 1,
				"OrgId" :getUrlParam().OrgId,
			},
			url : "http://172.29.0.63:9000/userinfo/VillageCadres",
			success : function(result) {
				var portrait = "";
				if(result.success){
					$(result.data).each(function(i) {
						portrait+="<div class=\"c_l_t_btn\" id=\"sid_"+this.sid+"\"><img width=\"141\" height=\"139\" src='images/zbqd/def.png'/><br/>"+this.name+"</div>";
					});
					$(".c_l_top").html(portrait);
					param.conference_id=result.conference_id;
					console.info(result.conference_id);
					paging({"star":"0","end":"3"});
				}
				setTimeout(changeSign(),3000);
			}
		});
	};
	
	/**
	 * 获取已签到人员信息
	 */
	var changeSign = function(){
		$.ajax({
			type : "GET",
			dataType : 'jsonp',
			async : false,
			data : {
				"callback" : 1,
				"OrgId" :  getUrlParam().OrgId,
				"conference_id" : param.conference_id
			},
			url : "http://172.29.0.63:9000/userinfo/VillageSign",		 
			success : function(result) {
				if (result.success) {
					$(result.data).each(function(i){
						$('#sid_'+this.sid).html("<img width=\"141\" height=\"139\" src='http://172.29.0.63:9000/userimg?sid="+this.sid+"'/><br/>"+this.name);
					});
				}
				setTimeout(changeSign(),3000);
			}
		});
	};
	

	/**
	 * 分页
	 * 
	 */
	var paging = function(data){
		$(".c_l_t_btn").each(function(i){
			$(this).removeClass("hidden");
			if(i<data.star||i>data.end){
				$(this).addClass("hidden");
			}
		});
	}
	
	/**
	 * 绑定键盘事件
	 */
	var handleKeyDown = function() {
		var clickNum = 1;
		var star = 0;
		var end = 3;
		$(document).keydown(function(e) {
			e = e || window.event; // 取得事件对象
			var keyCode = e.which || e.keyCode; // 按键值
			switch (keyCode) {
			case 37:// left
				$(".btn_on").removeClass("btn_on");
				if(clickNum==3){
					clickNum=2;
				}else if(clickNum==2){
					clickNum=1;
				}
				$(".btn_"+clickNum).addClass("btn_on");
				break;
			case 39:// right
				$(".btn_on").removeClass("btn_on");
				if(clickNum==1){
					clickNum=2;
				}else if(clickNum=2){
					clickNum=3
				}
				$(".btn_"+clickNum).addClass("btn_on");
				break;
			case 13:// 确定
				if(clickNum==1){
					star-=4;
					end-=4;
					if(star<0){
						star=0;
					}
					if(end<3){
						end=3;
					}
					paging({"star":star,"end":end});
				}else if(clickNum==2){
					if(end<$(".c_l_t_btn").length){
						star+=4;
						end+=4;
					}
					paging({"star":star,"end":end});
				}else if(clickNum==3){
					window.location.href="/PartyMemberEducation/rcdj/qcjs.jsp?OrgId="+getUrlParam().OrgId+"&stbNo="+getUrlParam().stbNo;
				}
				break;
			case 8:
				break;
			}
		});
	};
	
	return {
		init : function(){
			handleContent();
			handleKeyDown();
		}
	};
}();
ZBQD.init();