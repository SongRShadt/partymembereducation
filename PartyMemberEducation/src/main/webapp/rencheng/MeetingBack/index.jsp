<%@ page language="java" import="java.util.*" pageEncoding="utf-8"%>
<%
	String path = request.getContextPath();
	String basePath = request.getScheme() + "://"
			+ request.getServerName() + ":" + request.getServerPort()
			+ path + "/";
%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
<head>
<base href="<%=basePath%>">
<title>会议回看</title>
<meta http-equiv="Content-type" content="text/html; charset=utf-8" />
<meta name="page-view-size" content="1280*720" />
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<link rel="stylesheet" href="<%=basePath%>MeetingBack/css/index.css" type="text/css"></link></head>
<body>
	<div class="main">
		<div class="header">
		</div>
		<div class="content">
			<div class="contnet_left_up"></div>
			<div class="content_left">
				
			
			</div>
			<div class="contnet_left_down"></div>
			<div class="content_right">
				<img class="content_right_img" src="VideoConference/image/img/zhc_pic.png"></img>
			</div>
		</div>
		<div class="footer"></div>
	</div>

	<!-- core script -->
	<script type="text/javascript" src="<%=basePath%>js/jquery-1.8.3.js"></script>
	<script type="text/javascript" src="<%=basePath%>js/ConfigUtil.js"></script>
	<script type="text/javascript" src="<%=basePath%>MeetingBack/js/index.js"></script>
	<script type="text/javascript">
		jQuery(document).ready(function() {
			Index.init();
		});
	</script>
</body>
</html>
