/**
 * 帮助类
 */
var ConfigUtil = function() {
	return {
		/**
		 * 获取配置文件
		 * @returns
		 */
		getConfig : function() {
			var config;
			$.ajax({
				type : "post",
				dataType : 'json',
				async: false,
				url : ConfigUtil.getRootPath() + '/rcdj/js/config.json',
				success : function(result) {
					config=result;
				}
			});
			return config;
		},
		
		/**
		 * 获取项目根路径
		 * 
		 * @returns
		 */
		getRootPath : function() {
			var curWwwPath = window.document.location.href;// 获取当前网址，如： http://localhost:8080/ems/Pages/Basic/Person.jsp
			var pathName = window.document.location.pathname; // 获取主机地址之后的目录，如： /ems/Pages/Basic/Person.jsp
			var pos = curWwwPath.indexOf(pathName);
			var localhostPath = curWwwPath.substring(0, pos); // 获取主机地址，如： http://localhost:8080
			var projectName = pathName.substring(0, pathName.substr(1).indexOf('/') + 1);// 获取带"/"的项目名，如：/ems
			return (localhostPath + projectName);
		},
		/**
		 * 获取url参数
		 */
		getUrlParam : function(){
			var url = decodeURI(location.search); //获取url中"?"符后的字串
			var theRequest = new Object();
			if (url.indexOf("?") != -1) {
				var str = url.substr(1);
			    strs = str.split("&");
			    for(var i = 0; i < strs.length; i ++) {
			    	theRequest[strs[i].split("=")[0]]=(strs[i].split("=")[1]);
			    }
			}
			return theRequest;
		}
		
	};
}();