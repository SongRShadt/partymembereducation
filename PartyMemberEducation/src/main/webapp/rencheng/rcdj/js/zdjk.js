﻿/**
 * 站点监控
 */
var ZDJK = function() {
	var param = {
		clickNum :($.cookie("hyjkclick")=="null"||$.cookie("hyjkclick")==null||$.cookie("hyjkclick")==undefined)?1:Number($.cookie("hyjkclick")),//1,//右侧导航点击次数
		clickNum_r:($.cookie("hyjkclickr")=="null"||$.cookie("hyjkclickr")==null||$.cookie("hyjkclickr")==undefined)?0:Number($.cookie("hyjkclickr")),//0,//左侧内容点击次数
		leftOrRight:($.cookie("hyjklr")=="null"||$.cookie("hyjklr")==null||$.cookie("hyjklr")==undefined)?1:Number($.cookie("hyjklr")),//1,//
		contentStar:($.cookie("hyjkstartr")=="null"||$.cookie("hyjkstartr")==null||$.cookie("hyjkstartr")==undefined)?0:Number($.cookie("hyjkstartr")),// 0,
		contentEnd :12,
		contentSize:($.cookie("hyjksizer")=="null"||$.cookie("hyjksizer")==null||$.cookie("hyjksizer")==undefined)?1:Number($.cookie("hyjksizer")),// 1,
		
		navStar :($.cookie("hyjkstart")=="null"||$.cookie("hyjkstart")==null||$.cookie("hyjkstart")==undefined)?0:Number($.cookie("hyjkstart")),// 0,//开始
		navSize :($.cookie("hyjksize")=="null"||$.cookie("hyjksize")==null||$.cookie("hyjksize")==undefined)?1:Number($.cookie("hyjksize")),//总记录数
		navEnd : 5,//每页记录
		
	};
	var village ="";
	/**
	 * 获取url参数
	 */
	var getUrlParam = function(){
		var url = decodeURI(location.search); //获取url中"?"符后的字串
		var theRequest = new Object();
		if (url.indexOf("?") != -1) {
			var str = url.substr(1);
		    strs = str.split("&");
		    for(var i = 0; i < strs.length; i ++) {
		    	theRequest[strs[i].split("=")[0]]=(strs[i].split("=")[1]);
		    }
		}
		return theRequest;
	};
	
	var handleLevel=function(){
		$.ajax({
			type : "GET",
			dataType : 'json',
			url : "../village/stb/"+getUrlParam().stbNo,
			async:false,
			success : function(data) {
				if(data.success&&data.obj!=null&&data.obj!=""){
					village=data.obj;
				}else{
					window.location.href="golauncher=true";
				}
			},error:function(a,b,c){
				console.info("error");
				window.location.href="golauncher=true";
			}
		});
	}
	
	
	/**
	 * 初始导航区域
	 */
	var handleNav = function(start) {
		if(getUrlParam().type=="meeting"){
			$(".main").addClass("main_hyhf").removeClass("main");
		}
		if(start<param.navSize){
			var townId =village.townId;
			if(village.level==0){
				townId="all";
			} 
			$.ajax({
				async:false,
				type : "GET",
				dataType : 'json',
				data:{
					townId:townId,
					start:start,
					size:5
				},
				url : "../town/get",
				success : function(d) {
					if(d.success){
						var msg = eval("("+d.msg+")");
						param.navSize=msg.count;
						if(d.obj.length>0){
							param.navEnd=d.obj.length;
							var nav = "";
							$(d.obj).each(function(i){
								nav+="<div class='nav_btn nav_btn_"+(i+1)+"' townid='"+this.townId+"'>"+this.townName+"</div>";
							});
							$(".content_left").html(nav);
							$(".nav_btn_"+param.clickNum).addClass("nav_btn_down");
						}else{
							console.info("暂无数据！");
						}
					}
				},error:function(a,b,c){
					console.error("error");
					window.location.href="golauncher=true";
				}
			});
		} 
	};
	
	/**
	 * 初始内容区域
	 */
	var handleContent = function(start) {
		if(start<param.contentSize){
			$.ajax({
				type : "GET",
				dataType : 'json',
				async:false,
				data:{
					start:start,
					size:12
				},
				url : "../camera/town/"+getUrlParam().stbNo+"/"+$(".nav_btn_down").attr("townid")+"/0",
				success : function(data) {
					if(data.success){
						var msg = eval("("+data.msg+")");
						param.contentSize=msg.count;
						
						if(data.obj.length>0){
							param.contentEnd=data.obj.length;
							var content = "";
							if(data.obj!=""){
								$(data.obj).each(function(i){
									content += "<div class='content_btn content_btn_"+(i+1)+"' cameraNo='"+this.id+"' videourl='"+this.cameraUrl+"' villageId=\""+this.villageId+"\">"+this.cameraName+"</div>";
								});
							}
							$(".content_right").html(content);
							$(".content_btn_"+param.clickNum_r).addClass("content_btn_down");
						}else{
							console.info(data);
							$(".content_right").html("<div class='msg'>暂无村信息！"+data.obj+"--"+data.obj.length+"</div>");
						}
					}
				}
			});
		}
	}
	
	/**
	 * 绑定键盘事件
	 */
	var handleKeyDown = function() {
		$(document).keydown(function(e) {
			e = e || window.event; // 取得事件对象
			var keyCode = e.which || e.keyCode; // 按键值
			switch (keyCode) {
			case 37://left
				if(param.leftOrRight==1){
				}else if(param.leftOrRight==2){
					if(param.clickNum_r%2==0){
						if(param.clickNum_r==1&&param.contentStar==0){
							param.leftOrRight=1;
							$(".content_btn_down").removeClass("content_btn_down");
							param.clickNum_r=0;
						}else{
							$(".content_btn_down").removeClass("content_btn_down");
							param.clickNum_r--;
							if(param.clickNum_r<1){
								param.contentStar-=12;
								handleContent(param.contentStar);
								if(param.contentStar<0){
									param.clickNum_r=1;
									param.contentStar=0;
								}else{
									param.clickNum_r =$(".content_btn").length;
								}
							} 
							$(".content_btn_"+param.clickNum_r).addClass("content_btn_down");
						}
					}else{
						param.leftOrRight=1;
						$(".content_btn_"+param.clickNum_r).removeClass("content_btn_down");
						param.clickNum_r=0;
					}
					
				}
				break;
			case 39://right
				if(param.leftOrRight==1){
					param.leftOrRight=2;
					param.clickNum_r=1;
					$(".content_btn_"+param.clickNum_r).addClass("content_btn_down");
					
				}else if(param.leftOrRight==2){
					$(".content_btn_down").removeClass("content_btn_down");
					param.clickNum_r++;
					if(param.clickNum_r>param.contentEnd){
						param.contentStar+=param.contentEnd;
						handleContent(param.contentStar);
						if(param.contentStar<param.contentSize){
							param.clickNum_r =1;
						}else{
							param.contentStar=param.contentSize-$(".content_btn").length;
							param.clickNum_r=$(".content_btn").length;
						}	
					}
					$(".content_btn_"+param.clickNum_r).addClass("content_btn_down");
				}
				break;
			case 38:// up
				if(param.leftOrRight==1){
					$(".nav_btn_down").removeClass("nav_btn_down");
					param.clickNum--;
					if(param.clickNum<1){
						param.navStar-=5;
						handleNav(param.navStar);
						if(param.navStar<0){
							param.clickNum=1;
							param.navStar=0;
						}else{
							param.clickNum =$(".nav_btn").length;
						}
					}  
					$(".nav_btn_"+param.clickNum).addClass("nav_btn_down");
					param.contentStar=0;
					handleContent(param.contentStar);
				}
				if(param.leftOrRight==2){
					$(".content_btn_down").removeClass("content_btn_down");
					param.clickNum_r-=2;
					if(param.clickNum_r<1){
						param.contentStar-=12;
						handleContent(param.contentStar);
						if(param.contentStar<0){
							param.clickNum_r=2;
							param.contentStar=0;
						}else{
							param.clickNum_r =$(".content_btn").length;
						}
					} 
					$(".content_btn_"+param.clickNum_r).addClass("content_btn_down");
				}
				break;
			case 40:// down
				if(param.leftOrRight==1){
					param.clickNum_r=0;
					param.contentStar=0;
					$(".nav_btn_down").removeClass("nav_btn_down");
					param.clickNum++;
					if(param.clickNum>param.navEnd){
						param.navStar+=param.navEnd;
						handleNav(param.navStar);
						if(param.navStar<param.navSize){
							param.clickNum =1;
						}else{
							param.navStar=param.navSize-$(".nav_btn").length;
							param.clickNum=$(".nav_btn").length;
						}	
					}
					param.contentStar=0;
					$(".nav_btn_"+param.clickNum).addClass("nav_btn_down");
					handleContent(param.contentStar);
				}else if(param.leftOrRight==2){
					$(".content_btn_down").removeClass("content_btn_down");
					param.clickNum_r+=2;
					if(param.clickNum_r>param.contentEnd){
						param.contentStar+=param.contentEnd;
						handleContent(param.contentStar);
						if(param.contentStar<param.contentSize){
							param.clickNum_r =1;
						}else{
							param.contentStar=param.contentSize-$(".content_btn").length;
							param.clickNum_r=$(".content_btn").length;
						}	
					}
					if(param.clickNum_r>=$(".content_btn").length){
						param.clickNum_r=$(".content_btn").length;
					}
					$(".content_btn_"+param.clickNum_r).addClass("content_btn_down");
				}
				break;
			case 13:// ok
				if(param.leftOrRight==1){
				}else if(param.leftOrRight==2){
					$.cookie("hyjklr",param.leftOrRight);
					$.cookie("hyjkclick",param.clickNum);
					$.cookie("hyjkstart",param.navStar);
					$.cookie("hyjksize",param.navSize);
					$.cookie("hyjkclickr",param.clickNum_r);
					$.cookie("hyjkstartr",param.contentStar);
					$.cookie("hyjksizer",param.contentSize);
					if(getUrlParam().type=="meeting"){
						var url="../rcdj/zdjk_xq.jsp?stbNo="+getUrlParam().stbNo+"&cameraNo="+$(".content_btn_"+param.clickNum_r).attr("camerano")+"&title="+$(".content_btn_down").html();
						window.location.assign(encodeURI(url));
					}else{
						window.location.href= $(".content_btn_down").attr("videourl");
					}
				}
				break;
			case 8:
				break;
			}
		});
	};
	
	return {
		/**
		 * 初始化
		 * 
		 * @returns
		 */
		init : function() {
			$(".contnet_left_up").addClass("up");
			$(".contnet_left_down").addClass("down");
			handleLevel();
			handleNav(param.navStar);
			handleContent(param.contentStar);
			handleKeyDown();
		}
	};
}();