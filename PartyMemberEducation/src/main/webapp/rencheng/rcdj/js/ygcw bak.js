/**
 * 阳光村务—>村级组织建设、党务村务公开
 */
var YGCW = function() {
	var param = {
		orgId : 3,
		leftClick:1,
		rightClick:0,
		leftOrRight:0,
		contentRData : ""
	};
	
	var leftContent = [
	    {"id":"1","content":"村级组织建设"},
	    {"id":"2","content":"党务村务公开"},
	];
	var rightContent = [
	    {"id":"1","content":"党支部委员会名单","data":"1"},
	    {"id":"2","content":"村务监督委员会名单","data":"4"},
	    {"id":"3","content":"村配套组织负责人名单","data":"8"},
	    {"id":"4","content":"村民委员会名单","data":"7"},
	    {"id":"5","content":"理财小组名单","data":"3"},
	    {"id":"6","content":"村民代表名单","data":"2"},
	    {"id":"7","content":"党员名单","data":"6"},
	];
	var rightContentT = [
//		{"id":"1","content":"三年发展规划","data":"","url":""},
//		{"id":"2","content":"年度发展目标","data":"","url":""},
//		{"id":"3","content":"党费收缴及党员管理","data":"","url":""},
//		{"id":"4","content":"财务公开","data":"","url":""},
//		{"id":"5","content":"村级合同公示","data":"","url":""},
//		{"id":"6","content":"村集体资产资源管理","data":"","url":""}, 
//		{"id":"7","content":"享受农村低保情况","data":"","url":""},
//		{"id":"8","content":"村级组织运转经费","data":"","url":""},
	];
	
	/**
	 * 获取项目根路径
	 * 
	 * @returns
	 */
	var getRootPath = function() {
		var curWwwPath = window.document.location.href;// 获取当前网址，如： http://localhost:8080/ems/Pages/Basic/Person.jsp
		var pathName = window.document.location.pathname; // 获取主机地址之后的目录，如： /ems/Pages/Basic/Person.jsp
		var pos = curWwwPath.indexOf(pathName);
		var localhostPath = curWwwPath.substring(0, pos); // 获取主机地址，如： http://localhost:8080
		var projectName = pathName.substring(0, pathName.substr(1).indexOf('/') + 1);// 获取带"/"的项目名，如：/ems
		return (localhostPath + projectName);
	};
	/**
	 * 获取url参数
	 */
	var getUrlParam = function(){
		var url = location.search; //获取url中"?"符后的字串
		var theRequest = new Object();
		if (url.indexOf("?") != -1) {
			var str = url.substr(1);
		    strs = str.split("&");
		    for(var i = 0; i < strs.length; i ++) {
		    	theRequest[strs[i].split("=")[0]]=(strs[i].split("=")[1]);
		    }
		}
		return theRequest;
	}; 
	
	/**
	 * 获取党务村务公开
	 */
	var getContentData = function(){
		$.ajax({
			url : getRootPath()+"/crossDoMain/domain",
			type:"post",
			dataType:"json",
			data:{
				"httpUrl":"http://172.29.0.109:8070/sync_ott_sdjnrcdj/getChannelList",
				"key":getUrlParam().key
			},
			success : function(data){
				console.info(data);
				data = eval("("+data+")");
				if(data.success){
					console.info(data.dataList);
					$.each(data.dataList,function(i){
						rightContentT.push({"id":i+1,"content":this.title,"data":"","url":this.hrefUrl});
					});
				}
			}
		});
	};
	
	/**
	 * 初始化内容左侧区域
	 */
	var handleContentLeft = function() {
		var leftBtn = "";
		$(leftContent).each(function(i){
			leftBtn+="<div class=\"left_btn left_btn_"+leftContent[i].id+"\">"+leftContent[i].content+"</div>";
		});
		$(".c_left").html(leftBtn);
		$(".left_btn_1").addClass("left_btn_on");
	};

	/**
	 * 初始化内容右侧区域
	 */
	var handleContentRight = function(data) {
		var rightBtn = "";
		$(data).each(function(i){
			rightBtn+="<div class=\"right_btn right_btn_"+data[i].id+"\">"+data[i].content+"</div>";
		});
		$(".c_right").html(rightBtn);
	};

	/**
	 * 初始化键盘事件
	 */
	var handleKeyDown = function() {
		$(document).keydown(function(e) {
			e = e || window.event; // 取得事件对象
			var keyCode = e.which || e.keyCode; // 按键值
			switch (keyCode) {
			case 37://left
				param.leftOrRight=1;
				$(".right_btn_on").removeClass("right_btn_on");
				param.rightClick--;
				if(param.rightClick<1){
					param.rightClick=0;
					param.leftOrRight=0;
				}
				$(".right_btn_"+param.rightClick).addClass("right_btn_on");
				break;
			case 39://right
				param.leftOrRight=1;
				$(".right_btn_on").removeClass("right_btn_on");
				param.rightClick++;
				if(param.rightClick>$(".right_btn").length){
					param.rightClick=$(".right_btn").length;
				}
				$(".right_btn_"+param.rightClick).addClass("right_btn_on");
				break;
			case 38://up
				if(param.leftOrRight==0){
					$(".left_btn_on").removeClass("left_btn_on");
					param.leftClick--;
					if(param.leftClick<1){
						param.leftClick=1;
					}
					$(".left_btn_"+param.leftClick).addClass("left_btn_on");
					handleContentRight(rightContent);
				}else if(param.leftOrRight==1){
					$(".right_btn_on").removeClass("right_btn_on");
					if(param.rightClick>3){
						param.rightClick-=3;
					}
					$(".right_btn_"+param.rightClick).addClass("right_btn_on");
				}
				break;
			case 40://down
				if(param.leftOrRight==0){
					$(".left_btn_on").removeClass("left_btn_on");
					param.leftClick++;
					if(param.leftClick>$(".left_btn").length){
						param.leftClick=$(".left_btn").length;
					}
					$(".left_btn_"+param.leftClick).addClass("left_btn_on");
					handleContentRight(rightContentT);
				}else if(param.leftOrRight==1){
					$(".right_btn_on").removeClass("right_btn_on");
					if(param.rightClick<$(".right_btn").length-($(".right_btn").length%3===0?2:$(".right_btn").length%3)){
						param.rightClick+=3;
					}
					$(".right_btn_"+param.rightClick).addClass("right_btn_on");
				}
				break;
			case 13://确定
				if(param.leftOrRight==1){
					var title = "";
					var usertype="";
					if(param.leftClick==1){
						title=rightContent[param.rightClick-1].content;
						usertype=rightContent[param.rightClick-1].data;
						var url ="/PartyMemberEducation/rcdj/ygcw_info.jsp?OrgId="+getUrlParam().OrgId+"&UserType="+usertype+"&title="+title;
						window.location.assign(encodeURI(url));
					}else if(param.leftClick==2){
						$.get("/PartyMemberEducation/villageController/getKeyByOrgCode",{"orgCode":getUrlParam().OrgId},function(data){
							data = eval("("+data+")");
//							var url = "http://172.29.0.109:8070/sync_ott_sdjnrcdj/Entrance/CityModel/ModelEffect/ModelContext/ContextPage.jsp?rid="+data;
						});
						window.location.assign(rightContentT[param.rightClick-1].url);
					}
				}
				break;
			case 8:
				break;
			}
		});
	};

	return {
		init : function() {
			getContentData();
			handleContentLeft();
			handleContentRight(rightContent);
			handleKeyDown();
		},

	};

}();