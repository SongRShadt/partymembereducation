/**
 * 视频回看
 */
var ZDJK_XQ = function(){
	
	var param = {
		clickNum : 1,
		contentCount : 5,
		star : 0,
		end : 4
	};
	
	/**
	 * 获取url参数
	 */
	var getUrlParam = function(){
		var url = decodeURI(location.search); //获取url中"?"符后的字串
		var theRequest = new Object();
		if (url.indexOf("?") != -1) {
			var str = url.substr(1);
		    strs = str.split("&");
		    for(var i = 0; i < strs.length; i ++) {
		    	theRequest[strs[i].split("=")[0]]=(strs[i].split("=")[1]);
		    }
		}
		return theRequest;
	};
	
	/**
	 * 初始化内容标题
	 */
	 		var ip = "";
	var handleContentTitle = function(){

		$.ajax({
			type : "GET",
			dataType : 'json',
			data : {
				"cameraNo" : getUrlParam().cameraNo
			},
			url : "../cameraInfoController/getCameraInfoByCameraNo",
			success : function(data) {
				data = eval("("+data+")");
				console.info(data);
				if(data.success){
					var tmp = data.obj.cameraUrl;	
					console.info(tmp);			
					ip=	tmp.split("/")[2];
					console.info(ip);
				}
			}
		});
		
	};
	
	/**
	 * 初始内容区域
	 */
	var handleContent = function() {
		$(".content_title").html(decodeURI(getUrlParam().title));
		$.ajax({
			type : "GET",
			dataType : 'json',
			data : {
				"cameraNo" : getUrlParam().cameraNo
			},
			url : "../videoController/getAllVideoByCameraNo",
			success : function(data) {
				console.info(data);
				if(data.success){
					var content = "";
					$(data.obj).each(function(i){
						content += "<div data=\""+this.playUrl+"\" class=\"content_main_btn content_btn_"+(i+1)+"\">"+(this.startTime+"——"+this.endTime)+"</div>";
					});
					$(".content_main").html(content);
					if(data.obj.length>0){
						$(".content_btn_1").addClass("content_main_btn_down");
					}
					pagination({"star":0,"end":4});
				}
			}
		});
	};
	/**
	 * 分页
	 */
	var pagination = function(c){
		if($(".content_main_btn").length>param.contentCount){
			$(".content_main_btn").each(function(i){
				$(this).removeClass("btn_hidden");
				if(i<c.star||i>c.end){
					$(this).addClass("btn_hidden");
				}
			});
		} 
	};
	
	/**
	 * 绑定键盘事件
	 */
	var handleKeyDown = function() {
		$(document).keydown(function(e) {
			e = e || window.event; // 取得事件对象
			var keyCode = e.which || e.keyCode; // 按键值
			switch (keyCode) {
			case 38:// up
				$(".content_main_btn_down").removeClass("content_main_btn_down");
				param.clickNum--;
				if(param.clickNum<1){
					param.clickNum=1;
				}
				if(param.clickNum<=param.end-4&&param.star>0){
					param.star--;
					param.end--;
					pagination({"star":param.star,"end":param.end});
				}
				$(".content_btn_"+param.clickNum).addClass("content_main_btn_down");
				break;
			case 40:// down
				$(".content_main_btn_down").removeClass("content_main_btn_down");
				param.clickNum++;
				console.info(param.clickNum+"--"+param.contentCount);
				if(param.clickNum>=$(".content_main_btn").length){
					param.clickNum=$(".content_main_btn").length;
				}
				if(param.end<$(".content_main_btn").length-1&&param.clickNum>param.contentCount){
					param.star++;
					param.end++;
					pagination({"star":param.star,"end":param.end});
				}
				$(".content_btn_"+param.clickNum).addClass("content_main_btn_down");
				break;
			case 13:// ok
				window.location.href= $(".content_main_btn_down").attr("data");
				break;
			case 8:
				break;
			}
		});
	};
	return {
		init : function(){
			handleContentTitle();
			handleContent();
			handleKeyDown();
		},
	};
	
	
}();