/**
 * 组织关系转接
 */
var DFSJ = function() {
	
	var param = {
		userInfo : "",	
		navClick : 1,
		conClick : 0,
		jdClick : 0,
		navOrContnet : 0
	};
	
	/**
	 * 获取url参数
	 */
	var getUrlParam = function(){
		var url = decodeURI(location.search); //获取url中"?"符后的字串
		var theRequest = new Object();
		if (url.indexOf("?") != -1) {
			var str = url.substr(1);
		    strs = str.split("&");
		    for(var i = 0; i < strs.length; i ++) {
		    	theRequest[strs[i].split("=")[0]]=(strs[i].split("=")[1]);
		    }
		}
		return theRequest;
	};

	/**
	 * 初始化中间左侧区域
	 */
	var handleContentLeft = function() {
		$.ajax({
			type : "GET",
			dataType : 'jsonp',
			async : false,
			data : {
				"callback" : 1,
				"OrgId": getUrlParam().OrgId,
				"user_type":"6"
			},
			url : "http://172.29.0.63:9000/userinfo/userInfoService",
			success : function(result) {
				if(result.success){
					var data = result.data;
					param.userInfo = data;
					var person = "";
					$(data).each(function(i){
						if(i==0){
							person+="<div class=\"nav_btn nav_btn_on nav_btn_"+(i+1)+"\" data=\""+this.sid+"\">"+this.name+"</div>";
						}else if(i>5){
							person+="<div class=\"nav_btn hidden nav_btn_"+(i+1)+"\"  data=\""+this.sid+"\">"+this.name+"</div>";
						}else{
							person+="<div class=\"nav_btn nav_btn_"+(i+1)+"\"  data=\""+this.sid+"\">"+this.name+"</div>";
						}
						
					});
					$(".c_l_c_content").html(person);
					handleContentRight();
				}
			}
		});
	};
	/**
	 * 初始化中间右侧区域
	 */
	var handleContentRight = function() {
		$(".portrait").html("<img src='http://172.29.0.63:9000/userimg?sid="+$(".nav_btn_on").attr("data")+"'/>");
		$(".sex").html("性&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;别:"+param.userInfo[param.navClick-1].sex);
		$(".phone").html("电话号码:"+param.userInfo[param.navClick-1].phone);
		$(".sbrith").html("出生年月:"+(param.userInfo[param.navClick-1].sbrith).substring(0,param.userInfo[param.navClick-1].sbrith.length-2)+"**");
		$(".sid").html("身份证号:"+(param.userInfo[param.navClick-1].sid).substring(0,param.userInfo[param.navClick-1].sid.length-6)+"******");
		$(".sid").attr("data",param.userInfo[param.navClick-1].sid);
	};
	/**
	 * 分页
	 * 
	 */
	var navPaging = function(data){
		$(".nav_btn").each(function(i){
			$(this).removeClass("hidden");
			if(i<data.star||i>data.end){
				$(this).addClass("hidden");
			}
		});
	}
	/**
	 * 初始化键盘事件
	 */
	var handleKeyDown = function() {
		var star =0;
		var end=5;
		$(document).keydown(function(e) {
			e = e || window.event; // 取得事件对象
			var keyCode = e.which || e.keyCode; // 按键值
			$(".msg").html("");
			switch (keyCode) {
			case 37:
				if(param.navOrContnet==2){
					if(param.jdClick>$(".checkbox_jd").length){
						$(".submit").removeClass("submit_on");
					}
					$(".checkbox_jd_on").removeClass("checkbox_jd_on");
					if($(".checkbox_jd_"+param.jdClick).hasClass("checkbox_jd_check_on")){
						$(".checkbox_jd_"+param.jdClick).addClass("checkbox_jd_check");
						$(".checkbox_jd_check_on").removeClass("checkbox_jd_check_on");
					} 
					if(param.jdClick>0){
						param.jdClick--;
						if($(".checkbox_jd_"+param.jdClick).hasClass("checkbox_jd_check")){
							$(".checkbox_jd_"+param.jdClick).addClass("checkbox_jd_check_on");
						}else{
							$(".checkbox_jd_"+param.jdClick).addClass("checkbox_jd_on");
						}
						if(param.jdClick==0){
							param.navOrContnet=1;
							param.conClick=$(".checkbox").length;
							if($(".checkbox_"+param.conClick).hasClass("checkbox_check")){
								$(".checkbox_"+param.conClick).addClass("checkbox_check_on");
							}else{
								$(".checkbox_"+param.conClick).addClass("checkbox_on");
							}
						}
					}
				}else if(param.navOrContnet==1){
					$(".checkbox_on").removeClass("checkbox_on");
					if($(".checkbox_"+param.conClick).hasClass("checkbox_check_on")){
						$(".checkbox_"+param.conClick).addClass("checkbox_check");
						$(".checkbox_check_on").removeClass("checkbox_check_on");
					}
					$(".submit_on").removeClass("submit_on");
					if(param.conClick>0){
						if(param.conClick==$(".checkbox").length&&($(".checkbox_8").hasClass("checkbox_check")||$(".checkbox_8").hasClass("checkbox_check_on"))){
							$(".money_input").addClass("hidden");
							$(".money_input").focus();
							$(".money_span").attr("data",($(".money_input").val()==""?0:$(".money_input").val()));
							$(".money_span").html($(".money_input").val()==""?0:$(".money_input").val());
						}
						param.conClick--;
						if($(".checkbox_"+param.conClick).hasClass("checkbox_check")){
							$(".checkbox_"+param.conClick).addClass("checkbox_check_on");
						}else{
							$(".checkbox_"+param.conClick).addClass("checkbox_on");
						}
					}
					if(param.conClick==0){
						param.navOrContnet=0;
					}
				}
				break;
			case 39:
				if(param.navOrContnet==0){
					param.navOrContnet=1;
					param.conClick=1;
					if($(".checkbox_"+param.conClick).hasClass("checkbox_check")){
						$(".checkbox_"+param.conClick).addClass("checkbox_check_on");
					}else{
						$(".checkbox_"+param.conClick).addClass("checkbox_on");
					}
				}else if(param.navOrContnet==1){
					$(".checkbox_on").removeClass("checkbox_on");
					if($(".checkbox_"+param.conClick).hasClass("checkbox_check_on")){
						$(".checkbox_"+param.conClick).addClass("checkbox_check");
						$(".checkbox_check_on").removeClass("checkbox_check_on");
					}
					if(param.conClick<=$(".checkbox").length){
						if(param.conClick==$(".checkbox").length&&($(".checkbox_8").hasClass("checkbox_check")||$(".checkbox_8").hasClass("checkbox_check_on"))){
							$(".money_input").addClass("hidden");
							$(".money_input").focus();
							$(".money_span").attr("data",($(".money_input").val()==""?0:$(".money_input").val()));
							$(".money_span").html($(".money_input").val()==""?0:$(".money_input").val());
						}
						param.conClick++;
						if($(".checkbox_"+param.conClick).hasClass("checkbox_check")){
							$(".checkbox_"+param.conClick).addClass("checkbox_check_on");
						}else{
							$(".checkbox_"+param.conClick).addClass("checkbox_on");
						}
					}
					
					if(param.conClick>$(".checkbox").length){
						param.navOrContnet=2;
						param.jdClick=1;
						if($(".checkbox_jd_"+param.jdClick).hasClass("checkbox_jd_check")){
							$(".checkbox_jd_"+param.jdClick).addClass("checkbox_jd_check_on");
						}else{
							$(".checkbox_jd_"+param.jdClick).addClass("checkbox_jd_on");
						}
					}
				}else if(param.navOrContnet==2){
					$(".checkbox_jd_on").removeClass("checkbox_jd_on");
					if($(".checkbox_jd_"+param.jdClick).hasClass("checkbox_jd_check_on")){
						$(".checkbox_jd_"+param.jdClick).addClass("checkbox_jd_check");
						$(".checkbox_jd_check_on").removeClass("checkbox_jd_check_on");
					} 
					if(param.jdClick<=$(".checkbox_jd").length){
						param.jdClick++;
						if($(".checkbox_jd_"+param.jdClick).hasClass("checkbox_jd_check")){
							$(".checkbox_jd_"+param.jdClick).addClass("checkbox_jd_check_on");
						}else{
							$(".checkbox_jd_"+param.jdClick).addClass("checkbox_jd_on");
						}
					}
					if(param.jdClick>$(".checkbox_jd").length){
						$(".submit").addClass("submit_on");
					}
				}
				break;
			case 38://up
				if(param.navOrContnet==0){
					$(".checkbox_check").removeClass("checkbox_check");
					$(".checkbox_jd_check").removeClass("checkbox_jd_check");
					$(".nav_btn_on").removeClass("nav_btn_on");
					param.navClick--;
					if(param.navClick<1){
						param.navClick=1;
					}
					if(param.navClick<=end-5&&star>0){
						star--;end--;
						navPaging({"star":star,"end":end});
					}
					$(".nav_btn_"+param.navClick).addClass("nav_btn_on");
					handleContentRight();
				}else if(param.navOrContnet==1){
					if(param.conClick>4){
						$(".checkbox_on").removeClass("checkbox_on");
						if($(".checkbox_"+param.conClick).hasClass("checkbox_check_on")){
							$(".checkbox_"+param.conClick).addClass("checkbox_check");
							$(".checkbox_check_on").removeClass("checkbox_check_on");
						}
						if(param.conClick==$(".checkbox").length&&($(".checkbox_8").hasClass("checkbox_check")||$(".checkbox_8").hasClass("checkbox_check_on"))){
							$(".money_input").addClass("hidden");
							$(".money_input").focus();
							$(".money_span").attr("data",($(".money_input").val()==""?0:$(".money_input").val()));
							$(".money_span").html($(".money_input").val()==""?0:$(".money_input").val());
						}
						param.conClick-=4;
						if($(".checkbox_"+param.conClick).hasClass("checkbox_check")){
							$(".checkbox_"+param.conClick).addClass("checkbox_check_on");
						}else{
							$(".checkbox_"+param.conClick).addClass("checkbox_on");
						}
					}
					
				}else if(param.navOrContnet==2){
					if($(".submit").hasClass("submit_on")){
						$(".submit").removeClass("submit_on");
						param.jdClick=4;
						if($(".checkbox_jd_"+param.jdClick).hasClass("checkbox_jd_check")){
							$(".checkbox_jd_"+param.jdClick).addClass("checkbox_jd_check_on");
						}else{
							$(".checkbox_jd_"+param.jdClick).addClass("checkbox_jd_on");
						}
					}else{
						$(".checkbox_jd_on").removeClass("checkbox_jd_on");
						if($(".checkbox_jd_"+param.jdClick).hasClass("checkbox_jd_check_on")){
							$(".checkbox_jd_"+param.jdClick).addClass("checkbox_jd_check");
							$(".checkbox_jd_check_on").removeClass("checkbox_jd_check_on");
						} 
						param.navOrContnet=1;
						param.conClick= param.jdClick+4;
						if($(".checkbox_"+param.conClick).hasClass("checkbox_check")){
							$(".checkbox_"+param.conClick).addClass("checkbox_check_on");
						}else{
							$(".checkbox_"+param.conClick).addClass("checkbox_on");
						}
					}
				}
				break;
			case 40://down
				if(param.navOrContnet==0){
					$(".checkbox_check").removeClass("checkbox_check");
					$(".checkbox_jd_check").removeClass("checkbox_jd_check");
					$(".nav_btn_on").removeClass("nav_btn_on");
					param.navClick++;
					if(param.navClick>6&&param.navClick<$(".nav_btn").length+1){
						star++;
						end++;
						navPaging({"star":star,"end":end});
					}
					if(param.navClick>$(".nav_btn").length){
						param.navClick=$(".nav_btn").length;
					}
					$(".nav_btn_"+param.navClick).addClass("nav_btn_on");
					handleContentRight();
				}else if(param.navOrContnet==1){
					if(param.conClick<5){
						$(".checkbox_on").removeClass("checkbox_on");
						if($(".checkbox_"+param.conClick).hasClass("checkbox_check_on")){
							$(".checkbox_"+param.conClick).addClass("checkbox_check");
							$(".checkbox_check_on").removeClass("checkbox_check_on");
						}
						param.conClick+=4;
						if($(".checkbox_"+param.conClick).hasClass("checkbox_check")){
							$(".checkbox_"+param.conClick).addClass("checkbox_check_on");
						}else{
							$(".checkbox_"+param.conClick).addClass("checkbox_on");
						}
					}else {
						$(".checkbox_on").removeClass("checkbox_on");
						if($(".checkbox_"+param.conClick).hasClass("checkbox_check_on")){
							$(".checkbox_"+param.conClick).addClass("checkbox_check");
							$(".checkbox_check_on").removeClass("checkbox_check_on");
						}
						if(param.conClick==$(".checkbox").length&&($(".checkbox_8").hasClass("checkbox_check")||$(".checkbox_8").hasClass("checkbox_check_on"))){
							$(".money_input").addClass("hidden");
							$(".money_input").focus();
							$(".money_span").attr("data",($(".money_input").val()==""?0:$(".money_input").val()));
							$(".money_span").html($(".money_input").val()==""?0:$(".money_input").val());
						}
						$(".checkbox_jd_on").removeClass("checkbox_jd_on");
						if($(".checkbox_jd_"+param.jdClick).hasClass("checkbox_jd_check_on")){
							$(".checkbox_jd_"+param.jdClick).addClass("checkbox_jd_check");
							$(".checkbox_jd_check_on").removeClass("checkbox_jd_check_on");
						} 
						param.navOrContnet=2;
						param.jdClick= param.conClick-4;
						if($(".checkbox_jd_"+param.jdClick).hasClass("checkbox_jd_check")){
							$(".checkbox_jd_"+param.jdClick).addClass("checkbox_jd_check_on");
						}else{
							$(".checkbox_jd_"+param.jdClick).addClass("checkbox_jd_on");
						}
					}
				}else if(param.navOrContnet==2){
					$(".checkbox_jd_on").removeClass("checkbox_jd_on");
					if($(".checkbox_jd_"+param.jdClick).hasClass("checkbox_jd_check_on")){
						$(".checkbox_jd_"+param.jdClick).addClass("checkbox_jd_check");
						$(".checkbox_jd_check_on").removeClass("checkbox_jd_check_on");
					} 
					$(".submit").addClass("submit_on");
				}
				break;
			case 13://确定
				if(param.navOrContnet==1){
					$(".checkbox_check").removeClass("checkbox_check");
					$(".checkbox_on").addClass("checkbox_check_on");
					$(".money_span").html($(".checkbox_check_on").attr("data"));
					$(".money_span").attr("data",$(".checkbox_check_on").attr("data"));
					if(param.conClick==$(".checkbox").length){
						$(".money_input").removeClass("hidden");
						$(".money_input").focus();
					}
				}if(param.navOrContnet==2){
					if($(".submit").hasClass("submit_on")){
						if($(".money_span").attr("data")==undefined||$(".money_span").attr("data")==0){
							$(".msg").html("请选择金额!");
						}else if($(".checkbox_jd_check").attr("data")==undefined){
							$(".msg").html("请选择季度!");
						}else{
							$.ajax({
								type : "GET",
								dataType : 'jsonp',
								async : false,
								data : {
									"callback" : 1,
									"sid":$(".sid").attr("data"),
									"money":$(".money_span").attr("data"),
									"quarter":$(".checkbox_jd_check").attr("data")
								},
								url : "http://172.29.0.63:9000/userinfo/payment",
								success : function(result) {
									if(result.success){
										window.location.href="/PartyMemberEducation/rcdj/success.jsp";
									}
								}
							});
						}
					}else{
						if(param.jdClick<$(".checkbox_jd").length+1){
							$(".checkbox_jd_check").removeClass("checkbox_jd_check");
							$(".checkbox_jd_on").addClass("checkbox_jd_check_on");
						}
					}
				}
				break;
			case 8:
				break;
			}
		});
	};

	return {
		init : function() {
			handleContentLeft();
			handleKeyDown();
		}
	};
}();