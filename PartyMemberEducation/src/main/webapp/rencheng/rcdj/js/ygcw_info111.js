﻿/**
 * 阳光村务信息列表
 */
var YGCW_Info = function(){
	var param = {
		orgId : ConfigUtil.getUrlParam().OrgId,//村组织编号
		userType : ConfigUtil.getUrlParam().UserType, //用户类型
		title : ConfigUtil.getUrlParam().title
			
	};
	
	/**
	 * 获取url参数
	 */
	var getUrlParam = function(){
		var url = decodeURI(location.search); //获取url中"?"符后的字串
		var theRequest = new Object();
		if (url.indexOf("?") != -1) {
			var str = url.substr(1);
		    strs = str.split("&");
		    for(var i = 0; i < strs.length; i ++) {
		    	theRequest[strs[i].split("=")[0]]=(strs[i].split("=")[1]);
		    }
		}
		return theRequest;
	};
	/**
	 * 初始化内容区域
	 */
	var handleContent = function(){
		$.ajax({
			type : "GET",
			dataType : 'jsonp',
			async : false,
			data : {
				"callback" : 1,
				"OrgId" : getUrlParam().OrgId,
				"user_type" : getUrlParam().UserType
			},
			url : "http://172.29.0.63:9000/userinfo/userInfoService",
			success : function(result) {
			$(".content_text").html(getUrlParam().title);
				if(result.success){
					var content = "";
					$(result.data).each(function(i){						var bir = this.sbrith;
						content+="<tr>" +
								"<td height='126' width='102'><img width='102' height='126' src=\"http://172.29.0.63:9000/userimg?sid="+this.sid+"\"/></td>" +
								"<td>"+this.name+"</td>" +
								"<td>"+this.sex+"</td>" +
								"<td>"+bir.replace(/(\w)/g,function(a,b,c,d){return (c>(bir.length-3))?'*':a})+"</td>" +
								"<td>"+this.zzmm+"</td>" +
								"<td>"+(this.zw===undefined?"":this.zw)+"</td>" +
								"<td>"+this.phone+"</td>" +
								"</tr>";
					});
					var l = result.data.length%3;					if(result.data.length%3==0||result.data.length==0){
						l=0;
					}else if(result.data.length%3==1){
						l=3;
					}else{
						l=2;
					}
					for(var i=0;i<l;i++)
						content+="<tr><td height='126' width='102'></td><td></td><td></td><td></td><td></td><td></td><td></td></tr>";
					$(".c_info table tbody").html(content);
					paging({"star":"0","end":"2"});
				}
			}
		});
	};
	
	/**
	 * 分页
	 * 
	 */
	var paging = function(data){
		$(".c_info table tbody tr").each(function(i){
			$(this).removeClass("hidden");
			if(i<data.star||i>data.end){
				$(this).addClass("hidden");
			}
		});
	}
	/**
	 * 初始化键盘事件
	 */
	var handleKeyDown = function() {
		var star = 0;
		var end = 2;
		$(document).keydown(function(e) {
			e = e || window.event; // 取得事件对象
			var keyCode = e.which || e.keyCode; // 按键值
			switch (keyCode) {
			case 37://left
				$(".c_i_right_on").removeClass("c_i_right_on");
				star-=3;
				end-=3;
				if(star<0){
					star=0;
				}
				if(end<2){
					end=2;
				}
				$(".c_i_left").addClass("c_i_left_on");
				paging({"star":star,"end":end});
				break;
			case 39://right
				$(".c_i_left_on").removeClass("c_i_left_on");
				star+=3;			end+=3;
				
				if(star>$(".c_info table tbody tr").length-4){
					star=$(".c_info table tbody tr").length-4;
				}
				if(end>$(".c_info table tbody tr").length-2){
					end=$(".c_info table tbody tr").length-2;
				}
				if($(".c_info table tbody tr").length<=3){
					star=0;end=2;
				}
				$(".c_i_right").addClass("c_i_right_on");
				paging({"star":star,"end":end});
				break;
			case 13://确定
				break;
			case 8:
				break;
			}
		});
	};
	
	
	return {
		init : function(){
			handleContent();
			handleKeyDown();
		}
		
	};
}();