/**
 * 电视直播-党建网站
 */
var DJWZ = function() {
	var paramter = {
		clickNum : 1
	};
	
	var contentData = [{"id":"1","title":"共产党员网","url":"http://www.12371.cn/","imgurl":""},
	            {"id":"2","title":"党建网","url":"http://www.dangjian.cn/","imgurl":""},
	            {"id":"3","title":"人民网","url":"http://www.people.com.cn/","imgurl":""},
	            {"id":"4","title":"中国惠农网","url":"http://www.huinong.org.cn/","imgurl":""},
	            {"id":"5","title":"山东党建网","url":"http://www.sddjw.com/","imgurl":""},
	            {"id":"6","title":"全国党员干部现代远程教育网","url":"http://www.dygbjy.gov.cn:8008/index.html","imgurl":""},
	            {"id":"7","title":"齐鲁先锋网","url":"http://www.sd-taishan.gov.cn/","imgurl":""},
	            {"id":"8","title":"济宁党建网","url":"http://www.jnswzzb.gov.cn/","imgurl":""},
	            {"id":"9","title":"任城党建网","url":"http://dj.rencheng.gov.cn/","imgurl":""},];
	var handleContent = function(){
		var c = "";
		$(contentData).each(function(i){
			console.info(i);
			c +="<div class=\"content_btn\"></div>"; 
		});
		$(".content").html(c);
		$(".content_btn").each(function(i){
			if(i==0){
				$(this).addClass("content_btn_check");
			}
			$(this).addClass("btn_"+(i+1));
		});
	};
	
	/**
	 * 绑定键盘事件
	 */
	var handleKeyDown = function(){
		$(document).keydown(function(e) {
			e = e || window.event; // 取得事件对象
			var keyCode = e.which || e.keyCode; // 按键值
			switch (keyCode) {
			case 37://left
				$(".content_btn_check").removeClass("content_btn_check");
				paramter.clickNum--;
				if(paramter.clickNum<1){
					paramter.clickNum = 1;
				}
				$(".btn_"+paramter.clickNum).addClass("content_btn_check");
				break;
			case 39://right
				$(".content_btn_check").removeClass("content_btn_check");
				paramter.clickNum++;
				if(paramter.clickNum>$(".content_btn").length){
					paramter.clickNum =$(".content_btn").length;
				}
				$(".btn_"+paramter.clickNum).addClass("content_btn_check");
				break;
			case 38://up
				$(".content_btn_check").removeClass("content_btn_check");
				if(paramter.clickNum>3){
					paramter.clickNum-=3;
				}
				$(".btn_"+paramter.clickNum).addClass("content_btn_check");
				break;
			case 40://down
				$(".content_btn_check").removeClass("content_btn_check");
				if(paramter.clickNum<=$(".content_btn").length-3){
					paramter.clickNum+=3;
				}
				$(".btn_"+paramter.clickNum).addClass("content_btn_check");
				break;
			case 13://确定
				window.location.href=contentData[paramter.clickNum-1].url;
				break;
			case 8:
				break;
			}
		});
	}
	
	return {
		/**
		 * 初始化
		 * @returns
		 */
		init : function() {
			handleContent();
			handleKeyDown();
		}
	};
}();