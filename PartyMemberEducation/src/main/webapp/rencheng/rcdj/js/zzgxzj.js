/**
 * 组织关系转接
 */
var ZZGXZJ = function() {
	var param = {
		userInfo : "",	
		
		navClick : 1,
		conClick : 0,
		navOrContnet : 0
	};
	/**
	 * 获取url参数
	 */
	var getUrlParam = function(){
		var url = decodeURI(location.search); //获取url中"?"符后的字串
		var theRequest = new Object();
		if (url.indexOf("?") != -1) {
			var str = url.substr(1);
		    strs = str.split("&");
		    for(var i = 0; i < strs.length; i ++) {
		    	theRequest[strs[i].split("=")[0]]=(strs[i].split("=")[1]);
		    }
		}
		return theRequest;
	};
	/**
	 * 初始化中间左侧区域
	 */
	var handleContentLeft = function() {
		$.ajax({
			type : "GET",
			dataType : 'jsonp',
			async : false,
			data : {
				"callback" : 1,
				"OrgId" : getUrlParam().OrgId,
				"user_type": 6
			},
			url : "http://172.29.0.63:9000/userinfo/userInfoService",
			success : function(result) {
				if(result.success){
					var data = result.data;
					param.userInfo = data;
					var person = "";
					$(data).each(function(i){
						if(i==0){
							person+="<div class=\"nav_btn nav_btn_on nav_btn_"+(i+1)+"\" data=\""+this.sid+"\">"+this.name+"</div>";
						}else if(i>5){
							person+="<div class=\"nav_btn hidden nav_btn_"+(i+1)+"\"  data=\""+this.sid+"\">"+this.name+"</div>";
						}else{
							person+="<div class=\"nav_btn nav_btn_"+(i+1)+"\"  data=\""+this.sid+"\">"+this.name+"</div>";
						}
						
					});
					$(".c_l_c_content").html(person);
					handleContentRight();
				}
			}
		});
	};
	/**
	 * 初始化中间右侧区域
	 */
	var handleContentRight = function() {
		$(".portrait").html("<img src='http://172.29.0.63:9000/userimg?sid="+$(".nav_btn_on").attr("data")+"'/>");
		$(".sex").html("性&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;别:"+param.userInfo[param.navClick-1].sex);
		$(".phone").html("电话号码:"+param.userInfo[param.navClick-1].phone);
		$(".sbrith").html("出生年月:"+(param.userInfo[param.navClick-1].sbrith).substring(0,param.userInfo[param.navClick-1].sbrith.length-2)+"**");
		$(".sid").html("身份证号:"+(param.userInfo[param.navClick-1].sid).substring(0,param.userInfo[param.navClick-1].sid.length-6)+"******");
		$(".sid").attr("data",param.userInfo[param.navClick-1].sid);
	};
	/**
	 * 分页
	 * 
	 */
	var navPaging = function(data){
		$(".nav_btn").each(function(i){
			$(this).removeClass("hidden");
			if(i<data.star||i>data.end){
				$(this).addClass("hidden");
			}
		});
	}
	/**
	 * 初始化键盘事件
	 */
	var handleKeyDown = function() {
		var star =0;
		var end=5;
		$(document).keydown(function(e) {
			e = e || window.event; // 取得事件对象
			var keyCode = e.which || e.keyCode; // 按键值
			$(".msg").html("");
			switch (keyCode) {
			case 37:
				$(".checkbox_on").removeClass("checkbox_on");
				if($(".checkbox_"+param.conClick).hasClass("checkbox_check_on")){
					$(".checkbox_"+param.conClick).addClass("checkbox_check");
					$(".checkbox_check_on").removeClass("checkbox_check_on");
				}
				$(".submit_on").removeClass("submit_on");
				if(param.conClick==3){
					param.conClick=2;
				}else if(param.conClick==2){
					param.conClick=1;
				}else if(param.conClick==1){
					param.conClick=0;
					param.navOrContnet=0;
				}
				if($(".checkbox_"+param.conClick).hasClass("checkbox_check")){
					$(".checkbox_"+param.conClick).addClass("checkbox_check_on");
				}else{
					$(".checkbox_"+param.conClick).addClass("checkbox_on");
				}
				break;
			case 39:
				param.navOrContnet=1;
				$(".checkbox_on").removeClass("checkbox_on");
				if($(".checkbox_"+param.conClick).hasClass("checkbox_check_on")){
					$(".checkbox_"+param.conClick).addClass("checkbox_check");
					$(".checkbox_check_on").removeClass("checkbox_check_on");
				}
				if(param.conClick<3){
					param.conClick++;
				}
				if(param.conClick==3){
					$(".submit").addClass("submit_on");
				}
				if($(".checkbox_"+param.conClick).hasClass("checkbox_check")){
					$(".checkbox_"+param.conClick).addClass("checkbox_check_on");
				}else{
					$(".checkbox_"+param.conClick).addClass("checkbox_on");
				}
				break;
			case 38://up
				if(param.navOrContnet==0){
					$(".nav_btn_on").removeClass("nav_btn_on");
					param.navClick--;
					if(param.navClick<1){
						param.navClick=1;
					}
					if(param.navClick<=end-5&&star>0){
						star--;end--;
						navPaging({"star":star,"end":end});
					}
					$(".nav_btn_"+param.navClick).addClass("nav_btn_on");
					handleContentRight();
				}
				break;
			case 40://down
				if(param.navOrContnet==0){
					$(".nav_btn_on").removeClass("nav_btn_on");
					param.navClick++;
					if(param.navClick>6&&param.navClick<$(".nav_btn").length+1){
						star++;
						end++;
						navPaging({"star":star,"end":end});
					}
					if(param.lclick>$(".nav_btn").length){
						param.lclick=$(".nav_btn").length;
					}
					
					if(param.navClick>$(".nav_btn").length){
						param.navClick=$(".nav_btn").length;
					}
					$(".nav_btn_"+param.navClick).addClass("nav_btn_on");
					handleContentRight();
				}
				break;
			case 13://确定
				if(param.navOrContnet==1){
					if(param.conClick!=3){
						$(".checkbox_check").removeClass("checkbox_check");
						$(".checkbox_on").addClass("checkbox_check_on");
					}else{
						if($(".checkbox_check").attr("data")==undefined){
							$(".msg").html("请选择党员转入或转出!");
						}else if($(".checkbox_check").attr("data")==1){
							$.ajax({
								type : "GET",
								dataType : 'jsonp',
								async : false,
								data : {
									"callback" : 1,
									"OrgId" : 3,
									"sid": $(".sid").attr("data"),
								},
								url : "http://172.29.0.63:9000/userinfo/transferInConfirm",
								success : function(result) {
									console.info(result);
									if(result.success){
										window.location.href="/PartyMemberEducation/PartyMember/success.jsp";
									}
								}
							});
						}else if($(".checkbox_check").attr("data")==2){
							$.ajax({
								type : "GET",
								dataType : 'jsonp',
								async : false,
								data : {
									"callback" : 1,
									"OrgId" : 3,
									"sid": $(".sid").attr("data"),
									"msg":""
								},
								url : "http://172.29.0.63:9000/userinfo/transferOutConfirm",
								success : function(result) {
									console.info(result);
									if(result.success){
										window.location.href="/PartyMemberEducation/rcdj/success.jsp";
									}
								}
							});
						}
						
					}
				}
				break;
			case 8:
				break;
			}
		});
	};

	return {
		init : function() {
			handleContentLeft();
			handleKeyDown();
		}
	};
}();