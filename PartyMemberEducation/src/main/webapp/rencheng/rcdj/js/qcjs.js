﻿/**
 * 全程纪实
 */
var QCJS = function(){
	
	var clickNum = sessionStorage.qczsbtn==undefined?1:sessionStorage.qczsbtn;//1,//右侧导航点击次数//1;
	
	/**
	 * 获取url参数
	 */
	var getUrlParam = function(){
		var url = decodeURI(location.search); //获取url中"?"符后的字串
		var theRequest = new Object();
		if (url.indexOf("?") != -1) {
			var str = url.substr(1);
		    strs = str.split("&");
		    for(var i = 0; i < strs.length; i ++) {
		    	theRequest[strs[i].split("=")[0]]=(strs[i].split("=")[1]);
		    }
		}
		return theRequest;
	};
	var checkZclh = function() {
		$.ajax({
			url : "../village/checkzclh/"+getUrlParam().stbNo,
			type : "get",
			dataType : "json",
			async : false,
			success : function(data) {
				if(data.success){
					var msg = eval("("+data.msg+")");
					if(msg.ishave){
						if(data.obj.level==0){
							url = "../zclh/zbqd_lb.jsp?stbNo="+getUrlParam().stbNo;
						}else if(data.obj.level==1){	//镇级a089e4dc0c9f
							url = "../zclh/zbqd_lb.jsp?stbNo="+getUrlParam().stbNo+"&townId="+data.obj.townId;
						}else if(data.obj.level==2){									//村级a089e4dc0c91
							url = "../zclh/zbqd.jsp?OrgId="+data.obj.orgCode+"&v=true";
						}
						window.location.href=url;
					}else{
						console.info("无权限！");
						$(".msg").html(msg.msg);
						setTimeout(function(){
							$(".msg").html("");
						},2000);
					}
				}else{
					console.info("异常！");
					$(".msg").html(msg.msg);
					setTimeout(function(){
						$(".msg").html("");
					},2000);
				}
			},error : function(a,b,c){
				
			}
		});
	}
	/**
	 * 判断跳转连接
	 */
	var handleLink = function(d){
		$.ajax({
			url:"../stbController/getStbByStbNo",
			type:"post",
			dataType:"json",
			async: false,
			data : {
				"stbNo":getUrlParam().stbNo
			},
			success:function(data){
				var url = "";
				if(data.obj.orgCode==="0"){				//县级a089e4dc0c27
					url = "../"+d+"/zbqd_lb.jsp?stbNo="+getUrlParam().stbNo;
				}else if(Number(data.obj.orgCode)<0){	//镇级a089e4dc0c9f
					url = "../"+d+"/zbqd_lb.jsp?stbNo="+getUrlParam().stbNo+"&townId="+data.obj.townId;
				}else{									//村级a089e4dc0c91
					url = "../"+d+"/zbqd.jsp?OrgId="+data.obj.orgCode+"&v=true";
				}
				window.location.href=url;
			}
		});
	};
	
	
	var handleJK = function(d){
		$.ajax({
			url:"../stbController/getStbByStbNo",
			type:"post",
			dataType:"json",
			async: false,
			data : {
				"stbNo":getUrlParam().stbNo
			},
			success:function(data){
				var url="";
				if(d=="zdjk"){
					if(data.obj.orgCode==="0"){		
						url="../rcdj/zdjk.jsp?OrgId="+getUrlParam().OrgId+"&stbNo="+getUrlParam().stbNo;
					}else if(Number(data.obj.orgCode)<0){
						url="../rcdj/zdjk.jsp?OrgId="+getUrlParam().OrgId+"&stbNo="+getUrlParam().stbNo+"&townId="+data.obj.townId;
					}else{
						url="../rcdj/zdjk.jsp?OrgId="+getUrlParam().OrgId+"&stbNo="+getUrlParam().stbNo+"&townId="+data.obj.townId;
					}
				}else if(d=="hyhk"){
					if(data.obj.orgCode==="0"){		
						url="../rcdj/zdjk.jsp?stbNo="+getUrlParam().stbNo+"&type=meeting&cType=0";
					}else if(Number(data.obj.orgCode)<0){
						url="../rcdj/zdjk.jsp?stbNo="+getUrlParam().stbNo+"&type=meeting&cType=0&townId="+data.obj.townId;
					}else{
						url="../rcdj/zdjk.jsp?stbNo="+getUrlParam().stbNo+"&type=meeting&cType=0&townId="+data.obj.townId;
					}
				}
				window.location.href=url;
			}
		});
		
	};
	
	
	/**
	 * 初始化键盘事件
	 */
	var handleKeyDown = function() {
		if(clickNum==5){
			$(".zclh").addClass("zclh_on");
		}else if(clickNum==4){
			$(".zbqd").addClass("zbqd_on");
		}else if(clickNum==3){
			$(".hysz").addClass("hysz_on");
		}else if(clickNum==2){
			$(".hyhf").addClass("hyhf_on");
		}else if(clickNum==1){
			$(".hyjk").addClass("hyjk_on");
		}
		$(document).keydown(function(e) {
			e = e || window.event; // 取得事件对象
			var keyCode = e.which || e.keyCode; // 按键值
			switch (keyCode) {
			case 38://up
				if(clickNum==5){
					$(".zclh").removeClass("zclh_on");
					clickNum=4;
					$(".zbqd").addClass("zbqd_on");
				}else if(clickNum==4){
					$(".zbqd").removeClass("zbqd_on");
					clickNum=3;
					$(".hysz").addClass("hysz_on");
				}else if(clickNum==3){
					$(".hysz").removeClass("hysz_on");
					clickNum=2;
					$(".hyhf").addClass("hyhf_on");
				}else if(clickNum==2){
					$(".hyhf").removeClass("hyhf_on");
					clickNum=1;
					$(".hyjk").addClass("hyjk_on");
				}
				$(".c_m_right img").attr("src","images/qcjs/lzt/"+clickNum+".png");
				break;
			case 40://down
				if(clickNum==1){
					$(".hyjk").removeClass("hyjk_on");
					clickNum=2;
					$(".hyhf").addClass("hyhf_on");
				}else if(clickNum==2){
					$(".hyhf").removeClass("hyhf_on");
					clickNum=3;
					$(".hysz").addClass("hysz_on");
				}else if(clickNum==3){
					$(".hysz").removeClass("hysz_on");
					clickNum=4;
					$(".zbqd").addClass("zbqd_on");
				}else if(clickNum==4){
					$(".zbqd").removeClass("zbqd_on");
					clickNum=5;
					$(".zclh").addClass("zclh_on");
				}
				$(".c_m_right img").attr("src","images/qcjs/lzt/"+clickNum+".png");
				break;
			case 13://确定
				sessionStorage.qczsbtn=clickNum;
				if(clickNum==1){
					handleJK("zdjk");
				}else if(clickNum==2){
					handleJK("hyhk");
				}else if(clickNum==3){
					window.location.href="../PartySign/meetingtype.jsp?OrgId="+getUrlParam().OrgId+"&stbNo="+getUrlParam().stbNo;
				}else if(clickNum==4){
					handleLink("zbqd");
				}else if(clickNum==5){
					checkZclh();
				}
				break;
			}
		});
	};
	return {
		init : function(){
			$.cookie("hyjklr",null);
			$.cookie("hyjkclick",null);
			$.cookie("hyjkstart",null);
			$.cookie("hyjksize",null);
			$.cookie("hyjkclickr",null);
			$.cookie("hyjkstartr",null);
			$.cookie("hyjksizer",null);
			sessionStorage.clear();
			handleKeyDown();
		}
	};
}();
QCJS.init();