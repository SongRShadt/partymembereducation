<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%
	String path = request.getContextPath();
	String basePath = request.getScheme() + "://" + request.getServerName() + ":" + request.getServerPort() + path + "/";
%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<title>任城--->党员服务--->党务工作咨询</title>
<meta http-equiv="Content-type" content="text/html; charset=utf-8" />
<meta name="page-view-size" content="1280*720" />
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<link rel="stylesheet" href="<%=basePath%>rcdj/css/main.css" type="text/css"></link>
<link rel="stylesheet" href="<%=basePath%>rcdj/css/dwgzzx.css" type="text/css"></link>
</head>
<body>
<div class="main">
	<div class="header"></div>
	<div class="content">
		<div class="c_left">
			<div class="nav_btn nav_btn_1 nav_btn_on" style="margin-top:28px;">发展党员流程</div>
			<div class="nav_btn nav_btn_2">党费收缴用途</div>
			<div class="nav_btn nav_btn_3">党组织关系转接程序</div>
			<div class="nav_btn nav_btn_4">组织生活开展</div>
		</div>
		<div class="c_right">
			<div id="content_top" class="content_top">
	     	</div>
		</div>
	</div>
	<div class="footer"></div>
</div>

<!-- corescript -->
	<script type="text/javascript" src="<%=basePath%>rcdj/js/jquery-1.8.3.js"></script>
	<script type="text/javascript" src="<%=basePath%>rcdj/js/marqueeController.js"></script>
	<script type="text/javascript" src="<%=basePath%>rcdj/js/dwgzzx.js"></script>
	<script type="text/javascript">
		jQuery(document).ready(function() {
// 			document.onkeydown = evm_cms_domCtrl.trackKey;
 			DWGZZX.init();
		});
	</script>
</body>
</html>