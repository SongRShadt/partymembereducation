<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%
	String path = request.getContextPath();
	String basePath = request.getScheme() + "://"
			+ request.getServerName() + ":" + request.getServerPort()
			+ path + "/";
%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>阳光村务—>村级组织建设、党务村务公开</title>
<link rel="stylesheet" href="css/main.css" type="text/css"></link>
<link rel="stylesheet" href="css/ygcw.css" type="text/css"></link>
</head>
<body>
<div class="main">
	<div class="header">
		<div class="logo"></div>
	</div>
	<div class="content">
		<div class="c_left">
		</div>
		<div class="c_right"></div>
	</div>
<!-- 	<div class="footer"></div> -->
</div>

<!-- corescript -->
<script type="text/javascript" src="js/jquery-1.8.3.js"></script>
<script type="text/javascript" src="js/ygcw.js"></script>
<script type="text/javascript">
	jQuery(document).ready(function() {
		YGCW.init();
	});
</script>
</body>
</html>