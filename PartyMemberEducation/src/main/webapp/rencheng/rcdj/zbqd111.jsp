<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%
	String path = request.getContextPath();
	String basePath = request.getScheme() + "://"
			+ request.getServerName() + ":" + request.getServerPort()
			+ path + "/";
%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>坐班签到</title>
<link rel="stylesheet" href="<%=basePath%>rcdj/css/main.css" type="text/css"></link>
<link rel="stylesheet" href="<%=basePath%>rcdj/css/zbqd.css" type="text/css"></link>
</head>
<body>
<div class="main">
	<div class="header">
		<div class="logo"></div>
	</div>
	<div class="content">
		<div class="c_left">
			<div class="c_l_top">
			</div>
			<div class="c_l_bottom">
				<div class="btn btn_1 btn_on">上页</div>
				<div class="btn btn_2">下页</div>
				<div class="btn btn_3">返回</div>
			</div>
		</div>
		<div class="c_right"><img src="images/zbqd/zst/1.png"> </div>
	</div>
	<div class="footer"></div>
</div>

<!-- corescript -->
<script type="text/javascript" src="<%=basePath%>rcdj/js/jquery-1.8.3.js"></script>
<script type="text/javascript" src="<%=basePath%>rcdj/js/ConfigUtil.js"></script>
<script type="text/javascript" src="<%=basePath%>rcdj/js/zbqd.js"></script>
<script type="text/javascript">
	jQuery(document).ready(function() {
		ZBQD.init();
	});
</script>
</body>
</html>