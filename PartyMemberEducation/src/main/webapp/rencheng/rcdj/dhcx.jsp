<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%
	String path = request.getContextPath();
	String basePath = request.getScheme() + "://"
			+ request.getServerName() + ":" + request.getServerPort()
			+ path + "/";
%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>电话查询</title>
<link rel="stylesheet" href="<%=basePath%>rcdj/css/main.css" type="text/css"></link>
<link rel="stylesheet" href="<%=basePath%>rcdj/css/dhcx.css" type="text/css"></link>
</head>
<body>
<div class="main">
	<div class="header">
		<div class="logo"></div>
	</div>
	<div class="content">
		<div id="content_top" class="content_top">
			<img alt="" src="images/dhcx/nr/dhcx.png">
     	</div>
	</div>
	<div class="footer"></div>
</div>
<!-- 	document.onkeydown = evm_cms_domCtrl.trackKey; -->
<!-- corescript -->
<script type="text/javascript" src="<%=basePath%>rcdj/js/jquery-1.8.3.js"></script>
<script type="text/javascript" src="<%=basePath%>rcdj/js/marqueeController.js"></script>
<script type="text/javascript" src="<%=basePath%>rcdj/js/ConfigUtil.js"></script>
<script type="text/javascript" src="<%=basePath%>rcdj/js/dhcx.js"></script>
<script type="text/javascript">
	jQuery(document).ready(function() {
		DHCX.init();
	});
</script>
</body>
</html>