/**
 * 会议主题
 */
var MeetingTheme = function() {
	var param = {
		clickNum : 1,
		OrgId : 0,//村组织编号
	};
	/**
	 * 获取url参数
	 */
	var HandleGetRequest = function(){
		var url = location.search; //获取url中"?"符后的字串
		var theRequest = new Object();
		if (url.indexOf("?") != -1) {
			var str = url.substr(1);
		    strs = str.split("&");
		    for(var i = 0; i < strs.length; i ++) {
		    	theRequest[strs[i].split("=")[0]]=(strs[i].split("=")[1]);
		    }
		}
		return theRequest
	};
	/**
	 * 绑定键盘事件
	 */
	var handleKeyDown = function() {
		$(".content_check_btn").each(function(i) {
			if (i == 0) {
				$(this).addClass("content_check_btn_off_down");
			}
			$(this).attr("data",(i+1));
			$(this).addClass("btn_" + (i + 1));
		});
		
		if(HandleGetRequest().Topic_ID!= undefined){
			var spTopic = (HandleGetRequest().Topic_ID).split(",");
			for(var i in spTopic){
				if(spTopic[i]==1){
					$(".btn_"+spTopic[i]).addClass("content_check_btn_on_down");
				}else{
					$(".btn_"+spTopic[i]).addClass("content_check_btn_on_over");
				}
			}
		}
		$(document).keydown(function(e) {
			e = e || window.event; // 取得事件对象
			var keyCode = e.which || e.keyCode; // 按键值
			switch (keyCode) {
			case 37:// left
				$(".content_check_btn_off_down").removeClass("content_check_btn_off_down");
				if($(".btn_" + param.clickNum).hasClass("content_check_btn_on_down")){
					$(".content_check_btn_on_down").addClass("content_check_btn_on_over");
				}
				param.clickNum--;
				if (param.clickNum < 1) {
					param.clickNum = 11;
				}if(param.clickNum==11){
					$(".btn_" + param.clickNum).addClass("next_btn_on");
				}else{
					$(".next_btn_on").removeClass("next_btn_on");
					$(".btn_" + param.clickNum).addClass("content_check_btn_off_down");
					if($(".btn_" + param.clickNum).hasClass("content_check_btn_on_over")){
						$(".btn_" + param.clickNum).removeClass("content_check_btn_on_over");
						$(".btn_" + param.clickNum).addClass("content_check_btn_on_down");
					}
				}
				break;
			case 39:// right
				$(".content_check_btn_off_down").removeClass("content_check_btn_off_down");
				if($(".btn_" + param.clickNum).hasClass("content_check_btn_on_down")){
					$(".content_check_btn_on_down").addClass("content_check_btn_on_over");
				}
				param.clickNum++;
				if (param.clickNum > 11) {
					param.clickNum = 1;
				}if(param.clickNum==11){
					$(".btn_" + param.clickNum).addClass("next_btn_on");
				}else{
					$(".next_btn_on").removeClass("next_btn_on");
					$(".btn_" + param.clickNum).addClass("content_check_btn_off_down");
					if($(".btn_" + param.clickNum).hasClass("content_check_btn_on_over")){
						$(".btn_" + param.clickNum).removeClass("content_check_btn_on_over");
						$(".btn_" + param.clickNum).addClass("content_check_btn_on_down");
					}
				}
				break;
			case 38://up
				$(".content_check_btn_off_down").removeClass("content_check_btn_off_down");
				if($(".btn_" + param.clickNum).hasClass("content_check_btn_on_down")){
					$(".content_check_btn_on_down").addClass("content_check_btn_on_over");
				}
				if(param.clickNum>2&&param.clickNum!=11){
					param.clickNum-=2;
				}else if(param.clickNum==1){
					param.clickNum=11;
				}else if(param.clickNum==11){
					$(".next_btn_on").removeClass("next_btn_on");
					param.clickNum=10;
				} 
				if(param.clickNum!=11){
					$(".btn_" + param.clickNum).addClass("content_check_btn_off_down");
					if($(".btn_" + param.clickNum).hasClass("content_check_btn_on_over")){
						$(".btn_" + param.clickNum).removeClass("content_check_btn_on_over");
						$(".btn_" + param.clickNum).addClass("content_check_btn_on_down");
					}
				}else{
					$(".btn_" + param.clickNum).addClass("next_btn_on");
				}
				break;
			case 40://down
				$(".content_check_btn_off_down").removeClass("content_check_btn_off_down");
				if($(".btn_" + param.clickNum).hasClass("content_check_btn_on_down")){
					$(".content_check_btn_on_down").addClass("content_check_btn_on_over");
				}
				if(param.clickNum<10){
					param.clickNum+=2;
				}else if(param.clickNum==10){
					param.clickNum++;
				}
				else if(param.clickNum==11){
					$(".next_btn_on").removeClass("next_btn_on");
					param.clickNum=1;
				} 
				if(param.clickNum!=11){
					$(".btn_" + param.clickNum).addClass("content_check_btn_off_down");
					if($(".btn_" + param.clickNum).hasClass("content_check_btn_on_over")){
						$(".btn_" + param.clickNum).removeClass("content_check_btn_on_over");
						$(".btn_" + param.clickNum).addClass("content_check_btn_on_down");
					}
				}else{
					$(".btn_" + param.clickNum).addClass("next_btn_on");
				}
				break;
			case 13:// 确定
				if(param.clickNum!=11){
					if($(".btn_" + param.clickNum).hasClass("content_check_btn_on_down")||
					   $(".btn_" + param.clickNum).hasClass("content_check_btn_on_over")){
						$(".btn_" + param.clickNum).removeClass("content_check_btn_on_down");
						$(".btn_" + param.clickNum).removeClass("content_check_btn_on_over");
					}else{
						$(".btn_" + param.clickNum).addClass("content_check_btn_on_down");
					}
					$(".msg").html("");
				}else{
					var Topic_ID = "";
					$(".content_check_btn_on_over").each(function(i){
						Topic_ID+=$(this).attr("data")+",";
					});
					var MeetingName_ID=HandleGetRequest().MeetingName_ID;
					if(Topic_ID!=""){
						window.location.href="/PartyMemberEducation/PartySign/meetingsign.jsp?callback=1&OrgId="+HandleGetRequest().OrgId+"&MeetingName_ID="+MeetingName_ID+"&Topic_ID="+Topic_ID;
					}else{
						$(".msg").html("请选择会议主题!");
					}
				}
				break;
			case 8:
				break;
			}
		});
	}
	return {
		/**
		 * 初始化
		 * 
		 * @returns
		 */
		init : function() {
			handleKeyDown();
		}
	};
}();