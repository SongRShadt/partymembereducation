<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<meta name="page-view-size" content="1280*720" />
<title>便民电话查询</title>
<link type="text/css" rel="stylesheet" href="css/index.css"></link>
</head>
<body>
	<div class="main">
		<div class="header"></div>
		<div class="content">
			<div class="content_main">
				<div class="content_main_btn">区教体局便民电话</div>
				<div class="content_main_btn">区人社局便民电话</div>
				<div class="content_main_btn">区民政局便民电话</div>
				<div class="content_main_btn">区卫计局便民电话</div>
				<div class="content_main_btn">区环保局便民电话</div>
				<div class="content_main_btn">区住建局便民电话</div>
				<div class="content_main_btn">区文广新局便民电话</div>
				<div class="content_main_btn">区公安局便民电话</div>
			
			</div>
		
		</div>
		<div class="footer"></div>
	</div>
	<script type="text/javascript" src="js/jquery-1.8.3.js"></script>
	<script type="text/javascript" src="js/index.js"></script>
	<script type="text/javascript">
	jQuery(document).ready(function() {
		Index.init();
	});
	</script>
</body>
</html>