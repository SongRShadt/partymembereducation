/**
 * 坐班签到
 */
var ZBQD = function(){
	var param = {
		conference_id:0,
		signip:"172.29.0.63:9000"
	};
	var clickNum = 1;
	var star = 0;
	var end = 3;
	var btnData = [
	    {id:1,name:"上页"},
	    {id:2,name:"下页"},
	    {id:3,name:"返回"},
	];
 
	
	/**
	 * 获取url参数
	 */
	var getUrlParam = function(){
		var url = decodeURI(location.search); //获取url中"?"符后的字串
		var theRequest = new Object();
		if (url.indexOf("?") != -1) {
			var str = url.substr(1);
		    strs = str.split("&");
		    for(var i = 0; i < strs.length; i ++) {
		    	theRequest[strs[i].split("=")[0]]=(strs[i].split("=")[1]);
		    }
		}
		return theRequest;
	};
	
	/**
	 * 初始化按钮
	 */
	var handleBtn = function(){
		var btnHtml = "";
		if(getUrlParam().v){
			btnData.push({id:4,name:"视频录制"});
		}else{
			btnData.push({id:4,name:"坐班截图"});
		}
		$.each(btnData,function(i){
			if(i<3){
				btnHtml+="<div class='btn btn_"+(i+1)+"'>"+this.name+"</div>";
			}else{
				btnHtml+="<div class='btnb btn_"+(i+1)+"'>"+this.name+"</div>";
			}
		});
		$(".c_l_bottom").html(btnHtml);
		$(".btn_"+clickNum).addClass("btn_on");
	};
	
	
	/**
	 * 初始化内容区域
	 */
	var handleContent = function() {
		$.ajax({
			type : "GET",
			dataType : 'jsonp',
			async : false,
			data : {
				"callback" : 1,
				"OrgId" :getUrlParam().OrgId,
			},
			url : "http://"+param.signip+"/userinfo/VillageUser",
			success : function(result) {
				var portrait = "";
				if(result.success){
					$(result.data).each(function(i) {
						portrait+="<div class=\"c_l_t_btn\" id=\"sid_"+this.sid+"\"><img width=\"141\" height=\"139\" src='images/zbqd/def.png'/><br/>"+this.name+"</div>";
					});
					$(".c_l_top").html(portrait);
					param.conference_id=result.conference_id;
					paging({"star":"0","end":"3"});
				}
				setTimeout(changeSign(),3000);
			}
		});
	};
	
	/**
	 * 获取已签到人员信息
	 */
	var changeSign = function(){
		$.ajax({
			type : "GET",
			dataType : 'jsonp',
			async : false,
			data : {
				"callback" : 1,
				"OrgId" :  getUrlParam().OrgId,
				"conference_id" : param.conference_id
			},
			url : "http://"+param.signip+"/userinfo/VillageSign",		 
			success : function(result) {
				if (result.success) {
					$(result.data).each(function(i){
						$('#sid_'+this.sid).html("<img width=\"141\" height=\"139\" src='http://172.29.0.63:9000/userimg?sid="+this.sid+"'/><br/>"+this.name);
					});
				}
				setTimeout(changeSign(),3000);
			}
		});
	};
	/**
	 * 截图
	 */
	var screenshot = function(){
		$.get("../camera/saveImgByVillage/"+getUrlParam().villageId+"/2",function(d){
			d = eval("("+d+")");
			if(d.success){
				console.info("截图成功！");
				$(".content").append("<div class='msg'>截图成功！</div>");
				setTimeout(function(){$(".msg").remove();},3000);
			}else{
				$(".content").append("<div class='msg'>截图失败！</div>");
				setTimeout(function(){$(".msg").remove();},3000);
				console.info("error");
			}
		});
	};
	/**
	 * 录制
	 */
	var recording =function(){
		$.get("../camera/recording/"+getUrlParam().stbNo+"/2",function(d){
			d = eval("("+d+")");
			if(d.success){
				window.location.href=d.obj.cameraUrl;
			}else{
				$(".content").append("<div class='msg'>接口异常！</div>");
				setTimeout(function(){$(".msg").remove();},3000);
				console.info("error");
			}
		});
	};

	/**
	 * 分页
	 * 
	 */
	var paging = function(data){
		$(".c_l_t_btn").each(function(i){
			$(this).removeClass("hidden");
			if(i<data.star||i>data.end){
				$(this).addClass("hidden");
			}
		});
	};
	
	/**
	 * 绑定键盘事件
	 */
	var handleKeyDown = function() {
		$(document).keydown(function(e) {
			e = e || window.event; // 取得事件对象
			var keyCode = e.which || e.keyCode; // 按键值
			switch (keyCode) {
			case 37:// left
				if(clickNum==4){
					$(".btnb_on").removeClass("btnb_on");
					clickNum=3;
					$(".btn_"+clickNum).addClass("btn_on");
				}else if(clickNum==3){
					$(".btn_on").removeClass("btn_on");
					clickNum=2;
					$(".btn_"+clickNum).addClass("btn_on");
				}else if(clickNum==2){
					$(".btn_on").removeClass("btn_on");
					clickNum=1;
					$(".btn_"+clickNum).addClass("btn_on");
				}
				break;
			case 39:// right
				if(clickNum==1){
					$(".btn_on").removeClass("btn_on");
					clickNum=2;
					$(".btn_"+clickNum).addClass("btn_on");
				}else if(clickNum==2){
					$(".btn_on").removeClass("btn_on");
					clickNum=3;
					$(".btn_"+clickNum).addClass("btn_on");
				}else if(clickNum==3){
					$(".btn_on").removeClass("btn_on");
					clickNum=4;
					$(".btn_"+clickNum).addClass("btnb_on");
				}
				break;
			case 38:
				if(clickNum!=5){
					$(".btn_on").removeClass("btn_on");
					$(".btnb_on").removeClass("btnb_on");
					clickNum=5;
					$(".c_right").addClass("c_right_on");
				}
				break;
			case 40:
				if(clickNum==5){
					$(".c_right_on").removeClass("c_right_on");
					clickNum=1;
					$(".btn_"+clickNum).addClass("btn_on");
				}
				break;
			case 13:// 确定
				if(clickNum==1){
					star-=4;
					end-=4;
					if(star<0){
						star=0;
					}
					if(end<3){
						end=3;
					}
					paging({"star":star,"end":end});
				}else if(clickNum==2){
					if(end<$(".c_l_t_btn").length){
						star+=4;
						end+=4;
					}
					paging({"star":star,"end":end});
				}else if(clickNum==3){
					window.location.href="../rcdj/qcjs.jsp?OrgId="+getUrlParam().OrgId+"&stbNo="+getUrlParam().stbNo;
				}else if(clickNum==5){
					$.get("../camera/getByVillage/"+getUrlParam().villageId+"/2",function(d){
						d = eval("("+d+")");
						if(d.success){
							window.location.href=d.obj.cameraUrl;
						}else{
							console.info("error");
						}
					});
				}else if(clickNum==4){//坐班截图
					if(getUrlParam().v){
						recording();
					}else{
						screenshot();
					}
				}
				break;
			case 8:
				break;
			}
		});
	};
	
	return {
		init : function(){
			handleBtn();
			handleKeyDown();
		}
	};
}();
ZBQD.init();