<%@ page language="java" import="java.util.*" pageEncoding="utf-8"%>
<%
	String path = request.getContextPath();
	String basePath = request.getScheme() + "://" + request.getServerName() + ":" + request.getServerPort() + path + "/";
	request.setAttribute("path", basePath);
%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
<head>
<title>站点监控</title>
<meta http-equiv="Content-type" content="text/html; charset=utf-8" />
<meta name="page-view-size" content="1280*720" />
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<link rel="stylesheet" href="${path}zbqd/css/zbqd_lb.css" type="text/css"></link></head>
<body>
	<div class="main">
		<div class="header">
		</div>
		<div class="content">
			<div class="contnet_left_up"></div>
			<div class="content_left">
			
			</div>
			<div class="contnet_left_down"></div>
			<div class="content_right">
				
			
			</div>
		</div>
		<div class="footer"></div>
	</div>

	<!-- core script -->
	<script type="text/javascript" src="../js/jquery-1.8.3.js"></script>
	<script type="text/javascript" src="../js/jquery.cookie.js"></script>
	<script type="text/javascript" src="js/zbqd_lb.js"></script>
	<script type="text/javascript">
		jQuery(document).ready(function() {
			ZBQD_LB.init();
		});
	</script>
</body>
</html>
