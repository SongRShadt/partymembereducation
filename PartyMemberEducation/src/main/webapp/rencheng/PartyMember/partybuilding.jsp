<%@ page language="java" import="java.util.*" pageEncoding="utf-8"%>
<%
	String path = request.getContextPath();
	String basePath = request.getScheme() + "://"
			+ request.getServerName() + ":" + request.getServerPort()
			+ path + "/";
%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
<head>
<base href="<%=basePath%>">
<title>电视直播-济宁党建</title>
<meta http-equiv="Content-type" content="text/html; charset=utf-8" />
<meta name="page-view-size" content="1280*720" />
<meta name="viewport" content="width=device-width, initial-scale=1.0">

<link rel="stylesheet" href="<%=basePath%>PartyMember/css/partybuilding.css" type="text/css"></link>
</head>
<body>
	<div class="main">
		<div class="header">
			<div class="header_title">
				<div class="header_left_title">央视频道</div>
				<img class="header_arraw"
					src="<%=basePath%>PartyMember/image/arrow.png">
				<div class="header_right_title">党建频道</div>
				</img>
			</div>
		</div>
		<div class="content">
			<div class="content_btn">&nbsp;&nbsp;&nbsp;&nbsp;CCTV-1</div>
			<div class="content_btn">&nbsp;&nbsp;&nbsp;&nbsp;CCTV-3</div>
			<div class="content_btn">&nbsp;&nbsp;&nbsp;&nbsp;CCTV-13</div>
		</div>
		<div class="footer"></div>
	</div>
	<!-- corescript -->
	<script type="text/javascript" src="<%=basePath%>js/jquery-1.8.3.js"></script>
	<script type="text/javascript" src="<%=basePath%>PartyMember/js/partybuilding.js"></script>
	<script type="text/javascript">
		jQuery(document).ready(function() {
			PartBuilding.init();
		});
	</script>
</body>
</html>
