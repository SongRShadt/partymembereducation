<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Insert title here</title>
</head>
<body>
	<script type="text/javascript">
		/**
		 * 获取url参数
		 */
		var getUrlParam = function() {
			var url = decodeURI(location.search); //获取url中"?"符后的字串
			var theRequest = new Object();
			if (url.indexOf("?") != -1) {
				var str = url.substr(1);
				strs = str.split("&");
				for (var i = 0; i < strs.length; i++) {
					theRequest[strs[i].split("=")[0]] = (strs[i].split("=")[1]);
				}
			}
			return theRequest;
		};
		var url = "";
		if(getUrlParam().callmelady=="qcjs"){
			url = "/PartyMemberEducation/rcdj/qcjs.jsp";
		}
		if(getUrlParam().callmelady=="djyd"){
			url = "/PartyMemberEducation/PartyMember/Partybuildingmaterials.jsp";
		}
		if(getUrlParam().callmelady=="hsyz"){
			url = "/PartyMemberEducation/PartyMember/partybuildingwebsite.jsp";
		}
		if(getUrlParam().callmelady=="bmdhcx"){
			url = "/PartyMemberEducation/rcdj/dhcx.jsp";
		}
		if(getUrlParam().callmelady=="ygcw"){
			url = "/sync_ott_sdjnrcdj/Entrance/CityModel/ModelJSP/SDJNRCDJ_YGCW/index.html";
		}
		if(getUrlParam().callmelady=="xzzj"){
			url = "/sync_ott_sdjnrcdj/Entrance/CityModel/ModelJSP/SDJNRCDJ_XZZJ/index.html";
		}
		if(getUrlParam().callmelady=="hmzc"){
			url = "/sync_ott_sdjnrcdj/Entrance/CityModel/ModelEffect/ModelList/ListPage.jsp?key=HNZC";
		}
		if(getUrlParam().callmelady=="bjcs"){
			url = "/sync_ott_sdjnrcdj/Entrance/CityModel/ModelEffect/ModelListItems/ListItemsPage.jsp?key=BJCS";
		}
		if(getUrlParam().callmelady=="zpxx"){
			url = "/sync_ott_sdjnrcdj/Entrance/CityModel/ModelEffect/ModelListItems/ListItemsPage.jsp?key=ZPXX";
		}
		if(getUrlParam().callmelady=="qmcy"){
			url = "/sync_ott_sdjnrcdj/Entrance/CityModel/ModelEffect/ModelListItems/ListItemsPage.jsp?key=QMCY";
		}

		window.location.href = url;
	</script>
</body>
</html>