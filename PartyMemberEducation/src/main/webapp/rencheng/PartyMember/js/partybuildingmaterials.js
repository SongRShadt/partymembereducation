﻿/**
 * 电视直播-党建网站
 */
var PartyBuildingmaterials = function() {
	var paramter = {
		clickNum : 1
	};
	/**
	 * 绑定键盘事件
	 */
	var handleKeyDown = function(){
		$(".content_btn").each(function(i){
			if(i==0){
				$(this).addClass("content_btn_check");
			}
			$(this).addClass("btn_"+(i+1));
		});
		$(document).keydown(function(e) {
			e = e || window.event; // 取得事件对象
			var keyCode = e.which || e.keyCode; // 按键值
			switch (keyCode) {
			case 37://left
				$(".content_btn_check").removeClass("content_btn_check");
				paramter.clickNum--;
				if(paramter.clickNum<1){
					paramter.clickNum = 6;
				}
				$(".btn_"+paramter.clickNum).addClass("content_btn_check");
				break;
			case 39://right
				$(".content_btn_check").removeClass("content_btn_check");
				paramter.clickNum++;
				if(paramter.clickNum>6){
					paramter.clickNum =1;
				}
				$(".btn_"+paramter.clickNum).addClass("content_btn_check");
				break;
			case 38://up
				$(".content_btn_check").removeClass("content_btn_check");
				if(paramter.clickNum>3){
						paramter.clickNum-=3;
				}
				$(".btn_"+paramter.clickNum).addClass("content_btn_check");
				break;
			case 40://down
				$(".content_btn_check").removeClass("content_btn_check");
				if(paramter.clickNum<4){
						paramter.clickNum+=3;
				}
				$(".btn_"+paramter.clickNum).addClass("content_btn_check");
				break;
			case 13://确定
				if(paramter.clickNum==1){
					window.location.href="/sync_ott_sdjnrcdj/Entrance/CityModel/ModelEffect/ModelItems/ItemsTextPage.jsp?key=XJPZYJH&rid=null";
				}else if(paramter.clickNum==2){
					window.location.href="/sync_ott_sdjnrcdj/Entrance/CityModel/ModelEffect/ModelItems/ItemsTextPage.jsp?key=CJGL&rid=null";
				}if(paramter.clickNum==3){
					window.location.href="/sync_ott_sdjnrcdj/Entrance/CityModel/ModelEffect/ModelItems/ItemsTextPage.jsp?key=JCDJ&rid=null";
				}if(paramter.clickNum==4){
					window.location.href="/sync_ott_sdjnrcdj/Entrance/CityModel/ModelEffect/ModelListItems/ListItemsPage.jsp?key=WXDJLM";
				}if(paramter.clickNum==5){
					window.location.href="/sync_ott_sdjnrcdj/Entrance/CityModel/ModelEffect/ModelItems/ItemsTextPage.jsp?key=ZCLH&rid=null";
				}if(paramter.clickNum==6){
					window.location.href="/sync_ott_sdjnrcdj/Entrance/CityModel/ModelEffect/ModelItems/ItemsTextPage.jsp?key=SDKM&rid=null";
				}
				break;
			case 8:
				break;
			}
		});
	}
	
	return {
		/**
		 * 初始化
		 * @returns
		 */
		init : function() {
			handleKeyDown();
		}
	};
}();