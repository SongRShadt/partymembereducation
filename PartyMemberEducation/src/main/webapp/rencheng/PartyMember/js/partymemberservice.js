/**
 * 党员服务
 */
var PartyMemberService = function() {
	var paramter = {
		clickNum : 1
	};
	/**
	 * 绑定键盘事件
	 */
	var handleKeyDown = function(){
		$(".content_btn").each(function(i){
			if(i==0){
				$(this).addClass("content_btn_check");
			}
			$(this).addClass("btn_"+(i+1));
		});
		$(document).keydown(function(e) {
			e = e || window.event; // 取得事件对象
			var keyCode = e.which || e.keyCode; // 按键值
			switch (keyCode) {
			case 37://left
				$(".content_btn_check").removeClass("content_btn_check");
				paramter.clickNum--;
				if(paramter.clickNum<1){
					paramter.clickNum = 1;
				}
				$(".btn_"+paramter.clickNum).addClass("content_btn_check");
				break;
			case 39://right
				$(".content_btn_check").removeClass("content_btn_check");
				paramter.clickNum++;
				if(paramter.clickNum>4){
					paramter.clickNum =4;
				}
				$(".btn_"+paramter.clickNum).addClass("content_btn_check");
				break;
			case 38:
				$(".content_btn_check").removeClass("content_btn_check");
				if(paramter.clickNum>=3){
					paramter.clickNum -=2;
				}
				$(".btn_"+paramter.clickNum).addClass("content_btn_check");
				break;
			case 40:
				$(".content_btn_check").removeClass("content_btn_check");
				if(paramter.clickNum<=2){
					paramter.clickNum +=2;
				}
				$(".btn_"+paramter.clickNum).addClass("content_btn_check");
				break;
			case 13://确定
				if(paramter.clickNum==1){
				    window.location.href="rcdj/zzgxzj.jsp?OrgId="+ConfigUtil.getUrlParam().OrgId;
				}
				if(paramter.clickNum==2){
					window.location.href="rcdj/dfsj.jsp?OrgId="+ConfigUtil.getUrlParam().OrgId;
				}
				if(paramter.clickNum==3){
					window.location.href="rcdj/dwgzzx.jsp";
				}else if(paramter.clickNum==4){
					window.location.href="PartyMember/pic/callnumber.png";
				}
				break;
			case 8:
				break;
			}
		});
	}
	
	return {
		/**
		 * 初始化
		 * @returns
		 */
		init : function() {
			handleKeyDown();
		}
	};
}();