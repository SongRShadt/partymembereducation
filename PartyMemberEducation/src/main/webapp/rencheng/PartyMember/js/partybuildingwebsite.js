/**
 * 电视直播-党建网站
 */
var PartyBuildingWebSite = function() {
	var paramter = {
		clickNum : 1
	};
	/**
	 * 绑定键盘事件
	 */
	var handleKeyDown = function(){
		$(".content_btn").each(function(i){
			if(i==0){
		//		$(this).css("transform","scale(1.1,1.1)");
				$(this).css("-webkit-box-shadow","0 0 0px 10px red");
		//		$(".btn_"+paramter.clickNum +" img").attr("width","275").attr("height","125");
				$(this).addClass("content_btn_check");
			}
			$(this).addClass("btn_"+(i+1));
		});
		$(document).keydown(function(e) {
			e = e || window.event; // 取得事件对象
			var keyCode = e.which || e.keyCode; // 按键值
			switch (keyCode) {
			case 37://left
				$(".content_btn_check").css("transform","scale(1.0,1.0)");
		//		$(".content_btn_check img").attr("width","270").attr("height","120");
				$(".btn_"+paramter.clickNum).css("-webkit-box-shadow","0 0 0px 0px white");
				$(".content_btn_check").removeClass("content_btn_check");
				paramter.clickNum--;
				if(paramter.clickNum<1){
					paramter.clickNum = 9;		
				}
				$(".btn_"+paramter.clickNum).css("-webkit-box-shadow","0 0 0px 10px red");
		//		$(".btn_"+paramter.clickNum).css("transform","scale(1.1,1.1)");
		//		$(".btn_"+paramter.clickNum +" img").attr("width","275").attr("height","125");
				$(".btn_"+paramter.clickNum).addClass("content_btn_check");
				break;
			case 39://right
				
				$(".content_btn_check").css("transform","scale(1.0,1.0)");
		//		$(".content_btn_check img").attr("width","270").attr("height","120");
				$(".btn_"+paramter.clickNum).css("-webkit-box-shadow","0 0 0px 0px white");
				$(".content_btn_check").removeClass("content_btn_check");
			
				paramter.clickNum++;
				if(paramter.clickNum>9){
					paramter.clickNum =1;
				}
		//		$(".btn_"+paramter.clickNum).css("transform","scale(1.1,1.1)");
				$(".btn_"+paramter.clickNum).css("-webkit-box-shadow","0 0 0px 10px red");
		//		$(".btn_"+paramter.clickNum +" img").attr("width","275").attr("height","125");
				$(".btn_"+paramter.clickNum).addClass("content_btn_check");
				break;
			case 38://up
				$(".content_btn_check").css("transform","scale(1.0,1.0)");
		//		$(".content_btn_check img").attr("width","270").attr("height","120");
				$(".content_btn_check").removeClass("content_btn_check");
				$(".btn_"+paramter.clickNum).css("-webkit-box-shadow","0 0 0px 0px white");
				if(paramter.clickNum>3){
						paramter.clickNum-=3;
				}
		//		$(".btn_"+paramter.clickNum).css("transform","scale(1.1,1.1)");
				$(".btn_"+paramter.clickNum).css("-webkit-box-shadow","0 0 0px 10px red");
		//		$(".btn_"+paramter.clickNum +" img").attr("width","275").attr("height","125");
				$(".btn_"+paramter.clickNum).addClass("content_btn_check");
				break;
			case 40://down
				$(".content_btn_check").css("transform","scale(1.0,1.0)");
			//	$(".content_btn_check img").attr("width","270").attr("height","120");
				$(".btn_"+paramter.clickNum).css("-webkit-box-shadow","0 0 0px 0px white");
				$(".content_btn_check").removeClass("content_btn_check");
				if(paramter.clickNum<7){
						paramter.clickNum+=3;
				}
		//		$(".btn_"+paramter.clickNum).css("transform","scale(1.1,1.1)");
				$(".btn_"+paramter.clickNum).css("-webkit-box-shadow","0 0 0px 10px red");
		//		$(".btn_"+paramter.clickNum +" img").attr("width","275").attr("height","125");
				$(".btn_"+paramter.clickNum).addClass("content_btn_check");
				break;
			case 13://确定
				if(paramter.clickNum==1){
					window.location.href="http://www.sddjw.com/";
				}else if(paramter.clickNum==2){
					window.location.href="http://www.jnswzzb.gov.cn/";
				}if(paramter.clickNum==3){
					window.location.href="http://www.12371.cn/";
				}if(paramter.clickNum==4){
					window.location.href="http://dj.rencheng.gov.cn/";
				}
				break;
			case 8:
				break;
			}
		});
	}
	
	return {
		/**
		 * 初始化
		 * @returns
		 */
		init : function() {
			handleKeyDown();
		}
	};
}();