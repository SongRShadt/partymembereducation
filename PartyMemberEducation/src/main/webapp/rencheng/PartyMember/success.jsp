<%@ page language="java" import="java.util.*" pageEncoding="utf-8"%>
<%
	String path = request.getContextPath();
	String basePath = request.getScheme() + "://"
			+ request.getServerName() + ":" + request.getServerPort()
			+ path + "/";
%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
<head>
<base href="<%=basePath%>">
<title>SUCCESS</title>
<meta http-equiv="Content-type" content="text/html; charset=utf-8" />
<meta name="page-view-size" content="1280*720" />
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<link rel="stylesheet" href="<%=basePath%>PartyMember/css/success.css" type="text/css"></link>
</head>
<body>
	<div class="main">
		<div class="header">
			<div class="header_title">
			</div>
		</div> 
		<div class="content">
			<div class="accesspt"><img src="<%=basePath%>/PartyMember/image/accept.png"></img></div>
			<div class="s_text">提交成功！</div>
			<div class="djs">3秒后返回首页！</div>
		</div>
		<div class="footer"></div>
	</div>
	<!-- corescript -->
	<script type="text/javascript" src="<%=basePath%>js/jquery-1.8.3.js"></script>
	<script type="text/javascript" src="<%=basePath%>js/ConfigUtil.js"></script>
	<script type="text/javascript" src="<%=basePath%>PartyMember/js/success.js"></script>
	<script type="text/javascript">
		jQuery(document).ready(function() {
			Success.init();
		});
	</script>

</body>
</html>
