﻿<%@ page language="java" import="java.util.*" pageEncoding="utf-8"%>
<%
	String path = request.getContextPath();
	String basePath = request.getScheme() + "://"
			+ request.getServerName() + ":" + request.getServerPort()
			+ path + "/";
%>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
<head>
<base href="<%=basePath%>">
<title>会议主题</title>
<meta http-equiv="Content-type" content="text/html; charset=utf-8" />
<meta name="page-view-size" content="1280*720" />
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<link rel="stylesheet" href="<%=basePath%>PartySign/css/meetingtheme.css" type="text/css"></link></head>

<body>
	<div class="main">
		<div class="header">
		</div>
		<div class="content">
			<div class="content_check_btn">发展党员工作</div>
			<div class="content_check_btn">民主评议党员</div>
			<div class="content_check_btn">讨论、表决本村重大问题、重要事项</div>
			<div class="content_check_btn">传达上级指示、规划部署本村工作</div>
			<div class="content_check_btn">听取党支部书记工作汇报</div>
			<div class="content_check_btn">党员教育培训</div>
			<div class="content_check_btn">讨论通过对党员的奖惩</div>
			<div class="content_check_btn">选举党支部委员会、推选党代会代表</div>
			<div class="content_check_btn">组织生活会</div>
			<div class="content_check_btn">其它</div>
			<div class="content_check_btn next">下一步</div>
		</div>
		<div class="footer"></div>
	</div>
	<div class="msg"></div>

	<!-- core script -->
	<script type="text/javascript" src="<%=basePath%>js/jquery-1.8.3.js"></script>
	<script type="text/javascript" src="<%=basePath%>PartySign/js/meetingtheme.js"></script>
	<script type="text/javascript">
		jQuery(document).ready(function() {
			MeetingTheme.init();
		});
	</script>

</body>
</html>
