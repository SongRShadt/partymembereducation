<%@ page language="java" import="java.util.*" pageEncoding="utf-8"%>
<%
	String path = request.getContextPath();
	String basePath = request.getScheme() + "://"
			+ request.getServerName() + ":" + request.getServerPort()
			+ path + "/";
%>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
<head>
<base href="<%=basePath%>">
<title>会议主题</title>
<meta http-equiv="Content-type" content="text/html; charset=utf-8" />
<meta name="page-view-size" content="1280*720" />
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<link rel="stylesheet" href="<%=basePath%>PartySign/css/meetingtype.css" type="text/css"></link></head>

<body>
	<div class="main">
		<div class="header">
		</div>
		<div class="content">
			<div class="content_check_btn">支部党员大会</div>
			<div class="content_check_btn">支部委员会会议</div>
			<div class="content_check_btn">村“两委”联席会议</div>
			<div class="content_check_btn">村民代表会议</div>
			<div class="content_check_btn">党课</div>
			<div class="content_check_btn">其他</div>
			<div class="content_check_btn next">下一步</div>
		</div>
		<div class="footer"></div>
	</div>
	<div class="msg"></div>
	
	<!-- core script -->
	<script type="text/javascript" src="<%=basePath%>js/jquery-1.8.3.js"></script>
	<script type="text/javascript" src="<%=basePath%>PartySign/js/meetingtype.js"></script>
	<script type="text/javascript">
		jQuery(document).ready(function() {
			MeetingType.init();
		});
	</script>
</body>
</html>
