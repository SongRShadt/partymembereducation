<%@ page language="java" import="java.util.*" pageEncoding="utf-8"%>
<%
	String path = request.getContextPath();
	String basePath = request.getScheme() + "://"
			+ request.getServerName() + ":" + request.getServerPort()
			+ path + "/";
%>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
<head>
<base href="<%=basePath%>">
<title>驻村干部签到</title>
<meta http-equiv="Content-type" content="text/html; charset=utf-8" />
<meta name="page-view-size" content="1280*720" />
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<link rel="stylesheet" href="<%=basePath%>PartySign/css/villagesign.css" type="text/css"></link></head>
<body>
	<div class="main">
		<div class="header">
		</div>
		<div class="content">
			<div id="content_top" class="content_top">
     		</div>
		</div>
		<div class="footer"></div>
	</div>

	<!-- core script -->
	<script type="text/javascript" src="<%=basePath%>js/jquery-1.8.3.js"></script>
	<script type="text/javascript" src="<%=basePath%>js/marqueeController.js"></script>
	<script type="text/javascript" src="<%=basePath%>js/ConfigUtil.js"></script>
	<script type="text/javascript" src="<%=basePath%>PartySign/js/villagesign.js"></script>
	<script type="text/javascript">
		jQuery(document).ready(function() {
			VillageSign.init();
		});
	</script>
</body>
</html>
