/**
 * 会议类型
 */
var MeetingSign = function() {
	var param = {
		sid_check : "",
			
		cameraNo :"",
		streamId :"",//摄像头流编号
		taskid :"",//视频录制任务编号
		
		clickNum : 1,//点击次数
		countNum : 2,//需要焦点的数量
		
		callback : 0,//回调参数
		OrgId : 0,//村组织编号
		MeetingName_ID : 0,//会议名称ID
		Topic_ID : 0,//会议主题ID 
		
		conference_id : 0,//返回的会议ID
	};
	/**
	 * 获取url参数
	 */
	var HandleGetRequest = function(){
		var url = location.search; //获取url中"?"符后的字串
		var theRequest = new Object();
		if (url.indexOf("?") != -1) {
			var str = url.substr(1);
		    strs = str.split("&");
		    for(var i = 0; i < strs.length; i ++) {
		    	theRequest[strs[i].split("=")[0]]=(strs[i].split("=")[1]);
		    }
		}
		return theRequest;
	};
	var initParam = function(){
		param.callback=HandleGetRequest().callback;
		param.OrgId=HandleGetRequest().OrgId;
		param.MeetingName_ID=HandleGetRequest().MeetingName_ID;
		param.Topic_ID=HandleGetRequest().Topic_ID;
	}
	
	/**
	 * 初始化内容区域
	 */
	var handleContent = function() {
		initParam();
		$.ajax({
			type : "GET",
			dataType : 'jsonp',
			async : false,
			data : {
				"callback" : param.callback,
				"OrgId" : param.OrgId,
				"MeetingName_ID" : param.MeetingName_ID,
				"Topic_ID" : param.Topic_ID.replace(/,/g,"")
			},
			url : 'http://172.23.254.70:9000/userinfo/conferenceService',
			success : function(result) {
				console.info(result);
				var portrait = "";
				if(result.success){
					$(result.data).each(function(i) {
						 portrait +="<div class='content_header_portrait' id='sid_"+this.sid+"'><img src='PartySign/image/meetingsign_def.png'/><br/>"+this.name+"</div>";
					});
					$(".content_top").html(portrait);
					param.conference_id=result.conference_id;
				}
				setTimeout(changeSign(),3000);
			}
		});
	};
	
	/**
	 * 获取已签到人员信息
	 */
	var changeSign = function(){
		$.ajax({
			type : "GET",
			dataType : 'jsonp',
			async : false,
			data : {
				"callback" : param.callback,
				"OrgId" : param.OrgId,
				"conference_id" : param.conference_id
			},
			url : 'http://172.23.254.70:9000/userinfo/conferenceSign',		 
			success : function(result) {
				if (result.success) {
					$(result.data).each(function(i){
						$("#sid_"+this.sid+" img").attr("src","http://172.23.254.70:9000/userimg?sid="+this.sid);
						$("#sid_"+this.sid+" img").attr("width","130");
						$("#sid_"+this.sid+" img").attr("height","170");
						$(".content_top").prepend($("#sid_"+this.sid));
					});
				}
				setTimeout(changeSign(),3000);
			}
		});
	};
	/**
	 * 绑定键盘事件
	 */
	var handleKeyDown = function() {
		$(".content_bottom_btn").each(function(i) {
			if (i == 0) {
				$(this).addClass("content_bottom_btn_on");
			}
			$(this).addClass("btn_" + (i + 1));
		});
		$(document).keydown(function(e) {
			e = e || window.event; // 取得事件对象
			var keyCode = e.which || e.keyCode; // 按键值
			switch (keyCode) {
			case 37:// left
				$(".content_bottom_btn_on").removeClass("content_bottom_btn_on");
				param.clickNum--;
				if(param.clickNum<1){
					param.clickNum=param.countNum;
				}
				$(".btn_"+param.clickNum).addClass("content_bottom_btn_on");
				break;
			case 39:// right
				$(".content_bottom_btn_on").removeClass("content_bottom_btn_on");
				param.clickNum++;
				if(param.clickNum>param.countNum){
					param.clickNum=1;
				}
				$(".btn_"+param.clickNum).addClass("content_bottom_btn_on");
				break;
			case 13:// 确定
				if(param.clickNum==1){
					$.ajax({
						type:"post",
						dataType:"json",
						data:{
							"taskid":param.taskid,
							"cameraNo":param.cameraNo
						},
						url:"/PartyMemberEducation/videoController/stop",
						success:function(result){
							console.log();
						}
					});
					window.location.href="clicktolauncher=true";
				}else if(param.clickNum==2){
					$.ajax({
						type:"GET",
						dataType:"json",
						data:{
							stbNo:HandleGetRequest().stbNo
						},
						url:"yzcamera/recording",
						success:function(data){
							console.info(data);
							if(data.success){
								window.location.href=data.obj.cameraUrl+"?taskid="+data.obj.taskId;
							}
						},error:function(a,b,c){
							
						}
					});
					$.ajax({
						type : "GET",
						dataType : 'jsonp',
						async : false,
						data : {
							"callback" : param.callback,
							"conference_id" : param.conference_id
						},
						url : 'http://172.23.254.70:9000/userinfo/conferenceStart',		 
						success : function(result) {
							if(result.success){
								//console.info(url);
							}
						}
					});
				}
				break;
			case 8:
				break;
			}
		});
	};
	
	return {
		/**
		 * 初始化
		 * 
		 * @returns
		 */
		init : function() {
			document.onkeydown = evm_cms_domCtrl.trackKey;
			handleContent();
			handleKeyDown();
		}
	};
}();