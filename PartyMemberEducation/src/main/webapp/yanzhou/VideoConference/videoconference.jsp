<%@ page language="java" import="java.util.*" pageEncoding="utf-8"%>
<%
	String path = request.getContextPath();
	String basePath = request.getScheme() + "://"
			+ request.getServerName() + ":" + request.getServerPort()
			+ path + "/";
%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
<head>
<base href="<%=basePath%>">
<title>视频会议</title>
<meta http-equiv="Content-type" content="text/html; charset=utf-8" />
<meta name="page-view-size" content="1280*720" />
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<link rel="stylesheet" href="<%=basePath%>VideoConference/css/videoconference.css" type="text/css"></link></head>
<body>
	<div class="main">
		<div class="header">
			<div class="header_title">
				<div class="header_left_title">视频会议</div><img class="header_arraw"src="<%=basePath%>PartyMember/image/arrow.png"><div class="header_right_title">党建专栏</div></img>
			</div>
		</div>
		<div class="content">
			<div class="contnet_left_up"></div>
			<div class="content_left">
				
			
			</div>
			<div class="contnet_left_down"></div>
			<div class="content_right">
				<img class="content_right_img" src="VideoConference/image/img/zhc_pic.png"></img>
			</div>
		</div>
		<div class="footer"></div>
	</div>

	<!-- core script -->
	<script type="text/javascript" src="js/jquery-1.8.3.js"></script>
	<script type="text/javascript" src="js/ConfigUtil.js"></script>
	<script type="text/javascript" src="VideoConference/js/videoconference.js"></script>
	<script type="text/javascript">
		jQuery(document).ready(function() {
			VideoConference.init();
		});
	</script>
</body>
</html>
