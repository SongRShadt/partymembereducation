﻿<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-type" content="text/html; charset=utf-8" />
<meta name="page-view-size" content="1280*720" />
<style>

*{
  margin: 0;
  padding: 0;
}
.stb_date{
	font-size: 22px;
	font-weight:bold;
	
	
 	text-align:right;
  	color: #FFFFFF;
	top:10px; left:1000px; width:250px; height:30px ; line-height:30px
	}

.stb_time{
	font-size: 22px;
	font-weight:bold;
	text-align:right; 
 	color: #FFFFFF;
	top:40px; left:1000px; width:250px; height:30px ; line-height:30px
	}

body{
  font-size: 22px;
 
  color: #000000;
 
  background-repeat: no-repeat;
  background-color: transparent;  
}

#viewport{
	position: absolute;
	width: 1280px;
	height: 720px;
	background-image: url(images/bg.png);
}
 
.abspo{
	position: absolute;
	height: 151px;
	width: 86px;
}
.mod_focus{
  box-shadow:4px 4px 2px #000;
  z-index:1;
  transform:scale(1.15);
  -webkit-transform:scale(1.15);
  }
</style>
<title>演示</title>
</head>
<body >
	
	
	
<!--视图-->
<div class="viewport" id="viewport">
	 <!--日期--><!--导航-->
	<div id="nav">
	  <div id="nav_0" class="abspo" style="top:55px; left:649px;"><img src="images/map/nav/02.png"></div>
      <div id="nav_1" class="abspo" style="top:89px; left:814px;"><img src="images/map/nav/04.png"></div>
	  <div id="nav_2" class="abspo" style="top:102px; left:631px;"><img src="images/map/nav/06.png"></div>
        <div id="nav_3" class="abspo" style="top:119px; left:791px;"><img src="images/map/nav/08.png"></div>
        <div id="nav_4" class="abspo" style="top:239px; left:636px;"><img src="images/map/nav/10.png"></div>
        <div id="nav_5" class="abspo" style="top:323px; left:790px;"><img src="images/map/nav/12.png"></div>
      <div id="nav_6" class="abspo" style="top:310px; left:861px;"><img src="images/map/nav/14.png"></div>
	  <div id="nav_7" class="abspo" style="top:311px; left:954px;"><img src="images/map/nav/16.png"></div>
      <div id="nav_8" class="abspo" style="top:279px; left:973px;"><img src="images/map/nav/18.png"></div>
        <div id="nav_9" class="abspo" style="top:371px; left:954px;"><img src="images/map/nav/20.png"></div>
        
</div>
	<!--栏目模块 
 <div id="msg" class="abspo " style=" top:650px; left:630px; color: #FFFFFF; "></div>
 -->
</div>
<script type="text/javascript" src="js/jquery.js"></script>
<script type="text/javascript">
/*
 function clock(){
	//	clearTimeout( clock_timer );
	var weeks = ['日','一','二','三','四','五','六'];
	var dd = new Date();
	var	H = dd.getHours() < 10 ? ( '0' + dd.getHours() ) : dd.getHours(); //小时
	var	M = dd.getMinutes() < 10 ? ( '0' + dd.getMinutes() ) : dd.getMinutes(); //分钟
	var	S = dd.getSeconds() < 10 ? ( '0' + dd.getSeconds() ) : dd.getSeconds(); //秒
	var	Y = dd.getFullYear(); //年
	var	MONTH = dd.getMonth() + 1;
	MONTH = MONTH < 10 ?('0'+MONTH):MONTH; //月
	var	D = dd.getDate(); //日
	D = D < 10 ?('0'+D):D;
	var	W = dd.getDay();
	var	Z =  weeks[dd.getDay()];
	document.getElementById('stb_date').innerHTML= Y + '.' + MONTH + '.' + D + '&nbsp;&nbsp;&nbsp;' +'星期' + Z ;
	document.getElementById('stb_time').innerHTML=  H + ' : ' + M + ' : ' + S ;	 
	clock_timer = setTimeout('clock()', 1000 );
}
window.onload = clock;
*/
/*********************************************************************/

//判断是否有class

function hasClass( elem, className ){  
	var reg = new RegExp('(\\s|^)' + className + '(\\s|$)');	
	return elem.className.match(reg);
}
//添加class
function addClass( elem, value ){
	if( !elem.className ){
		elem.className=value;
	}else{
		var oValue = elem.className;
		oValue += ' ';
		oValue += value;
		elem.className = oValue;
	}
	return this;
} 

//移除class
function removeClass( elem, className ){
	className = className.replace(/^\s*|\s*$/g,'');
	if ( hasClass( elem, className ) ){
		var reg = new RegExp('(\\s|^)' + className + '(\\s|$)');
		elem.className = elem.className.replace(reg, ' ');
	}
	return this;
}


/**************************************************************************/

//要替换的一些数据
var data={
	nav:[
		{img1:'images/map/nav/01.png',img2:'images/map/nav/02.png',url:'village.jsp?townId=ee9934e7-3771-4f54-a039-ae0a01f0ac5a'},
		{img1:'images/map/nav/03.png',img2:'images/map/nav/04.png',url:'village.jsp?townId=7a60e394-a5da-4e81-8261-b9fca39edc46'},
		{img1:'images/map/nav/05.png',img2:'images/map/nav/06.png',url:'village.jsp?townId=f4f32b85-96ed-4618-8788-90fc23f610d0'},
		{img1:'images/map/nav/07.png',img2:'images/map/nav/08.png',url:'village.jsp?townId=26b5a404-b5da-4408-b4e3-8b58a30ab715'},
		{img1:'images/map/nav/09.png',img2:'images/map/nav/10.png',url:'village.jsp?townId=7c7971ed-76f7-4f97-a8ca-c124cc6c6a41'},
		{img1:'images/map/nav/11.png',img2:'images/map/nav/12.png',url:'village.jsp?townId=b0a6d9e3-db29-4f66-9bf5-a0e33299f21c'},
		{img1:'images/map/nav/13.png',img2:'images/map/nav/14.png',url:'village.jsp?townId=13a464d3-e961-4ae6-8222-ef59dc0869cc'},
		{img1:'images/map/nav/15.png',img2:'images/map/nav/16.png',url:'village.jsp?townId=3acf10de-d030-4004-9bee-a49a9412817d'},
		{img1:'images/map/nav/17.png',img2:'images/map/nav/18.png',url:'village.jsp?townId=cfa32110-6521-4ece-80c2-c7f3c8740463'},
		{img1:'images/map/nav/19.png',img2:'images/map/nav/20.png',url:'village.jsp?townId=6b1fda6d-c0ef-4628-a393-fae11a0dce4e'}
		
	]
	};
	
//定义当前的位置
var curr_position={
		curr_Level:0,
		curr_nav_Index:0
};
	

//当前在导航栏的键盘处理
function showBlur(e){
	document.getElementById('nav_'+curr_position.curr_nav_Index).getElementsByTagName('img')[0].src=data.nav[curr_position.curr_nav_Index].img2;
}


function showFocus(e){
	document.getElementById('nav_'+curr_position.curr_nav_Index).getElementsByTagName('img')[0].src=data.nav[curr_position.curr_nav_Index].img1;
}


//跳转
function goto(e){
		//document.getElementById('msg').innerHTML=data.nav[curr_position.curr_nav_Index].url;
		location.href=data.nav[curr_position.curr_nav_Index].url;
}

/**
* 遥控器事件处理
*/
function grabEvent(e) {
	e = e || window.event; //取得事件对象 
	var keyCode = e.which || e.keyCode; //按键值
	
	switch (keyCode) {		
		case 37://left	
		
			showBlur();			 
			if(curr_position.curr_Level==0){//当前光标在导航栏
				switch(curr_position.curr_nav_Index){
					case 0:
						curr_position.curr_nav_Index=1;
					break;
					case 1:
						curr_position.curr_nav_Index=0;
					break;					
					case 2:
						curr_position.curr_nav_Index=3;
					break;
					case 3:
						curr_position.curr_nav_Index=2;
					break;
					case 4:
						curr_position.curr_nav_Index=8;
					break;
					case 5:
						curr_position.curr_nav_Index=4;
					break;
					case 6:
						curr_position.curr_nav_Index=5;
					break;
					case 7:
						curr_position.curr_nav_Index=6;
					break;
					case 8:
						curr_position.curr_nav_Index=7;
					break;
					case 9:
						curr_position.curr_nav_Index=5;
					break;
					
					
					
					
				}			
			}	
			showFocus();
			break;
		
		case 38: //up
		 
		 	showBlur();
			if(curr_position.curr_Level==0){//当前光标在导航栏
				switch(curr_position.curr_nav_Index){
					
					case 0:
						curr_position.curr_nav_Index=4;
					break;
					case 4:
						curr_position.curr_nav_Index=2;
					break;		
					case 2:
						curr_position.curr_nav_Index=0;
					break;	
					case 1:
						curr_position.curr_nav_Index=5;
					break;	
					case 3:
						curr_position.curr_nav_Index=1;
					break;			
					case 5:
						curr_position.curr_nav_Index=3;
					break;	
					case 6:
						curr_position.curr_nav_Index=3;
					break;	
					case 7:
						curr_position.curr_nav_Index=3;
					break;	
					case 8:
						curr_position.curr_nav_Index=3;
					break;	
					case 9:
						curr_position.curr_nav_Index=8;
					break;	
					
					
				}	
			}				
			showFocus();
			break;	 
		 
		case 39://RIGHT
		 
		 	showBlur();
			if(curr_position.curr_Level==0){//当前光标在导航栏
				switch(curr_position.curr_nav_Index){
					case 0:
						curr_position.curr_nav_Index=1;
					break;
					case 1:
						curr_position.curr_nav_Index=0;
					break;
					case 2:
						curr_position.curr_nav_Index=3;
					break;
					case 3:
						curr_position.curr_nav_Index=2;
					break;
					case 4:
						curr_position.curr_nav_Index=5;
					break;
					case 5:
						curr_position.curr_nav_Index=6;
					break;	
					case 6:
						curr_position.curr_nav_Index=7;
					break;	
					case 7:
						curr_position.curr_nav_Index=8;
					break;	
					case 8:
						curr_position.curr_nav_Index=4;
					break;	
					case 9:
						curr_position.curr_nav_Index=9;
					break;	
				
				}			
			}		
			showFocus();
			break;
		 
		case 40://DOWN	
			 
		 	showBlur();
			if(curr_position.curr_Level==0){//当前光标在导航栏
				switch(curr_position.curr_nav_Index){
					case 0:
						curr_position.curr_nav_Index=2;
					break;
					case 1:
						curr_position.curr_nav_Index=3;
					break;
					case 2:
						curr_position.curr_nav_Index=4;
					break;
					case 3:
						curr_position.curr_nav_Index=6;
					break;
					case 4:
						curr_position.curr_nav_Index=0;
					break;
					case 5:
						curr_position.curr_nav_Index=1;
					break;
					case 6:
						curr_position.curr_nav_Index=5;
					break;
					case 7:
						curr_position.curr_nav_Index=9;
					break;
					case 8:
						curr_position.curr_nav_Index=9;
					break;
					case 9:
						curr_position.curr_nav_Index=1;
					break;
					
				
				}	
			}		
			showFocus();
			break;
			
		case 13: //确认		 
			goto();
		break;
	}
	
	//处理键盘
	try {
		e.preventDefault(); //取消默认操作
	} catch (e) {
	}
	return 0;
	 
}


document.onirkeypress = grabEvent;
document.onkeydown = grabEvent;
showFocus();
</script>
</body>
</html>