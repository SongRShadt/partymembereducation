﻿
<!DOCTYPE HTML>
<html>
<head>
<meta http-equiv="Content-type" content="text/html; charset=utf-8" />
<meta name="page-view-size" content="1280*720" />
<style>
* {
	margin: 0;
	padding: 0;
}

.stb_date {
	font-size: 22px;
	font-weight: bold;
	text-align: right;
	color: #FFFFFF;
	top: 10px;
	left: 1000px;
	width: 250px;
	height: 30px;
	line-height: 30px
}

.stb_time {
	font-size: 22px;
	font-weight: bold;
	text-align: right;
	color: #FFFFFF;
	top: 40px;
	left: 1000px;
	width: 250px;
	height: 30px;
	line-height: 30px
}

body {
	font-size: 22px;
	color: #000000;
	background-repeat: no-repeat;
	background-color: transparent;
}

#viewport {
	position: absolute;
	width: 1280px;
	height: 720px;
	background-image: url(images/bg.png);
}

.abspo {
	position: absolute;
}

.mod_focus {
	box-shadow: 4px 4px 2px #000;
	z-index: 1;
	transform: scale(1.15);
	-webkit-transform: scale(1.15);
}
</style>
<title>演示</title>
</head>
<body>
	<!--视图-->
	<div class="viewport" id="viewport">
		<!--日期-->
		<!--导航-->
		<div id="nav">
			<div id="nav_0" class="abspo" style="top: 39px; left: 543px;">
				<img src="images/nav/big/cgc.png">
			</div>
			<div id="nav_1" class="abspo" style="top: 24px; left: 656px;">
				<img src="images/nav/big/eslp.png">
			</div>
			<div id="nav_2" class="abspo" style="top: 55px; left: 737px;">
				<img src="images/nav/big/lyjd.png">
			</div>
			<div id="nav_3" class="abspo" style="top: 119px; left: 624px;">
				<img src="images/nav/big/nzjd.png">
			</div>
			<div id="nav_4" class="abspo" style="top: 171px; left: 601px;">
				<img src="images/nav/big/ajjd.png">
			</div>
			<div id="nav_5" class="abspo" style="top: 323px; left: 570px;">
				<img src="images/nav/big/tkjd.png">
			</div>
			<div id="nav_6" class="abspo" style="top: 433px; left: 577px;">
				<img src="images/nav/big/ytz.png">
			</div>
			
			<div id="nav_7" class="abspo" style="top: 408px; left: 896px;">
				<img src="images/nav/small/jcjd.png">
			</div>
			<div id="nav_8" class="abspo" style="top: 395px; left: 1005px;">
				<img src="images/nav/small/xyjd.png">
			</div>
			<div id="nav_9" class="abspo" style="top: 448px; left: 975px;">
				<img src="images/nav/small/ghjd.png">
			</div>
			<div id="nav_10" class="abspo" style="top: 447px; left: 1025px;">
				<img src="images/nav/small/fqjd.png">
			</div>
			<div id="nav_11" class="abspo" style="top: 444px; left: 1064px;">
				<img src="images/nav/small/gygjd.png">
			</div>
			<div id="nav_12" class="abspo" style="top: 492px; left: 884px;">
				<img src="images/nav/small/jyjd.png">
			</div>
			<div id="nav_13" class="abspo" style="top: 499px; left: 1006px;">
				<img src="images/nav/small/yhjd.png">
			</div>
			<div id="nav_14" class="abspo" style="top: 507px; left: 913px;">
				<img src="images/nav/small/nyjd.png">
			
		</div>
		<!--栏目模块 
 <div id="msg" class="abspo " style=" top:650px; left:630px; color: #FFFFFF; "></div>
 -->
	</div>
	<script type="text/javascript" src="js/jquery-1.8.3.js"></script>
<script type="text/javascript" src="js/jquery.cookie.js"></script>
	<script type="text/javascript">
	$.cookie('clickNum',null);
	$.cookie("navStar",null);
	$.cookie("navEnd",null);
	$.cookie('leftClick',null);
	$.cookie("rightClick",null);
	$.cookie("leftOrRight",null);
		/*********************************************************************/
		
		//判断是否有class
		function hasClass(elem, className) {
			var reg = new RegExp('(\\s|^)' + className + '(\\s|$)');
			return elem.className.match(reg);
		}
		//添加class
		function addClass(elem, value) {
			if (!elem.className) {
				elem.className = value;
			} else {
				var oValue = elem.className;
				oValue += ' ';
				oValue += value;
				elem.className = oValue;
			}
			return this;
		}

		//移除class
		function removeClass(elem, className) {
			className = className.replace(/^\s*|\s*$/g, '');
			if (hasClass(elem, className)) {
				var reg = new RegExp('(\\s|^)' + className + '(\\s|$)');
				elem.className = elem.className.replace(reg, ' ');
			}
			return this;
		}

		/**************************************************************************/

		//要替换的一些数据
		var data = {
			nav : [
					{
						img1 : 'images/nav/big/cgc.png',
						img2 : 'images/nav/big/cgc_down.png',
						url : 'http://172.29.0.109:8060/PartyMemberEducation/ygcw/village.jsp?townId=1'
					},
					{
						img1 : 'images/nav/big/eslp.png',
						img2 : 'images/nav/big/eslp_down.png',
						url : 'http://172.29.0.109:8060/PartyMemberEducation/ygcw/village.jsp?townId=001'
					},
					{
						img1 : 'images/nav/big/lyjd.png',
						img2 : 'images/nav/big/lyjd_down.png',
						url : 'http://172.29.0.109:8060/PartyMemberEducation/ygcw/village.jsp?townId=2'
					},
					{
						img1 : 'images/nav/big/nzjd.png',
						img2 : 'images/nav/big/nzjd_down.png',
						url : 'http://172.29.0.109:8060/PartyMemberEducation/ygcw/village.jsp?townId=3'
					},
					{
						img1 : 'images/nav/big/ajjd.png',
						img2 : 'images/nav/big/ajjd_down.png',
						url : 'http://172.29.0.109:8060/PartyMemberEducation/ygcw/village.jsp?townId=4'
					},
					{
						img1 : 'images/nav/big/tkjd.png',
						img2 : 'images/nav/big/tkjd_down.png',
						url : 'http://172.29.0.109:8060/PartyMemberEducation/ygcw/village.jsp?townId=5'
					},
					{
						img1 : 'images/nav/big/ytz.png',
						img2 : 'images/nav/big/ytz_down.png',
						url : 'http://172.29.0.109:8060/PartyMemberEducation/ygcw/village.jsp?townId=015'
					},{
						img1 : 'images/nav/small/jcjd.png',
						img2 : 'images/nav/small/jcjd_down.png',
						url : 'http://172.29.0.109:8060/PartyMemberEducation/ygcw/village.jsp?townId=7'
					},{
						img1 : 'images/nav/small/xyjd.png',
						img2 : 'images/nav/small/xyjd_down.png',
						url : 'http://172.29.0.109:8060/PartyMemberEducation/ygcw/village.jsp?townId=002'
					},{
						img1 : 'images/nav/small/ghjd.png',
						img2 : 'images/nav/small/ghjd_down.png',
						url : 'http://172.29.0.109:8060/PartyMemberEducation/ygcw/village.jsp?townId=004'
					},{
						img1 : 'images/nav/small/fqjd.png',
						img2 : 'images/nav/small/fqjd_down.png',
						url : 'http://172.29.0.109:8060/PartyMemberEducation/ygcw/village.jsp?townId=8'
					},{
						img1 : 'images/nav/small/gygjd.png',
						img2 : 'images/nav/small/gygjd_down.png',
						url : 'http://172.29.0.109:8060/PartyMemberEducation/ygcw/village.jsp?townId=9'
					},{
						img1 : 'images/nav/small/jyjd.png',
						img2 : 'images/nav/small/jyjd_down.png',
						url : 'http://172.29.0.109:8060/PartyMemberEducation/ygcw/village.jsp?townId=10'
					},{
						img1 : 'images/nav/small/yhjd.png',
						img2 : 'images/nav/small/yhjd_down.png',
						url : 'http://172.29.0.109:8060/PartyMemberEducation/ygcw/village.jsp?townId=005'
					},{
						img1 : 'images/nav/small/nyjd.png',
						img2 : 'images/nav/small/nyjd_down.png',
						url : 'http://172.29.0.109:8060/PartyMemberEducation/ygcw/village.jsp?townId=11'
					}	
					]
		};

		//定义当前的位置
		var curr_position = {
			curr_Level : 0,
			curr_nav_Index : 0
		};

		//当前在导航栏的键盘处理
		function showBlur(e) {
			document.getElementById('nav_' + curr_position.curr_nav_Index)
					.getElementsByTagName('img')[0].src = data.nav[curr_position.curr_nav_Index].img1;
		}

		function showFocus(e) {
			document.getElementById('nav_' + curr_position.curr_nav_Index)
					.getElementsByTagName('img')[0].src = data.nav[curr_position.curr_nav_Index].img2;
		}

		/**
		 * 遥控器事件处理
		 */
		function grabEvent(e) {
			e = e || window.event; //取得事件对象 
			var keyCode = e.which || e.keyCode; //按键值

			switch (keyCode) {
			case 37://left	
				showBlur();
				curr_position.curr_nav_Index--;
				if(curr_position.curr_nav_Index<0){
					curr_position.curr_nav_Index=0;
				}
				showFocus();
				break;

			case 38: //up
				showBlur();
				switch (curr_position.curr_nav_Index) {
					case 3:
						curr_position.curr_nav_Index = 0;
						break;
					case 4:
						curr_position.curr_nav_Index = 3;
						break;
					case 5:
						curr_position.curr_nav_Index = 4;
						break;
					case 6:
						curr_position.curr_nav_Index = 5;
						break;
					case 9:
						curr_position.curr_nav_Index = 7;
						break;
					case 10:
						curr_position.curr_nav_Index = 8;
						break;
					case 12:
						curr_position.curr_nav_Index = 9;
						break;
					case 14:
						curr_position.curr_nav_Index = 12;
						break;
				    }
				showFocus();
				break;

			case 39://RIGHT
				showBlur();
				curr_position.curr_nav_Index++;
				if(curr_position.curr_nav_Index>14){
					curr_position.curr_nav_Index=14;
				}
				showFocus();
				break;

			case 40://DOWN	

				showBlur();
				    switch (curr_position.curr_nav_Index) {
					case 0:
					case 1:
					case 2:
						curr_position.curr_nav_Index = 3;
						break;
					case 3:
						curr_position.curr_nav_Index = 4;
						break;
					case 4:
						curr_position.curr_nav_Index = 5;
						break;
					case 5:
						curr_position.curr_nav_Index = 6;
						break;


					case 7:
						curr_position.curr_nav_Index = 9;
						break;
					case 8:
						curr_position.curr_nav_Index = 10;
						break;
					case 9:
						curr_position.curr_nav_Index = 12;
						break;
					case 12:
					case 13:
						curr_position.curr_nav_Index = 14;
						break;
				    }
		 
				showFocus();
				break;

			case 13: //确认		 
				location.href = data.nav[curr_position.curr_nav_Index].url;
			case 8:
				return true;
				break;
			}

		}

		document.onirkeypress = grabEvent;
		document.onkeydown = grabEvent;
		showFocus();
	</script>
</body>
</html>