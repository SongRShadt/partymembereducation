﻿/**
 * 阳光村务跳转判断
 */
var Index = function(){
	
	/**
	 * 获取项目根路径
	 * 
	 * @returns
	 */
	var getRootPath = function() {
		var curWwwPath = window.document.location.href;// 获取当前网址，如： http://localhost:8080/ems/Pages/Basic/Person.jsp
		var pathName = window.document.location.pathname; // 获取主机地址之后的目录，如： /ems/Pages/Basic/Person.jsp
		var pos = curWwwPath.indexOf(pathName);
		var localhostPath = curWwwPath.substring(0, pos); // 获取主机地址，如： http://localhost:8080
		var projectName = pathName.substring(0, pathName.substr(1).indexOf('/') + 1);// 获取带"/"的项目名，如：/ems
		return (localhostPath + projectName);
	};
	
	/**
	 * 获取url参数
	 */
	var getUrlParam = function(){
		var url = decodeURI(location.search); //获取url中"?"符后的字串
		var theRequest = new Object();
		if (url.indexOf("?") != -1) {
			var str = url.substr(1);
		    strs = str.split("&");
		    for(var i = 0; i < strs.length; i ++) {
		    	theRequest[strs[i].split("=")[0]]=(strs[i].split("=")[1]);
		    }
		}
		return theRequest;
	};
	/**
	 * 判断跳转连接
	 */
	var handleLink = function(){
		console.info(getUrlParam().stbNo);
		$.ajax({
			url:getRootPath()+"/stbController/getStbByStbNo",
			type:"post",
			dataType:"json",
			data : {
				"stbNo":getUrlParam().stbNo
			},
			success:function(data){
				var url = "";
				console.info(data);
				if(data.obj.orgCode==="0"){//县级
					url = "map.jsp";
				}else if(Number(data.obj.orgCode)<0){//镇级
					url = "village.jsp?townId="+data.obj.townId;
				}else{//村级
					url = getRootPath()+"/rcdj/ygcw.jsp?key="+data.obj.villageKeyValue+"&OrgId="+data.obj.orgCode;
				}
				
			window.location.replace(url);			},error:function(XMLHttpRequest, textStatus, errorThrown){
				
			}
		});
	};
	
	
	return {
		init : function(){
			$.cookie('clickNum',null);
			$.cookie("navStar",null);
			$.cookie("navEnd",null);
			$.cookie('leftClick',null);
			$.cookie("rightClick",null);
			$.cookie("leftOrRight",null);
			handleLink();
		}
	}
	
}();