/**
 * 村级
 */
var Village = function() {
	
	var param = {
		upOrDown : 0,//0村列表1翻页
		upClick:($.cookie('clickNum')==="null"||$.cookie('clickNum')===undefined)?0:Number($.cookie('clickNum')),//村列表区域
		downClick :0,//翻页区域
		contentData : "",
	};
	
	var page = {
		star: ($.cookie('navStar')==="null"||$.cookie('navStar')===undefined)?0:Number($.cookie('navStar')),//开始//0,
		end:($.cookie('navEnd')==="null"||$.cookie('navEnd')===undefined)?19:Number($.cookie('navEnd')),//19
	};
	/**
	 * 获取项目根路径
	 * 
	 * @returns
	 */
	var getRootPath = function() {
		var curWwwPath = window.document.location.href;// 获取当前网址，如： http://localhost:8080/ems/Pages/Basic/Person.jsp
		var pathName = window.document.location.pathname; // 获取主机地址之后的目录，如： /ems/Pages/Basic/Person.jsp
		var pos = curWwwPath.indexOf(pathName);
		var localhostPath = curWwwPath.substring(0, pos); // 获取主机地址，如： http://localhost:8080
		var projectName = pathName.substring(0, pathName.substr(1).indexOf('/') + 1);// 获取带"/"的项目名，如：/ems
		return (localhostPath + projectName);
	};
	
	/**
	 * 获取url参数
	 */
	var getUrlParam = function(){
		var url = decodeURI(location.search); //获取url中"?"符后的字串
		var theRequest = new Object();
		if (url.indexOf("?") != -1) {
			var str = url.substr(1);
		    strs = str.split("&");
		    for(var i = 0; i < strs.length; i ++) {
		    	theRequest[strs[i].split("=")[0]]=(strs[i].split("=")[1]);
		    }
		}
		return theRequest;
	};
	
	/**
	 * 初始化村列表
	 */
	var handleVillage = function(){
		$(".logo").css("background-image","url('"+getRootPath()+"/ygcw/images/village/logo/"+getUrlParam().townId+".png')");
		$.ajax({
			url :getRootPath()+"/villageController/getVillageByTownId",
			type:"post",
			dataType:"json",
			data : {
				townId : getUrlParam().townId
			},
			success:function(data){
				console.info(page.star+"==="+page.end);
				if(data.success){
					param.contentData = data.obj;
					pages({"star":page.star,"end":page.end});
					$(".btn_"+param.upClick).addClass("c_m_btn_on");
				}
			},error:function(XMLHttpRequest,textStatus,errorThrow){
				
			}
		});
	};
	
	var pages = function(paging){
		var content = "";
		$.each(param.contentData,function(i){
			if(i>=paging.star&&i<=paging.end)
				content+="<div class='c_m_btn' key='"+this.villageKeyValue+"' data='"+this.orgCode+"'>"+this.villageName+"</div>";
		});
		$(".c_main").html(content);
		$(".c_m_btn").each(function(i){
			$(this).addClass("btn_"+i);
		});
	};
	
	/**
	 * 绑定键盘事件
	 */
	var handleKeyDown = function(){
		$(document).keydown(function(e) {
			e = e || window.event; // 取得事件对象
			var keyCode = e.which || e.keyCode; // 按键值
			switch (keyCode) {
			case 37://left
				if(param.upOrDown==0){
					$(".c_m_btn_on").removeClass("c_m_btn_on");
					if(param.upClick>0){
						param.upClick--;
					}
					$(".btn_"+param.upClick).addClass("c_m_btn_on");
				}
				if(param.upOrDown==1){
					$(".next_on").removeClass("next_on");
					$(".next_1").addClass("next_on");
				}
				break;
			case 39://right
				if(param.upOrDown==0){
					$(".c_m_btn_on").removeClass("c_m_btn_on");
					if(param.upClick<$(".c_m_btn").length-1){
						param.upClick++;
					}
					$(".btn_"+param.upClick).addClass("c_m_btn_on");
				}
				if(param.upOrDown==1){
					$(".next_on").removeClass("next_on");
					$(".next_2").addClass("next_on");
				}
				break;
			case 38://up
				if(param.upOrDown==0){
					$(".c_m_btn_on").removeClass("c_m_btn_on");
					if(param.upClick>4){
						param.upClick-=5;
					}
					$(".btn_"+param.upClick).addClass("c_m_btn_on");
				}
				if(param.upOrDown==1){
					param.upOrDown=0;
					param.upClick=0;
					$(".next_on").removeClass("next_on");
					$(".btn_"+param.upClick).addClass("c_m_btn_on");
				}
				break;
			case 40://down
				if(param.upOrDown==0){
					$(".c_m_btn_on").removeClass("c_m_btn_on");
					if(param.upClick<$(".c_m_btn").length-5){
						param.upClick+=5;
						$(".btn_"+param.upClick).addClass("c_m_btn_on");
					}else{
						param.upOrDown=1;
						param.upClick=0;
						$(".next_1").addClass("next_on");
					}
				}
				break;
			case 13://确定
				if(param.upOrDown==0){
									
					$.cookie('clickNum',param.upClick);
					$.cookie("navStar",page.star);
					$.cookie("navEnd",page.end);
					if(($(".c_m_btn_on").html()).indexOf("社区")!=-1){
						window.location.href=getRootPath()+"/rcdj/ygcw.jsp?OrgId="+$(".c_m_btn_on").attr("data")+"&key="+$(".c_m_btn_on").attr("key")+"&b=true";
					}else{
						window.location.href=getRootPath()+"/rcdj/ygcw.jsp?OrgId="+$(".c_m_btn_on").attr("data")+"&key="+$(".c_m_btn_on").attr("key")+"&b=false";
					}
					
				}
				if(param.upOrDown==1){
					if($(".next_1").hasClass("next_on")){
						if(page.star>0){
							page.end-=20;
							page.star-=20;
						}
					}
					if($(".next_2").hasClass("next_on")){
						if(page.end<param.contentData.length){
							page.end+=20;
							page.star+=20;
						}
					}
					pages({"star":page.star,"end":page.end});
				}
				break;
			case 8:
				break;
			}
		});
	}
	
	
	return {
		init : function() {
			$.cookie('leftClick',null);
			$.cookie("rightClick",null);
			$.cookie("leftOrRight",null);
			handleVillage();
			handleKeyDown();
		}

	};

}();