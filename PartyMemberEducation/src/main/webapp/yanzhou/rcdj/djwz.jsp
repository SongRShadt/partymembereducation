<%@ page language="java" import="java.util.*" pageEncoding="utf-8"%>
<%
	String path = request.getContextPath();
	String basePath = request.getScheme() + "://" + request.getServerName() + ":" + request.getServerPort() + path + "/";
	request.setAttribute("path", basePath);
%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
<head>
<base href="<%=basePath%>">
<title>任城--->党建网站</title>
<meta http-equiv="Content-type" content="text/html; charset=utf-8" />
<meta name="page-view-size" content="1280*720" />
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<link rel="stylesheet" href="${path}rcdj/css/djwz.css" type="text/css"></link>
</head>
<body>
	<div class="main">
		<div class="header">
			<div class="header_title">
				<div class="header_left_title"></div>
			</div>
		</div>
		<div class="content">
		</div>
	</div>
	<!-- corescript -->
	<script type="text/javascript" src="${path}rcdj/js/jquery-1.8.3.js"></script>
	<script type="text/javascript" src="${path}rcdj/js/djwz.js"></script>
	<script type="text/javascript">
		jQuery(document).ready(function() {
			DJWZ.init();
		});
	</script>

</body>
</html>
