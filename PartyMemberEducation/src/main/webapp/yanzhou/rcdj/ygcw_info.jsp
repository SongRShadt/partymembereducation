<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%
	String path = request.getContextPath();
	String basePath = request.getScheme() + "://"
			+ request.getServerName() + ":" + request.getServerPort()
			+ path + "/";
%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>阳光村务—>信息列表</title>
<link rel="stylesheet" href="<%=basePath%>rcdj/css/main.css" type="text/css"></link>
<link rel="stylesheet" href="<%=basePath%>rcdj/css/ygcw_info.css" type="text/css"></link>
</head>
<body>
<div class="main">
	<div class="header">
		<div class="logo"></div>
	</div>
	<div class="content">
		<div class="content_main">
			<div class="content_text">党支部委员会名单</div>
			<div class="c_i_main">
				<div class="c_i_left"></div>
				<div class="c_info">
					<table cellpadding="0" cellspacing="0">
						<thead>
							<tr>
								<td width="110">照片</td>
								<td width="260">姓名</td>
								<td width="280">性别</td>
								<td width="280">出生年月</td>
								<td width="300">政治面貌</td>
								<td width="280">职务</td>
								<td width="280">联系方式</td>
							</tr>
						</thead>
						<tbody>
						</tbody>
					</table>
				</div>
				<div class="c_i_right"></div>
			</div>
		</div>
	</div>
	<div class="footer"></div>
</div>

<!-- corescript -->
<script type="text/javascript" src="<%=basePath%>rcdj/js/jquery-1.8.3.js"></script>
<script type="text/javascript" src="<%=basePath%>rcdj/js/ConfigUtil.js"></script>
<script type="text/javascript" src="<%=basePath%>rcdj/js/ygcw_info.js"></script>
<script type="text/javascript">
	jQuery(document).ready(function() {
		YGCW_Info.init();
	});
</script>
</body>
</html>