<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%
	String path = request.getContextPath();
	String basePath = request.getScheme() + "://"
			+ request.getServerName() + ":" + request.getServerPort()
			+ path + "/";
%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>全程纪实—>会议设置、会议回放、坐班签到</title>
<link rel="stylesheet" href="<%=basePath%>rcdj/css/main.css" type="text/css"></link>
<link rel="stylesheet" href="<%=basePath%>rcdj/css/qcjs.css" type="text/css"></link>
</head>
<body>
<div class="main">
	<div class="header">
		<div class="logo"></div>
	</div>
	<div class="content">
		<div class="c_main">
			<div class="c_m_left">
				<div class="btn hyjk hyjk_on">会议监控</div>
				<div class="btn hyhf">会议回看</div>
				<div class="btn hysz">会议设置</div>
				<div class="btn zbqd">坐班签到</div>
			</div>
			<div class="c_m_right">
				<img width="528" height="364" src="images/qcjs/lzt/1.png">
			</div>
		</div>
	</div>
</div>

<!-- corescript -->
<script type="text/javascript" src="<%=basePath%>rcdj/js/jquery-1.8.3.js"></script>
<script type="text/javascript" src="<%=basePath%>rcdj/js/ConfigUtil.js"></script>
<script type="text/javascript" src="<%=basePath%>rcdj/js/qcjs.js"></script>
<script type="text/javascript">
	jQuery(document).ready(function() {
		QCJS.init();
	});
</script>
</body>
</html>