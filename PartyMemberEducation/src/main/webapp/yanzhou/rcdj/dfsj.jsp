<%@ page language="java" import="java.util.*" pageEncoding="utf-8"%>
<%
	String path = request.getContextPath();
	String basePath = request.getScheme() + "://"
			+ request.getServerName() + ":" + request.getServerPort()
			+ path + "/";
%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
<head>
<base href="<%=basePath%>">
<title>兖州--->党费收缴</title>
<meta http-equiv="Content-type" content="text/html; charset=utf-8" />
<meta name="page-view-size" content="1280*720" />
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<link rel="stylesheet" href="<%=basePath%>rcdj/css/main.css" type="text/css"></link>
<link rel="stylesheet" href="<%=basePath%>rcdj/css/dfsj.css" type="text/css"></link>
<script type="text/javascript">
// window.location.href="http://172.29.0.111:5080/hls/wzsdtcac.m3u8";
</script>
</head>
<body>
	<div class="main">
		<div class="header"></div>
	<div class="content">
		<div class="c_left">
			<div class="c_l_title">党员姓名</div>
			<div class="c_l_c_content">
			</div>
		</div>
		<div class="c_right">
			<div class="c_r_title">党员信息</div>
			<div class="c_r_content">
				<div class="c_r_c_top">
					<div class="portrait"></div>
					<div class="sex"></div>
					<div class="phone"></div>
					<div class="sbrith"></div>
					<div class="sid"></div>
				</div>
				<div class="c_r_c_middle">
					<div class="checkbox checkbox_1" data="3">3元</div>
					<div class="checkbox checkbox_2" data="6">6元</div>
					<div class="checkbox checkbox_3" data="9">9元</div>
					<div class="checkbox checkbox_4" data="12">12元</div> 
					<div class="checkbox checkbox_5" data="15">15元</div>
					<div class="checkbox checkbox_6" data="18">18元</div>
					<div class="checkbox checkbox_7" data="30">30元</div>
					<div class="checkbox checkbox_8" data="0">其他</div>
				</div>
				<div class="c_r_c_jd">
					<div class="checkbox_jd_title">季度选择:</div>
					<div class="checkbox_jd checkbox_jd_1" data="1">1季度</div>
					<div class="checkbox_jd checkbox_jd_2" data="2">2季度</div>
					<div class="checkbox_jd checkbox_jd_3" data="3">3季度</div>
					<div class="checkbox_jd checkbox_jd_4" data="4">4季度</div>
				</div>
				<div class="c_r_c_bottom">
					<div class="money">缴纳金额:<span class="money_span">0</span><input class="money_input hidden" type="text"/> </div>
					<div class="msg"></div>
					<div class="submit">提交</div>
				</div>
			</div>
		</div>
	</div>
	<div class="footer"></div>
	</div>
	<!-- corescript -->
	<script type="text/javascript" src="<%=basePath%>rcdj/js/jquery-1.8.3.js"></script>
	<script type="text/javascript" src="<%=basePath%>rcdj/js/dfsj.js"></script>
	<script type="text/javascript">
		jQuery(document).ready(function() {
 			DFSJ.init();
		});
	</script>

</body>
</html>
