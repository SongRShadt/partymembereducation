/**
 * 全程纪实
 */
var QCJS = function(){
	/**
	 * 获取url参数
	 */
	var getUrlParam = function(){
		var url = decodeURI(location.search); //获取url中"?"符后的字串
		var theRequest = new Object();
		if (url.indexOf("?") != -1) {
			var str = url.substr(1);
		    strs = str.split("&");
		    for(var i = 0; i < strs.length; i ++) {
		    	theRequest[strs[i].split("=")[0]]=(strs[i].split("=")[1]);
		    }
		}
		return theRequest;
	};
	/**
	 * 初始化键盘事件
	 */
	var handleKeyDown = function() {
		var clickNum = 1;
		$(document).keydown(function(e) {
			e = e || window.event; // 取得事件对象
			var keyCode = e.which || e.keyCode; // 按键值
			switch (keyCode) {
			case 38://up
				if(clickNum==4){
					$(".zbqd").removeClass("zbqd_on");
					clickNum=3;
					$(".hysz").addClass("hysz_on");
				}else if(clickNum==3){
					$(".hysz").removeClass("hysz_on");
					clickNum=2;
					$(".hyhf").addClass("hyhf_on");
				}else if(clickNum==2){
					$(".hyhf").removeClass("hyhf_on");
					clickNum=1;
					$(".hyjk").addClass("hyjk_on");
				}
				$(".c_m_right img").attr("src","images/qcjs/lzt/"+clickNum+".png");
				break;
			case 40://down
				if(clickNum==1){
					$(".hyjk").removeClass("hyjk_on");
					clickNum=2;
					$(".hyhf").addClass("hyhf_on");
				}else if(clickNum==2){
					$(".hyhf").removeClass("hyhf_on");
					clickNum=3;
					$(".hysz").addClass("hysz_on");
				}else if(clickNum==3){
					$(".hysz").removeClass("hysz_on");
					clickNum=4;
					$(".zbqd").addClass("zbqd_on");
				}
				$(".c_m_right img").attr("src","images/qcjs/lzt/"+clickNum+".png");
				break;
			case 13://确定
				if(clickNum==1){
					//window.location.href="/PartyMemberEducation/rcdj/zdjk.jsp?OrgId="+getUrlParam().OrgId+"&stbNo="+getUrlParam().stbNo;
					window.location.href="/PartyMemberEducation/VideoConference/videoconference.jsp"+"?stbNo="+getUrlParam().stbNo;
				}else if(clickNum==2){http://172.29.0.109:8070/PartyMemberEducation/rcdj/images/qcjs/lzt/1.png
					window.location.href="/PartyMemberEducation/rcdj/zdjk.jsp?stbNo="+getUrlParam().stbNo+"&type=meeting&cType=0";
				}else if(clickNum==3){
					window.location.href="/PartyMemberEducation/PartySign/meetingtype.jsp?OrgId="+getUrlParam().OrgId+"&stbNo="+getUrlParam().stbNo;
				}else if(clickNum==4){
					window.location.href="/PartyMemberEducation/rcdj/zbqd.jsp?OrgId="+getUrlParam().OrgId+"&stbNo="+getUrlParam().stbNo;
				}
				
				break;
			case 8:
				break;
			}
		});
	};
	return {
		
		init : function(){
			handleKeyDown();
		}
	};
}();