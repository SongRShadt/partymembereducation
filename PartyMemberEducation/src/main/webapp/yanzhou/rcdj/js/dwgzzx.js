/**
 *  任城——>党员服务——>党务工作咨询
 */
var DWGZZX = function(){
	param = {
		leftOrRight : 0,
		leftClick : 1
			
	};
	var data = [{"id":"1","title":"党费收缴工作流程与规范"       		   ,"data":"&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;1、交纳党费的意义：按期交纳党费，是共产党员应尽的义务，也是党员关心党的事业的具体表现。各党（工）委要教育党员自觉地按时按标准交纳党费。<br>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;2、党员交纳党费的计算基数：凡有工资收入的党员，每月以国家规定的工资总额中相对固定的、经常性的工资收入为计算基数，按规定比例交纳党费，其中教育行业包括绩效工资。党员交纳党费的比例为：每月工资收入（税后）在3000元以下(含3000元)者，交纳月工资收入的0.5％；3000元以上至5000元(含5000元)者，交纳1％；5000元以上至10000元(含10000元)者，交纳1.5％；10000元以上者，交纳2％。农民党员每月交纳党费0.5元。学生党员（包括没有工资收入的研究生）、下岗失业的党员、依靠抚恤或救济的党员、领取当地最低生活保障金的城乡党员，每月交纳0.2元。离退休干部、职工中的党员，每月以实际领取的离退休费总额或养老金总额为计算基数，5000元以下（含5000元）的按0.5％交纳党费，5000元以上的按1％交纳党费。<br>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;3、对党员交纳党费的要求：（1）本人交纳党费；（2）必须按期交纳党费；（3）必须按规定标准交纳党费。<br>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;4、党费收缴必须建立严格的收缴和管理制度：（1）党支部要指定专人收缴与管理；（2）要建立专帐管理；（3）党费要按月上缴，不得拖延，任何人不得侵占或滥用；（4）对不按照规定交纳党费的党员，其所在党组织应及时对其进行批评教育，限期改正。对无正当理由，连续6个月不交纳党费的党员，按自行脱党处理。"},
	            {"id":"2","title":"发展党员工作流程与规范"       		   ,"data":"&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;1、培养培训入党积极分子按照“两推一公示”程序，由党支部全体党员和群众代表进行无记名投票推荐，召开支部委员会研究，对推荐的入党申请人进行不少于7天的公示，公示无异议的填写入党积极分子写实簿。每年举办一次入党积极分子培训班，对列为发展对象的入党积极分子进行不少于5天的集中培训。<br>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;2、确定发展对象入党积极分子经过至少一年的培养教育后，在听取党支部、培养人和党内外群众意见的基础上，由党支部进行审查，形成综合性的政审材料。基层党（工）委对党支部上报的发展对象进行初审，报县委组织部进行审查，审查合格的填写《入党志愿书》。<br>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;3、接收预备党员党支部确定两名正式党员作为发展对象的入党介绍人。所在党支部对发展对象填写的《入党志愿书》和有关情况进行严格审查，经集体讨论认为合格后，提交党支部委员会研究，讨论是否召开党员大会接收其为预备党员。<br>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;4、发展党员谈话在召开党员大会接收预备党员后，将发展对象材料报上级党委审批，上级党委派人对纳新的预备党员进行考察谈话，征求党内外群众意见，向党委提出谈话审批意见。<br>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;5、预备党员的考察和转正预备党员在预备期满前一周时间左右，应主动向党支部提出书面转正申请。预备党员所在党支部对其在预备期的表现认真讨论，并提出能否按期转正的意见。对拟提交支部大会讨论转正的预备党员，以书面形式公示，公示期为7天。"},
	            {"id":"3","title":"党员组织关系转接工作流程与规范"		   ,"data":"&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;1、党员组织关系接收对于县外转入的党员关系，一般应由县以上组织部门出具党员组织关系介绍信，并在有效期内予以转接到基层党委，再由基层党委将党员编入党支部。对于超过有效期的党员组织关系，给予批评教育并补缴党费；对于超过预备期的预备党员组织关系转接，应当向党员说明情况，并重新进行培养。<br>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;2、党员组织关系转出对于县内党员组织关系转接，由基层党委直接互相接转，不用通过县委组织部；对于转出县外的党员组织关系，由基层党委向县委组织部开具组织关系介绍信，再由县委组织部将党员组织关系转到相应县以上组织部门。"},
	            {"id":"4","title":"村党支部换届选举工作流程与规范"		   ,"data":"&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;1、确定职数。<br>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;召开村党组织会议，研究确定新一届党组织成员职数（1000人以下的村设3职，其他村不超过5职），并报乡镇党委研究同意。<br>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;2、民主推荐。<br>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;乡镇党委选派换届指导组采取召开民主推荐会议或入户走访的形式，让党员和群众代表等额推荐新一届村党支部成员候选人初步人选。参与推荐的党员应占本村党员总数的2/3以上，参加推荐的群众代表一般应不少于本村总户数的85%。是党员的户代表，只参加党内推荐，不再参加群众民主推荐。乡镇换届指导组对党员和群众代表民主推荐情况进行统计，根据党员推荐票和群众推荐票相加之和，由高到低排出应选人数2倍左右的推荐人选名单，提交乡镇党委研究确定后，将村党组织成员候选人按姓氏笔画顺序进行考察公示，公示时间为3至7天。<br>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;3、组织考察。<br>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;乡镇党委成立考察组对下届村党支部成员候选人考察人选的政治素质、工作能力、群众基础和是否具备党支部成员的标准等条件进行考察，广泛征求党员意见。考察采取个别谈话的方式进行。<br>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;4、确定候选人。<br>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;乡镇党委听取考察情况汇报后，按多于应选人数20%的数量，提出新一届村党支部成员候选人名单。<br>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;5、选举任用。<br>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;召开党员大会，组织党员酝酿乡镇党委提出的村党支部成员候选人，选举产生新一届党支部委员。选举结果报乡镇党委审批。参加选举的正式党员必须超过应到会党员的五分之四，方可进行选举，被选举人获得的赞成票超过实到会有选举权的人数的一半，始得当选。然后召开新一届村党支部委员会第一次全体会议，选举产生村党支部书记、副书记，选举结果报乡镇党委审批。"},
	            {"id":"5","title":"党代表选举工作流程与规范"     		   ,"data":"&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;1、选举单位根据上级党组织分配的代表名额和有关要求，组织所属党组织或全体党员，充分酝酿讨论，提出代表候选人推荐名单；选举单位党组织根据所属党组织或多数党员的意见，从推荐名单中，按照多于代表百分之三十以上的比例提出候选人推荐人选名单，与上级党组织沟通。根据上级党组织提出的代表构成比例的要求，按多于代表百分之二十以上的差额比例，提出代表候选人初步人选名单，报所属党委沟通同意后，当天公示代表候选人初步人选名单，公示时间为3天以上；<br>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;2、代表候选人初步人选推荐单位党组织，在广泛征求党内外群众意见的基础上，写出考核材料，填报《代表候选人预备人选登记表》；<br>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;3、选举单位召开党的委员会全体会议，在充分讨论的基础上，确定代表候选人预备人选名单并报所属上级党委，同时，将代表候选人预备人选酝酿提名的方法步骤、提名原则及代表候选人预备人选的结构等情况写出代表候选人预备人选酝酿情况的报告，连同《代表候选人预备人选登记表》一起报上级党组织审查。代表多的选举单位还应填报《代表候选人预备人选名册》；<br>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;4、经上级党组织审查同意后，选举单位即可召开党代表大会或党员大会，对代表候选人预备人选进行充分酝酿，根据多数选举人的意见确定候选人，进行选举。具体程序如下。<br>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;（1）会前召开支委会商定：<br>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;一是上报镇党委召开选举大会时间。<br>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;二是通知参会人员。<br>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;三是确定会议主持人：党支部书记。<br>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;四是根据镇党委审查后的代表候选人预备人选确定代表候选人建议名单。<br>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;五是制定选举办法（草案）。<br>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;六是提出监票员、计票人员建议名单。<br>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;七是制定选票。<br>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;八是准备会议主持词<br>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;（2）选举大会议程<br>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;一是召开党员大会酝酿确定代表候选人，酝酿讨论通过代表候选人建议名单。<br>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;二是通过选举办法。<br>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;三是通过监票员、计票人员建议名单。<br>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;四是进行大会选举。首先清点到会人数有选举权的到会人数应超过应到会人数的4/5。其次，由监票员、计票人员发放选票，由主持人讲解选票填写说明。第三、党员填写选票并投票。第四，统计投票。第五，宣布选举结果。<br>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;八、上报选举结果（选举产生党代表大会代表的会议形式、选举方式、代表名额、代表构成比例等；并附代表名册、代表登记表等。）"},
	            {"id":"6","title":"党支部换届选举工作流程与规范"  		   ,"data":"&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;1、在党的支部委员会任期届满之前，召开支委会，研究召开党员大会进行换届选举的有关事宜，制定工作计划，确定召开党员大会的时间、指导思想、主要议程及委员职数，并向上级党组织呈报请示。<br>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;2、召开党员大会，对党员进行换届选举的目的、意义和党的民主集中制、党员权利及义务的宣传教育；支委进行述职，并做好民主评议和民主推荐工作。<br>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;3、召开支委会，讨论本届支部的工作报告；组织酝酿、推荐下届委员会委员候选人预备人选，并报上级党组织审批。<br>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;4、召开党员大会，向全体党员报告工作并组织讨论；将经上级党组织批准的支委候选人名单提交党员大会讨论，根据多数党员的意见确定正式候选人。<br>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;5、召开党员大会进行选举。主要程序有：通过大会选举办法和选举工作人员名单、宣布正式候选人名单、投票选举出支部委员（实行差额选举）、宣布当选人名单。<br>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;6、召开第一次支委会，选举委员会书记、副书记。<br>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;7、进行支委分工，讨论制订新一届任期目标责任制，提出配套组织建设意见，将任期目标责任制提交党员大会讨论。<br>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;8、支部委员会向上级党组织报送选举结果；做好落选人的思想工作；做好换届选举的总结和资料归档工作。"},
	            {"id":"7","title":"设置基层党组织工作流程与规范"  	       ,"data":"&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;1、设置党支部的原则和要求<br>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;设置基层党组织总的原则是要有利于经济建设和改革开放，有利于党组织自身建设的加强，有利于党的领导的加强和改善按照党员人数以及地域和单位设置党组织，是设置党组织的一般原则。按照党员人数设置党组织，可以使基层党的上级组织和下级组织、党组织的领导干部和普通党员保持适当比例，有利于基层党组织的精干高效，有利于基层各级党组织的效能得到充分发挥。<br>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;党章规定：“企业、农村、机关、学校、科研院所、街道、人民解放军连队和其他基层单位，凡是有正式党员三人以上的，都应当成立党的基层组织。党的基层组织，根据工作需要和党员人数，经上级党组织批准，分别设立党的基层委员会、总支部委员会、支部委员会。<br>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;根据上述精神，设置党支部的基本要求是：凡有正式党员3人以上不足50人的基层单位，设立党的支部委员会；党员在50人以上不足100人的，可设立总支部委员会，下设若干支部委员会；经上级党组织批准，党员超过50人的也可不设总支部委员会，而设支部委员会；党员超过100人的，可设立党的基层委员会，但经上级党组织批准，也可不设党的基层委员会，而设总支部委员会；正式党员不足3人，没有条件单独成立党支部的单位，可与邻近单位的党员组成联合党支部；凡有正式党员3人以上，执行临时任务时间较短或因某种原因暂时不能成立正式党支部的，可成立临时党支部；部分对国计民生影响重大的科研院所、工矿企业、外交外贸机构和边防要塞机关，虽然党员不足50人或100人，但领导力量较强的，经上一级党委批准，也可以建立党的基层委员会或总支部委员会。对已经成立的党支部，由于种种原因党员人数减少至3人以下(不含3人)时，如工作需要，且在短期内(一般不超过6个月)能增加党员，经上级党组织同意，党支部可暂时保留。但这样的支部不能形成决议或作出决定。如该支部在短期内不能增加党员，则应予以撤销。撤销后，原支部党员可与临近单位(部门)党员组成联合党支部。<br>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;2、工作流程<br>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;（1）向所属上级党委写出建立（撤销、合并）党组织的请示。请示的内容包括：建制单位的工作性质、人员数量等简要情况；现有正式党员、预备党员的数量，建立（撤销、合并）党组织的依据和理由；所建（撤销、合并）党组织的性质；委员会组成人数和委员设置方案等。所属上级党委审核后，报县委组织部审批。<br>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;（2）县委组织部批准建立（合并）党组织的，应召开党员大会，以无记名投票方式差额选举产生委员会；县委组织部批准撤销（合并）党组织的，要通知被撤销支部的所有党员，告知他们所去的党组织，并开具组织关系介绍信，将组织关系转到新的党支部，使他们能及时参加组织生活。<br>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;（3）县委组织部批准建立（合并）党组织的，应召开委员会会议，等额提名或直接选举产生书记、副书记，并对委员进行分工。 <br>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;（4）县委组织部批准建立（合并）党组织的，应向上级党委写出委员会组成的请示。请示的主要内容包括：选举委员会的依据；党员大会进行选举的简要情况，包括时间、出席大会的党员情况、候选人名额与应选人名额差额情况、选举结果等；委员会选举书记；副书记的情况以及党支部委员的分工情况。 <br>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;（5）上级批复。委员会开始工作，履行自己职责。"},
	            {"id":"8","title":"困难党员救助基金利息金管理发放流程与规范","data":"&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;1、救助基金利息金收入<br>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;（1）根据与县非税收入管理局签订的协议，部办公室根据财政部门安排和部工作需要，编报年度预算初步方案。<br>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;（2）年度预算初步方案由分管部长审核把关，向常委部长汇报后提请部长办公会研究，通过后上报县财政局。<br>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp（3）每月8月中旬，由组织科根据工作需要提出书面申请，向县财政局提交申请，同时积极协调做好资金划拨工作。<br>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;（4）设立单独帐套，资金到位后，由办公室财务人员入账，专门科目统一管理，确保专款专用。<br>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;2、救助基金利息金支出救助基金利息金原则上每年春节、“七一”前后两次研究发放。如遇特殊情况，经部长办公会研究同意后，可随时进行救助。<br>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;（1）申请。原则上由困难党员本人向所在党支部提出申请（因重大疾病而提出救助的，需同时提供县级或以上医院的疾病证明和缴费凭证），经党支部研究同意后，填写上报《生活困难党员救助审批表》。<br>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;（2）审核。实行党支部初审、党委复审、县委组织部审批的三级审核制度。拟救助对象填写《生活困难党员救助审批表》后，由所在党支部核实情况报所在党委审查；党委复审同意后，将《生活困难党员救助审批表》连同有关证明材料上报县委组织部审批；县委组织部根据情况提出救助意见，并由部长办公会研究决定。<br>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;（3）公示。对县委组织部审批的困难党员救助对象，各党委要在其所在党支部进行为期7天的公示，接受党员和群众监督，对不符合条件的取消救助资格。<br>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;（4）发放。县委组织部根据审批情况将救助资金直接拨付到有关党委，由各党委负责发放给困难党员个人。实行救助对象签字（盖章）回执制度，所有发放的救助资金必须由本人签字（盖章）后才能作为入帐凭证。"},
	            {"id":"9","title":"民主生活会工作流程与规范"       	   ,"data":"&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;1、会前准备阶段<br>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;（1）根据班子成员中存在的主要问题和上级党组织的要求，确定主题。<br>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;（2）确定列席人员名单。根据会议主题，可吸收与会议内容有关的其他干部列席参加，便于更好地解决问题。<br>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;（3）班子成员在会前要交换意见，沟通思想。<br>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;（4）听取或征求党内外群众的意见和建议。<br>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;（5）报告上级党组织。应提前15天将会议主题、开会时间、地点、参加会议人员名单等材料报告县纪委和县委组织部，以便于上级党组织决定派人参加。<br>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;2、开好会议，开展批评和自我批评<br>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;（1）民主生活会由班子负责人召集并主持。<br>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;（2）通报上次民主生活会提出的问题的整改情况。<br>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;（3）通报党内外群众的意见和建议。<br>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;（4）围绕主题，与会同志充分发表意见，认真开展批评和自我批评。<br>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;（5）因故缺席人员可以提交书面发言，书面发言由主持人在会上宣读并列入会议记录。<br>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;（6）在会议上对检查和反映出来的问题要积极制订整改措施，切实加以解决。<br>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;（7）与会的上级党组织人员发言，给生活会以具体指导。<br>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;（8）认真作好生活会的会议记录。<br>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;3、会后完善阶段<br>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;（1）及时向上级党组织报送民主生活会情况的书面报告和会议记录。<br>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;（2）主持人或由主持人委托出席会议的其他同志将会议情况和批评意见转告缺席人员。<br>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;（3）对会上提出的问题，及时落实整改措施。对群众普遍关心的问题，要将整改措施用适当的方式向群众公布，接受群众监督。<br>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;（4）信息意见要反馈。民主生活会情况向下级党组织及广大党员通报后，要及时了解群众反映的建议及意见。看问题找得准不准，改进措施得力不得力，以及落实改进措施的情况。<br>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;（5）与会人员必须遵守保密纪律，需要保密的内容，不得外泄或扩散。对违反保密纪律的人，应视情节轻重给予必要的处分。<br>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;（6）建立档案。开完民主生活会后，要将生活会的资料立卷归档。包括：会议的原始记录及生活会情况报告，征求群众意见资料，每位成员的发言材料等。<br>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;（7）材料上报。会后15天内，要形成民主生活会专题报告，报告要有会议召开的情况和领导班子成员发言材料，也要有会前征求意见材料、会后制定的整改措施和上年度整改措施落实的情况，分别报县纪委和县委组织部。"},
	            {"id":"10","title":"村级组织运转经费资金使用流程与规范"	   ,"data":"&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;1、支出使用范围<br>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;运转经费必须用于党的活动，具体使用范围为村级党组织日常办公费用、村庄公共服务设施维修维护费用、党员干部教育培训费用、村庄公益事业佣工费用、其他必要因公支出；不得用于与党组织运转和活动无关的项目开支。<br>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;2、资金拨付程序每年年初，由县委组织部联合农业局在深入摸底排查的基础上，向县财政局提供需补助运转经费的村庄名单和经费发放方案。县财政局负责对经费发放方案进行审核，并将所补运转经费及时足额分季度由县财政局乡财县管中心直接下拨到乡镇经管站，由乡镇经管站分村记账管理。<br>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;3、资金报账程序健全“村财乡代管”和村会计委托代理制，乡镇经管站设立村级党组织运转经费办理服务窗口，实行专人管理、专账核算。运转经费的使用须经支部提议、村民理财小组同意，由支出人收集完整支出票据，票据由经办人、村党支部书记、村委会主任和村报账员共同签字确认，经村民主理财小组审核同意签字盖章后，按规定时间由村报账员到乡镇“三资”代理服务中心报销入账。各村支出票据必须经乡镇经管站监管会计严格审核把关，经乡镇组织委员、分管领导确认无误签字后予以报销。对超出范围、不符合报销条件的拒绝支出。收支情况要每月在村公开栏和汶上党建网进行公开。"},
	            ];
	
	/**
	 * 初始化导航
	 */
	var handleNav = function(p){
		var nav = "";
		$(data).each(function(i){
			var title = "";
			if(this.title.length>7){
				title=this.title.substring(0,7)+"...";
			}
			if(i==param.leftClick-1){
				nav+="<div class=\"nav_btn nav_btn_"+(i+1)+" nav_btn_on\" style=\"margin-top:0px;\"><marquee scrollamount=\"2\">"+this.title+"</marquee></div>";
			}else{
				nav+="<div class=\"nav_btn nav_btn_"+(i+1)+"\">"+title+"</div>";
			}
		});
		$(".c_left").html(nav);
		$(".nav_btn").each(function(i){
			$(this).removeClass("hidden");
			if(i<p.star||i>p.end){
				$(this).addClass("hidden");
			}
		});
	};
	
	/**
	 * 初始化内容
	 */
	var handleContent = function(){
		$(".content_top").html(data[0].data);
	};
	
	/**
	 * 分页
	 * 
	 */
	var navPaging = function(data){
		$(".nav_btn").each(function(i){
			$(this).removeClass("hidden");
			if(i<data.star||i>data.end){
				$(this).addClass("hidden");
			}
		});
	}
	
	/**
	 * 初始化键盘事件
	 */
	var handleKeyDown = function() {
		var star =0;
		var end=6;
		$(document).keydown(function(e) {
			e = e || window.event; // 取得事件对象
			var keyCode = e.which || e.keyCode; // 按键值
			switch (keyCode) {
			case 37://left
				param.leftOrRight=0;
				$(".content_top").removeClass("content_top_on");
				evm_cms_domCtrl.gestrue.pause = !evm_cms_domCtrl.gestrue.pause;
                evm_cms_domCtrl.gestrue.init(evm_cms_domCtrl.gestrue.T);
                return false;
				break;
			case 39://right
				param.leftOrRight=1;
				$(".content_top").addClass("content_top_on");
				break;
			case 38://up
				if(param.leftOrRight==0){
					$(".nav_btn_on").removeClass("nav_btn_on");
					if(param.leftClick>1){
						param.leftClick--;
					}
					$(".nav_btn_"+param.leftClick).addClass("nav_btn_on");
					if(param.leftClick<=end-6&&star>0){
						star--;end--;
					}
					console.info({"star":star,"end":end});
					handleNav({"star":star,"end":end});
					$(".content_top").html(data[param.leftClick-1].data);
				}else if(param.leftOrRight==1){
					 evm_cms_domCtrl.gestrue.pause = false;
					 evm_cms_domCtrl.gestrue.init(2);
					 return false;
				}
				break;
			case 40://down
				if(param.leftOrRight==0){
					$(".nav_btn_on").removeClass("nav_btn_on");
					if(param.leftClick<$(".nav_btn").length){
						param.leftClick++;
					}
					$(".nav_btn_"+param.leftClick).addClass("nav_btn_on");
					if(param.leftClick>7&&param.leftClick<$(".nav_btn").length+1){
						star++;
						end++;
						if(star>$(".nav_btn").length-7){
							star=$(".nav_btn").length-7
						}
						if(end>star+6){
							end = star+6;
						}
					}
					handleNav({"star":star,"end":end});
					$(".content_top").html(data[param.leftClick-1].data);
				}else if(param.leftOrRight==1){
					evm_cms_domCtrl.gestrue.pause = false;
	                evm_cms_domCtrl.gestrue.init(1);
	                return false;
				}
				break;
			case 13://确定
				if(param.leftOrRight==1){
					evm_cms_domCtrl.gestrue.pause = !evm_cms_domCtrl.gestrue.pause;
	                evm_cms_domCtrl.gestrue.init(evm_cms_domCtrl.gestrue.T);
	                return false;
				}
				break;
			case 8:
				break;
			}
		});
	};
	
	return {
		init : function(){
			handleNav({"star":"0","end":"6"});
			handleContent();
			handleKeyDown();
		}
	};
	
}();