/**
 * 站点监控
 */
var ZDJK = function() {
	var param = {
		clickNum : 1,//右侧导航点击次数
		clickNum_r:1,//左侧内容点击次数
		leftOrRight:1,//
		contentStar : 0,
		contentEnd :5,
		contentCount : 6,
		
		navCount : 5,//导航栏每页条数
		navStar : 0,//开始
		navEnd : 4,//结束
		
	};
	/**
	 * 获取url参数
	 */
	var getUrlParam = function(){
		var url = decodeURI(location.search); //获取url中"?"符后的字串
		var theRequest = new Object();
		if (url.indexOf("?") != -1) {
			var str = url.substr(1);
		    strs = str.split("&");
		    for(var i = 0; i < strs.length; i ++) {
		    	theRequest[strs[i].split("=")[0]]=(strs[i].split("=")[1]);
		    }
		}
		return theRequest;
	};
	
	/**
	 * 初始导航区域
	 */
	var handleNav = function() {
		if(getUrlParam().type!=undefined){
			$(".main").addClass("main2");
		}
		
		$.ajax({
			type : "GET",
			dataType : 'json',
			url : "/PartyMemberEducation/townInfoController/gettownbystbno",
			data:{stbNo:getUrlParam().stbNo},
			success : function(data) {
				data = eval("("+data+")");
				if(data.success){
					$(data.obj).each(function(i){
						$(".content_left").append("<div class='nav_btn nav_btn_"+(i+1)+"' townId='"+this.townId+"'>"+this.townName+"</div>");
						if(i==0){
							$(".nav_btn_1").addClass("nav_btn_down");
						}
					});
					$(".contnet_left_up").addClass("up");
					$(".contnet_left_down").addClass("down");
					pagination({"star":0,"end":4,"leftOrRight":param.leftOrRight});
					handleContent();
				}
			}
		});
	};
	
	/**
	 * 初始内容区域
	 */
	var handleContent = function() {
		$.ajax({
			type : "GET",
			dataType : 'json',
			data : {
				"townId" : $(".nav_btn_down").attr("townId"),
				"stbNo" : getUrlParam().stbNo,
				"cType" : getUrlParam().cType
			},
			url : "/PartyMemberEducation/meetingRoomController/getMeetingRoomByTown",
			success : function(data) {
				data = eval("("+data+")");
				if(data.success){
					var content = "";
					$(data.obj).each(function(i){
						content += "<div class='content_btn content_btn_"+(i+1)+"' camerano=\""+this.cameraNo+"\" videourl='"+this.cameraUrl+"'>"+this.cameraName+"</div>";
					});
					$(".content_right").html(content)
					pagination({"star":0,"end":5,"leftOrRight":2});
				}
			}
		});
	}
	
	/**
	 * 导航栏分页
	 */
	var pagination = function(c){
		if(c.leftOrRight==1){
			if($(".nav_btn").length>param.navCount){
				$(".nav_btn").each(function(i){
					$(this).removeClass("nav_btn_hidden");
					if(i<c.star||i>c.end){
						$(this).addClass("nav_btn_hidden");
					}
				});
			} 
		}
		if(c.leftOrRight==2){
			if($(".content_btn").length>param.navCount){
				$(".content_btn").each(function(i){
					$(this).removeClass("nav_btn_hidden");
					if(i<c.star||i>c.end){
						$(this).addClass("nav_btn_hidden");
					}
				});
			} 
		}
	}
	
	/**
	 * 绑定键盘事件
	 */
	var handleKeyDown = function() {
		$(document).keydown(function(e) {
			e = e || window.event; // 取得事件对象
			var keyCode = e.which || e.keyCode; // 按键值
			switch (keyCode) {
			case 37:
				if(param.leftOrRight==1){
					param.leftOrRight=2;
					$(".content_btn_"+param.clickNum_r).addClass("content_btn_down");
				}else if(param.leftOrRight==2){
					param.leftOrRight=1;
					$(".content_btn_"+param.clickNum_r).removeClass("content_btn_down");
				}
				break;
			case 39:
				if(param.leftOrRight==1){
					param.leftOrRight=2;
					$(".content_btn_"+param.clickNum_r).addClass("content_btn_down");
				}else if(param.leftOrRight==2){
					param.leftOrRight=1;
					$(".content_btn_"+param.clickNum_r).removeClass("content_btn_down");
				}
				break;
			case 38:// up
				if(param.leftOrRight==1){
					$(".nav_btn_down").removeClass("nav_btn_down");
					param.clickNum--;
					if(param.clickNum<1){
						param.clickNum = $(".nav_btn").length;
						param.navStar=$(".nav_btn").length-5;
						param.navEnd=$(".nav_btn").length-1;
					}else if(param.clickNum<=$(".nav_btn").length-5&&param.navStar>0){
						param.navStar--;
						param.navEnd--;
					} 
					pagination({"star":param.navStar,"end":param.navEnd,"leftOrRight":param.leftOrRight});
					$(".nav_btn_"+param.clickNum).addClass("nav_btn_down");
					handleContent();
				}
				if(param.leftOrRight==2){
					$(".content_btn_down").removeClass("content_btn_down");
					param.clickNum_r--;
					if(param.clickNum_r<1){
						param.clickNum_r = $(".content_btn").length;
						param.contentStar=$(".content_btn").length-6;
						param.contentEnd=$(".content_btn").length-1;
					}else if(param.clickNum_r<=$(".content_btn").length-6&&param.contentStar>0){
						param.contentStar--;
						param.contentEnd--;
					} 
					pagination({"star":param.contentStar,"end":param.contentEnd,"leftOrRight":param.leftOrRight});
					$(".content_btn_"+param.clickNum_r).addClass("content_btn_down");
				}
				
				break;
			case 40:// down
				if(param.leftOrRight==1){
					$(".nav_btn_down").removeClass("nav_btn_down");
					param.clickNum++;
					if(param.clickNum>=$(".nav_btn").length){
						param.clickNum=$(".nav_btn").length;
					}
					if(param.clickNum>param.navCount){
						if(param.clickNum<=$(".nav_btn").length&&param.navEnd<$(".nav_btn").length-1){
							param.navStar++;
							param.navEnd++;
						}else{
							param.clickNum = 1;
							param.navStar=0;
							param.navEnd=4;
						}
						pagination({"star":param.navStar,"end":param.navEnd,"leftOrRight":param.leftOrRight});
					}
					$(".nav_btn_"+param.clickNum).addClass("nav_btn_down");
					handleContent();
				}else if(param.leftOrRight==2){
					$(".content_btn_down").removeClass("content_btn_down");
					param.clickNum_r++;
					if(param.clickNum_r>=$(".content_btn").length){
						param.clickNum_r=$(".content_btn").length;
					}
					if(param.clickNum_r>param.contentCount){
						if(param.clickNum_r<=$(".content_btn").length&&param.contentEnd<$(".content_btn").length-1){
							param.contentStar++;
							param.contentEnd++;
						}else{
							param.clickNum_r = 1;
							param.contentStar=0;
							param.contentEnd=5;
						}
						pagination({"star":param.contentStar,"end":param.contentEnd,"leftOrRight":param.leftOrRight});
					}
					$(".content_btn_"+param.clickNum_r).addClass("content_btn_down");
				}
				break;
			case 13:// ok
				if(param.leftOrRight==1){
				}else if(param.leftOrRight==2){
					if(getUrlParam().type=="meeting"){
						var url="/PartyMemberEducation/rcdj/zdjk_xq.jsp?stbNo="+getUrlParam().stbNo+"&cameraNo="+$(".content_btn_"+param.clickNum_r).attr("camerano")+"&title="+$(".content_btn_down").html();
						window.location.assign(encodeURI(url));
					}else{
						window.location.href= $(".content_btn_down").attr("videourl");
					}
				}
				break;
			case 8:
				break;
			}
		});
	};
	
	return {
		/**
		 * 初始化
		 * 
		 * @returns
		 */
		init : function() { 
			handleNav();
			handleKeyDown();
		}
	};
}();