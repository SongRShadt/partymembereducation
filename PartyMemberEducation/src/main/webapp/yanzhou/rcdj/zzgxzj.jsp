<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%
	String path = request.getContextPath();
	String basePath = request.getScheme() + "://"
			+ request.getServerName() + ":" + request.getServerPort()
			+ path + "/";
%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>兖州--->组织关系转接</title>
<link rel="stylesheet" href="<%=basePath%>rcdj/css/main.css" type="text/css"></link>
<link rel="stylesheet" href="<%=basePath%>rcdj/css/zzgxzj.css" type="text/css"></link>
</head>
<body>
<div class="main">
	<div class="header"></div>
	<div class="content">
		<div class="c_left">
			<div class="c_l_title">党员姓名</div>
			<div class="c_l_c_content">
			</div>
		</div>
		<div class="c_right">
			<div class="c_r_title">党员信息</div>
			<div class="c_r_content">
				<div class="c_r_c_top">
					<div class="portrait"></div>
					<div class="sex"></div>
					<div class="phone"></div>
					<div class="sbrith"></div>
					<div class="sid"></div>
				</div>
				<div class="c_r_c_middle">
					<div class="checkbox checkbox_1" data="1">党员转入</div>
					<div class="checkbox checkbox_2" data="2">党员转出</div>
				</div>
				<div class="c_r_c_bottom">
					<div class="msg"></div>
					<div class="submit">提交</div>
				</div>
			</div>
		</div>
	</div>
	<div class="footer"></div>
</div>
<script type="text/javascript" src="<%=basePath%>rcdj/js/jquery-1.8.3.js"></script>
<script type="text/javascript" src="<%=basePath%>rcdj/js/ConfigUtil.js"></script>
<script type="text/javascript" src="<%=basePath%>rcdj/js/zzgxzj.js"></script>
<script type="text/javascript">
jQuery(document).ready(function(){
	ZZGXZJ.init();
});
</script>

</body>
</html>