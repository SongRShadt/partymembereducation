/**
 *滚动
 */
var evm_cms_domCtrl = {
    getObj: function () {
        if (document.getElementsByTagName("marquee"))
            return document.getElementsByTagName("marquee");
        else
            return false;
    },
    marquee_replace: function () {
    },
    gestrue: {
        _D: null,
        _H: 0,
        nowT: 0,
        timeOut: null,
        T: null, //方向
        pause: false,
        init: function (T) {
            clearTimeout(this.timeOut);
            if (!this.pause) {
                this._D = this._getObj();
                this.rollStart(T);
                this.T = T;
            }
        },
        _getObj: function () {
            return document.getElementById("content_top");
        },
        rollStart: function (type) {
            if (this._D) {
                this._H = this._D.scrollHeight - this._D.offsetHeight;

                this.timeOut = setTimeout(function () {
                    evm_cms_domCtrl.gestrue.autoScroll(evm_cms_domCtrl.gestrue._H, evm_cms_domCtrl.gestrue._D);
                }, 70);
            }
        },
        autoScroll: function (H, D_) {
            this.nowT = D_.scrollTop;
            if (this.T == 1) {
                if (this.nowT < H) {
                    D_.scrollTop = D_.scrollTop + 5;
                    evm_cms_domCtrl.gestrue.rollStart(this.T);
                }
            } else if (this.T == 2) {
                if (this.nowT > 0) {
                    D_.scrollTop = D_.scrollTop - 5;
                    evm_cms_domCtrl.gestrue.rollStart(this.T);
                }
            }
        }
    },
    scrollController: function (evt) {
        var val = evt.which || evt.keyCode;
        switch (val) {
            case 13:
                evm_cms_domCtrl.gestrue.pause = !evm_cms_domCtrl.gestrue.pause;
                evm_cms_domCtrl.gestrue.init(evm_cms_domCtrl.gestrue.T);
                return false;
                break;
            case 38:
            case 306:
                evm_cms_domCtrl.gestrue.pause = false;
                evm_cms_domCtrl.gestrue.init(2);
                return false;
                break;
            case 40:
            case 307:
                evm_cms_domCtrl.gestrue.pause = false;
                evm_cms_domCtrl.gestrue.init(1);
                return false;
                break;
        }
    },
    trackKey: function (event) {
        return evm_cms_domCtrl.scrollController(event);
    }
};


