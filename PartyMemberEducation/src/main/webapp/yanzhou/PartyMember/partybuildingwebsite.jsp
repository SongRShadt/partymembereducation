﻿<%@ page language="java" import="java.util.*" pageEncoding="utf-8"%>
<%
	String path = request.getContextPath();
	String basePath = request.getScheme() + "://"
			+ request.getServerName() + ":" + request.getServerPort()
			+ path + "/";
%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
<head>
<base href="<%=basePath%>">
<title>电视直播-党建网站</title>
<meta http-equiv="Content-type" content="text/html; charset=utf-8" />
<meta name="page-view-size" content="1280*720" />
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<script type="text/javascript">
window.location.href="/PartyMemberEducation/rcdj/djwz.jsp";
</script>
<link rel="stylesheet" href="<%=basePath%>PartyMember/css/partybuildingwebsite.css" type="text/css"></link>
</head>
<body>
	<div class="main">
		<div class="header">
			<div class="header_title">
				<div class="header_left_title"></div><div class="header_right_title" style="margin-left:120px;">党建网站</div></img>
			</div>
		</div> 
		<div class="content">
			<div class="content_btn" style="background: url(<%=basePath%>PartyMember/image/01.png); background-repeat:no-repeat;"></div>
			<div class="content_btn" style="background: url(<%=basePath%>PartyMember/image/02.png); background-repeat:no-repeat;"></div>
			<div class="content_btn" style="background: url(<%=basePath%>PartyMember/image/03.png); background-repeat:no-repeat;"></div>
			<div class="content_btn" style="background: url(<%=basePath%>PartyMember/image/04.png); background-repeat:no-repeat;"></div>
			<div class="content_btn" style="background: url(<%=basePath%>PartyMember/image/05.png);background-repeat:no-repeat;"></div>
			<div class="content_btn" style="background: url(<%=basePath%>PartyMember/image/06.png);background-repeat:no-repeat;"></div>
			<div class="content_btn" style="background: url(<%=basePath%>PartyMember/image/07.png); background-repeat:no-repeat;"></div>
			<div class="content_btn" style="background: url(<%=basePath%>PartyMember/image/08.png); background-repeat:no-repeat;"></div>
			<div class="content_btn" style="background: url(<%=basePath%>PartyMember/image/09.png);background-repeat:no-repeat;"></div>
		</div>
		<div class="footer"></div>
	</div>
	<!-- corescript -->
	<script type="text/javascript" src="<%=basePath%>js/jquery-1.8.3.js"></script>
	<script type="text/javascript"
		src="<%=basePath%>PartyMember/js/partybuildingwebsite.js"></script>
	<script type="text/javascript">
		jQuery(document).ready(function() {
			PartyBuildingWebSite.init();
		});
	</script>

</body>
</html>
