<%@ page language="java" import="java.util.*" pageEncoding="utf-8"%>
<%
	String path = request.getContextPath();
	String basePath = request.getScheme() + "://"
			+ request.getServerName() + ":" + request.getServerPort()
			+ path + "/";
%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
<head>
<base href="<%=basePath%>">
<title>组织关系转接</title>
<meta http-equiv="Content-type" content="text/html; charset=utf-8" />
<meta name="page-view-size" content="1280*720" />
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<link rel="stylesheet" href="<%=basePath%>PartyMember/css/service_transfer.css" type="text/css"></link>
</head>
<body>
	<div class="main">
		<div class="header">
<!-- 				<div class="logo"></div> -->
			<div class="header_title">
<%-- 				<div class="header_left_title">组织关系转接</div><img class="header_arraw"src="<%=basePath%>PartyMember/image/arrow.png"><div class="header_right_title">党员服务</div></img> --%>
			</div>
		</div> 
		<div class="content">
			<div class="content_left">
				<div class="content_left_top">姓名</div>
				<div class="content_left_bottom  clickarea_1"></div>
			</div>
			<div class="content_right">
				<div class="content_right_top">党员信息</div>
				<div class="content_right_bottom">
					<div class="content_right_bottom_top">
						<div class="content_right_bottom_top_left"></div>
						<div class="content_right_bottom_top_right">
							<div class="sex"></div>
							<div class="phone"></div>
							<div class="sbrith"></div>
							<div class="sid"></div>
						</div>
					</div>
					<div class="content_right_bottom_bottom clickarea_2" >
						<div class="content_right_bottom_bottom_btn">反映发展党员问题</div>
						<div class="content_right_bottom_bottom_btn">困难党员救助</div>
						<div class="content_right_bottom_bottom_btn">咨询致富信息</div>
						<div class="content_right_bottom_bottom_btn" style="width: 248px;">转入党员确认</div>
						<div class="content_right_bottom_bottom_btn">转出党员确认</div>
						<div class="content_right_bottom_bottom_next">提交</div>
					</div>
				</div>
			</div>
		</div>
		<div class="submit_msg"></div>
		<div class="footer"></div>
	</div>
	<!-- corescript -->
	<script type="text/javascript" src="<%=basePath%>js/jquery-1.8.3.js"></script>
	<script type="text/javascript" src="<%=basePath%>js/ConfigUtil.js"></script>
	<script type="text/javascript"
		src="<%=basePath%>PartyMember/js/service_transfer.js"></script>
	<script type="text/javascript">
		jQuery(document).ready(function() {
 			ServiceTransfer.init();
		});
	</script>

</body>
</html>
