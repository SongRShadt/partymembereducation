<%@ page language="java" import="java.util.*" pageEncoding="utf-8"%>
<%
	String path = request.getContextPath();
	String basePath = request.getScheme() + "://"
			+ request.getServerName() + ":" + request.getServerPort()
			+ path + "/";
%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
<head>
<base href="<%=basePath%>">
<title>党员服务</title>
<meta http-equiv="Content-type" content="text/html; charset=utf-8" />
<meta name="page-view-size" content="1280*720" />
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<link rel="stylesheet" href="<%=basePath%>PartyMember/css/partymemberservice.css" type="text/css"></link>
</head>
<body>
	<div class="main">
		<div class="header">
			<div class="header_title">
			</div>
		</div> 
		<div class="content">
			
			<div class="content_btn">&nbsp;&nbsp;&nbsp;&nbsp;组织关系转接</div>
			<div class="content_btn">&nbsp;&nbsp;&nbsp;&nbsp;党费收缴</div>
			<div class="content_btn">&nbsp;&nbsp;&nbsp;&nbsp;党务工作咨询</div>
			<div class="content_btn">&nbsp;&nbsp;&nbsp;&nbsp;党员热线服务</div>
		</div>
		<div class="footer"></div>
	</div>
	<!-- corescript -->
	<script type="text/javascript" src="<%=basePath%>js/jquery-1.8.3.js"></script>
	<script type="text/javascript" src="<%=basePath%>js/ConfigUtil.js"></script>
	<script type="text/javascript" src="<%=basePath%>PartyMember/js/partymemberservice.js"></script>
	<script type="text/javascript">
		jQuery(document).ready(function() {
			PartyMemberService.init();
		});
	</script>

</body>
</html>
