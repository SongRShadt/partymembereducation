﻿/**
 * 县级党建
 */
var CountyPartBuilding = function() {
	var paramter = {
		clickNum : 1
	};
	/**
	 * 绑定键盘事件
	 */
	var handleKeyDown = function() {
		$(".content_btn").each(function(i) {
			if (i == 0) {
				$(this).addClass("content_btn_check");
			}
			$(this).addClass("btn_" + (i + 1));
		});
		$(document).keydown(function(e) {
			e = e || window.event; // 取得事件对象
			var keyCode = e.which || e.keyCode; // 按键值
			switch (keyCode) {
			case 37:// left
				$(".content_btn_check").removeClass("content_btn_check");
				paramter.clickNum--;
				if (paramter.clickNum < 1) {
					paramter.clickNum = 1;
				}
				$(".btn_" + paramter.clickNum).addClass("content_btn_check");
				break;
			case 39:// right
				$(".content_btn_check").removeClass("content_btn_check");
				paramter.clickNum++;
				if (paramter.clickNum > 8) {
					paramter.clickNum = 8;
				}
				$(".btn_" + paramter.clickNum).addClass("content_btn_check");
				break;
			case 38:
				if (paramter.clickNum>=4&&paramter.clickNum<=8) {
					$(".content_btn_check").removeClass("content_btn_check");
					paramter.clickNum -= 3;
					$(".btn_" + paramter.clickNum).addClass("content_btn_check");
				}
				break;
			case 40:
				
				
				if (paramter.clickNum>=1&&paramter.clickNum<=5) {
					$(".content_btn_check").removeClass("content_btn_check");
					paramter.clickNum += 3;
					$(".btn_" + paramter.clickNum).addClass("content_btn_check");
				}
				
				break;
			case 13:// 确定
				if(paramter.clickNum==1){
											window.location.href="http://172.29.0.108:1935/live/cctv1.stream/playlist.m3u8";
				}
				if(paramter.clickNum==2){
}
				break;
			case 8:
				break;
			}
		});
	}

	return {
		/**
		 * 初始化
		 * 
		 * @returns
		 */
		init : function() {
			handleKeyDown();
		}
	};
}();