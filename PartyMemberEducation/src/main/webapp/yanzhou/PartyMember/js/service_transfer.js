/**
 * 组织关系转接
 */
var ServiceTransfer = function() {
	/**
	 * 属性列表
	 */
	var param = {
		navData : "",//数据源
		clickArea : 2,//键盘区域
		areaNow : 1,//当前区域
		clickNum : 1,//左侧导航点击次数
		clickNum_right :1,//右侧点击次数
		dataSize : 1,
		
		star : 0,
		end : 6,
		
		serviceIp : ConfigUtil.getConfig().ip,//服务器ip
		
		callback : 1,//回调参数
		OrgId : ConfigUtil.getUrlParam().OrgId,//村组织编号
		user_type : ConfigUtil.getConfig().user_type,//用户类型
	};
	
	/**
	 * 获取党员信息
	 */
	var handlePersonInfo = function(){
		$.ajax({
			type : "GET",
			dataType : 'jsonp',
			async : false,
			data : {
				"callback" : param.callback,
				"OrgId" : param.OrgId,
				"user_type": param.user_type
			},
			url : "http://"+param.serviceIp+"/userinfo/userInfoService",
			success : function(result) {
				if(result.success){
					var data = result.data;
					param.navData = data;
					param.dataSize= data.length;
					var person = "";
					$(data).each(function(i){
						person+="<div class='content_left_bottom_person'>"+this.name+"</div>";
					});
					$(".content_right_bottom_top_left").html("<img src='http://"+param.serviceIp+"/userimg?sid="+data[0].sid+"'/>");
					$(".sex").html("性别:"+data[0].sex);
					$(".phone").html("电话号码:"+data[0].phone);
					$(".sbrith").html("出生年月:"+data[0].sbrith);
					$(".sid").html("身份证号:"+data[0].sid);
					$(".sid").attr("data",param.navData[param.clickNum-1].sid);
					$(".content_left_bottom").html(person);
					handleNav();
				}
			}
		});
	};
	/**
	 * 初始化需要键盘移动区域
	 */
	var handleNav = function(data){
		for(var i =0;i<param.clickArea;i++){
			$(".clickarea_"+(i+1)+" div").each(function(j){
				if(i==0&&j==0){
					$(this).addClass("content_left_bottom_person_down");
				}if(i==0&&j>6){
					$(this).attr("style","display: none;");
				}
				$(this).addClass("clickarea_"+(i+1)+"_"+(j+1));
				$(this).attr("data",j+1);
			});
		} 
	};
	/**
	 * 导航分页
	 */
	var limtNav = function(c){
		$(".clickarea_"+(param.areaNow)+" div").each(function(i){
			if(i<c.star||i>c.end){
				$(this).attr("style","display: none;");
			}else{
				$(this).attr("style","");
			} 
		});
	};
	
	/**
	 * 绑定键盘事件
	 */
	var handleKeyDown = function(){
		$(document).keydown(function(e) {
			e = e || window.event; // 取得事件对象
			var keyCode = e.which || e.keyCode; // 按键值
			switch (keyCode) {
			case 37://left
				if(param.clickNum_right==1){
					if($(".clickarea_2_1").hasClass("checkbox_btn_off_down")){
						param.areaNow=1;
						$(".checkbox_btn_off_down").removeClass("checkbox_btn_off_down");
					}else if($(".clickarea_2_1").hasClass("checkbox_btn_on_down")){
						$(".clickarea_2_1").removeClass("checkbox_btn_on_down");
						$(".clickarea_2_1").addClass("checkbox_btn_on_over");
						param.areaNow=1;
					}
				}
				if(param.areaNow == 2){
					if(param.clickNum_right==6){
						if($(".clickarea_2_"+param.clickNum_right).hasClass("submit_btn_down")){
							$(".submit_btn_down").removeClass("submit_btn_down");
						}
					}
					if($(".clickarea_2_"+param.clickNum_right).hasClass("checkbox_btn_off_down")){
						$(".checkbox_btn_off_down").removeClass("checkbox_btn_off_down");
					}
					if($(".clickarea_2_"+param.clickNum_right).hasClass("checkbox_btn_on_down")){
						$(".checkbox_btn_on_down").addClass("checkbox_btn_on_over");
						$(".checkbox_btn_on_down").removeClass("checkbox_btn_on_down");
					} 
					param.clickNum_right--;
					if(param.clickNum_right<1){
						param.clickNum_right=1;
					}
					console.info(param.clickNum_right);
					if($(".clickarea_2_"+param.clickNum_right).hasClass("checkbox_btn_on_over")){
						$(".clickarea_2_"+param.clickNum_right).addClass("checkbox_btn_on_down");
					}else if($(".clickarea_2_"+param.clickNum_right).hasClass("checkbox_btn_off_down")==false){
						$(".clickarea_2_"+param.clickNum_right).addClass("checkbox_btn_off_down");
					}
				}
				
				break;
			case 39://right
				if(param.areaNow==1){
					param.areaNow=2;
					if($(".clickarea_2_1").hasClass("checkbox_btn_on_over")){
						$(".clickarea_2_1").addClass("checkbox_btn_on_down");
					}else if($(".clickarea_2_1").hasClass("checkbox_btn_off_down")==false){
						$(".clickarea_2_1").addClass("checkbox_btn_off_down");
					}
				}else if(param.areaNow==2){
					if($(".clickarea_2_"+param.clickNum_right).hasClass("checkbox_btn_off_down")){
						$(".checkbox_btn_off_down").removeClass("checkbox_btn_off_down");
					}
					if($(".clickarea_2_"+param.clickNum_right).hasClass("checkbox_btn_on_down")){
						$(".checkbox_btn_on_down").addClass("checkbox_btn_on_over");
						$(".checkbox_btn_on_down").removeClass("checkbox_btn_on_down");
					} 
					param.clickNum_right++;
					if(param.clickNum_right==6){
						$(".clickarea_2_"+param.clickNum_right).addClass("submit_btn_down");
					}
					if(param.clickNum_right>6){
						param.clickNum_right=6;
					}
					if($(".clickarea_2_"+param.clickNum_right).hasClass("checkbox_btn_on_over")){
						$(".clickarea_2_"+param.clickNum_right).addClass("checkbox_btn_on_down");
					}else if($(".clickarea_2_"+param.clickNum_right).hasClass("checkbox_btn_off_down")==false){
						$(".clickarea_2_"+param.clickNum_right).addClass("checkbox_btn_off_down");
					}
				}
				break;
				
			case 38://up
				if(param.areaNow==1){
					$(".checkbox_btn_on_over").removeClass("checkbox_btn_on_over");
					$(".clickarea_"+param.areaNow+"_"+param.clickNum).removeClass("content_left_bottom_person_down");
					param.clickNum--;
					if(param.clickNum<1){
						param.clickNum = 1;
					}
					if(param.clickNum<=param.end-6&&param.star>0){
						param.star--;
						param.end--;
						limtNav({"star":param.star,"end":param.end});
					}
					$(".clickarea_"+param.areaNow+"_"+param.clickNum).addClass("content_left_bottom_person_down");
					$(".content_right_bottom_top_left").html("<img src='http://"+param.serviceIp+"/userimg?sid="+param.navData[param.clickNum-1].sid+"'/>");
					$(".sex").html("性别:"+param.navData[param.clickNum-1].sex);
					$(".phone").html("电话号码:"+param.navData[param.clickNum-1].phone);
					$(".sbrith").html("出生年月:"+param.navData[param.clickNum-1].sbrith);
					$(".sid").html("身份证号:"+param.navData[param.clickNum-1].sid);
					$(".sid").attr("data",param.navData[param.clickNum-1].sid);
				}else if(param.areaNow==2){
					$(".submit_btn_down").removeClass("submit_btn_down");
					if($(".clickarea_2_"+param.clickNum_right).hasClass("checkbox_btn_off_down")){
						$(".checkbox_btn_off_down").removeClass("checkbox_btn_off_down");
					}
					if($(".clickarea_2_"+param.clickNum_right).hasClass("checkbox_btn_on_down")){
						$(".checkbox_btn_on_down").addClass("checkbox_btn_on_over");
						$(".checkbox_btn_on_down").removeClass("checkbox_btn_on_down");
					}
					if(param.clickNum_right==4){
						param.clickNum_right=1;
						$(".clickarea_2_"+param.clickNum_right).addClass("checkbox_btn_off_down");
					}else if(param.clickNum_right==5){
						param.clickNum_right=2;
						$(".clickarea_2_"+param.clickNum_right).addClass("checkbox_btn_off_down");
					}
					else if(param.clickNum_right==6){
						param.clickNum_right=5;
						$(".clickarea_2_"+param.clickNum_right).addClass("checkbox_btn_off_down");
					}
				}
				break;
			case 40://down
				if(param.areaNow==1){
					$(".checkbox_btn_on_over").removeClass("checkbox_btn_on_over");
					$(".clickarea_"+param.areaNow+"_"+param.clickNum).removeClass("content_left_bottom_person_down");
					param.clickNum++;
					if(param.clickNum>param.dataSize){
						param.clickNum=param.dataSize;
						limtNav({"star":param.star,"end":param.end});
					}
					if(param.clickNum>7){
						if(param.end<param.dataSize-1){
							param.star++;
							param.end++;
							limtNav({"star":param.star,"end":param.end});
						}
					}
					$(".content_right_bottom_top_left").html("<img src='http://"+param.serviceIp+"/userimg?sid="+param.navData[param.clickNum-1].sid+"'/>");
					$(".sex").html("性别："+param.navData[param.clickNum-1].sex);
					$(".phone").html("电话号码："+param.navData[param.clickNum-1].phone);
					$(".sbrith").html("出生年月:"+param.navData[param.clickNum-1].sbrith);
					$(".sid").html("身份证号:"+param.navData[param.clickNum-1].sid);
					$(".sid").attr("data",param.navData[param.clickNum-1].sid);
					$(".clickarea_"+param.areaNow+"_"+param.clickNum).addClass("content_left_bottom_person_down");
				}else if(param.areaNow==2){
					if($(".clickarea_2_"+param.clickNum_right).hasClass("checkbox_btn_off_down")){
						$(".checkbox_btn_off_down").removeClass("checkbox_btn_off_down");
					}
					if($(".clickarea_2_"+param.clickNum_right).hasClass("checkbox_btn_on_down")){
						$(".checkbox_btn_on_down").addClass("checkbox_btn_on_over");
						$(".checkbox_btn_on_down").removeClass("checkbox_btn_on_down");
					}
					if(param.clickNum_right==1){
						param.clickNum_right=4;
						$(".clickarea_2_"+param.clickNum_right).addClass("checkbox_btn_off_down");
					}else if(param.clickNum_right==2){
						param.clickNum_right=5;
						$(".clickarea_2_"+param.clickNum_right).addClass("checkbox_btn_off_down");
					}else if(param.clickNum_right==3||param.clickNum_right==4||param.clickNum_right==5){
						param.clickNum_right=6;
						$(".clickarea_2_"+param.clickNum_right).addClass("submit_btn_down");
					} 
				}
				
				if(param.areaNow==1){
					$(".clickarea_"+param.areaNow+"_"+param.clickNum_right).removeClass("content_left_bottom_person_down");
					
				}
					
				break;
			case 13://确定
				if(param.clickNum_right<6){
					$(".checkbox_btn_on_over").removeClass("checkbox_btn_on_over");
					$(".checkbox_btn_off_down").addClass("checkbox_btn_on_down");
					$(".checkbox_btn_off_down").removeClass("checkbox_btn_off_down");
				}else if(param.clickNum_right==6){
					$.ajax({
						type : "GET",
						dataType : 'jsonp',
						async : false,
						data : {
							"callback" : param.callback,
							"OrgId" : param.OrgId,
							"sid":$(".sid").attr("data"),
							"msg":$(".checkbox_btn_on_over").attr("data")
						},
						url : "http://"+param.serviceIp+"/userinfo/transferOutConfirm",
						success : function(result) {
							if(result.success){
								$(".submit_msg").html("提交成功！");
								window.location.href="/PartyMemberEducation/PartyMember/success.jsp";
							}
						}
					});
				}
				
				break;
			case 8:
				break;
			}
		});
	}
	
	return {
		/**
		 * 初始化
		 * @returns
		 */
		init : function() {
			handlePersonInfo();
			handleNav();
			handleKeyDown();
		},
	};
}();