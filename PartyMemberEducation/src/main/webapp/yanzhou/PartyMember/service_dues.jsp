<%@ page language="java" import="java.util.*" pageEncoding="utf-8"%>
<%
	String path = request.getContextPath();
	String basePath = request.getScheme() + "://"
			+ request.getServerName() + ":" + request.getServerPort()
			+ path + "/";
%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
<head>
<base href="<%=basePath%>">
<title>党费收缴</title>
<meta http-equiv="Content-type" content="text/html; charset=utf-8" />
<meta name="page-view-size" content="1280*720" />
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<link rel="stylesheet" href="<%=basePath%>PartyMember/css/service_dues.css" type="text/css"></link>
</head>
<body>
	<div class="main">
		<div class="header">
			<div class="header_title">
				<div class="header_left_title">党费收缴</div><img class="header_arraw"src="<%=basePath%>PartyMember/image/arrow.png"><div class="header_right_title">党员服务</div></img>
			</div>
		</div> 
		<div class="content">
			<div class="content_left">
				<div class="content_left_top">姓名</div>
				<div class="content_left_bottom  clickarea_1"></div>
			</div>
			<div class="content_right">
				<div class="content_right_top">党员信息</div>
				<div class="content_right_bottom">
					<div class="content_right_bottom_top">
						<div class="content_right_bottom_top_left"></div>
						<div class="content_right_bottom_top_right">
							<div class="sex"></div>
							<div class="phone"></div>
							<div class="sbrith"></div>
							<div class="sid"></div>
						</div>
					</div>
					<div class="content_right_bottom_bottom clickarea_2" >
						<div class="content_right_bottom_bottom_btn" money="3">3元</div>
						<div class="content_right_bottom_bottom_btn" money="6">6元</div>
						<div class="content_right_bottom_bottom_btn" money="9">9元</div>
						<div class="content_right_bottom_bottom_btn" money="12">12元</div>
						<div class="content_right_bottom_bottom_btn" money="15">15元</div>
						<div class="content_right_bottom_bottom_btn" money="18">18元</div>
						<div class="content_right_bottom_bottom_btn" money="30">30元</div>
						<div class="content_right_bottom_bottom_btn" money="0">其他</div>
						<div class="check_jd" style="background: none;font-weight: bold;margin-left: 20px;">季度选择:</div>
						<div class="check_jd check_jd_1" jd="1">1季度</div>
						<div class="check_jd check_jd_2" jd="2">2季度</div>
						<div class="check_jd check_jd_3" jd="3">3季度</div>
						<div class="check_jd check_jd_4" jd="4">4季度</div>
						<div class="check_jd check_jf" style="background: none;font-weight: bold;margin-left: 20px;">缴费金额:<span id="money"></span><input type="text" class="money" style="display:none;" /></div>
						<div class="content_right_bottom_bottom_next">提交</div>
					</div>
				</div>
			</div>
			<div class="msg"></div>
		
		</div>
		<div class="footer"></div>
	</div>
	<!-- corescript -->
	<script type="text/javascript" src="<%=basePath%>js/jquery-1.8.3.js"></script>
	<script type="text/javascript" src="<%=basePath%>js/ConfigUtil.js"></script>
	<script type="text/javascript"
		src="<%=basePath%>PartyMember/js/service_dues.js"></script>
	<script type="text/javascript">
		jQuery(document).ready(function() {
 			ServiceDues.init();
		});
	</script>

</body>
</html>
