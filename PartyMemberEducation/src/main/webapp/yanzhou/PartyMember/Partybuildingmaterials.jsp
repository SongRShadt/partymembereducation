<%@ page language="java" import="java.util.*" pageEncoding="utf-8"%>
<%
	String path = request.getContextPath();
	String basePath = request.getScheme() + "://"
			+ request.getServerName() + ":" + request.getServerPort()
			+ path + "/";
%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
<head>
<base href="<%=basePath%>">
<title>电视直播-党建网站</title>
<meta http-equiv="Content-type" content="text/html; charset=utf-8" />
<meta name="page-view-size" content="1280*720" />
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<link rel="stylesheet" href="<%=basePath%>PartyMember/css/partybuildingmaterials.css" type="text/css"></link>
</head>
<body>
	<div class="main">
		<div class="header">
		</div> 
		<div class="content" >
			<div class="content_btn">习近平重要讲话</div>
			<div class="content_btn">村居管理</div>
			<div class="content_btn">基层党建</div>
			<div class="content_btn">微型党课</div>
			<div class="content_btn">驻村联户</div>
			<div class="content_btn">时代楷模</div>
		</div>
		
	</div>
	<!-- corescript -->
	<script type="text/javascript" src="<%=basePath%>js/jquery-1.8.3.js"></script>
	<script type="text/javascript"
		src="<%=basePath%>PartyMember/js/partybuildingmaterials.js"></script>
	<script type="text/javascript">
		jQuery(document).ready(function() {
			PartyBuildingmaterials.init();
		});
	</script>

</body>
</html>
