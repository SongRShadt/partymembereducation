[{
	"astro" : {"sr" : "05:59","ss" : "18:14"},
	"cond" : {"code_d" : "101","code_n" : "100","txt_d" : "多云","txt_n" : "晴"},
	"date" : "2015-09-20",
	"hum" : "35",
	"pcpn" : "0.0",
	"pop" : "0",
	"pres" : "1012",
	"tmp" : {"max" : "29","min" : "17"},
	"vis" : "10",
	"wind" : {"deg" : "85","dir" : "东风","sc" : "微风","spd" : "4"}
},{
	"astro" : {"sr" : "05:59","ss" : "18:12"},
	"cond" : {"code_d" : "100","code_n" : "100","txt_d" : "晴","txt_n" : "晴"},
	"date" : "2015-09-21",
	"hum" : "42",
	"pcpn" : "0.0",
	"pop" : "0",
	"pres" : "1012",
	"tmp" : {"max" : "29","min" : "18"},
	"vis" : "10",
	"wind" : {"deg" : "175","dir" : "东南风",	"sc" : "微风","spd" : "4"}
}, {
	"astro" : {"sr" : "06:00","ss" : "18:11"},
	"cond" : {"code_d" : "101",	"code_n" : "100","txt_d" : "多云","txt_n" : "晴"},
	"date" : "2015-09-22",
	"hum" : "41",
	"pcpn" : "0.0",
	"pop" : "2",
	"pres" : "1010",
	"tmp" : {"max" : "28","min" : "18"},
	"vis" : "10",
	"wind" : {"deg" : "176","dir" : "南风","sc" : "微风","spd" : "4"}
}, {
	"astro" : {"sr" : "06:01","ss" : "18:09"},
	"cond" : {"code_d" : "101","code_n" : "305","txt_d" : "多云","txt_n" : "小雨"},
	"date" : "2015-09-23",
	"hum" : "42",
	"pcpn" : "0.0",
	"pop" : "0",
	"pres" : "1008",
	"tmp" : {"max" : "29","min" : "20"},
	"vis" : "10",
	"wind" : {"deg" : "163","dir" : "无持续风向","sc" : "微风",	"spd" : "4"	}
}, {
	"astro" : {"sr" : "06:02","ss" : "18:08"},
	"cond" : {"code_d" : "305","code_n" : "101","txt_d" : "小雨","txt_n" : "多云"},
	"date" : "2015-09-24",
	"hum" : "62",
	"pcpn" : "3.2",
	"pop" : "63",
	"pres" : "1009",
	"tmp" : {"max" : "23","min" : "18"},
	"vis" : "10",
	"wind" : {"deg" : "149","dir" : "无持续风向","sc" : "微风",	"spd" : "4"}
}, {
	"astro" : {"sr" : "06:02","ss" : "18:06"},
	"cond" : {"code_d" : "101","code_n" : "101","txt_d" : "多云","txt_n" : "多云"},
	"date" : "2015-09-25",
	"hum" : "44",
	"pcpn" : "0.0",
	"pop" : "54",
	"pres" : "1009",
	"tmp" : {"max" : "30","min" : "17"},
	"vis" : "10",
	"wind" : {"deg" : "43","dir" : "无持续风向","sc" : "微风","spd" : "4"}
}, {
	"astro" : {"sr" : "06:03","ss" : "18:05"},
	"cond" : {"code_d" : "100",	"code_n" : "100","txt_d" : "晴",	"txt_n" : "晴"},
	"date" : "2015-09-26",
	"hum" : "34",
	"pcpn" : "0.0",
	"pop" : "0",
	"pres" : "1011",
	"tmp" : {"max" : "29","min" : "17"},
	"vis" : "10",
	"wind" : {"deg" : "106","dir" : "无持续风向",	"sc" : "微风","spd" : "4"}
}]