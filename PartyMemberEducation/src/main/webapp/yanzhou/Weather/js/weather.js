var Weather = function() {
	var handleData = function() {
		$.ajax({
			type : "post",
			dataType : "json",
			url : "/PartyMemberEducation/weatherController/getWeather",
			success : function(data) {
				data = eval("(" + data + ")");
				console.info(data);
				var h1 = "", h2 = "", h3 = "", h4 = "", h5 = "";
				$(data.obj).each(function(i) {
					if (i == 0) {
						$(".todayinfo").html(data.obj[i].date+"&nbsp;"+data.obj[i].week);
						$(".day1").attr("src",data.obj[i].url);
						$(".txt1").html(data.obj[i].txt_d);
						$(".sc1").html(data.obj[i].dir+data.obj[i].sc);
						$(".wd1").html(data.obj[i].tmpmin+"℃&nbsp;~&nbsp;"+data.obj[i].tmpmax+"℃");
						$(".week1").html(data.obj[i].week);
					}
					if (i == 1) {
						$(".day2").attr("src",data.obj[i].url);
						$(".txt2").html(data.obj[i].txt_d);
						$(".sc2").html(data.obj[i].dir+data.obj[i].sc);
						$(".wd2").html(data.obj[i].tmpmin+"℃&nbsp;~&nbsp;"+data.obj[i].tmpmax+"℃");
						$(".week2").html(data.obj[i].week);
						$(".date2").html(data.obj[i].date);
					}
					if (i == 2) {
						$(".day3").attr("src",data.obj[i].url);
						$(".txt3").html(data.obj[i].txt_d);
						$(".sc3").html(data.obj[i].dir+data.obj[i].sc);
						$(".wd3").html(data.obj[i].tmpmin+"℃&nbsp;~&nbsp;"+data.obj[i].tmpmax+"℃");
						$(".week3").html(data.obj[i].week);
						$(".date3").html(data.obj[i].date);
					}
					if (i == 3) {
						$(".day4").attr("src",data.obj[i].url);
						$(".txt4").html(data.obj[i].txt_d);
						$(".sc4").html(data.obj[i].dir+data.obj[i].sc);
						$(".wd4").html(data.obj[i].tmpmin+"℃&nbsp;~&nbsp;"+data.obj[i].tmpmax+"℃");
						$(".week4").html(data.obj[i].week);
						$(".date4").html(data.obj[i].date);
					}
					if (i == 4) {
						$(".day5").attr("src",data.obj[i].url);
						$(".txt5").html(data.obj[i].txt_d);
						$(".sc5").html(data.obj[i].dir+data.obj[i].sc);
						$(".wd5").html(data.obj[i].tmpmin+"℃&nbsp;~&nbsp;"+data.obj[i].tmpmax+"℃");
						$(".week5").html(data.obj[i].week);
						$(".date5").html(data.obj[i].date);
					}
				});
			}
		});
	};

	return {
		init : function() {
			handleData();
		},
	};
}();