<%@ page language="java" import="java.util.*" pageEncoding="utf-8"%>
<%
	String path = request.getContextPath();
	String basePath = request.getScheme() + "://"
			+ request.getServerName() + ":" + request.getServerPort()
			+ path + "/";
%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
<head>
<base href="<%=basePath%>">
<link rel="stylesheet" href="<%=basePath%>/Weather/css/weather.css" type="text/css"></link>
</head>
<body>
	<div class="main" >
		<div class="header"> 
			<div style="position:absolute;left:950px;top:50px; font-weight: bold;font-family: '黑体';font-size: 38px;letter-spacing:3px;">兖州天气预报</div>
		</div>
		<div class="content" > 
			<div class="todayinfo" style="position:relative;top:50px;left:50px; width: 400px;height:30px; font-size: 30px;">09年21日&nbsp;&nbsp;星期二</div> 
			<div class="content_1">
				<img class="day1" src="images/baoyu.png"></img> 
				<div class="wd1" style="font-size:38px;font-weight:bold; position: relative;top: 30px;"></div>
				<div class="txt1" style="font-size:20px; position: relative;top: 45px;"></div>	
				<div class="sc1" style="font-size:20px; position: relative;top: 60px;"></div>				
			</div>
			<div class="content_2"> 
				<div class="week2" style="font-size:20px; position: relative;top: 10px; "></div>
				<div class="date2" style="font-size:20px; position: relative;top: 20px; "></div>
				<img class="day2" style="width: 100px;height: 100px; position: relative;top: 30px; " src="images/baoyu.png"  ></img> 
				<div class="wd2"  style="font-size:20px; position: relative;top: 54px;  "></div>
				<div class="txt2" style="font-size:20px; position: relative;top: 65px;"></div>	
				<div class="sc2" style="font-size:20px; position: relative;top: 74px;"></div>
			</div>
			<div class="content_3">
				<div class="week3" style="font-size:20px; position: relative;top: 10px; "></div>
				<div class="date3"   style="font-size:20px; position: relative;top: 20px; "></div>
				<img class="day3" style="width: 100px;height: 100px; position: relative;top: 30px; " src="images/baoyu.png"  ></img> 
				<div class="wd3" style="font-size:20px; position: relative;top: 54px;  "></div>
				<div class="txt3" style="font-size:20px; position: relative;top: 65px;"></div>	
				<div class="sc3" style="font-size:20px; position: relative;top: 74px;"></div>
			</div>
			<div class="content_4">
				<div class="week4" style="font-size:20px; position: relative;top: 10px; "></div>
				<div class="date4" style="font-size:20px; position: relative;top: 20px; "></div>
				<img class="day4" style="width: 100px;height: 100px; position: relative;top: 30px; " src="images/baoyu.png"  ></img> 
				<div class="wd4" style="font-size:20px; position: relative;top: 54px;  "></div>
				<div class="txt4" style="font-size:20px; position: relative;top: 65px;"></div>	
				<div class="sc4" style="font-size:20px; position: relative;top: 74px;"></div>
			</div>
			<div class="content_5" >
				<div class="week5" style="font-size:20px; position: relative;top: 10px; "></div>
				<div class="date5" style="font-size:20px; position: relative;top: 20px; "></div>
				<img class="day5" style="width: 100px;height: 100px; position: relative;top: 30px; " src="images/baoyu.png"  ></img> 
				<div class="wd5" style="font-size:20px; position: relative;top: 54px;  "></div>
				<div class="txt5" style="font-size:20px; position: relative;top: 65px;"></div>	
				<div class="sc5" style="font-size:20px; position: relative;top: 74px;"></div>
			</div>
		</div>
		<div class="footer"></div>
	</div>
	<script type="text/javascript" src="<%=basePath%>/js/jquery-1.8.3.js"></script>
	<script type="text/javascript" src="<%=basePath%>/Weather/js/weather.js"></script>
	<script type="text/javascript">
		jQuery(document).ready(function() {
			Weather.init();
		});
	</script>
</body>
</html>
