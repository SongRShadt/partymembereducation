var Index = function() {
	var param = {
		clickNum :1,
	};
	var contentData = [
	    {"id":"1","name":"典型经验事迹片","url":""},
	    {"id":"2","name":"基层党建工作案例","url":""},
	    {"id":"3","name":"党风廉政教育专题","url":""},
	    {"id":"4","name":"党史文献纪录片","url":""},
	    {"id":"5","name":"《党建视线》栏目","url":""},
	];
	
	var handelContent = function() {
		$(".header_title").html("党员教育");
		$(".content_btn").each(function(i){
			if(i==0){
				$(this).addClass("content_btn_down");
			}
			$(this).addClass("content_btn_"+(i+1));
			$(this).html(contentData[i].name);
		});
	};
	/**
	 * 绑定键盘事件
	 */
	var handleKeyDown = function() {
		$(document).keydown(function(e) {
			e = e || window.event; // 取得事件对象
			var keyCode = e.which || e.keyCode; // 按键值
			switch (keyCode) {
			case 38://up
				$(".msg").html("");
				$(".content_btn_down").removeClass("content_btn_down");
				if(param.clickNum>2){
					param.clickNum-=2;
				}
				$(".content_btn_"+param.clickNum).addClass("content_btn_down");
				break;
			case 40://down
				$(".msg").html("");
				$(".content_btn_down").removeClass("content_btn_down");
				if(param.clickNum<$(".content_btn").length-1){
					param.clickNum+=2;
				}
				$(".content_btn_"+param.clickNum).addClass("content_btn_down");
				break;
			case 37:// left
				$(".msg").html("");
				$(".content_btn_down").removeClass("content_btn_down");
				if(param.clickNum>1){
					param.clickNum--;
				}
				$(".content_btn_"+param.clickNum).addClass("content_btn_down");
				break;
			case 39:// right
				$(".msg").html("");
				$(".content_btn_down").removeClass("content_btn_down");
				if(param.clickNum<$(".content_btn").length){
					param.clickNum++;
				}
				$(".content_btn_"+param.clickNum).addClass("content_btn_down");
				break;
			case 13:// 确定
				if(contentData[param.clickNum-1].url!=""){
					window.location.href=contentData[param.clickNum-1].url;
				}else{
					$(".msg").html("该频道暂不可播放！请尝试其他频道！");
				}
				break;
			case 8:
				break;
			}
		});
	}
	return {
		init : function() {
			handelContent();
			handleKeyDown();
		}

	}
}();