<%@ page language="java" import="java.util.*" pageEncoding="utf-8"%>
<%
String path = request.getContextPath();
String basePath = request.getScheme()+"://"+request.getServerName()+":"+request.getServerPort()+path+"/";
request.setAttribute("basePath", basePath);
%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
<head>
<title>党建网站</title>
<meta http-equiv="Content-type" content="text/html; charset=utf-8" />
<meta name="page-view-size" content="1280*720" />
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<link rel="stylesheet" href="${basePath}ws/css/style.css" type="text/css"></link>
<link rel="stylesheet" href="${basePath}ws/djweb/css/index.css" type="text/css"></link>
</head>
<body>
	<div class="main">
		<div class="header">
			<div class="header_context">
				<div class="header_title"></div>
<!-- 				<div class="header_arrow"></div> -->
<!-- 				<div class="header_name"></div> -->
			</div>			
		</div>
		<div class="content">
			<div class="content_main">
				<div class="content_btn"></div>
				<div class="content_btn"></div>
				<div class="content_btn"></div>
				<div class="content_btn"></div>
			</div>
			<div class="msg"></div>
		</div>
	</div>
	<!-- corescript -->
	<script type="text/javascript" src="${basePath}js/jquery-1.8.3.js"></script>
	<script type="text/javascript" src="${basePath}ws/js/ConfigUtil.js"></script>
	<script type="text/javascript" src="${basePath}ws/djweb/js/index.js"></script>
	<script type="text/javascript">
		jQuery(document).ready(function() {
			Index.init();
		});
	</script>
</body>
</html>
