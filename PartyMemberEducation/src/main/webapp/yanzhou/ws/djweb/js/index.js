﻿var Index = function() {
	var param = {
		clickNum :1,
	};
	var contentData = [
	    {"id":"1","name":"汶上党建网","url":"http://www.wsdjw.gov.cn/"},
	    {"id":"2","name":"济宁党建网","url":"http://www.jnswzzb.gov.cn/"},
	    {"id":"3","name":"齐鲁先锋网","url":"http://www.sd-taishan.gov.cn/"},
	    {"id":"4","name":"共产党员网","url":"http://www.12371.cn/"},
	];
	
	var handelContent = function() {
		$(".header_title").html("党建网站");
		$(".content_btn").each(function(i){
			if(i==0){
				$(this).addClass("content_btn_down");
			}
			$(this).addClass("content_btn_"+(i+1));
			$(this).html(contentData[i].name);
		});
	};
	/**
	 * 绑定键盘事件
	 */
	var handleKeyDown = function() {
		$(document).keydown(function(e) {
			e = e || window.event; // 取得事件对象
			var keyCode = e.which || e.keyCode; // 按键值
			switch (keyCode) {
			case 38://up
				$(".msg").html("");
				$(".content_btn_down").removeClass("content_btn_down");
				if(param.clickNum>2){
					param.clickNum-=2;
				}
				$(".content_btn_"+param.clickNum).addClass("content_btn_down");
				break;
			case 40://down
				$(".msg").html("");
				$(".content_btn_down").removeClass("content_btn_down");
				if(param.clickNum<$(".content_btn").length-1){
					param.clickNum+=2;
				}
				$(".content_btn_"+param.clickNum).addClass("content_btn_down");
				break;
			case 37:// left
				$(".msg").html("");
				$(".content_btn_down").removeClass("content_btn_down");
				if(param.clickNum>1){
					param.clickNum--;
				}
				$(".content_btn_"+param.clickNum).addClass("content_btn_down");
				break;
			case 39:// right
				$(".msg").html("");
				$(".content_btn_down").removeClass("content_btn_down");
				if(param.clickNum<$(".content_btn").length){
					param.clickNum++;
				}
				$(".content_btn_"+param.clickNum).addClass("content_btn_down");
				break;
			case 13:// 确定
				if(contentData[param.clickNum-1].url!=""){
					window.location.href=contentData[param.clickNum-1].url;
				}else{
					$(".msg").html("该链接暂不可用！请尝试其他链接！");
				}
				break;
			case 8:
				break;
			}
		});
	}
	return {
		init : function() {
			handelContent();
			handleKeyDown();
		}

	}
}();