var Index = function() {
	var param = {
		clickNum :1,
	};
	var contentData = [
	    {"id":"1","name":"中央一套","url":""},
	    {"id":"2","name":"中央二套","url":""},
	    {"id":"3","name":"中央十三套","url":""},
	    {"id":"4","name":"山东卫视","url":""},
	    {"id":"5","name":"山东农科","url":""},
	    {"id":"6","name":"济宁公共","url":""},
	    {"id":"7","name":"济宁党建","url":""},
	    {"id":"8","name":"汶上新闻","url":""},
	    {"id":"9","name":"汶上生活","url":""},
	];
	
	var handelContent = function() {
		$(".header_title").html("党建频道");
		$(".content_btn").each(function(i){
			if(i==0){
				$(this).addClass("content_btn_down");
			}
			$(this).addClass("content_btn_"+(i+1));
			$(this).html(contentData[i].name);
		});
	};
	/**
	 * 绑定键盘事件
	 */
	var handleKeyDown = function() {
		$(document).keydown(function(e) {
			e = e || window.event; // 取得事件对象
			var keyCode = e.which || e.keyCode; // 按键值
			switch (keyCode) {
			case 38://up
				$(".msg").html("");
				$(".content_btn_down").removeClass("content_btn_down");
				if(param.clickNum>3){
					param.clickNum-=3;
				}
				$(".content_btn_"+param.clickNum).addClass("content_btn_down");
				break;
			case 40://down
				$(".msg").html("");
				$(".content_btn_down").removeClass("content_btn_down");
				if(param.clickNum<$(".content_btn").length-2){
					param.clickNum+=3;
				}
				$(".content_btn_"+param.clickNum).addClass("content_btn_down");
				break;
			case 37:// left
				$(".msg").html("");
				$(".content_btn_down").removeClass("content_btn_down");
				if(param.clickNum>1){
					param.clickNum--;
				}
				$(".content_btn_"+param.clickNum).addClass("content_btn_down");
				break;
			case 39:// right
				$(".msg").html("");
				$(".content_btn_down").removeClass("content_btn_down");
				if(param.clickNum<$(".content_btn").length){
					param.clickNum++;
				}
				$(".content_btn_"+param.clickNum).addClass("content_btn_down");
				break;
			case 13:// 确定
				if(contentData[param.clickNum-1].url!=""){
					window.location.href=contentData[param.clickNum-1].url;
				}else{
					$(".msg").html("该频道暂不可播放！请尝试其他频道！");
				}
				break;
			case 8:
				break;
			}
		});
	}
	return {
		init : function() {
			handelContent();
			handleKeyDown();
		}

	}
}();