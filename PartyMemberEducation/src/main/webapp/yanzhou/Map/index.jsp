﻿<%@ page language="java" import="java.util.*" pageEncoding="utf-8"%>
<!DOCTYPE HTML>
<html>
<head>
<meta http-equiv="Content-type" content="text/html; charset=utf-8" />
<meta name="page-view-size" content="1280*720" />
<style>
* {
	margin: 0;
	padding: 0;
}

.stb_date {
	font-size: 22px;
	font-weight: bold;
	text-align: right;
	color: #FFFFFF;
	top: 10px;
	left: 1000px;
	width: 250px;
	height: 30px;
	line-height: 30px
}

.stb_time {
	font-size: 22px;
	font-weight: bold;
	text-align: right;
	color: #FFFFFF;
	top: 40px;
	left: 1000px;
	width: 250px;
	height: 30px;
	line-height: 30px
}

body {
	font-size: 22px;
	color: #000000;
	background-repeat: no-repeat;
	background-color: transparent;
}

#viewport {
	position: absolute;
	width: 1280px;
	height: 720px;
	background-image: url(images/bg.png);
}

.abspo {
	position: absolute;
}

.mod_focus {
	box-shadow: 4px 4px 2px #000;
	z-index: 1;
	transform: scale(1.15);
	-webkit-transform: scale(1.15);
}
</style>
<title>演示</title>
</head>
<body>
	<!--视图-->
	<div class="viewport" id="viewport">
		<!--日期-->
		<!--导航-->
		<div id="nav">
			<div id="nav_0" class="abspo" style="top:67px; left:581px;">
				<img src="images/nav/02.png">
			</div>
			<div id="nav_1" class="abspo" style="top:289px; left:855px;">
				<img src="images/nav/04.png">
			</div>
			<div id="nav_2" class="abspo" style="top:500px; left:973px;">
				<img src="images/nav/06.png">
			</div>
			<div id="nav_3" class="abspo" style="top:398px; left:918px;">
				<img src="images/nav/08.png">
			</div>
			<div id="nav_4" class="abspo" style="top:206px; left:505px;">
				<img src="images/nav/10.png">
			</div>
			<div id="nav_5" class="abspo" style="top:371px; left:771px;">
				<img src="images/nav/12.png">
			</div>
			<div id="nav_6" class="abspo" style="top:336px; left:626px;">
				<img src="images/nav/14.png">
			</div>
			<div id="nav_7" class="abspo" style="top:437px; left:736px;">
				<img src="images/nav/16.png">
			</div>
			<div id="nav_8" class="abspo" style="top:202px; left:645px;">
				<img src="images/nav/18.png">
			</div>
			<div id="nav_9" class="abspo" style="top:129px; left:743px;">
				<img src="images/nav/20.png">
			</div>
			<div id="nav_10" class="abspo" style="top:108px; left:632px;">
				<img src="images/nav/22.png">
			</div>
			<div id="nav_11" class="abspo" style="top:246px; left:555px;">
				<img src="images/nav/24.png">
			</div>
			<div id="nav_12" class="abspo" style="top:93px; left:495px;">
				<img src="images/nav/26.png">
			</div>
			<div id="nav_13" class="abspo" style="top:333px; left:749px;">
				<img src="images/nav/28.png">
			</div>
			<div id="nav_14" class="abspo" style="top:253px; left:732px;">
				<img src="images/nav/30.png">
			</div>
			<div id="nav_15" class="abspo" style="top:312px; left:764px;">
				<img src="images/nav/32.png">
			</div>

		</div>
		<!--栏目模块 
 <div id="msg" class="abspo " style=" top:650px; left:630px; color: #FFFFFF; "></div>
 -->
	</div>
	<script type="text/javascript" src="js/jquery.js"></script>
	<script type="text/javascript">
/*
 function clock(){
	//	clearTimeout( clock_timer );
	var weeks = ['日','一','二','三','四','五','六'];
	var dd = new Date();
	var	H = dd.getHours() < 10 ? ( '0' + dd.getHours() ) : dd.getHours(); //小时
	var	M = dd.getMinutes() < 10 ? ( '0' + dd.getMinutes() ) : dd.getMinutes(); //分钟
	var	S = dd.getSeconds() < 10 ? ( '0' + dd.getSeconds() ) : dd.getSeconds(); //秒
	var	Y = dd.getFullYear(); //年
	var	MONTH = dd.getMonth() + 1;
	MONTH = MONTH < 10 ?('0'+MONTH):MONTH; //月
	var	D = dd.getDate(); //日
	D = D < 10 ?('0'+D):D;
	var	W = dd.getDay();
	var	Z =  weeks[dd.getDay()];
	document.getElementById('stb_date').innerHTML= Y + '.' + MONTH + '.' + D + '&nbsp;&nbsp;&nbsp;' +'星期' + Z ;
	document.getElementById('stb_time').innerHTML=  H + ' : ' + M + ' : ' + S ;	 
	clock_timer = setTimeout('clock()', 1000 );
}
window.onload = clock;
*/
/*********************************************************************/

//判断是否有class

function hasClass( elem, className ){  
	var reg = new RegExp('(\\s|^)' + className + '(\\s|$)');	
	return elem.className.match(reg);
}
//添加class
function addClass( elem, value ){
	if( !elem.className ){
		elem.className=value;
	}else{
		var oValue = elem.className;
		oValue += ' ';
		oValue += value;
		elem.className = oValue;
	}
	return this;
} 

//移除class
function removeClass( elem, className ){
	className = className.replace(/^\s*|\s*$/g,'');
	if ( hasClass( elem, className ) ){
		var reg = new RegExp('(\\s|^)' + className + '(\\s|$)');
		elem.className = elem.className.replace(reg, ' ');
	}
	return this;
}


/**************************************************************************/

//要替换的一些数据
var data={
	nav:[
		{img1:'images/nav/01.png',img2:'images/nav/02.png',url:'../Page/Town/index.jsp?key=ZXDZ'},
		{img1:'images/nav/03.png',img2:'images/nav/04.png',url:'../Page/Town/index.jsp?key=TSJD'},
		{img1:'images/nav/05.png',img2:'images/nav/06.png',url:'../Page/Town/index.jsp?key=GSJD'},
		{img1:'images/nav/07.png',img2:'images/nav/08.png',url:'../Page/Town/index.jsp?key=QQJD'},
		{img1:'images/nav/09.png',img2:'images/nav/10.png',url:'../Page/Town/index.jsp?key=DSZ'},
		{img1:'images/nav/11.png',img2:'images/nav/12.png',url:'../Page/Town/index.jsp?key=ZZZ'},
		{img1:'images/nav/13.png',img2:'images/nav/14.png',url:'../Page/Town/index.jsp?key=THZ'},
		{img1:'images/nav/15.png',img2:'images/nav/16.png',url:'../Page/Town/index.jsp?key=CQZ'},
		{img1:'images/nav/17.png',img2:'images/nav/18.png',url:'../Page/Town/index.jsp?key=TPZ'},
		{img1:'images/nav/19.png',img2:'images/nav/20.png',url:'../Page/Town/index.jsp?key=BSZ'},
		{img1:'images/nav/21.png',img2:'images/nav/22.png',url:'../Page/Town/index.jsp?key=TCZ'},
		{img1:'images/nav/23.png',img2:'images/nav/24.png',url:'../Page/Town/index.jsp?key=ZSZ'},
		{img1:'images/nav/25.png',img2:'images/nav/26.png',url:'../Page/Town/index.jsp?key=XCZ'},
		{img1:'images/nav/27.png',img2:'images/nav/28.png',url:'../Page/Town/index.jsp?key=GLZ'},
		{img1:'images/nav/29.png',img2:'images/nav/30.png',url:'../Page/Town/index.jsp?key=SQZ'},
		{img1:'images/nav/31.png',img2:'images/nav/32.png',url:'../Page/Town/index.jsp?key=KZZ'},
		{img1:'images/nav/33.png',img2:'images/nav/34.png',url:'../Page/Town/index.jsp?key=kzz'}
	]};
	
//定义当前的位置
var curr_position={
		curr_Level:0,
		curr_nav_Index:0
};
	

//当前在导航栏的键盘处理
function showBlur(e){
	if(curr_position.curr_nav_Index==16){
		document.getElementById('nav_'+curr_position.curr_nav_Index).style.backgroundImage="url('images/nav/34.png')";
	}else{
		document.getElementById('nav_'+curr_position.curr_nav_Index).getElementsByTagName('img')[0].src=data.nav[curr_position.curr_nav_Index].img2;
	}
}


function showFocus(e){
	if(curr_position.curr_nav_Index==16){
		document.getElementById('nav_'+curr_position.curr_nav_Index).style.backgroundImage="url('images/nav/33.png')";
	}else{
		document.getElementById('nav_'+curr_position.curr_nav_Index).getElementsByTagName('img')[0].src=data.nav[curr_position.curr_nav_Index].img1;
	}
}


//跳转
function goto(e){
		//document.getElementById('msg').innerHTML=data.nav[curr_position.curr_nav_Index].url;
		location.href=data.nav[curr_position.curr_nav_Index].url;
}

/**
* 遥控器事件处理
*/
function grabEvent(e) {
	e = e || window.event; //取得事件对象 
	var keyCode = e.which || e.keyCode; //按键值
	
	switch (keyCode) {		
		case 37://left	
		
			showBlur();			 
			if(curr_position.curr_Level==0){//当前光标在导航栏
				switch(curr_position.curr_nav_Index){
					case 0:
						curr_position.curr_nav_Index=12;
					break;
					case 1:
						curr_position.curr_nav_Index=9;
					break;
					case 2:
						curr_position.curr_nav_Index=1;
					break;
					case 3:
						curr_position.curr_nav_Index=1;
					break;
					case 4:
						curr_position.curr_nav_Index=3;
					break;
					case 5:
						curr_position.curr_nav_Index=4;
					break;
					case 6:
						curr_position.curr_nav_Index=5;
					break;
					case 7:
						curr_position.curr_nav_Index=6;
					break;
					case 8:
						curr_position.curr_nav_Index=7;
					break;
					case 9:
						curr_position.curr_nav_Index=8;
					break;
					case 10:
						curr_position.curr_nav_Index=9;
					break;
					case 11:
						curr_position.curr_nav_Index=10;
					break;
					case 12:
						curr_position.curr_nav_Index=11;
					break;
					case 13:
						curr_position.curr_nav_Index=7;
					break;
					case 14:
						curr_position.curr_nav_Index=13;
					break;
					case 15:
						curr_position.curr_nav_Index=14;
					break;
					case 16:
						curr_position.curr_nav_Index=15;
					break;
				}			
			}	
			showFocus();
			break;
		
		case 38: //up
		 
		 	showBlur();
			if(curr_position.curr_Level==0){//当前光标在导航栏
				switch(curr_position.curr_nav_Index){
					case 0:
						
					break;
					case 1:
						curr_position.curr_nav_Index=0;
					break;
					case 2:
						curr_position.curr_nav_Index=0;
					break;
					case 3:
						curr_position.curr_nav_Index=2;
					break;
					case 4:
						curr_position.curr_nav_Index=15;
					break;
					case 5:
						curr_position.curr_nav_Index=6;
					break;
					case 6:
						curr_position.curr_nav_Index=5;
					break;
					case 7:
						curr_position.curr_nav_Index=6;
					break;
					case 8:
						curr_position.curr_nav_Index=9;
					break;
					case 9:
						curr_position.curr_nav_Index=0;
					break;
					case 10:
						curr_position.curr_nav_Index=1;
					break;
					case 11:
						curr_position.curr_nav_Index=3;
					break;
					case 12:
						curr_position.curr_nav_Index=4;
					break;
					case 13:
						curr_position.curr_nav_Index=8;
					break;
					case 14:
						curr_position.curr_nav_Index=10;
					break;
					case 15:
						curr_position.curr_nav_Index=11;
					break;
					case 16:
						curr_position.curr_nav_Index=7;
					break;
					
				}	
			}				
			showFocus();
			break;	 
		 
		case 39://RIGHT
		 
		 	showBlur();
			if(curr_position.curr_Level==0){//当前光标在导航栏
				switch(curr_position.curr_nav_Index){
					case 0:
						curr_position.curr_nav_Index=10;
					break;
					case 1:
						curr_position.curr_nav_Index=2;
					break;
					case 2:
						curr_position.curr_nav_Index=3;
					break;
					case 3:
						curr_position.curr_nav_Index=4;
					break;
					case 4:
						curr_position.curr_nav_Index=5;
					break;
					case 5:
						curr_position.curr_nav_Index=6;
					break;
					case 6:
						curr_position.curr_nav_Index=7;
					break;
					case 7:
						curr_position.curr_nav_Index=0;
					break;
					case 8:
						curr_position.curr_nav_Index=9;
					break;
					case 9:
						curr_position.curr_nav_Index=10;
					break;
					case 10:
						curr_position.curr_nav_Index=11;
					break;
					case 11:
						curr_position.curr_nav_Index=12;
					break;
					case 12:
						curr_position.curr_nav_Index=5;
					break;
					case 13:
						curr_position.curr_nav_Index=14;
					break;
					case 14:
						curr_position.curr_nav_Index=15;
					break;
					case 15:
						curr_position.curr_nav_Index=12;
					break;
					
				}			
			}		
			showFocus();
			break;
		 
		case 40://DOWN	
			 
		 	showBlur();
			if(curr_position.curr_Level==0){//当前光标在导航栏
				switch(curr_position.curr_nav_Index){
					case 0:
						curr_position.curr_nav_Index=11;
					break;
					case 1:
						curr_position.curr_nav_Index=10;
					break;
					case 2:
						curr_position.curr_nav_Index=3;
					break;
					case 3:
						curr_position.curr_nav_Index=11;
					break;
					case 4:
						curr_position.curr_nav_Index=11;
					break;
					case 6:
						curr_position.curr_nav_Index=5;
					break;
					case 8:
						curr_position.curr_nav_Index=13;
					break;
					case 9:
						curr_position.curr_nav_Index=14;
					break;
					case 10:
						curr_position.curr_nav_Index=14;
					break;
					case 11:
						curr_position.curr_nav_Index=15;
					break;
					case 12:
					case 15:
					case 14:
					case 7:
					case 5:
						curr_position.curr_nav_Index=16;
					break;
					case 13:
						curr_position.curr_nav_Index=0;
					break;
				}	
			}		
			showFocus();
			break;
			
		case 13: //确认		 
			goto();
		case 8:
			return true;
		break;
	}
	
	
	 
}


document.onirkeypress = grabEvent;
document.onkeydown = grabEvent;
showFocus();
</script>
</body>
</html>