/**
 * 会议类型
 */
var Index = function() {
	var param = {
		clickNum : 1,//点击次数
		navCount : 5,//导航栏每页条数
		navStar : 0,//开始
		navEnd : 4,//结束
		
		stbNo : ConfigUtil.getUrlParam().stbNo//机顶盒mac
	};
	/**
	 * 初始导航区域
	 */
	var handleNav = function() {
		$.ajax({
			type : "GET",
			dataType : 'json',
			data : {
				"stbNo" : param.stbNo
			},
			url : "/PartyMemberEducation/cameraInfoController/getCameraInfoByStbNo",
			success : function(data) {
				data = eval("("+data+")");
				if(data.success){
					$(data.obj).each(function(i){
						$(".content_left").append("<div class='nav_btn nav_btn_"+(i+1)+"' cameraUrl='"+this.cameraUrl+"' imgUrl='"+this.imgUrl+"' cameraNo='"+this.cameraNo+"'>"+this.cameraName+"</div>");
						if(i==0){
							$(".nav_btn_1").addClass("nav_btn_down");
							$(".content_right_img").attr("src",$(".nav_btn_"+param.clickNum).attr("imgurl"));
						}
					});
					$(".contnet_left_up").addClass("up");
					$(".contnet_left_down").addClass("down");
					if(data.obj.length>param.navCount){
						pagination({"star":0,"end":4});
					}	
				}
			}
		});
	};
	
	/**
	 * 导航栏分页
	 */
	var pagination = function(c){
		if($(".nav_btn").length>param.navCount){
			$(".nav_btn").each(function(i){
				$(this).removeClass("nav_btn_hidden");
				if(i<c.star||i>c.end){
					$(this).addClass("nav_btn_hidden");
				}
			});
		}else{
		}
	}
	
	/**
	 * 绑定键盘事件
	 */
	var handleKeyDown = function() {
		$(document).keydown(function(e) {
			e = e || window.event; // 取得事件对象
			var keyCode = e.which || e.keyCode; // 按键值
			switch (keyCode) {
			case 38:// up
				$(".nav_btn_down").removeClass("nav_btn_down");
				param.clickNum--;
				if(param.clickNum<1){
					param.clickNum = $(".nav_btn").length;
					param.navStar=$(".nav_btn").length-5;
					param.navEnd=$(".nav_btn").length-1;
				}else if(param.clickNum<=$(".nav_btn").length-5&&param.navStar>0){
					param.navStar--;
					param.navEnd--;
				} 
				pagination({"star":param.navStar,"end":param.navEnd});
				$(".content_right_img").attr("src",$(".nav_btn_"+param.clickNum).attr("imgurl"));
				$(".nav_btn_"+param.clickNum).addClass("nav_btn_down");
				break;
			case 40:// down
				$(".nav_btn_down").removeClass("nav_btn_down");
				param.clickNum++;
				console.info(param.clickNum+"-"+$(".nav_btn").length+"-"+param.navCount);
				if(param.clickNum>=$(".nav_btn").length){
					param.clickNum=$(".nav_btn").length;
				}
				if(param.clickNum>param.navCount){
					if(param.clickNum<=$(".nav_btn").length&&param.navEnd<$(".nav_btn").length-1){
						param.navStar++;
						param.navEnd++;
					}else{
						param.clickNum = 1;
						param.navStar=0;
						param.navEnd=4;
					}
					pagination({"star":param.navStar,"end":param.navEnd});
				}
				$(".content_right_img").attr("src",$(".nav_btn_"+param.clickNum).attr("imgurl"));
				$(".nav_btn_"+param.clickNum).addClass("nav_btn_down");
				break;
			case 13:// ok
				if($(".nav_btn_down").attr("camerano")=="99999"){
					window.location.href=$(".nav_btn_down").attr("cameraUrl")+"?stbNo="+param.stbNo+"&type=meeting&cType=1";
				}else if($(".nav_btn_down").attr("camerano")=="99998"){
					window.location.href=$(".nav_btn_down").attr("cameraUrl")+"?stbNo="+param.stbNo+"&type=meeting&cType=0";
				}else{
					window.location.href="MeetingBack/meetingback.jsp?cameraNo=" + $(".nav_btn_down").attr("camerano")+"&type=meeting&title="+$(".nav_btn_down").html();
				}
				break;
			case 8:
				break;
			}
		});
	};
	
	return {
		/**
		 * 初始化
		 * 
		 * @returns
		 */
		init : function() {
			handleNav();
			handleKeyDown();
		}
	};
}();