/**
 * 
 */
var Government = function(){
	var param = {
		OrgId : ConfigUtil.getUrlParam().OrgId,//村组织编号
		user_type :1,//用户类型
		
		leftOrRight : 1,//光标位置是左侧还是右侧
		leftClick : 1,//左侧光标位置
		leftStar : 0,//左侧开始记录
		leftEnd : 4,//左侧结束记录
		rightClick : 1,//右侧光标位置
		rightStar:0,//右侧开始记录
		rightEnd :2,//右侧结束记录
		
	};
	/**
	 * 初始化内容区域
	 */
	var handleContent = function(){
		console.info(param.OrgId);
		$.ajax({
			type : "GET",
			dataType : 'jsonp',
			async : false,
			data : {
				"callback" : 1,
				"OrgId" : param.OrgId,
				"user_type" : param.user_type
			},
			url : "http://172.23.254.68:9000/userinfo/userInfoService",
			success : function(result) {
				if(result.success){
					var content = "";
					$(result.data).each(function(i){
						content+="<div class=\"c_btn c_btn_"+(i+1)+"\"><img src=\"http://172.23.254.68:9000/userimg?sid="+this.sid+"\"/><div class=\"u_info\"><div class=\"user\">姓名:"+this.name+"</div><div class=\"user\">出生日期:"+this.sbrith+"</div><div class=\"user\">性别:"+this.sex+"</div><div class=\"user\">政治面貌:"+this.zzmm+"</div><div class=\"user\">职务:"+(this.zw==undefined?"":this.zw)+"</div><div class=\"user\">联系方式:"+this.phone.substring(0,11)+"</div></div></div>";
					});
					$(".c_right").html(content);
					pagination({"star":param.rightStar,"end":param.rightEnd,"leftOrRight":2});
				}
			}
		});
	};
	
	/**
	 * 初始化导航栏
	 */
	var handleNav = function(){
	};
	
	/**
	 * 分页
	 */
	var pagination = function(c){
		if(c.leftOrRight==1){
			$(".nav_btn").each(function(i){
				$(this).removeClass("hidden");
				if(i<c.star||i>c.end){
					$(this).addClass("hidden");
				}
			});
		}else if(c.leftOrRight==2){
			$(".c_btn").each(function(i){
				$(this).removeClass("hidden");
				if(i<c.star||i>c.end){
					$(this).addClass("hidden");
				}
			});
		}
	}
	
	/**
	 * 绑定键盘事件
	 */
	var handleKeyDown = function(){
		$(document).keydown(function(e) {
			e = e || window.event; // 取得事件对象
			var keyCode = e.which || e.keyCode; // 按键值
			switch (keyCode) {
			case 37://left
				param.leftOrRight = 1;
				$(".c_btn_down").removeClass("c_btn_down");
				break;
			case 39://right
				param.leftOrRight = 2;
				$(".c_btn_"+param.rightClick).addClass("c_btn_down");
				break;
			case 38:// up
				if(param.leftOrRight==1){
					$(".nav_btn_on").removeClass("nav_btn_on");
					param.leftClick--;
					if(param.leftClick<1){
						param.leftClick=1;
					}
					if(param.leftClick<=param.leftEnd-4&&param.leftStar>0){
						param.leftStar--;
						param.leftEnd--;
						pagination({"star":param.leftStar,"end":param.leftEnd,"leftOrRight":param.leftOrRight});
					} 
					$(".nav_btn_"+param.leftClick).addClass("nav_btn_on");
					param.user_type = $(".nav_btn_on").attr("data");
					handleContent();
				}else if(param.leftOrRight==2){
					$(".c_btn_down").removeClass("c_btn_down");
					param.rightClick--;
					if(param.rightClick<1){
						param.rightClick=1;
					}
					if(param.rightClick<=param.rightEnd-2&&param.rightStar>0){
						param.rightStar--;
						param.rightEnd--;
						pagination({"star":param.rightStar,"end":param.rightEnd,"leftOrRight":param.leftOrRight});
					} 
					$(".c_btn_"+param.rightClick).addClass("c_btn_down");
				}
				break;
			case 40:// down
				if(param.leftOrRight==1){
					$(".nav_btn_on").removeClass("nav_btn_on");
					param.leftClick++;
					if(param.leftClick>=$(".nav_btn").length){
						param.leftClick=$(".nav_btn").length;
					}
					if(param.leftClick>5&&param.leftEnd<$(".nav_btn").length-1){
						param.leftStar++;
						param.leftEnd++;
						pagination({"star":param.leftStar,"end":param.leftEnd,"leftOrRight":param.leftOrRight});
					}
					$(".nav_btn_"+param.leftClick).addClass("nav_btn_on");
					param.user_type = $(".nav_btn_on").attr("data");
					handleContent();
				}else if(param.leftOrRight==2){
					$(".c_btn_down").removeClass("c_btn_down");
					param.rightClick++;
					if(param.rightClick>=$(".c_btn").length){
						param.rightClick=$(".c_btn").length;
					}
					if(param.rightClick>3&&param.rightEnd<$(".c_btn").length-1){
						param.rightStar++;
						param.rightEnd++;
						pagination({"star":param.rightStar,"end":param.rightEnd,"leftOrRight":param.leftOrRight});
					}
					$(".c_btn_"+param.rightClick).addClass("c_btn_down");
				}
				break;
			case 13:// ok
				
				break;
			case 8:
				break;
			}
		});
	};
	return {
		init : function(){
			handleKeyDown();
			handleContent();
		},
		
	};
}();