<%@ page language="java" import="java.util.*" pageEncoding="utf-8"%>
<%
	String path = request.getContextPath();
	String basePath = request.getScheme() + "://"
			+ request.getServerName() + ":" + request.getServerPort()
			+ path + "/";
%>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
<head>
<base href="<%=basePath%>">
<title>政务公开</title>
<meta http-equiv="Content-type" content="text/html; charset=utf-8" />
<meta name="page-view-size" content="1280*720" />
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<link rel="stylesheet" href="<%=basePath%>Government/css/index.css" type="text/css"></link></head>
</head>

<body>
<div class="main">
	<div class="header">
		<div class="h_title">政务公开</div>
	</div>
	<div class="content">
		<div class="c_left">
			<div class="nav_up"></div>
			<div class="nav_btn nav_btn_1 nav_btn_on" data="1">党支部委员会</div>
			<div class="nav_btn nav_btn_2" data="2">村民代表</div>
			<div class="nav_btn nav_btn_3" data="3">村民理财小组</div>
			<div class="nav_btn nav_btn_4" data="4">村务监督委员会</div>
			<div class="nav_btn nav_btn_5" data="6">党员</div>
			<div class="nav_btn nav_btn_6 hidden" data="7">村委成员</div>
			<div class="nav_btn nav_btn_7 hidden" data="8">村配套组织</div>
			<div class="nav_down"></div>
		</div>
		<div class="c_right">
		</div>
	</div>
	<div class="footer"></div>
</div>
<!-- js core -->
<script type="text/javascript" src="<%=basePath%>js/jquery-1.8.3.js"></script>
<script type="text/javascript" src="<%=basePath%>js/ConfigUtil.js"></script>
<script type="text/javascript" src="<%=basePath%>Government/js/index.js"></script>
<script type="text/javascript">
	jQuery(document).ready(function() {
			Government.init();
	});
</script>
</body>
</html>
